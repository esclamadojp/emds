<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accident_Incident extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'accident_incident';
		else 
			return 'Accident Incident';
	} 

	public function index(){		
		$this->load->model('Page');		
        $this->Page->set_page($this->modulename('link'));
	}

	public function ailist()
	{ 
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));			

			die(json_encode($this->generateailist($query, $date_from, $date_to, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateailist($query, $date_from, $date_to, $transaction_type)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$limitQuery = "";
			if($transaction_type == 'Grid')
			{
				$limit = $_GET['limit'];
				$start = $_GET['start'];
				$limitQuery = " LIMIT $start, $limit";
			}

			$commandText = "SELECT 
								a.id,
								a.date,
								a.time,
								a.injury,
								b.description,
								c.description AS area_desc,
								d.residents,
								e.staff,
								f.agencies
							FROM ai_details a
								LEFT JOIN ai_kinds b ON b.id = a.kind_id
								LEFT JOIN ai_area c ON c.id = a.area_id	
								LEFT JOIN (SELECT 
										detail_id,
										GROUP_CONCAT(CONCAT(lname,', ',fname) SEPARATOR ' and ') residents
									   FROM ai_details_residents a
									   JOIN residents b ON b.id = a.resident_id
									   GROUP BY detail_id) d ON d.detail_id = a.id
								LEFT JOIN (SELECT 
										detail_id,
										GROUP_CONCAT(CONCAT(lname,', ',fname) SEPARATOR ' and ') staff
									   FROM ai_details_staff a
									   JOIN staff b ON b.id = a.staff_id
									   GROUP BY detail_id) e ON e.detail_id = a.id
								LEFT JOIN (SELECT 
										detail_id,
										GROUP_CONCAT(description SEPARATOR ', ') agencies
									   FROM ai_details_agencies a
									   JOIN ai_agencies b ON b.id = a.agency_id
									   GROUP BY detail_id) f ON f.detail_id = a.id
							WHERE
								(a.date between '$date_from' and '$date_to') and
							    (	
							    	a.injury like '%$query%' or 
					    		 	b.description like '%$query%' or
					    		 	c.description like '%$query%'
					    		)
								AND a.active = 1
							ORDER BY date DESC
							$limitQuery";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$commandText = "SELECT count(a.id) AS count
							FROM ai_details a
								LEFT JOIN ai_kinds b ON b.id = a.kind_id
								LEFT JOIN ai_area c ON c.id = a.area_id	
								LEFT JOIN (SELECT 
										detail_id,
										GROUP_CONCAT(CONCAT(lname,', ',fname) SEPARATOR ' and ') residents
									   FROM ai_details_residents a
									   JOIN residents b ON b.id = a.resident_id
									   GROUP BY detail_id) d ON d.detail_id = a.id
								LEFT JOIN (SELECT 
										detail_id,
										GROUP_CONCAT(CONCAT(lname,', ',fname) SEPARATOR ' and ') staff
									   FROM ai_details_staff a
									   JOIN staff b ON b.id = a.staff_id
									   GROUP BY detail_id) e ON e.detail_id = a.id
								LEFT JOIN (SELECT 
										detail_id,
										GROUP_CONCAT(description SEPARATOR ', ') agencies
									   FROM ai_details_agencies a
									   JOIN ai_agencies b ON b.id = a.agency_id
									   GROUP BY detail_id) f ON f.detail_id = a.id
							WHERE
								(a.date between '$date_from' and '$date_to') and
							    (	
							    	a.injury like '%$query%' or 
					    		 	b.description like '%$query%' or
					    		 	c.description like '%$query%'
					    		)
								AND a.active = 1";
			$result = $this->db->query($commandText);
			$query_count = $result->result(); 

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,
					'date' 			=> date('m/d/Y',strtotime($value->date)),
					'time' 			=> $value->time,
					'description' 	=> $value->description,
					'area'			=> $value->area_desc,
					'injury'		=> $value->injury,
					'residents'		=> strtoupper($value->residents),
					'staff' 		=> strtoupper($value->staff),
					'agencies' 		=> $value->agencies);
			}

			$data['totalCount'] = $query_count[0]->count;
			return $data;

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function accidentincidentcrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$date			= date('Y-m-d',strtotime($this->input->post('date')));
			$time			= $this->input->post('time');
			$kind_id		= $this->input->post('ai_kinds');
			$description_ai	= strip_tags(trim($this->input->post('description_ai')));
			$area_id		= $this->input->post('ai_area');
			$injury			= $this->input->post('injury');
			$description_injury = strip_tags(trim($this->input->post('description_injury')));
			$witness		= strip_tags(trim($this->input->post('witness')));
			$ai_interventions = $this->input->post('ai_interventions');
			$ai_preventive_measures	= $this->input->post('ai_preventive_measures');
			$ai_agencies	= $this->input->post('ai_agencies');
			$summary		= strip_tags(trim($this->input->post('summary')));
			$conclusion_id	= $this->input->post('ai_conclusions');
			$reported_to_id	= $this->input->post('reported_to');
			$date_report_completed = date('Y-m-d',strtotime($this->input->post('date_report_completed')));
			$report_completed_by = $this->input->post('report_completed_by');
			$type			= $this->input->post('type');

			if ($type == "Delete")
			{				
				$commandText = "UPDATE ai_details set active = 0 where id = $id";
				$result = $this->db->query($commandText);

				$this->load->library('session');		
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('".$this->modulename('Label')."', $id, 'ai_details', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);						
			}
			else
			{				
				$this->load->model('ai_details');
				if ($type == "Add") 
					$id = 0;
				if ($type == "Edit") 				
					$this->ai_details->id = $id;

				$this->ai_details->kind_id 			= $kind_id;
				$this->ai_details->area_id 			= $area_id;
				$this->ai_details->conclusion_id 	= $conclusion_id;
				$this->ai_details->reported_to_id 	= $reported_to_id;
				$this->ai_details->description_ai 	= $description_ai;
				$this->ai_details->date 			= $date;
				$this->ai_details->time 			= $time;
				$this->ai_details->injury			= $injury;
				$this->ai_details->description_injury = $description_injury;
				$this->ai_details->witness 			= $witness;
				$this->ai_details->summary 			= $summary;
				$this->ai_details->date_report_completed = $date_report_completed;
				$this->ai_details->report_completed_by = $report_completed_by;
				$this->ai_details->active 			= 1;
				$this->ai_details->save($id);	

				if ($type == "Add") 
				{
					$commandText = "select id from ai_details order by id desc limit 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$id = $query_result[0]->id;								
				}

				#add/update accident & incident residents
				$commandText = "delete from ai_details_residents where detail_id = $id";
				$result = $this->db->query($commandText);

				$this->load->model('ai_details_residents');				
				$residents	=explode(',',$this->input->post('residents'));
				foreach($residents as $key => $value) 
				{
					if($value != null)
					{
						$this->ai_details_residents->detail_id		= $id;
						$this->ai_details_residents->resident_id 	= $value;
						$this->ai_details_residents->save(0);	
					}
				}

				#add/update accident & incident staff
				$commandText = "delete from ai_details_staff where detail_id = $id";
				$result = $this->db->query($commandText);

				$this->load->model('ai_details_staff');				
				$staff	=explode(',',$this->input->post('staff'));
				foreach($staff as $key => $value) 
				{
					if($value != null)
					{
						$this->ai_details_staff->detail_id		= $id;
						$this->ai_details_staff->staff_id 		= $value;
						$this->ai_details_staff->save(0);	
					}
				}

				#ai_details_interventions
				$commandText = "delete from ai_details_interventions where detail_id = $id";
				$result = $this->db->query($commandText);

				$this->load->model('ai_details_interventions');				
				$ai_interventions	=explode(',',$ai_interventions);
				foreach($ai_interventions as $key => $value) 
				{
					$this->ai_details_interventions->detail_id		= $id;
					$this->ai_details_interventions->intervention_id= $value;
					$this->ai_details_interventions->save(0);	
				}

				#ai_details_preventive_measures
				$commandText = "delete from ai_details_preventive_measures where detail_id = $id";
				$result = $this->db->query($commandText);

				$this->load->model('ai_details_preventive_measures');				
				$ai_preventive_measures	=explode(',',$ai_preventive_measures);
				foreach($ai_preventive_measures as $key => $value) 
				{
					$this->ai_details_preventive_measures->detail_id			= $id;
					$this->ai_details_preventive_measures->preventive_measure_id= $value;
					$this->ai_details_preventive_measures->save(0);	
				}

				#ai_details_agencies
				$commandText = "delete from ai_details_agencies where detail_id = $id";
				$result = $this->db->query($commandText);

				$this->load->model('ai_details_agencies');				
				$ai_agencies	=explode(',',$ai_agencies);
				foreach($ai_agencies as $key => $value) 
				{
					$this->ai_details_agencies->detail_id	= $id;
					$this->ai_details_agencies->agency_id	= $value;
					$this->ai_details_agencies->save(0);	
				}

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'ai_details', $type, $this->modulename('Label'));
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function view()
	{
		try 
		{
			$id = $this->input->post('id');
			die(json_encode($this->generateview($id)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateview($id)
	{
		try 
		{	
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$commandText = "SELECT 
								a.*,
								b.description as kind_desc,
								c.description AS area_desc,
								d.intervention_id,
								d.intervention_desc,
								e.preventive_measure_id,
								e.preventive_measure_desc,
								f.agency_id,
								f.agency_desc,
								g.description AS conclusion_desc,
								h.description AS reported_desc,
								CONCAT(i.lname,', ',i.fname) AS completed_by
							FROM ai_details a
								LEFT JOIN ai_kinds b ON b.id = a.kind_id
								LEFT JOIN ai_area c ON c.id = a.area_id	
								LEFT JOIN 
									(SELECT 	
										a.detail_id,
										GROUP_CONCAT(a.intervention_id) intervention_id,
										GROUP_CONCAT(b.description) intervention_desc	
									 FROM ai_details_interventions a
										JOIN ai_interventions b ON a.intervention_id = b.id
									 WHERE a.detail_id = $id) d ON d.detail_id = a.id
								LEFT JOIN 
									(SELECT 	
										a.detail_id,
										GROUP_CONCAT(a.preventive_measure_id) preventive_measure_id,
										GROUP_CONCAT(b.description) preventive_measure_desc	
									 FROM ai_details_preventive_measures a
										JOIN ai_preventive_measures b ON a.preventive_measure_id = b.id
									 WHERE a.detail_id = $id) e ON e.detail_id = a.id
								LEFT JOIN 
									(SELECT 	
										a.detail_id,
										GROUP_CONCAT(a.agency_id) agency_id,
										GROUP_CONCAT(b.description) agency_desc	
									 FROM ai_details_agencies a
										JOIN ai_agencies b ON a.agency_id = b.id
									 WHERE a.detail_id = $id) f ON f.detail_id = a.id
								LEFT JOIN ai_conclusions g ON g.id = a.conclusion_id
								LEFT JOIN ai_agencies h ON h.id = a.reported_to_id
								LEFT JOIN staff i ON a.report_completed_by = i.id
							WHERE a.id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['id'] 				= $value->id;
				$record['date'] 			= date('m/d/Y',strtotime($value->date));
				$record['time'] 			= $value->time;
				$record['kind_id'] 			= $value->kind_id;
				$record['kind_desc'] 		= $value->kind_desc;
				$record['description_ai']   = $value->description_ai;
				$record['area_id'] 			= $value->area_id;
				$record['area_desc'] 		= $value->area_desc;
				$record['injury'] 			= $value->injury;
				$record['description_injury'] = $value->description_injury;
				$record['witness'] 			= $value->witness;
				$record['intervention_id'] 	= $value->intervention_id;
				$record['intervention_desc'] = $value->intervention_desc;
				$record['preventive_measure_id'] = $value->preventive_measure_id;
				$record['preventive_measure_desc'] = $value->preventive_measure_desc;
				$record['agency_id'] 		= $value->agency_id;
				$record['agency_desc'] 		= $value->agency_desc;
				$record['summary'] 			= $value->summary;
				$record['conclusion_id'] 	= $value->conclusion_id;
				$record['conclusion_desc'] 	= $value->conclusion_desc;
				$record['reported_to_id'] 	= $value->reported_to_id;
				$record['reported_desc'] 	= $value->reported_desc;
				$record['date_report_completed'] = date('m/d/Y',strtotime($value->date_report_completed));;
				$record['report_completed_id'] = $value->report_completed_by;
				$record['report_completed_desc'] = strtoupper($value->completed_by);

				$commandText = "SELECT 
									a.resident_id,
									CONCAT(lname,', ',fname) AS resident_name
								FROM ai_details_residents a
									LEFT JOIN residents b ON a.resident_id = b.id
								WHERE a.detail_id = $id";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 

				foreach($query_result as $key => $val) 
				{	
					$data['resident_id'][] 		= $val->resident_id;
					$data['resident_name'][] 	= strtoupper($val->resident_name);
				}
				$data['resident_count'] = count($query_result);

				$commandText = "SELECT 
									a.staff_id,
									CONCAT(lname,', ',fname) AS staff_name
								FROM ai_details_staff a
									LEFT JOIN staff b ON a.staff_id = b.id
								WHERE a.detail_id = $id";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 

				foreach($query_result as $key => $val) 
				{	
					$data['staff_id'][] 	= $val->staff_id;
					$data['staff_name'][] 	= strtoupper($val->staff_name);
				}
				$data['staff_count'] = count($query_result);
			}

			$data['totalCount'] = count($query_result);
			$data['data'] = $record;
			$data['success'] = true;

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{
		$this->load->model('Session');$this->Session->Validate();

		if($this->input->post('reporttype') == 'Record')
			$id 		= $this->input->post('id');
		else
		{
			$query 		= addslashes(strip_tags(trim($this->input->post('query'))));
			$date_from 	= date('Y-m-d',strtotime($this->input->post('date_from')));
			$date_to 	= date('Y-m-d',strtotime($this->input->post('date_to')));
		}

		$type 		= $this->input->post('reporttype');
		$filetype 	= $this->input->post('filetype');


		$response = array();
        $response['success'] = true;

		if($this->input->post('reporttype') == 'Record')
		{
        	$response['filename'] = $this->exportpdfAIRecord($this->generateview($id));
        	// $this->load->model('Logs'); $this->Logs->audit_logs($id, 'ai_details', 'Report', $this->modulename('Label'));
        	$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values ($id, '".$this->modulename('Label')."', 'ai_details', ".$this->session->userdata('id').", 'Report', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
		}
        else
        {
	        if($filetype == "PDF")
	        	$response['filename'] = $this->exportpdfAIList($this->generateailist($query, $date_from, $date_to, 'Report'), $query, $date_from, $date_to);
	        else         	
				$response['filename'] = $this->exportexcelAIList($this->generateailist($query, $date_from, $date_to, 'Report'), $query, $date_from, $date_to);
			// $this->load->model('Logs'); $this->Logs->audit_logs(0, 'accident_incident', 'Report-'.$filetype, $this->modulename('Label').' (From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');    
			$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')." (From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'accident_incident', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
        }

		die(json_encode($response));
	}

	public function exportexcelAIList($data, $query, $date_from, $date_to)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "AIList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("AIList")
					      ->setSubject("Report")
					      ->setDescription("Generating AIList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B9:I9')->getFont()->setBold(true);

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'Accident / Incident Details');
			###DATE

			if(!$query) $query = 'NULL';

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Searched by:")		
					      ->setCellValue("B6", "Date From:")
					      ->setCellValue("B7", "Date To:")
					      ->setCellValue("C5", $query)
					      ->setCellValue("C6", date('m/d/Y',strtotime($date_from)))
					      ->setCellValue("C7", date('m/d/Y',strtotime($date_to)))
					      ->setCellValue("B9", "Date")
					      ->setCellValue("C9", "Time")
					      ->setCellValue("D9", "Kind of A/I")
					      ->setCellValue("E9", "Area")
					      ->setCellValue("F9", "Injury")
					      ->setCellValue("G9", "Agencies Involved")
					      ->setCellValue("H9", "Residents")
					      ->setCellValue("I9", "Staff");

			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+11), $data['data'][$i]['date'])
						      ->setCellValue("C".($i+11), $data['data'][$i]['time'])
						      ->setCellValue("D".($i+11), $data['data'][$i]['description'])
						      ->setCellValue("E".($i+11), $data['data'][$i]['area'])
						      ->setCellValue("F".($i+11), $data['data'][$i]['injury'])
						      ->setCellValue("G".($i+11), $data['data'][$i]['agencies'])
						      ->setCellValue("H".($i+11), $data['data'][$i]['residents'])
						      ->setCellValue("I".($i+11), $data['data'][$i]['staff']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+19), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+20), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfAIList($data, $query, $date_from, $date_to)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "AIList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('AIList');
			$pdf->SetSubject('AIList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('L', 'LETTER');

			if(!$query) $query = 'NULL';

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Accident / Incident List</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>
					<table>
						<tr style="font-size:26px;">
						  <td width="10%">Searched by</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$query.'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td>Date From</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_from)).'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td>Date To</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_to)).'</td>
						</tr>

					</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:26px;">
					  <td width="8%" style="padding: 10px;" align="left"><b>Date</b></td>
					  <td width="8%" style="padding: 10px;" align="left"><b>Time</b></td>
					  <td width="10%" style="padding: 10px;" align="left"><b>Kind of A/I</b></td>
					  <td width="10%" style="padding: 10px;" align="left"><b>Area</b></td>
					  <td width="10%" style="padding: 10px;" align="left"><b>Injury</b></td>
					  <td width="15%" style="padding: 10px;" align="left"><b>Agencies Involved</b></td>
					  <td width="20%" style="padding: 10px;" align="left"><b>Residents</b></td>
					  <td width="20%" style="padding: 10px;" align="left"><b>Staff</b></td>
					</tr>';

			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				$html .= '<tr style="font-size:26px;">
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['date'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['time'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['description'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['area'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['injury'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['agencies'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['residents'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['staff'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfAIRecord($data)
	{
		try 
		{			
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "AIRecord".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('AIRecord');
			$pdf->SetSubject('AIRecord');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Accident / Incident Record</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>

					<table border="1" cellpadding="10">
						<tr style="font-size:26px;">
						  <td width="75%">';
						  
			$html .= '<table>
						<tr style="font-size:26px;">
						  <td valign="top" width="25%"><b>Date</b></td>
						  <td valign="top" width="2%" ><b>:</b></td>
						  <td valign="top" width="73%">'.$data['data']['date'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Time</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['time'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Kind of A/I</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['kind_desc'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Description of A/I</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" style="text-align: justify">'.$data['data']['description_ai'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Area</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['area_desc'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Injury</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['time'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Description of Injury</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" style="text-align: justify">'.$data['data']['injury'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Witnessed by</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['witness'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Intervention/s</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['intervention_desc'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Preventive Measures</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['preventive_measure_desc'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Agencies Involved</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['agency_desc'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Summary</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" style="text-align: justify">'.$data['data']['summary'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Conclusion</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['conclusion_desc'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Reported To</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['reported_desc'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>A/I Report Completed</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['date_report_completed'].'</td>
						</tr>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Report Completed by</b></td>
						  <td valign="top" ><b>:</b></td>
						  <td valign="top" >'.$data['data']['report_completed_desc'].'</td>
						</tr>
					 </table>';

				$html .= '</td>
						  <td width="25%">';

				$html .= '<table>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Residents</b></td>
						</tr>';


			for ($i = 0; $i<$data['resident_count'];$i++)
			{
				$html .= '
						<tr style="font-size:26px;">
						  <td valign="top" >'.($i+1).'.) '.$data['resident_name'][$i].'</td>
						</tr>';
			}

				$html .= '</table><br><table>
						<tr style="font-size:26px;">
						  <td valign="top" ><b>Staff</b></td>
						</tr>';


			for ($i = 0; $i<$data['staff_count'];$i++)
			{
				$html .= '
						<tr style="font-size:26px;">
						  <td valign="top" >'.($i+1).'.) '.$data['staff_name'][$i].'</td>
						</tr>';
			}
						  
				$html .= '</table></td>
						</tr>
					</table>';

			

			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}