<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit_Logs extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'audit_logs';
		else 
			return 'Audit Logs';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));
	}

	public function loglist()
	{ 
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));			

			die(json_encode($this->generateloglist($query, $date_from, $date_to, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateloglist($query, $date_from, $date_to, $transaction_type)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$limitQuery = "";
			if($transaction_type == 'Grid')
			{
				$limit = $_GET['limit'];
				$start = $_GET['start'];
				$limitQuery = " LIMIT $start, $limit";
			}

			$commandText = "SELECT 
								a.*,
								CONCAT(lname, ', ', fname, ' ',mname) AS created_by
							FROM audit_logs a 
								LEFT JOIN users b ON a.created_by = b.id
								LEFT JOIN staff c ON b.staff_id = c.id
							WHERE
								(a.date_created >= '$date_from' and a.date_created <= '$date_to') and
							    (a.transaction_type like '%$query%' or 
					    		 a.query_type like '%$query%' or
					    		 lname like '%$query%' or
					    		 fname like '%$query%' or
					    		 mname like '%$query%')
							ORDER BY a.date_created DESC, a.time_created DESC
							$limitQuery";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$commandText = "SELECT count(*) as count
							FROM audit_logs a 
								LEFT JOIN users b ON a.created_by = b.id
								LEFT JOIN staff c ON b.staff_id = c.id
							WHERE
								(a.date_created >= '$date_from' and a.date_created <= '$date_to') and
							    (a.transaction_type like '%$query%' or 
					    		 a.query_type like '%$query%' or
					    		 lname like '%$query%' or
					    		 fname like '%$query%' or
					    		 mname like '%$query%')";
			$result = $this->db->query($commandText);
			$query_count = $result->result(); 

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 				=> $value->id,
					'table'				=> $value->entity,
					'date_created' 		=> date('m/d/Y',strtotime($value->date_created)).' '.date('h:i:sa',strtotime($value->time_created)),
					'transaction_type'	=> $value->transaction_type,
					'transaction_id' 	=> $value->transaction_id,
					'query_type'		=> $value->query_type,
					'created_by'		=> strtoupper($value->created_by));
			}

			$data['totalCount'] = $query_count[0]->count;
			return $data;

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{
		$response = array();
        $response['success'] = true;
        if($this->input->post('reporttype') == 'List')
        {
			$query 		= addslashes(strip_tags(trim($this->input->post('query'))));
	    	$date_from 	=  $this->input->post('date_from');
	    	$date_to 	=  $this->input->post('date_to');

    		$response['filename'] = $this->exportpdfList($this->generateloglist($query, $date_from, $date_to, 'Report'), $query, $date_from, $date_to);
        }
    	else {
	    	$id 				=  $this->input->post('id');
	    	$auditlog_id 		=  $this->input->post('auditlog_id');
	    	$table 				=  $this->input->post('table');
	    	$transaction_type 	=  $this->input->post('transaction_type');

    		$response['filename'] = $this->exportpdfRecord($this->generateview($id, $auditlog_id, $table, $transaction_type));
    	}
		die(json_encode($response));
	}

	public function exportpdfList($data, $query, $date_from, $date_to)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "LogList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('LogList');
			$pdf->SetSubject('LogList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
			
			if(!$query) $query = 'NULL';
			
			$html = 'Audit Logs<br><br>

					<table>
						<tr style="font-size:24px;">
						  <td width="10%">Searched by</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$query.'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date From</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_from)).'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date To</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_to)).'</td>
						</tr>
					</table>

					<br>

					<table border="1" cellpadding="2">						
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td width="13%"  style="padding: 10px;" align="left">Date & Time</td>
						  <td width="40%"  style="padding: 10px;" align="left">Transaction Type</td>
						  <td width="10%"  style="padding: 10px;" align="center">ID</td>
						  <td width="10%"  style="padding: 10px;" align="center">Query Type</td>
						  <td width="25%"  style="padding: 10px;" align="left">User</td>
						</tr>';


			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				$html .= '<tr style="font-size:15px;">
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['date_created'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['transaction_type'].'</td>
					  <td style="padding: 10px;" align="center">'.$data['data'][$i]['transaction_id'].'</td>
					  <td style="padding: 10px;" align="center">'.$data['data'][$i]['query_type'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['created_by'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfRecord($data)
	{
		try 
		{
			
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "TransactionLog".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('TransactionLog');
			$pdf->SetSubject('TransactionLog');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
			
			$html = 'Audit Logs - Transaction<br><br>

					<table border="1" cellpadding="2">						
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td width="10%"  style="padding: 10px;" align="left">Date Created</td>
						  <td width="40%"  style="padding: 10px;" align="left">'.$data['date_created'].'</td>
						</tr>
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td style="padding: 10px;" align="left">Time Create</td>
						  <td style="padding: 10px;" align="left">'.$data['time_created'].'</td>
						</tr>
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td style="padding: 10px;" align="left">Created by</td>
						  <td style="padding: 10px;" align="left">'.$data['created_by'].'</td>
						</tr>
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td style="padding: 10px;" align="left">Query Type</td>
						  <td style="padding: 10px;" align="left">'.$data['query_type'].'</td>
						</tr>
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td style="padding: 10px;" align="left">Transaction Type</td>
						  <td style="padding: 10px;" align="left">'.$data['transaction_type'].'</td>
						</tr>
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td style="padding: 10px;" align="left">Transaction ID</td>
						  <td style="padding: 10px;" align="left">'.$data['transaction_id'].'</td>
						</tr>';


			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				$html .= '<tr style="font-size:15px;">
					  <td style="padding: 10px;" align="left">'.$data['label'][$i].'</td>
					  <td style="padding: 10px;" align="left">'.$data['value'][$i].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function view()
	{
		try 
		{
			$id 	= $this->input->post('id');
			$table 	= $this->input->post('table');
			$transaction_type 	= $this->input->post('transaction_type');
			die(json_encode($this->generateview($id, 0, $table, $transaction_type)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateview($id, $auditlog_id, $table, $transaction_type)
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			if($table == 'wound_details')
			{
				$commandText = "SELECT 
									a.date,	
									CONCAT (b.lname, ', ', b.fname) AS name,									
									c.description AS stage_desc,
									d.description AS wound_type_desc,
									e.description AS infection_desc,
									h.description AS site_desc,
									a.wound_no
								FROM wound_details a
									JOIN wound_header a1 ON a.wound_id = a1.id
									LEFT JOIN residents b ON a1.resident_id = b.id									
									LEFT JOIN stages c ON a.stage_id = c.id									
									LEFT JOIN wound_types d ON a1.wound_type_id = d.id
									LEFT JOIN infections e ON a1.infection_id = e.id
									LEFT JOIN sites h ON a1.site_id = h.id
								WHERE a.id = $id";	
			}
			else if($table == 'residents_profile')
			{
				$commandText = "SELECT 	
								CONCAT (c.lname, ', ', c.fname) AS name,
								a.birthdate,
								a.original_admission_date,
								a.latest_admission_date,
								b.description room_desc,
								d.description diagnosis,
								e.description allergies
							FROM residents_profile a 
								LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
								LEFT JOIN residents c ON a.resident_id = c.id
								LEFT JOIN (SELECT 
									a.resident_id,
									b.description
									FROM residents_profile_diagnosis a
									LEFT JOIN diagnosis b ON a.diagnose_id = b.id
									WHERE a.resident_id = $id) d ON d.resident_id = a.resident_id
								LEFT JOIN (SELECT 
									a.resident_id,
									b.description
									FROM residents_profile_allergies a
									LEFT JOIN allergies b ON a.allergy_id = b.id
									WHERE a.resident_id = $id) e ON e.resident_id = a.resident_id
							WHERE a.resident_id = $id";	
			}
			else if($table == 'residents')
				$commandText = "SELECT CONCAT (lname, ', ', fname) AS name, age, sex FROM residents WHERE id = $id";	
			else if($table == 'staff')
			{
				$commandText = "SELECT 
									a.id,
									CONCAT(lname, ', ', fname, ' ',mname) AS name,
									b.description AS department_desc,
									c.description AS position_desc	
								FROM staff a
									LEFT JOIN departments b ON a.department_id = b.id
									LEFT JOIN positions c ON a.position_id = c.id 
									WHERE a.id = $id";	
			}
			else if($table == 'resident_case_details')
			{
				$commandText = "SELECT 
									a.date,		
									a.case_type,
									c.description AS room_desc,
									e.description AS infection_desc,
									d.diagnose_desc,
									CONCAT (c1.lname, ', ', c1.fname) AS name	
								FROM resident_case_details a
									JOIN resident_case_header b ON a.header_case_id = b.id
									JOIN residents c1 ON b.resident_id = c1.id
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c ON b.room_id = c.id
									LEFT JOIN (SELECT 	
										a1.header_case_id,
										GROUP_CONCAT(diagnose_id) diagnose_id,
										GROUP_CONCAT(b1.description) diagnose_desc	
										FROM resident_case_diagnosis a1
										JOIN diagnosis b1 ON a1.diagnose_id = b1.id
										WHERE a1.header_case_id = (SELECT header_case_id FROM resident_case_details WHERE id = $id)) d ON a.header_case_id = d.header_case_id
									LEFT JOIN infections e ON b.infection_id = e.id
								WHERE a.id = $id";	
			}
			else if($table == 'immunizations_details' & $transaction_type == 'Immunization(Resident)')
			{
				$commandText = "SELECT 
									a.date,	
									a.administered,
									CONCAT (b.lname, ', ', b.fname) AS name,
									c.description AS room_desc,
									d.description AS immunization_desc
								FROM immunizations_details a
									JOIN residents b ON a.name_id = b.id
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c ON a.room_id = c.id
									LEFT JOIN immunizations d ON a.immunization_id = d.id
								WHERE a.id = $id";	
			}
			else if($table == 'immunizations_details' & $transaction_type == 'Immunization(Staff)')
			{
				$commandText = "SELECT 
									a.date,	
									a.administered,
									CONCAT (b.lname, ', ', b.fname) AS name,
									d.description AS immunization_desc
								FROM immunizations_details a
									JOIN staff b ON a.name_id = b.id
									LEFT JOIN immunizations d ON a.immunization_id = d.id
								WHERE a.id = $id";	
			}
			else if($table == 'inservice_details')
			{
				$commandText = "SELECT 		
									a.servicedate,
									a.duration,
									a.instructor,
									b.description AS shift_desc,
									c.description AS topic_desc,
									d.staff_name		
								FROM inservice_details a
									LEFT JOIN shifts b ON a.shift_id = b.id
									LEFT JOIN topics c ON a.topic_id = c.id
									LEFT JOIN (SELECT 
										service_id,
										GROUP_CONCAT(CONCAT(lname,', ',fname) SEPARATOR ' and ') AS staff_name
										FROM inservice_details_staff a
										LEFT JOIN staff b ON a.staff_id = b.id
										WHERE service_id = $id) d ON d.service_id = a.id
								WHERE a.id = $id";	
			}
			else if($table == 'consult_details')
			{
				$commandText = "SELECT 
									CONCAT (b.lname, ', ', b.fname) AS name,
									c.description AS room_desc,
									a.date,
									a.time,
									d.description AS consult_desc	
								FROM consult_details a
									JOIN residents b ON a.resident_id = b.id
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c ON a.room_id = c.id
									LEFT JOIN consults d ON a.consult_id = d.id
								WHERE a.id = $id";	
			}
			else if($table == 'ai_details')
			{
				$commandText = "SELECT 
									a.date,
									a.time,
									b.description AS kind_desc,
									c.description AS area_desc,
									d.resident_name,
									e.staff_name
								FROM ai_details a
									LEFT JOIN ai_kinds b ON b.id = a.kind_id
									LEFT JOIN ai_area c ON c.id = a.area_id	
									LEFT JOIN (SELECT 
										a.detail_id,
										GROUP_CONCAT(CONCAT(lname,', ',fname) SEPARATOR ' and ') AS resident_name
										FROM ai_details_residents a
										LEFT JOIN residents b ON a.resident_id = b.id
										WHERE a.detail_id = $id) d ON d.detail_id = a.id
									LEFT JOIN (SELECT 
										a.detail_id,
										GROUP_CONCAT(CONCAT(lname,', ',fname) SEPARATOR ' and ') AS staff_name
										FROM ai_details_staff a
										LEFT JOIN staff b ON a.staff_id = b.id
										WHERE a.detail_id = $id) e ON e.detail_id = a.id
								WHERE a.id = $id";	
			}
			else if($table == 'residents_medical_records')
			{
				$commandText = "SELECT 
									a.*,
									CONCAT (b.lname, ', ', b.fname) AS name,
									IF(a.status=1, 'Active', 'Closed') status_label
								FROM residents_medical_records a
									JOIN residents b ON a.resident_id = b.id
								WHERE a.id = $id";	
			}
			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			$data['totalCount'] = 0;
			foreach($query_result as $key => $value) 
			{	
				if($table == 'wound_details')
				{
					$record[0] = date('m/d/Y',strtotime($value->date));	
					$record[1] = strtoupper($value->name);	
					$record[2] = $value->wound_type_desc;
					$record[3] = $value->infection_desc;
					$record[4] = $value->site_desc;
					$record[5] = $value->wound_no;	
					$record[6] = $value->stage_desc;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Date';	
					$record[1] = 'Name';	
					$record[2] = 'Type';
					$record[3] = 'Origin';
					$record[4] = 'Site';
					$record[5] = 'Site No.';						
					$record[6] = 'Stage';						
					$data['label'] = $record;

					$data['totalCount'] = 7;
				}
				else if($table == 'residents_profile')
				{
					$record[0] = strtoupper($value->name);
					$record[1] = date('m/d/Y',strtotime($value->birthdate));	
					$record[2] = 'Original: '.date('m/d/Y',strtotime($value->original_admission_date)). ' Latest: '.date('m/d/Y',strtotime($value->latest_admission_date));
					$record[3] = $value->diagnosis;
					$record[4] = $value->allergies;	
					$record[5] = $value->room_desc;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Name';	
					$record[1] = 'Birthdate';	
					$record[2] = 'Admission Date';
					$record[3] = 'General Diagnosis';
					$record[4] = 'Allergies';						
					$record[5] = 'Room';						
					$data['label'] = $record;

					$data['totalCount'] = 6;
				}
				else if($table == 'residents')
				{
					$record[0] = strtoupper($value->name);
					$record[1] = $value->age;	
					$record[2] = $value->sex;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Name';	
					$record[1] = 'Age';	
					$record[2] = 'Sex';
					$data['label'] = $record;

					$data['totalCount'] = 3;
				}
				else if($table == 'staff')
				{
					$record[0] = strtoupper($value->name);
					$record[1] = $value->position_desc;	
					$record[2] = $value->department_desc;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Name';	
					$record[1] = 'Position';	
					$record[2] = 'Department';
					$data['label'] = $record;

					$data['totalCount'] = 3;
				}
				else if($table == 'resident_case_details')
				{
					$record[0] = date('m/d/Y',strtotime($value->date));
					$record[1] = strtoupper($value->name);
					$record[2] = $value->diagnose_desc;	
					$record[3] = $value->infection_desc;	
					$record[4] = $value->room_desc;	
					$record[5] = $value->case_type;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Date';	
					$record[1] = 'Name';	
					$record[2] = 'Diagnosis';
					$record[3] = 'Origin of Infection';
					$record[4] = 'Room';
					$record[5] = 'Case Type';
					$data['label'] = $record;

					$data['totalCount'] = 6;
				}
				else if($table == 'immunizations_details' & $transaction_type == 'Immunization(Resident)')
				{
					$record[0] = date('m/d/Y',strtotime($value->date));
					$record[1] = strtoupper($value->name);
					$record[2] = $value->immunization_desc;	
					$record[3] = $value->administered;	
					$record[4] = $value->room_desc;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Date';	
					$record[1] = 'Name';	
					$record[2] = 'Immunization';
					$record[3] = 'Administered';
					$record[4] = 'Room';
					$data['label'] = $record;

					$data['totalCount'] = 5;
				}
				else if($table == 'immunizations_details' & $transaction_type == 'Immunization(Staff)')
				{
					$record[0] = date('m/d/Y',strtotime($value->date));
					$record[1] = strtoupper($value->name);
					$record[2] = $value->immunization_desc;	
					$record[3] = $value->administered;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Date';	
					$record[1] = 'Name';	
					$record[2] = 'Immunization';
					$record[3] = 'Administered';
					$data['label'] = $record;

					$data['totalCount'] = 4;
				}
				else if($table == 'inservice_details')
				{
					$record[0] = date('m/d/Y',strtotime($value->servicedate));
					$record[1] = $value->shift_desc;
					$record[2] = $value->topic_desc;	
					$record[3] = $value->duration;	
					$record[4] = $value->instructor;	
					$record[5] = strtoupper($value->staff_name);
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Service Date';	
					$record[1] = 'Shift';	
					$record[2] = 'Topic';
					$record[3] = 'Duration';
					$record[4] = 'Instructor / By';
					$record[5] = 'Participants';
					$data['label'] = $record;

					$data['totalCount'] = 6;
				}
				else if($table == 'consult_details')
				{
					$record[0] = strtoupper($value->name);
					$record[1] = $value->room_desc;
					$record[2] = date('m/d/Y',strtotime($value->date));
					$record[3] = $value->time;	
					$record[4] = $value->consult_desc;	
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Name';	
					$record[1] = 'Room';	
					$record[2] = 'Date';
					$record[3] = 'Time';
					$record[4] = 'Consult';
					$data['label'] = $record;

					$data['totalCount'] = 5;
				}
				else if($table == 'ai_details')
				{
					$record[0] = date('m/d/Y',strtotime($value->date));
					$record[1] = $value->time;
					$record[2] = $value->kind_desc;
					$record[3] = $value->area_desc;
					$record[4] = strtoupper($value->resident_name);
					$record[5] = strtoupper($value->staff_name);
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Date';	
					$record[1] = 'Time';	
					$record[2] = 'Kind of A/I';
					$record[3] = 'Area';
					$record[4] = 'Residents';
					$record[5] = 'Staff';
					$data['label'] = $record;

					$data['totalCount'] = 6;
				}
				else if($table == 'residents_medical_records')
				{
					if($value->discharge_date)
						$discharge_date = date('m/d/Y',strtotime($value->discharge_date));
					else 
						$discharge_date = "";

					$record[0] = strtoupper($value->name);
					$record[1] = $value->record_no;
					$record[2] = date('m/d/Y',strtotime($value->admission_date));					
					$record[3] = $discharge_date;
					$record[4] = $value->status_label;
					$data['value'] = $record;

					$record = [];
					$record[0] = 'Name';	
					$record[1] = 'Record No.';	
					$record[2] = 'Admission Date';
					$record[3] = 'Discharge Date';
					$record[4] = 'Status';
					$data['label'] = $record;

					$data['totalCount'] = 5;
				}
			}

			
			$commandText = "SELECT 
								a.*,
								CONCAT(lname, ', ', fname, ' ',mname) AS created_by
							FROM audit_logs a 
								LEFT JOIN users b ON a.created_by = b.id
								LEFT JOIN staff c ON b.staff_id = c.id
							WHERE a.id = $auditlog_id";	
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{				
				$data['date_created'] 		= date('m/d/Y',strtotime($value->date_created));
				$data['time_created'] 		= date('h:i:sa',strtotime($value->time_created));
				$data['transaction_type'] 	= $value->transaction_type;
				$data['transaction_id'] 	= $value->transaction_id;
				$data['query_type'] 		= $value->query_type;
				$data['created_by'] 		= strtoupper($value->created_by);
			}

			$data['success'] = true;

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	
}