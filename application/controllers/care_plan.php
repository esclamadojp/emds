<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Care_Plan extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'care_plan';
		else 
			return 'Care Plan';
	} 

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));
	}

	public function careplanList()
	{ 
		try 
		{						
			die(json_encode($this->generatecareplanList(addslashes(strip_tags(trim($_GET['query']))))));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generatecareplanList($query)
	{	
		#update session
		$this->load->model('Session');$this->Session->Validate();

		$limit = $_GET['limit'];
		$start = $_GET['start'];
		$limitQuery = " LIMIT $start, $limit";
		
		$commandText = "SELECT 
							a.id,
							b.description AS category,
							a.description AS diagnosis
						FROM diagnosis a
							LEFT JOIN diagnosis_category b ON a.cat_id = b.id
						WHERE (b.description like '%$query%' or a.description like '%$query%')
							AND a.care_plan = 1
						ORDER BY b.description ASC, a.description ASC
						$limitQuery";		
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		$commandText = "SELECT count(a.id) AS count
						FROM diagnosis a
							LEFT JOIN diagnosis_category b ON a.cat_id = b.id
						WHERE (b.description like '%$query%' or a.description like '%$query%')
							AND a.care_plan = 1
						ORDER BY b.description ASC, a.description ASC";
		$result = $this->db->query($commandText);
		$query_count = $result->result(); 

		if(count($query_result) == 0) 
		{
			$data["totalCount"] = 0;
			$data["data"] 		= array();
			die(json_encode($data));
		}	

		foreach($query_result as $key => $value) 
		{	
			$data['data'][] = array(
				'id' 		=> strtoupper($value->id),
				'category' 	=> strtoupper($value->category),
				'diagnosis' => strtoupper($value->diagnosis));
		}			

		$data['totalCount'] = $query_count[0]->count;
		return $data;
	}

	public function gicList()
	{ 
		try 
		{			
			die(json_encode($this->generateregicList($_GET['id'], $_GET['type'])));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateregicList($id, $type)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$commandText = "SELECT 
								a.*
							FROM residents_profile_care_plan_defaults a 
							WHERE a.diagnosis_id = $id 
								AND a.type = '$type' 
							ORDER BY a.id ASC";	
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{								
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
				'id' 			=> $value->id,
				'description' 	=> $value->remarks);
			}	

			$data["success"] = true;
			$data["count"] = count($query_result);

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function giccrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$diagnosis_id	= $this->input->post('diagnosis_id');
			$remarks		= strip_tags(trim($this->input->post('remarks')));
			$ttype			= $this->input->post('ttype');
			$status			= $this->input->post('gic_status');
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "delete from residents_profile_care_plan_defaults where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$this->load->model('residents_profile_care_plan_defaults');

				if ($type == "Add")
					$id = 0;
				if ($type == "Edit") 				
					$this->residents_profile_care_plan_defaults->id = $id;
				
				$this->residents_profile_care_plan_defaults->diagnosis_id 	= $diagnosis_id;
				$this->residents_profile_care_plan_defaults->remarks 				= $remarks;
				$this->residents_profile_care_plan_defaults->type					= $ttype;
				$this->residents_profile_care_plan_defaults->save($id);

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'residents_profile_care_plan_defaults', $type, $this->modulename('Label').' CARE PLAN');
			}
			
			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}

	public function gicView()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');

			$commandText = "select * from residents_profile_care_plan_defaults where id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['remarks'] 	= $value->remarks;					
			}

			$data['data'] = $record;
			$data['success'] = true;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	
}