<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charts extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'charts';
		else 
			return 'Charts';
	}

	public function index() { 
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));
	}

	public function diagnosis_category()
	{
		$commandText = "SELECT * FROM diagnosis_category WHERE infection_control = 1 ORDER BY description ASC";
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		foreach($query_result as $key => $value) 
		{	
			$data[] = array(
				'data' 			=> 0,
				'id' 			=> $value->id,
				'name' 			=> 'List of Resident/s',
				'cat' 			=> $value->description);
		}

		return $data;
	}

	public function precautions_category()
	{
		$commandText = "SELECT * FROM transmission_precautions_category WHERE infection_control = 1 ORDER BY description ASC";
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		foreach($query_result as $key => $value) 
		{	
			$data[] = array(
				'data' 			=> 0,
				'id' 			=> $value->id,
				'name' 			=> 'List of Resident/s',
				'cat' 			=> $value->description);
		}

		return $data;
	}

	public function chartdata()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));
			$building_id= $_GET['building_id'];
			$floor_id 	= $_GET['floor_id'];
			$infection_id = $_GET['infection_id'];

			if($building_id != 0) $building_id = " d.id = $building_id AND ";
			else $building_id = null;

			if($floor_id != 0) $floor_id = " c.id = $floor_id AND ";
			else $floor_id = null;

			if($infection_id != 0) $infection_id = " a.infection_id = $infection_id AND ";
			else $infection_id = null;

			#for diagnosis data
			$commandText = "SELECT 
								d.cat_id,
								a.diagnose_id,
								b.resident_id,
								c.fname,
								c.mname,
								c.lname,
								c.age,
								c.sex,
								e.description AS rooms_desc
							FROM resident_case_diagnosis a
								JOIN resident_case_header b ON b.id = a.header_case_id
								JOIN residents c ON b.resident_id = c.id
								JOIN diagnosis d ON a.diagnose_id = d.id
								LEFT JOIN (SELECT a.id, CONCAT(alias,'-',b.description,'-',a.description) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) e ON b.room_id = e.id
							WHERE header_case_id IN (
								SELECT 
									a.id
								FROM resident_case_header a 
									JOIN rooms b ON b.id = a.room_id
									JOIN floors c ON c.id = b.cat_id
									JOIN buildings d ON d.id = c.cat_id
								WHERE 
									$building_id
									$floor_id
									$infection_id
									(DATE BETWEEN '$date_from' AND '$date_to')
								)
								AND b.active = 1
							GROUP BY resident_id, cat_id
							ORDER BY cat_id ASC";	
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$diagnosis_category = $this->diagnosis_category();
			$tp_category = $this->precautions_category();
			foreach($query_result as $key => $value) 
			{	
				$x = 0;
				foreach($diagnosis_category as $key) 
				{	
					if($diagnosis_category[$x]['id'] == $value->cat_id)
					{
						$details = array();
						$id = $value->resident_id;
						$name = strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname);
						$rooms_desc = $value->rooms_desc;
						$sex = $value->sex;

						$diagnosis_category[$x]['data'] +=1;	
						
						if($value->sex == "Male") $extension = "<font color=blue size=1.5> (Male), ".$value->rooms_desc."</font>";
						else $extension = "<font color=green size=1.5> (Female), ".$value->rooms_desc."</font>";
						$resident_name = ($diagnosis_category[$x]['data']).'.) <a href="#" onclick="LoadResidentCases(\'chart\', \''.$id.'\', \''.$name.'\', \''.$rooms_desc.'\', \''.$sex.'\')">'.$name.'</a>'.$extension;

					    $diagnosis_category[$x]['name'] .= '<br>'.$resident_name;
					}				
					$x++;
				}
				$x = 0;
			}

			foreach($diagnosis_category as $key => $value) 
			{
				$data['data'][] = array(
					'data' 			=> $value['data'],
					'name' 			=> $value['name'],
					'cat' 			=> $value['cat']);
			}

			#for precaution data
			$commandText = "SELECT 
								c.id,
								d.resident_id,
								e.fname,
								e.mname,
								e.lname,
								e.age,
								e.sex,
								f.description AS rooms_desc
							FROM resident_case_details a
								JOIN transmission_precautions b ON b.id	= a.precaution_id
								JOIN transmission_precautions_category c ON c.id = b.cat_id
								JOIN resident_case_header d ON d.id = a.header_case_id
								JOIN residents e ON d.resident_id = e.id
								LEFT JOIN (SELECT a.id, CONCAT(alias,'-',b.description,'-',a.description) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) f ON d.room_id = f.id
							WHERE header_case_id IN (
								SELECT 
									a.id
								FROM resident_case_header a 
									JOIN rooms b ON b.id = a.room_id
									JOIN floors c ON c.id = b.cat_id
									JOIN buildings d ON d.id = c.cat_id
								WHERE 
									$building_id
									$floor_id
									$infection_id
									(DATE BETWEEN '$date_from' AND '$date_to')
								)
							GROUP BY resident_id, c.id
							ORDER BY c.id ASC";							
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$diagnosis_category = $this->diagnosis_category();
			$tp_category = $this->precautions_category();
			foreach($query_result as $key => $value) 
			{	
				$x = 0;
				foreach($tp_category as $key) 
				{	
					if($tp_category[$x]['id'] == $value->id)
					{
						$tp_category[$x]['data'] +=1;	
						
						if($value->sex == "Male") $extension = "<font color=blue size=1.5> (Male), ".$value->rooms_desc."</font>";
						else $extension = "<font color=green size=1.5> (Female), ".$value->rooms_desc."</font>";
						$resident_name = ($tp_category[$x]['data']).'.) '.strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname).$extension;

					    $tp_category[$x]['name'] .= '<br>'.$resident_name;
					}				
					$x++;
				}
				$x = 0;
			}

			foreach($tp_category as $key => $value) 
			{
				$data['data'][] = array(
					'data' 			=> $value['data'],
					'name' 			=> $value['name'],
					'cat' 			=> $value['cat']);
			}
			
			$data['count'] = count($query_result);
			die(json_encode($data));

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function griddata()
	{ 
		try 
		{
			$type = $_GET['type'];
			$building_id = $_GET['building_id'];
			$infection_id = $_GET['infection_id'];
			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));

			die(json_encode($this->generategridlist($type, $date_from, $date_to, $building_id, $infection_id, 'List')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generategridlist($type, $date_from, $date_to, $building_id, $infection_id, $transaction_type)
	{
		try 
		{
			if($building_id != 0) $building_id = " d.id = $building_id AND ";
			else $building_id = null;

			if($infection_id != 0) $infection_id = " a.infection_id = $infection_id AND ";
			else $infection_id = null;


			if($building_id == 0)
				$commandText = "SELECT a.* FROM (SELECT a.id, CONCAT(alias,'-',a.description) AS description FROM floors a JOIN buildings b ON a.cat_id = b.id) a ORDER BY a.description ASC";
			else
				$commandText = "SELECT * FROM floors where cat_id = $building_id ORDER BY description ASC";
			$result = $this->db->query($commandText);
			$query_floor = $result->result(); 

			$data['metaData']['columns'][] = array('text' => 'Category \ Floors', 'dataIndex' => 'category', 'width' => '100', 'locked' => true);
			$data['metaData']['fields'][] = array('name' => 'original_category');			
			
			foreach($query_floor as $key => $value) 
			{
				$data['metaData']['columns'][] = array('text' => $value->description, 'dataIndex' => 'floor'.$value->id, 'width' => '50', 'align' => 'center');
				$data['metaData']['fields'][] = array('name' => 'floor'.$value->id);
			}
			$data['metaData']['columns'][] = array('text' => 'Total', 'dataIndex' => 'total', 'width' => '50', 'align' => 'center');
			$data['metaData']['fields'][] = array('name' => 'total');
			$data['metaData']['fields'][] = array('name' => 'category');

			$data['columnsCount'] = count($data['metaData']['columns']);
			$diagnosis_category = $this->diagnosis_category();
			$tp_category = $this->precautions_category();

			#for diagnosis
			$commandText = "SELECT 
								d.cat_id,
								f.id AS floor_id,
								f.description,
								a.diagnose_id,
								b.resident_id,
								c.fname,
								c.mname,
								c.lname,
								c.age,
								c.sex
							FROM resident_case_diagnosis a
								JOIN resident_case_header b ON b.id = a.header_case_id
								JOIN residents c ON b.resident_id = c.id
								JOIN diagnosis d ON a.diagnose_id = d.id
								JOIN rooms e ON e.id = b.room_id
								JOIN floors f ON f.id = e.cat_id
							WHERE header_case_id IN (
								SELECT 
									a.id
								FROM resident_case_header a 
									JOIN rooms b ON b.id = a.room_id
									JOIN floors c ON c.id = b.cat_id
									JOIN buildings d ON d.id = c.cat_id
								WHERE  
									$building_id
									$infection_id
									(DATE BETWEEN '$date_from' AND '$date_to')
								)
								AND b.active = 1
							GROUP BY resident_id, cat_id
							ORDER BY cat_id ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($diagnosis_category as $key => $value) 
			{				
				foreach($query_floor as $key => $floor_val)
					$floor['floor'.$floor_val->id] = 0;
				$x=0;
				foreach($query_result as $key => $diagnose_value) 
				{
					if($diagnose_value->cat_id == $value['id'])
					{
						$floor['floor'.$diagnose_value->floor_id] +=1;
						$x++;
					}
				}
				$floor['category'] = $value['cat'];
				$floor['original_category'] = $value['cat'];
				$floor['total'] = $x;
				$data['data'][] = $floor;
			}			

			$commandText = "SELECT 
								c.id,
								d.resident_id,
								g.id AS floor_id,
								e.fname,
								e.mname,
								e.lname,
								e.age,
								e.sex
							FROM resident_case_details a
								JOIN transmission_precautions b ON b.id	= a.precaution_id
								JOIN transmission_precautions_category c ON c.id = b.cat_id
								JOIN resident_case_header d ON d.id = a.header_case_id
								JOIN residents e ON d.resident_id = e.id
								JOIN rooms f ON f.id = d.room_id
								JOIN floors g ON g.id = f.cat_id
							WHERE header_case_id IN (
								SELECT 
									a.id
								FROM resident_case_header a 
									JOIN rooms b ON b.id = a.room_id
									JOIN floors c ON c.id = b.cat_id
									JOIN buildings d ON d.id = c.cat_id
								WHERE 
									$building_id
									$infection_id
									(DATE BETWEEN '$date_from' AND '$date_to')
								)
							GROUP BY resident_id, c.id
							-- GROUP BY c.id,header_case_id,resident_id
							ORDER BY c.id ASC";

			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($tp_category as $key => $value) 
			{				
				foreach($query_floor as $key => $floor_val)
					$floor['floor'.$floor_val->id] = 0;
				$x=0;
				foreach($query_result as $key => $precaution_value) 
				{
					if($precaution_value->id == $value['id'])
					{
						$floor['floor'.$precaution_value->floor_id] +=1;
						$x++;
					}
				}
				$floor['category'] = '<b>'.$value['cat'].'<b>';
				$floor['original_category'] = $value['cat'];
				$floor['total'] = $x;
				$data['data'][] = $floor;
			}

			$data['categoryCount'] = count($data['data']);
			$data['count'] = count($query_result);
		
			// #removing ZERO
			$x = 0;
			foreach($data['data'] as $key) 
			{	
				foreach($query_floor as $key => $floor_val)	
					if($data['data'][$x]['floor'.$floor_val->id] == 0)
						$data['data'][$x]['floor'.$floor_val->id] = "";
				$x++;
			}	
			
			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function caseentrylist()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$cat_id = $_GET['cat_id'];

			if($cat_id == 0)
				$commandText = "SELECT b.id, CONCAT(alias,'-',b.description) AS description FROM floors b JOIN buildings c ON b.cat_id = c.id where (b.description like '%$query%' or alias like '%$query%') order by description asc";
			else 
				$commandText = "SELECT b.id, CONCAT(b.description) AS description FROM floors b where b.cat_id = $cat_id and (b.description like '%$query%') order by description asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,						
					'description' 	=> $value->description);
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportdocument()
	{
		$response = array();
        $response['success'] = true;

		$type 			=  $this->input->post('type');
    	$date_from 		=  $this->input->post('date_from');
    	$date_to 		=  $this->input->post('date_to');
    	$building_id 	=  $this->input->post('building_id');
    	$infection_id 	=  $this->input->post('infection_id');
    	$title 			=  $this->input->post('title');

		$response['filename'] = $this->exportexcelChartList($this->generategridlist($type, $date_from, $date_to, $building_id, $infection_id, 'Report'), $title, $date_from, $date_to);

		die(json_encode($response));
	}

	public function exportexcelChartList($data, $title, $date_from, $date_to)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ChartList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ChartList")
					      ->setSubject("Report")
					      ->setDescription("Generating ChartList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");
			
	      	$columnChar1 = 'B';		
	      	$columnChar2 = 'C';		

			for ($i = 0; $i<$data['columnsCount'];$i++){
				#Dimensions
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnChar2)->setWidth(14);

				#Font & Alignment
				$objPHPExcel->getActiveSheet()->getStyle($columnChar1.'9')->getFont()->setBold(true);	
				$objPHPExcel->getActiveSheet()->getStyle($columnChar2.'9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$columnChar1++;
				$columnChar2++;
			}
			
			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);

			#Header Title
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'Infection Control');
			###DATE

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Filter:")		
					      ->setCellValue("B6", "Date From:")
					      ->setCellValue("B7", "Date To:")
					      ->setCellValue("C5", $title)
					      ->setCellValue("C6", date('m/d/Y',strtotime($date_from)))
					      ->setCellValue("C7", date('m/d/Y',strtotime($date_to)));

			$columnChar = 'B';		
			for ($i = 0; $i<$data['columnsCount'];$i++){
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnChar."9", $data['metaData']['columns'][$i]['text']);
				$columnChar++;
			}

			$rownNumber = 10;
			for ($x = 0; $x<$data['categoryCount'];$x++){
				$columnChar = 'B';		
				$columnChar1 = 'C';
				for ($y = 0; $y<$data['columnsCount'];$y++){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($columnChar.$rownNumber, $data['data'][$x][$data['metaData']['fields'][$y]['name']]);

					#alignment
					$objPHPExcel->getActiveSheet()->getStyle($columnChar1.$rownNumber)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

					$columnChar++;
					$columnChar1++;
				}				

				#wrap				
				$objPHPExcel->getActiveSheet()->getStyle('B'.$rownNumber)->getAlignment()->setWrapText(true); 

				$rownNumber++;
			}
	      	
			$this->load->library('session');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($rownNumber+9).':L11'.($rownNumber+10))->getFont()->setSize(8); 
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($rownNumber+9), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($rownNumber+10), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}