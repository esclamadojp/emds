<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commonquery extends CI_Controller {
	/**
	*/
	public function updateSession() 
	{
		try 
		{ 						
			$this->load->model('Session');		
        	$this->Session->Update();
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	
	
	public function maintenancecrud() 
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id			= $this->input->post('id');
			$description= strip_tags(trim($this->input->post('description')));
			$alias		= strip_tags(trim($this->input->post('alias')));
			$crudtype	= $this->input->post('crudtype');
			$cat_id		= $this->input->post('cat_id');			
			$care_plan	= $this->input->post('care_plan');		
			$type		= $this->input->post('type');
			$infection_control = $this->input->post('infection_control');
			
			$this->load->model('Access'); $this->Access->rights(null, $crudtype, 'Maintenance');
			if ($crudtype == "Delete")
			{
				$commandText = "delete from $type where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{			
				if ($crudtype == "Add") 
				{
					if ($type == 'floors' || $type == 'rooms' || $type == 'diagnosis' || $type == 'transmission_precautions' || $type == 'immunizations')
						$commandText = "select * from $type where cat_id = $cat_id and description = '".addslashes($description)."'";
					else 
						$commandText = "select * from $type where description = '".addslashes($description)."'";

					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					if ($type == 'rooms') $this->load->model('Rooms'); 
					else if ($type == 'infections') $this->load->model('Infections'); 
					else if ($type == 'diagnosis') $this->load->model('Diagnosis'); 
					else if ($type == 'signs_symptoms') $this->load->model('Signs_Symptoms'); 
					else if ($type == 'routes') $this->load->model('Routes'); 
					else if ($type == 'tests') $this->load->model('Tests'); 
					else if ($type == 'conditions') $this->load->model('Conditions'); 
					else if ($type == 'transmission_precautions') $this->load->model('Transmission_Precautions'); 
					else if ($type == 'consults') $this->load->model('Consults'); 
					else if ($type == 'physicians') $this->load->model('Physicians'); 
					else if ($type == 'administered_sites') $this->load->model('Administered_Sites'); 
					else if ($type == 'reactions') $this->load->model('Reactions'); 									
					else if ($type == 'interventions') $this->load->model('Interventions');					
					else if ($type == 'indurations') $this->load->model('Indurations');					
					else if ($type == 'confirmation_tests') $this->load->model('Confirmation_Tests');					
					else if ($type == 'positions') $this->load->model('Positions');					
					else if ($type == 'departments') $this->load->model('Departments');					
					else if ($type == 'shifts') $this->load->model('Shifts');					
					else if ($type == 'topics') $this->load->model('Topics');
					else if ($type == 'medications') $this->load->model('Medications');
					else if ($type == 'dosages') $this->load->model('Dosages');
					else if ($type == 'tablets') $this->load->model('Tablets');
					else if ($type == 'frequencies') $this->load->model('Frequencies');
					else if ($type == 'durations') $this->load->model('Durations');
					else if ($type == 'reasons') $this->load->model('Reasons');
					else if ($type == 'status') $this->load->model('Status');
					else if ($type == 'locations') $this->load->model('Locations');
					else if ($type == 'wound_types') $this->load->model('Wound_Types');
					else if ($type == 'sites') $this->load->model('Sites');
					else if ($type == 'stages') $this->load->model('Stages');
					else if ($type == 'exudates') $this->load->model('Exudates');
					else if ($type == 'exudates_amount') $this->load->model('Exudates_Amount');
					else if ($type == 'odors') $this->load->model('Odors');
					else if ($type == 'wound_beds') $this->load->model('Wound_Beds');
					else if ($type == 'peri_wounds') $this->load->model('Peri_Wounds');
					else if ($type == 'temperatures') $this->load->model('Temperatures');
					else if ($type == 'allergies') $this->load->model('Allergies');
					else if ($type == 'treatments') $this->load->model('Treatments');
					else if ($type == 'immunizations') $this->load->model('Immunizations');
					else if ($type == 'diagnosis_category') $this->load->model('Diagnosis_Category');
					else if ($type == 'floors') $this->load->model('Floors');
					else if ($type == 'transmission_precautions_category') $this->load->model('Transmission_Precautions_Category');
					else if ($type == 'immunizations_category') $this->load->model('Immunizations_Category');
					else if ($type == 'buildings') $this->load->model('Buildings');
					else if ($type == 'ai_agencies') $this->load->model('ai_agencies');
					else if ($type == 'ai_area') $this->load->model('ai_area');
					else if ($type == 'ai_conclusions') $this->load->model('ai_conclusions');
					else if ($type == 'ai_kinds') $this->load->model('ai_kinds');
					else if ($type == 'ai_interventions') $this->load->model('ai_interventions');
					else if ($type == 'ai_preventive_measures') $this->load->model('ai_preventive_measures');					
					else if ($type == 'risk_factors') $this->load->model('risk_factors');					
					else if ($type == 'dietary_supplements') $this->load->model('dietary_supplements');					
					else if ($type == 'materials') $this->load->model('materials');										
				}

				if ($crudtype == "Edit") 				
				{
					if ($type == 'floors' || $type == 'rooms' || $type == 'diagnosis' || $type == 'transmission_precautions' || $type == 'immunizations')
						$commandText = "select * from $type where cat_id = $cat_id and description = '".addslashes($description)."' and id <> '$id'";
					else 
						$commandText = "select * from $type where description = '".addslashes($description)."' and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					if ($type == 'rooms') { $this->load->model('Rooms'); $this->Rooms->id = $id;}					
					else if ($type == 'infections') { $this->load->model('Infections'); $this->Infections->id = $id;}
					else if ($type == 'diagnosis') { $this->load->model('Diagnosis'); $this->Diagnosis->id = $id;}
					else if ($type == 'signs_symptoms') { $this->load->model('Signs_Symptoms'); $this->Signs_Symptoms->id = $id;}
					else if ($type == 'routes') { $this->load->model('Routes'); $this->Routes->id = $id;}
					else if ($type == 'tests') { $this->load->model('Tests'); $this->Tests->id = $id;}
					else if ($type == 'conditions') { $this->load->model('Conditions'); $this->Conditions->id = $id;}
					else if ($type == 'transmission_precautions') { $this->load->model('Transmission_Precautions'); $this->Transmission_Precautions->id = $id;}
					else if ($type == 'consults') { $this->load->model('Consults'); $this->Consults->id = $id;}
					else if ($type == 'physicians') { $this->load->model('Physicians'); $this->Physicians->id = $id;}					
					else if ($type == 'administered_sites') { $this->load->model('Administered_Sites'); $this->Administered_Sites->id = $id;}
					else if ($type == 'reactions') { $this->load->model('Reactions'); $this->Reactions->id = $id;}
					else if ($type == 'interventions') { $this->load->model('Interventions'); $this->Interventions->id = $id;}
					else if ($type == 'indurations') { $this->load->model('Indurations'); $this->Indurations->id = $id;}
					else if ($type == 'confirmation_tests') { $this->load->model('Confirmation_Tests'); $this->Confirmation_Tests->id = $id;}
					else if ($type == 'positions') { $this->load->model('Positions'); $this->Positions->id = $id;}
					else if ($type == 'departments') { $this->load->model('Departments'); $this->Departments->id = $id;}
					else if ($type == 'shifts') { $this->load->model('Shifts'); $this->Shifts->id = $id;}
					else if ($type == 'topics') { $this->load->model('Topics'); $this->Topics->id = $id;}
					else if ($type == 'medications') { $this->load->model('Medications'); $this->Medications->id = $id;}
					else if ($type == 'dosages') { $this->load->model('Dosages'); $this->Dosages->id = $id;}
					else if ($type == 'tablets') { $this->load->model('Tablets'); $this->Tablets->id = $id;}
					else if ($type == 'frequencies') { $this->load->model('Frequencies'); $this->Frequencies->id = $id;}
					else if ($type == 'durations') { $this->load->model('Durations'); $this->Durations->id = $id;}
					else if ($type == 'reasons') { $this->load->model('Reasons'); $this->Reasons->id = $id;}
					else if ($type == 'status') { $this->load->model('Status'); $this->Status->id = $id;}
					else if ($type == 'locations') { $this->load->model('Locations'); $this->Locations->id = $id;}
					else if ($type == 'wound_types') { $this->load->model('Wound_Types'); $this->Wound_Types->id = $id;}
					else if ($type == 'sites') { $this->load->model('Sites'); $this->Sites->id = $id;}
					else if ($type == 'stages') { $this->load->model('Stages'); $this->Stages->id = $id;}
					else if ($type == 'exudates') { $this->load->model('Exudates'); $this->Exudates->id = $id;}
					else if ($type == 'exudates_amount') { $this->load->model('Exudates_Amount'); $this->Exudates_Amount->id = $id;}
					else if ($type == 'odors') { $this->load->model('Odors'); $this->Odors->id = $id;}
					else if ($type == 'wound_beds') { $this->load->model('Wound_Beds'); $this->Wound_Beds->id = $id;}
					else if ($type == 'peri_wounds') { $this->load->model('Peri_Wounds'); $this->Peri_Wounds->id = $id;}
					else if ($type == 'temperatures') { $this->load->model('Temperatures'); $this->Temperatures->id = $id;}
					else if ($type == 'allergies') { $this->load->model('Allergies'); $this->Allergies->id = $id;}
					else if ($type == 'treatments') { $this->load->model('Treatments'); $this->Treatments->id = $id;}
					else if ($type == 'immunizations') { $this->load->model('Immunizations'); $this->Immunizations->id = $id;}
					else if ($type == 'diagnosis_category') { $this->load->model('Diagnosis_Category'); $this->Diagnosis_Category->id = $id;}
					else if ($type == 'floors') { $this->load->model('Floors'); $this->Floors->id = $id;}
					else if ($type == 'transmission_precautions_category') { $this->load->model('Transmission_Precautions_Category'); $this->Transmission_Precautions_Category->id = $id;}
					else if ($type == 'immunizations_category') { $this->load->model('Immunizations_Category'); $this->Immunizations_Category->id = $id;}
					else if ($type == 'buildings') { $this->load->model('Buildings'); $this->Buildings->id = $id;}
					else if ($type == 'ai_agencies') { $this->load->model('ai_agencies'); $this->ai_agencies->id = $id;}
					else if ($type == 'ai_area') { $this->load->model('ai_area'); $this->ai_area->id = $id;}
					else if ($type == 'ai_conclusions') { $this->load->model('ai_conclusions'); $this->ai_conclusions->id = $id;}
					else if ($type == 'ai_kinds') { $this->load->model('ai_kinds'); $this->ai_kinds->id = $id;}
					else if ($type == 'ai_interventions') { $this->load->model('ai_interventions'); $this->ai_interventions->id = $id;}
					else if ($type == 'ai_preventive_measures') { $this->load->model('ai_preventive_measures'); $this->ai_preventive_measures->id = $id;}
					else if ($type == 'risk_factors') { $this->load->model('risk_factors'); $this->risk_factors->id = $id;}
					else if ($type == 'dietary_supplements') { $this->load->model('dietary_supplements'); $this->dietary_supplements->id = $id;}
					else if ($type == 'materials') { $this->load->model('materials'); $this->materials->id = $id;}					
				}

				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"$type description already exist.");
					die(json_encode($data));
				}

				$this->load->library('session');
	
				if ($crudtype == "Add") 
				{
					if ($type == 'rooms') { $this->Rooms->cat_id = $cat_id; $this->Rooms->description = $description; $this->Rooms->save(0); }
					else if ($type == 'infections') { $this->Infections->description = $description; $this->Infections->save(0); }
					else if ($type == 'diagnosis') { $this->Diagnosis->cat_id = $cat_id; $this->Diagnosis->care_plan = $care_plan; $this->Diagnosis->description = $description; $this->Diagnosis->save(0); }
					else if ($type == 'signs_symptoms') { $this->Signs_Symptoms->description = $description; $this->Signs_Symptoms->save(0); }
					else if ($type == 'routes') { $this->Routes->description = $description; $this->Routes->save(0); }
					else if ($type == 'tests') { $this->Tests->description = $description; $this->Tests->save(0); }
					else if ($type == 'conditions') { $this->Conditions->description = $description; $this->Conditions->save(0); }
					else if ($type == 'transmission_precautions') { $this->Transmission_Precautions->cat_id = $cat_id; $this->Transmission_Precautions->description = $description; $this->Transmission_Precautions->save(0); }
					else if ($type == 'consults') { $this->Consults->description = $description; $this->Consults->save(0); }
					else if ($type == 'physicians') { $this->Physicians->description = $description; $this->Physicians->save(0); }
					else if ($type == 'administered_sites') { $this->Administered_Sites->description = $description; $this->Administered_Sites->save(0); }
					else if ($type == 'reactions') { $this->Reactions->description = $description; $this->Reactions->save(0); }
					else if ($type == 'interventions') { $this->Interventions->description = $description; $this->Interventions->save(0); }
					else if ($type == 'indurations') { $this->Indurations->description = $description; $this->Indurations->save(0); }
					else if ($type == 'confirmation_tests') { $this->Confirmation_Tests->description = $description; $this->Confirmation_Tests->save(0); }
					else if ($type == 'positions') { $this->Positions->description = $description; $this->Positions->save(0); }
					else if ($type == 'departments') { $this->Departments->description = $description; $this->Departments->save(0); }
					else if ($type == 'shifts') { $this->Shifts->description = $description; $this->Shifts->save(0); }
					else if ($type == 'topics') { $this->Topics->description = $description; $this->Topics->save(0); }
					else if ($type == 'medications') { $this->Medications->description = $description; $this->Medications->save(0); }
					else if ($type == 'dosages') { $this->Dosages->description = $description; $this->Dosages->save(0); }
					else if ($type == 'tablets') { $this->Tablets->description = $description; $this->Tablets->save(0); }
					else if ($type == 'frequencies') { $this->Frequencies->description = $description; $this->Frequencies->save(0); }
					else if ($type == 'durations') { $this->Durations->description = $description; $this->Durations->save(0); }
					else if ($type == 'reasons') { $this->Reasons->description = $description; $this->Reasons->save(0); }
					else if ($type == 'status') { $this->Status->description = $description; $this->Status->save(0); }
					else if ($type == 'locations') { $this->Locations->description = $description; $this->Locations->save(0); }
					else if ($type == 'wound_types') { $this->Wound_Types->description = $description; $this->Wound_Types->save(0); }
					else if ($type == 'sites') { $this->Sites->description = $description; $this->Sites->save(0); }
					else if ($type == 'stages') { $this->Stages->order = 0; $this->Stages->description = $description; $this->Stages->save(0); }
					else if ($type == 'exudates') { $this->Exudates->description = $description; $this->Exudates->save(0); }
					else if ($type == 'exudates_amount') { $this->Exudates_Amount->description = $description; $this->Exudates_Amount->save(0); }
					else if ($type == 'odors') { $this->Odors->description = $description; $this->Odors->save(0); }
					else if ($type == 'wound_beds') { $this->Wound_Beds->description = $description; $this->Wound_Beds->save(0); }
					else if ($type == 'peri_wounds') { $this->Peri_Wounds->description = $description; $this->Peri_Wounds->save(0); }
					else if ($type == 'temperatures') { $this->Temperatures->description = $description; $this->Temperatures->save(0); }
					else if ($type == 'allergies') { $this->Allergies->description = $description; $this->Allergies->save(0); }
					else if ($type == 'treatments') { $this->Treatments->description = $description; $this->Treatments->save(0); }
					else if ($type == 'immunizations') { $this->Immunizations->cat_id = $cat_id; $this->Immunizations->description = $description; $this->Immunizations->save(0); }
					else if ($type == 'diagnosis_category') { $this->Diagnosis_Category->description = $description; $this->Diagnosis_Category->infection_control = $infection_control; $this->Diagnosis_Category->save(0); }
					else if ($type == 'floors') { $this->Floors->cat_id = $cat_id; $this->Floors->description = $description; $this->Floors->save(0); }
					else if ($type == 'transmission_precautions_category') { $this->Transmission_Precautions_Category->description = $description; $this->Transmission_Precautions_Category->infection_control = $infection_control; $this->Transmission_Precautions_Category->save(0); }
					else if ($type == 'immunizations_category') { $this->Immunizations_Category->description = $description; $this->Immunizations_Category->save(0); }
					else if ($type == 'buildings') { $this->Buildings->description = $description;$this->Buildings->alias = $alias; $this->Buildings->save(0); }
					else if ($type == 'ai_agencies') { $this->ai_agencies->description = $description; $this->ai_agencies->save(0); }
					else if ($type == 'ai_area') { $this->ai_area->description = $description; $this->ai_area->save(0); }
					else if ($type == 'ai_conclusions') { $this->ai_conclusions->description = $description; $this->ai_conclusions->save(0); }
					else if ($type == 'ai_kinds') { $this->ai_kinds->description = $description; $this->ai_kinds->save(0); }
					else if ($type == 'ai_interventions') { $this->ai_interventions->description = $description; $this->ai_interventions->save(0); }
					else if ($type == 'ai_preventive_measures') { $this->ai_preventive_measures->description = $description; $this->ai_preventive_measures->save(0); }
					else if ($type == 'risk_factors') { $this->risk_factors->description = $description; $this->risk_factors->save(0); }
					else if ($type == 'dietary_supplements') { $this->dietary_supplements->description = $description; $this->dietary_supplements->save(0); }
					else if ($type == 'materials') { $this->materials->description = $description; $this->materials->save(0); }										
				}
				
				if ($crudtype == "Edit")
				{
					if ($type == 'rooms') { $this->Rooms->cat_id = $cat_id; $this->Rooms->description = $description; $this->Rooms->save($id); }
					else if ($type == 'infections') { $this->Infections->description = $description; $this->Infections->save($id); }
					else if ($type == 'diagnosis') { $this->Diagnosis->cat_id = $cat_id; $this->Diagnosis->care_plan = $care_plan; $this->Diagnosis->description = $description; $this->Diagnosis->save($id); }
					else if ($type == 'signs_symptoms') { $this->Signs_Symptoms->description = $description; $this->Signs_Symptoms->save($id); }
					else if ($type == 'routes') { $this->Routes->description = $description; $this->Routes->save($id); }
					else if ($type == 'tests') { $this->Tests->description = $description; $this->Tests->save($id); }
					else if ($type == 'conditions') { $this->Conditions->description = $description; $this->Conditions->save($id); }
					else if ($type == 'transmission_precautions') { $this->Transmission_Precautions->cat_id = $cat_id; $this->Transmission_Precautions->description = $description; $this->Transmission_Precautions->save($id); }
					else if ($type == 'consults') { $this->Conditions->description = $description; $this->Conditions->save($id); }
					else if ($type == 'physicians') { $this->Physicians->description = $description; $this->Physicians->save($id); }
					else if ($type == 'administered_sites') { $this->Administered_Sites->description = $description; $this->Administered_Sites->save($id); }
					else if ($type == 'reactions') { $this->Reactions->description = $description; $this->Reactions->save($id); }
					else if ($type == 'interventions') { $this->Interventions->description = $description; $this->Interventions->save($id); }
					else if ($type == 'indurations') { $this->Indurations->description = $description; $this->Indurations->save($id); }
					else if ($type == 'confirmation_tests') { $this->Confirmation_Tests->description = $description; $this->Confirmation_Tests->save($id); }
					else if ($type == 'positions') { $this->Positions->description = $description; $this->Positions->save($id); }
					else if ($type == 'departments') { $this->Departments->description = $description; $this->Departments->save($id); }
					else if ($type == 'shifts') { $this->Shifts->description = $description; $this->Shifts->save($id); }
					else if ($type == 'topics') { $this->Topics->description = $description; $this->Topics->save($id); }
					else if ($type == 'medications') { $this->Medications->description = $description; $this->Medications->save($id); }
					else if ($type == 'dosages') { $this->Dosages->description = $description; $this->Dosages->save($id); }
					else if ($type == 'tablets') { $this->Tablets->description = $description; $this->Tablets->save($id); }
					else if ($type == 'frequencies') { $this->Frequencies->description = $description; $this->Frequencies->save($id); }
					else if ($type == 'durations') { $this->Durations->description = $description; $this->Durations->save($id); }
					else if ($type == 'reasons') { $this->Reasons->description = $description; $this->Reasons->save($id); }
					else if ($type == 'status') { $this->Status->description = $description; $this->Status->save($id); }
					else if ($type == 'locations') { $this->Locations->description = $description; $this->Locations->save($id); }
					else if ($type == 'wound_types') { $this->Wound_Types->description = $description; $this->Wound_Types->save($id); }
					else if ($type == 'sites') { $this->Sites->description = $description; $this->Sites->save($id); }
					else if ($type == 'stages') { $this->Stages->order = 0; $this->Stages->description = $description; $this->Stages->save($id); }
					else if ($type == 'exudates') { $this->Exudates->description = $description; $this->Exudates->save($id); }
					else if ($type == 'exudates_amount') { $this->Exudates_Amount->description = $description; $this->Exudates_Amount->save($id); }
					else if ($type == 'odors') { $this->Odors->description = $description; $this->Odors->save($id); }
					else if ($type == 'wound_beds') { $this->Wound_Beds->description = $description; $this->Wound_Beds->save($id); }
					else if ($type == 'peri_wounds') { $this->Peri_Wounds->description = $description; $this->Peri_Wounds->save($id); }
					else if ($type == 'temperatures') { $this->Temperatures->description = $description; $this->Temperatures->save($id); }
					else if ($type == 'allergies') { $this->Allergies->description = $description; $this->Allergies->save($id); }
					else if ($type == 'treatments') { $this->Treatments->description = $description; $this->Treatments->save($id); }
					else if ($type == 'immunizations') { $this->Immunizations->cat_id = $cat_id; $this->Immunizations->description = $description; $this->Immunizations->save($id); }
					else if ($type == 'diagnosis_category') { $this->Diagnosis_Category->description = $description; $this->Diagnosis_Category->infection_control = $infection_control; $this->Diagnosis_Category->save($id); }
					else if ($type == 'floors') { $this->Floors->cat_id = $cat_id; $this->Floors->description = $description; $this->Floors->save($id); }
					else if ($type == 'transmission_precautions_category') { $this->Transmission_Precautions_Category->description = $description; $this->Transmission_Precautions_Category->infection_control = $infection_control; $this->Transmission_Precautions_Category->save($id); }					
					else if ($type == 'immunizations_category') { $this->Immunizations_Category->description = $description; $this->Immunizations_Category->save($id); }
					else if ($type == 'buildings') { $this->Buildings->description = $description;$this->Buildings->alias = $alias; $this->Buildings->save($id); }
					else if ($type == 'ai_agencies') { $this->ai_agencies->description = $description; $this->ai_agencies->save($id); }
					else if ($type == 'ai_area') { $this->ai_area->description = $description; $this->ai_area->save($id); }
					else if ($type == 'ai_conclusions') { $this->ai_conclusions->description = $description; $this->ai_conclusions->save($id); }
					else if ($type == 'ai_kinds') { $this->ai_kinds->description = $description; $this->ai_kinds->save($id); }
					else if ($type == 'ai_interventions') { $this->ai_interventions->description = $description; $this->ai_interventions->save($id); }
					else if ($type == 'ai_preventive_measures') { $this->ai_preventive_measures->description = $description; $this->ai_preventive_measures->save($id); }
					else if ($type == 'risk_factors') { $this->risk_factors->description = $description; $this->risk_factors->save($id); }
					else if ($type == 'dietary_supplements') { $this->dietary_supplements->description = $description; $this->dietary_supplements->save($id); }
					else if ($type == 'materials') { $this->materials->description = $description; $this->materials->save($id); }					
				} 
			}
			
			$arr = array();  
			$arr['success'] = true;
			if ($crudtype == "Add") 
				$arr['data'] = "Successfully Created";
			if ($crudtype == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($crudtype == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function maintenanceview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id = $this->input->post('id');
			$type = $this->input->post('type');
			$category = $this->input->post('category');
			
			if($category == 'diagnosis_category')
				$commandText = "SELECT 
									a.id,
									a.care_plan,
									b.id as cat_id,
									b.description category, 
									a.description
								FROM $type a 
									LEFT JOIN $category b ON a.cat_id = b.id
								WHERE a.id = $id";
			else if($category)
				$commandText = "SELECT 
									a.id,
									b.id as cat_id,
									b.description category, 
									a.description
								FROM $type a 
									LEFT JOIN $category b ON a.cat_id = b.id
								WHERE a.id = $id";
			else
				$commandText = "select * from $type where id = $id";

			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{					
				$record['id'] 			= $value->id;					
				$record['description']	= $value->description;	
				if($type == 'diagnosis')
					$record['care_plan'] = $value->care_plan;	
				if($type == 'diagnosis_category' || $type == 'transmission_precautions_category')
					$record['infection_control'] = $value->infection_control;	
				if($type == 'buildings')
					$record['alias'] = $value->alias;	

				if($category)
				{
					$record['cat_id']	= $value->cat_id;
					$record['category']	= $value->category;
				}
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function maintenancelist(){
		try 
		{
			$query 	= addslashes(strip_tags(trim($_GET['query'])));
			$type 	= $_GET['type'];
			$category 	= $_GET['category'];

			if($category)
			{
				if ($category == 'floors') 
					$category = "(SELECT b.id, CONCAT(alias,'-',b.description) AS description FROM floors b JOIN buildings c ON b.cat_id = c.id)";

				$commandText = "SELECT 
									a.*,
									b.description category, 
									a.description
								FROM $type a 
									LEFT JOIN $category b ON a.cat_id = b.id 
								where a.description like '%$query%' or b.description like '%$query%'
								ORDER BY b.description ASC, a.description ASC";	
			}
			else
				$commandText = "select * from $type where description like '%$query%' order by description asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result();  

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			if($category)
			{
				$data['metaData']['columns'][] = array('dataIndex' => 'id', 'hidden' => true);			
				$data['metaData']['columns'][] = array('text' => 'Category', 'dataIndex' => 'category', 'width' => '30%');
				$data['metaData']['columns'][] = array('text' => strtoupper($type), 'dataIndex' => 'description', 'width' => '69%');

				$data['metaData']['fields'][] = array('name' => 'id', 'type' => 'int');
				$data['metaData']['fields'][] = array('name' => 'category');			
				$data['metaData']['fields'][] = array('name' => 'description');		
			}
			else
			{
				if ($type == 'diagnosis_category' || $type == 'transmission_precautions_category')
				{
					$data['metaData']['columns'][] = array('text' => 'Infection Control?', 'dataIndex' => 'infection_control', 'width' => '30%', 'align' => 'center');
					$data['metaData']['fields'][] = array('name' => 'infection_control');
				}
				if ($type == 'buildings')
				{
					$data['metaData']['columns'][] = array('text' => 'Alias', 'dataIndex' => 'alias', 'width' => '30%', 'align' => 'center');
					$data['metaData']['fields'][] = array('name' => 'alias');
				}
				$data['metaData']['columns'][] = array('dataIndex' => 'id', 'hidden' => true);			
				$data['metaData']['columns'][] = array('text' => strtoupper($type), 'dataIndex' => 'description', 'flex' => 1);
				$data['metaData']['fields'][] = array('name' => 'id', 'type' => 'int');
				$data['metaData']['fields'][] = array('name' => 'description');			
			}


			foreach($query_result as $key => $value) 
			{	
				if($category)
					$data['data'][] = array(
						'id' 			=> $value->id,
						'category' 		=> $value->category,
						'description' 	=> $value->description);
				else
				{
					if ($type == 'diagnosis_category' || $type == 'transmission_precautions_category')
					{
						if($value->infection_control)
							$infection_control = '<font color=green>Yes</font>';
						else 
							$infection_control = '<font color=red>No</font>';
						$data['data'][] = array(
							'id' 			=> $value->id,						
							'infection_control'	=> $infection_control,
							'description' 	=> $value->description);
					}
					else if ($type == 'buildings')
					{
						$data['data'][] = array(
							'id' 			=> $value->id,						
							'alias'			=> $value->alias,
							'description' 	=> $value->description);
					}
					else
					$data['data'][] = array(
						'id' 			=> $value->id,						
						'description' 	=> $value->description);
				}
			}

			$data['count'] = count($query_result);

			die(json_encode($data));

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function caseentrylist()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$type = $_GET['type'];
			$category = $_GET['category'];

			if($type == 'rooms')				
				$commandText = "SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id where (b.description like '%$query%' or a.description like '%$query%' or alias like '%$query%') order by description asc";
			else if($type == 'floors')				
				$commandText = "SELECT b.id, CONCAT(alias,'-',b.description) AS description FROM floors b JOIN buildings c ON b.cat_id = c.id where (b.description like '%$query%' or alias like '%$query%') order by description asc";
			else if($type == 'resident_census')
				$commandText = "SELECT * from $type where description like '%$query%' order by id asc";
			else
				$commandText = "SELECT * from $type where description like '%$query%' order by description asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				if($category)
					$data['data'][] = array(
						'id' 			=> $value->id,
						'cat_id'		=> $value->cat_id,
						'description' 	=> $value->description);
				else
					$data['data'][] = array(
						'id' 			=> $value->id,						
						'description' 	=> $value->description);

			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function residentcrud() 
	{
		try 
		{ 			
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$fname			= strip_tags(trim($this->input->post('fname')));
			$mname			= strip_tags(trim($this->input->post('mname')));
			$lname			= strip_tags(trim($this->input->post('lname')));
			$age			= $this->input->post('age');
			$type			= $this->input->post('type');
			
			if($this->input->post('sex') == '1') $sex='Male'; else $sex='Female';
			$this->load->model('Access'); $this->Access->rights('residents_profile', $type, null);
			if ($type == "Delete")
			{
				$commandText = "SELECT id FROM resident_case_header WHERE resident_id = $id
								UNION 
								SELECT id FROM consult_details WHERE resident_id = $id
								UNION 
								SELECT id FROM immunizations_details WHERE name_type = 'Resident' AND name_id = $id
								UNION
								SELECT id FROM wound_header WHERE resident_id = $id";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 
				
				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Cannot delete record with dependence.");
					die(json_encode($data));
				}

				$commandText = "UPDATE residents set active = 0 where id = $id";
				$result = $this->db->query($commandText);

				$this->load->library('session');		
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('Resident', $id, 'residents', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);

			}
			else
			{				
				if ($type == "Add") 
				{
					$commandText = "select * from residents where fname = '".addslashes($fname)."' and mname = '".addslashes($mname)."' and lname = '".addslashes($lname)."' and active = 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$this->load->model('Residents');
				}
				if ($type == "Edit") 				
				{
					$commandText = "select * from residents where fname = '".addslashes($fname)."' and mname = '".addslashes($mname)."' and lname = '".addslashes($lname)."' and id <> '$id' and active = 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$this->load->model('Residents');
					$this->Residents->id = $id;
				}

				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Resident name already exist.");
					die(json_encode($data));
				}
 
				$this->Residents->fname 	= $fname;
				$this->Residents->mname 	= $mname;
				$this->Residents->lname		= $lname;
				$this->Residents->age		= $age;
				$this->Residents->sex 		= $sex;
				$this->Residents->status 	= $this->input->post('status');
				$this->Residents->active 	= 1;

				if ($type == "Add") $this->Residents->save(0);	
				if ($type == "Edit") $this->Residents->save($id);	

				if ($type == "Add") 
				{
					$commandText = "select id from residents order by id desc limit 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$id = $query_result[0]->id;

					$this->load->model('Residents_Profile');
					$this->Residents_Profile->resident_id 				= $id;
					$this->Residents_Profile->record_no 				= 1;
					$this->Residents_Profile->original_admission_date 	= date('Y-m-d');
					$this->Residents_Profile->latest_admission_date 	= date('Y-m-d');
					$this->Residents_Profile->save(0);	

					$this->load->model('residents_medical_records');
					$this->residents_medical_records->resident_id 		= $id;
					$this->residents_medical_records->record_no 		= 1;
					$this->residents_medical_records->admission_date 	= date('Y-m-d');
					$this->residents_medical_records->discharge_date	= null;
					$this->residents_medical_records->status			= 1;
					$this->residents_medical_records->active			= 1;
					$this->residents_medical_records->save(0);

				}

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'residents', $type, 'Resident');
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}		

	public function residentview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id = $this->input->post('id');
			
			$commandText = "select * from residents where id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				if ($value->sex == 'Male') $sex = 1; else $sex = 2;
				$record['id'] 		= $value->id;					
				$record['fname']	= $value->fname;	
				$record['mname']	= $value->mname;	
				$record['lname']	= $value->lname;	
				$record['age']		= $value->age;	
				$record['sex']		= $sex;
				$record['status']	= $value->status;	
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function staffcrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$fname			= strip_tags(trim($this->input->post('fname')));
			$mname			= strip_tags(trim($this->input->post('mname')));
			$lname			= strip_tags(trim($this->input->post('lname')));
			$position_id	= $this->input->post('positions');
			$department_id	= $this->input->post('departments');
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights('userinformation', $type, null);		
			if ($type == "Delete")
			{
				$commandText = "SELECT id FROM immunizations_details WHERE name_type = 'Staff' AND name_id = $id AND active = 1
								UNION 
								SELECT a.id FROM inservice_details_staff a LEFT JOIN inservice_details b ON a.service_id = b.id WHERE a.staff_id = $id AND b.active = 1";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 
				
				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Cannot delete record with dependence.");
					die(json_encode($data));
				}

				$commandText = "UPDATE staff set active = 0 where id = $id";
				$result = $this->db->query($commandText);

				$this->load->library('session');		
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('Staff', $id, 'staff', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}
			else
			{				
				if ($type == "Add") 
				{
					$commandText = "select * from staff where fname = '".addslashes($fname)."' and mname = '".addslashes($mname)."' and lname = '".addslashes($lname)."'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$this->load->model('staff');
				}
				if ($type == "Edit") 				
				{
					$commandText = "select * from staff where fname = '".addslashes($fname)."' and mname = '".addslashes($mname)."' and lname = '".addslashes($lname)."' and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$this->load->model('staff');
					$this->staff->id = $id;
				}

				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Name already exist.");
					die(json_encode($data));
				}

				$this->staff->fname 	= $fname;
				$this->staff->mname 	= $mname;
				$this->staff->lname		= $lname;
				$this->staff->position_id	= $position_id;
				$this->staff->department_id = $department_id;
				$this->staff->active 	= 1;

				if ($type == "Add") $this->staff->save(0);	
				if ($type == "Edit") $this->staff->save($id);	

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'staff', $type, 'Staff');
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function staffview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id = $this->input->post('id');
			
			$commandText = "SELECT 
								a.id,
								a.fname,
								a.mname,
								a.lname,
								a.position_id,
								a.department_id,
								b.description AS position_desc,
								c.description AS department_desc	
							FROM (staff a 
								LEFT JOIN positions b ON a.position_id = b.id)
								LEFT JOIN departments c ON a.department_id = c.id
							WHERE a.id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['id'] 			= $value->id;					
				$record['fname']		= $value->fname;	
				$record['mname']		= $value->mname;	
				$record['lname']		= $value->lname;	
				$record['position_id']	= $value->position_id;	
				$record['position_desc']= $value->position_desc;
				$record['department_id']= $value->department_id;	
				$record['department_desc']= $value->department_desc;	
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	
	
	public function resident_profilecrud() 
	{
		try 
		{  
			#update session
			$this->load->model('Session');$this->Session->Validate();
			$this->load->model('Access'); $this->Access->rights('residents_profile', 'Edit', null);

			$resident_id = $this->input->post('resident_id');
			$room_id = $this->input->post('rooms');
			$diagnosis = $this->input->post('diagnosis_id');
			$allergies = $this->input->post('allergies_id');
			$independent = $this->input->post('independent');
			$assist = $this->input->post('assist');
			$dependent = $this->input->post('dependent');

			if($this->input->post('latest_discharge_date'))
				$latest_discharge_date = date('Y-m-d',strtotime($this->input->post('latest_discharge_date')));
			else
				$latest_discharge_date = null;

			#fetch record no.
			$commandText = "SELECT record_no FROM residents_medical_records WHERE resident_id = $resident_id AND status = 1 and active = 1";
			$result = $this->db->query($commandText);
			$query_recordno = $result->result();
			$record_no = $query_recordno[0]->record_no;

			$commandText = "SELECT IF(GROUP_CONCAT(id)IS NULL, 0, id) AS id  FROM residents_profile WHERE resident_id = $resident_id AND record_no = $record_no";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$id = $query_result[0]->id;

			if($id == 0)
			{
				$this->load->model('Residents_Profile');
				$this->Residents_Profile->resident_id 				= $id;
				$this->Residents_Profile->original_admission_date 	= date('Y-m-d');
				$this->Residents_Profile->latest_admission_date 	= date('Y-m-d');
				$this->Residents_Profile->save(0);	
				$id = $this->Residents_Profile->id;
			}

			$this->load->model('Residents_Profile');
			$this->Residents_Profile->id 					= $id;			
			$this->Residents_Profile->resident_id 			= $resident_id;
			$this->Residents_Profile->record_no 			= $record_no;		
			$this->Residents_Profile->room_id 				= $room_id;			
			$this->Residents_Profile->medicare				= strip_tags(trim($this->input->post('medicare')));				
			$this->Residents_Profile->medicaid 				= strip_tags(trim($this->input->post('medicaid')));					
			$this->Residents_Profile->original_admission_date = date('Y-m-d',strtotime($this->input->post('original_admission_date')));
			$this->Residents_Profile->latest_admission_date = date('Y-m-d',strtotime($this->input->post('latest_admission_date')));
			$this->Residents_Profile->latest_discharge_date = $latest_discharge_date;
			$this->Residents_Profile->birthdate 			= date('Y-m-d',strtotime($this->input->post('birthdate')));
			$this->Residents_Profile->weight 				= $this->input->post('weight');			
			$this->Residents_Profile->others 				= strip_tags(trim($this->input->post('others')));
			$this->Residents_Profile->save($id);	
			$profile_id = $this->Residents_Profile->id;

			#update age
			$commandText = "UPDATE residents SET age = (SELECT TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) AS age FROM residents_profile WHERE resident_id = $resident_id AND record_no = $record_no) WHERE id = $resident_id";
			$result = $this->db->query($commandText);

			#diagnosis
			$commandText = "DELETE from residents_profile_diagnosis WHERE profile_id = $profile_id";
			$result = $this->db->query($commandText);

			$this->load->model('Residents_Profile_Diagnosis');
			$diagnosis=explode(',',$diagnosis);
			foreach($diagnosis as $key => $value) 
			{
				$this->Residents_Profile_Diagnosis->profile_id	= $profile_id;
				$this->Residents_Profile_Diagnosis->diagnose_id = $value;
				$this->Residents_Profile_Diagnosis->save(0);	

				$commandText = "SELECT * FROM residents_profile_care_plan WHERE profile_id = $profile_id AND diagnose_id = $value AND created_by = 0";	
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 

				if(count($query_result) == 0) 
				{			
					$commandText = "SELECT * FROM residents_profile_care_plan_defaults WHERE diagnosis_id = $value";	
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					foreach($query_result as $key => $val) 
					{	
						$this->load->model('residents_profile_care_plan');
						$this->residents_profile_care_plan->profile_id 				= $profile_id;
						$this->residents_profile_care_plan->diagnose_id 			= $value;
						$this->residents_profile_care_plan->remarks 				= $val->remarks;
						$this->residents_profile_care_plan->date 					= date('Y-m-d');
						$this->residents_profile_care_plan->created_by				= 0;
						$this->residents_profile_care_plan->date_created			= date('Y-m-d');
						$this->residents_profile_care_plan->type					= $val->type;
						$this->residents_profile_care_plan->status					= 1;
						$this->residents_profile_care_plan->save(0);
						
					}			
				}
			}

			#allergies
			$commandText = "DELETE from residents_profile_allergies WHERE profile_id = $profile_id";
			$result = $this->db->query($commandText);

			$this->load->model('Residents_Profile_Allergies');
			$allergies=explode(',',$allergies);
			foreach($allergies as $key => $value) 
			{
				$this->Residents_Profile_Allergies->profile_id	= $profile_id;
				$this->Residents_Profile_Allergies->allergy_id = $value;
				$this->Residents_Profile_Allergies->save(0);	
			}

			#adl
			$commandText = "delete from residents_profile_adl where profile_id = $profile_id";
			$result = $this->db->query($commandText);
			
			$this->load->model('residents_profile_adl');			
			$independent=explode(',',$independent);
			$assist=explode(',',$assist);
			$dependent=explode(',',$dependent);			

			for ($i=0; $i < count($independent); $i++) 
			{ 
				$this->residents_profile_adl->profile_id		= $profile_id;
				$this->residents_profile_adl->adl_id			= $i+1;
				$this->residents_profile_adl->independent 		= null;
				$this->residents_profile_adl->assist 			= null;
				$this->residents_profile_adl->dependent 		= null;
				
				if($independent[$i] == 1) 
					$this->residents_profile_adl->independent 	= 1;
				if($assist[$i] == 1) 
					$this->residents_profile_adl->assist 		= 1;
				if($dependent[$i] == 1) 
					$this->residents_profile_adl->dependent 	= 1;
				$this->residents_profile_adl->save(0);					
			}

			#census			
			$commandText = "delete from resident_profile_census where profile_id = $profile_id AND census_id IN (".$this->input->post('active_checkbox').")";
			$result = $this->db->query($commandText);	

			$this->load->model('resident_profile_census');
			for ($i=1; $i <= 52; $i++) 
			{ 
				$this->resident_profile_census->profile_id	= $profile_id;
				$this->resident_profile_census->census_id = $i;
				if($this->input->post('panelCheckBox'.$i))
					$this->resident_profile_census->save(0);					
			}		
			

 			$this->load->model('Logs'); $this->Logs->audit_logs($profile_id, 'residents_profile', 'Update', 'Resident Profile');

			$arr = array();  
			$arr['success'] = true;
			$arr['data'] = "Successfully Updated";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function resident_profileview()
	{ 
		try 
		{			
			$resident_id = $this->input->post('resident_id');

			die(json_encode($this->generateresident_profileview($resident_id)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateresident_profileview($resident_id)
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$commandText = "SELECT record_no FROM residents_medical_records WHERE resident_id = $resident_id AND status = 1 and active = 1";
			$result = $this->db->query($commandText);
			$query_medical = $result->result();

			if(count($query_medical) == 0)
			{
				$data = array("success"=> true, "data"=>'NO RECORD');
				die(json_encode($data));	
			}
			$record_no = $query_medical[0]->record_no;

			$record = array();
			$profile_id = 0;
			$commandText = "SELECT IF(COUNT(src) = 0, 'pic1.png', src) as src FROM residents_image WHERE resident_id = $resident_id ORDER BY id DESC LIMIT 1";
			$result = $this->db->query($commandText);
			$query_image = $result->result();
			$record['image'] = $query_image[0]->src;

			$commandText = "SELECT 	
								a.*,
								b.description as room_desc
							FROM residents_profile a 								
								LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
							WHERE a.resident_id = $resident_id
								AND record_no = $record_no";
								// print_r($commandText);die();
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$profile_id					= $value->id; 
				$record['room_id'] 			= $value->room_id;
				$record['room_desc'] 		= $value->room_desc;
				$record['medicare']			= $value->medicare;
				$record['medicaid'] 		= $value->medicaid;
				$record['original_admission_date'] = $value->original_admission_date;
				$record['latest_admission_date'] = $value->latest_admission_date;
				$record['latest_discharge_date'] = $value->latest_discharge_date;
				$record['birthdate'] 		= $value->birthdate;
				$record['weight'] 			= $value->weight;
				$record['others']			= $value->others;
			}

			#diagnosis
			$commandText = "SELECT 
								diagnose_id,
								b.description
							FROM residents_profile_diagnosis a
								LEFT JOIN diagnosis b ON a.diagnose_id = b.id
							WHERE a.profile_id = $profile_id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$data['diagnose_id'][] = $value->diagnose_id;
				$data['diagnose_desc'][] = $value->description;
			}
			$data['diagnose_count'] = count($query_result);

			#allergies
			$commandText = "SELECT 
								allergy_id,
								b.description	
							FROM residents_profile_allergies a
								LEFT JOIN allergies b ON a.allergy_id = b.id
							WHERE a.profile_id = $profile_id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$data['allergy_id'][] = $value->allergy_id;
				$data['allergy_desc'][] = $value->description;
			}
			$data['allergy_count'] = count($query_result);

			#adl
			$commandText = "SELECT 
								b.description,
								b.independent AS findependent,
								b.assist AS fassist,
								b.dependent AS fdependent,
								a.independent,
								a.assist,
								a.dependent
							FROM adl b
								LEFT JOIN (SELECT * FROM residents_profile_adl WHERE profile_id = $profile_id) a ON a.adl_id = b.id
							ORDER BY a.id ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$data['description'][] 	= $value->description;
				$data['independent'][] 	= $value->independent;
				$data['assist'][] 		= $value->assist;
				$data['dependent'][] 	= $value->dependent;
				$data['findependent'][] = $value->findependent;
				$data['fassist'][] 		= $value->fassist;
				$data['fdependent'][] 	= $value->fdependent;
			}
			$data['adl_count'] = count($query_result);

			#census
			$commandText = "SELECT 
								a.*,
								IF(b.census_id IS NOT NULL, 1, 0) AS census
							FROM resident_census a 
								LEFT JOIN (SELECT * FROM resident_profile_census WHERE profile_id = $profile_id) b ON a.id = b.census_id
							ORDER BY a.id ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$data['census_description'][] = $value->description;
				$data['census_type'][] = $value->type;
				$data['census'][] = $value->census;				
			}
			$data['census_count'] = count($query_result);

			$data['data'] = $record;
			$data['success'] = true;
			
			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function staffname()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));

			$commandText = "SELECT id, CONCAT(lname,', ',fname) as description FROM staff where lname like '%$query%' or fname like '%$query%' order by lname asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,						
					'description' 	=> strtoupper($value->description));
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function residentname()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));

			$commandText = "SELECT id, CONCAT(lname,', ',fname) as description FROM residents where lname like '%$query%' or fname like '%$query%' order by lname asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,						
					'description' 	=> strtoupper($value->description));
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{
		$this->load->model('Session');$this->Session->Validate();
		
		$id		= $this->input->post('id');
		$name	= $this->input->post('name');

		$response = array();
        $response['success'] = true;
 
        // $this->load->model('Logs'); $this->Logs->audit_logs($id, 'residents_profile', 'Report', 'Resident Profile');
        $commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values ($id, 'Resident Profile', 'residents_profile', ".$this->session->userdata('id').", 'Report', '".date('Y-m-d')."', '".date('H:i:s')."')";
		$result = $this->db->query($commandText);

    	$response['filename'] = $this->exportpdfResidentProfile($this->generateresident_profileview($id), $name);
		die(json_encode($response));
	}

	public function exportpdfResidentProfile($data, $name)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$pdf->SetFont('freeserif', '', 12, '', false);
			$fDate = date("Ymd_His"); 
			$filename = "ResidentProfile".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ResidentProfile');
			$pdf->SetSubject('ResidentProfile');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
			
			if($data['data']['image'])
			{
				list($txt, $ext) = explode(".", $data['data']['image']);
				$pdf->Image('profile_pic/'.$data['data']['image'], 10, 20, 30, 30, $ext, null, '', true, 300, '', false, false, 0, false, false, false);
			}
			else 
				$pdf->Image('profile_pic/pic1.png', 10, 20, 30, 30, 'PNG', null, '', true, 300, '', false, false, 0, false, false, false);

			$image = '&#x2713;';

			if($data['data']['original_admission_date'])
				$latest_discharge_date = date('m/d/Y',strtotime($data['data']['original_admission_date']));
			else $latest_discharge_date = null;

			$html = $name.' Demographic <br><br>

			<table>
				<tr>
					  <td width="20%"></td>
					  <td width="80%">
						<table>
							<tr style="font-size:24px;">
							  <td width="25%" align="left"><b>General Diagnosis </b></td>
							  <td width="1%" align="left"><b>:</b></td>
							  <td width="79%" align="left">'.implode(', ',$data['diagnose_desc']).'</td>
							</tr>
							<tr style="font-size:24px;">
							  <td align="left"><b>Allergies</b></td>
							  <td align="left"><b>:</b></td>
							  <td align="left">'.implode(', ',$data['allergy_desc']).'</td>
							</tr>
							<tr style="font-size:24px;">
							  <td align="left"><b>Room</b></td>
							  <td align="left"><b>:</b></td>
							  <td align="left">'.$data['data']['room_desc'].'</td>
							</tr>
							<tr style="font-size:24px;">
							  <td align="left"><b>Original Admission Date</b></td>
							  <td align="left"><b>:</b></td>
							  <td align="left">'.date('m/d/Y',strtotime($data['data']['original_admission_date'])).'</td>
							</tr>
							<tr style="font-size:24px;">
							  <td align="left"><b>Latest Admission Date</b></td>
							  <td align="left"><b>:</b></td>
							  <td align="left">'.date('m/d/Y',strtotime($data['data']['latest_admission_date'])).'</td>
							</tr>
							<tr style="font-size:24px;">
							  <td align="left"><b>Latest Discharge Date</b></td>
							  <td align="left"><b>:</b></td>
							  <td align="left">'.$latest_discharge_date.'</td>
							</tr>
							<tr style="font-size:24px;">
							  <td align="left"><b>Birthdate</b></td>
							  <td align="left"><b>:</b></td>
							  <td align="left">'.date('m/d/Y',strtotime($data['data']['birthdate'])).'</td>
							</tr>
							<tr style="font-size:24px;">
							  <td align="left"><b>Weight (lbs.)</b></td>
							  <td align="left"><b>:</b></td>
							  <td align="left">'.$data['data']['weight'].'</td>
							</tr>
						</table>
					  </td>
			  	</tr>
			</table>

			<div style="font-size:24px;">
				DEPARTMENT OF HEALTH AND HUMAN SERVICES<br>
				CENTERS FOR MEDICARE & MEDICAID SERVICES
			</div>

			<table cellpadding="2">
				<tr style="font-size:40px;">
				  <td colspan="5" border="1" align="center">RESIDENT CENSUS AND CONDITIONS OF RESIDENT</td>
				</tr>

				<tr style="font-size:24px;">
				  <td border="1">Provider No.<br>...</td>
				  <td border="1">Medicare (F75)<br>'.$data['data']['medicare'].'</td>
				  <td border="1">Medicaid (F76)<br>'.$data['data']['medicaid'].'</td>
				  <td border="1">Other (F77)<br>'.$data['data']['others'].'</td>
				  <td border="1">Total Residents (F78)<br>...</td>
				</tr>

				<tr style="font-size:24px;">
				  <td width="20%" border="1" colspan="2"><b>ADL</b></td>
				  <td width="27%" border="1"><b>Independent</b></td>
				  <td width="27%" border="1"><b>Assist of One or Two Staff</b></td>
				  <td width="26%" border="1"><b>Dependent</b></td>
				</tr>';

			for ($i=0; $i < $data['adl_count']; $i++) 
			{ 
				$independent = "";
				if($data['independent'][$i]) $independent = $image;

				$assist = "";
				if($data['assist'][$i]) $assist = $image;

				$dependent = "";
				if($data['dependent'][$i]) $dependent = $image;

				$html .='
					<tr style="font-size:24px;">
					  <td width="20%" border="1" colspan="2">'.$data['description'][$i].' </td>
					  <td width="27%" border="1">'.$data['findependent'][$i].' '.$independent.'</td>
					  <td width="27%" border="1">'.$data['fassist'][$i].' '.$assist.'</td>
					  <td width="26%" border="1">'.$data['fdependent'][$i].' '.$dependent.'</td>
					</tr>';
			}

			$html .='
			</table>

			<table cellpadding="4">
				<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>A. Bowel/Bladder Status</b><br></td>
							</tr>';

						$censusCount = 0;
						while($data['census_type'][$censusCount] == 'A')
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>B. Mobility</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'B')
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
			  	</tr>

			  	<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>C. Mental Status<br>F108-114 - indicate the number of residents with:</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'C')
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>D. Skin Integrity<br>F115-118 - indicate the number of residents with:</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'D')
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
			  	</tr>

			  	<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>E. Special Care<br>F119-132 - indicate the number of residents receiving:</b><br></td>
							</tr>';
						
						$censusCountTemp = $censusCount;
						$x = 0;
						while($data['census_type'][$censusCountTemp] == 'E'){
							$x++; $censusCountTemp++;
						}
						$x /=2; 
						
						while($data['census_type'][$censusCount] == 'E' & $x>0)
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
							$x--;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'E')
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
			  	</tr>

			  	<tr><td></td><td></td></tr>
			  	<tr><td></td><td></td></tr>
			  	
			  	<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>F. Medications<br>F133-139 - indicate the number of residents receiving:</b><br></td>
							</tr>';
						
						while($data['census_type'][$censusCount] == 'F')
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>G. Other</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'G')
						{
							$census = "";
							if($data['census'][$censusCount]) $census = $image;

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
							if($censusCount == $data['census_count']) break;
						}
						
						$html .='</table>
					  </td>
			  	</tr>

			</table>

			<table cellpadding="4">
				<tr style="font-size:24px;">
					<td border="1" width="50%">Signature of Person Completing the Form<br><br><br></td>
					<td border="1" width="30%">Title</td>
					<td border="1" width="20%">Date</td>
				</tr>
			</table>

			<div style="font-size:30px;">TO BE COMPLETED BY SURVEY TEAM</div>

			<table>
				<tr style="font-size:24px;">
					<td width="5%">F146</td>
					<td width="50%">Was ombudsman office notified prior to survey?</td>
					<td width="10%">___ YES</td>
					<td width="10%">___ NO</td>
				</tr>
				<tr style="font-size:24px;">
					<td>F147</td>
					<td>Was ombudsman present during any portion of the survey?</td>
					<td>___ YES</td>
					<td>___ NO</td>
				</tr>
				<tr style="font-size:24px;">
					<td>F148</td>
					<td>Medication error rate _______%</td>
					<td>___ YES</td>
					<td>___ NO</td>
				</tr>
			</table>

			<br><br><br>';

			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}