<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consults extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'consults';
		else 
			return 'Consultation';
	} 

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));
	}	
  
 	public function namelist()
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$status = $_GET['status'];

			if($status == 1) $status = '';
			else if($status == 2) $status = ' status = true and ';
			else $status = ' status = false and ';

			$commandText = "SELECT * 
							FROM residents 
							WHERE 
								$status
								(fname like '%$query%' or 
							    mname like '%$query%' or
							    lname like '%$query%')
							AND active = 1
							ORDER BY lname ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				if($value->sex == "Male") $extension = "<font color=blue size=1.5> (Male), ".$value->age."yrs.</font>";
				else $extension = "<font color=green size=1.5> (Female), ".$value->age."yrs.</font>";

				$names[] = array(
					'id' 	=> $value->id,
					'name' 	=> strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname).$extension,
					'text' 	=> strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname).$extension,
					'leaf' 	=> true);
			}

			$record = array();
			$record['id'] 		= 0;
			$record['text'] 	= 'Names';
			$record['cls'] 		= 'folder';
			$record['expanded'] = true;
			$record['children'] = $names;

			$data = array();
			$data[] = $record;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function consultscrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id			= $this->input->post('id');
			$resident_id= $this->input->post('resident_id');
			$date		= $this->input->post('date');
			$time		= $this->input->post('time');
			$room_id	= $this->input->post('rooms');
			$consult_id	= $this->input->post('consults');
			$reason_id	= $this->input->post('reasons');
			$location_id= $this->input->post('locations');
			$status_id	= $this->input->post('status');
			$materials	= $this->input->post('materials');
			$transportation_status	= $this->input->post('transportation_status');
			$others		= strip_tags(trim($this->input->post('others')));
			$accompanied_by	= strip_tags(trim($this->input->post('accompanied_by')));
			$type		= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "UPDATE consult_details set active = 0 where id = $id";
				$result = $this->db->query($commandText);

				$this->load->library('session');		
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('".$this->modulename('Label')."', $id, 'consult_details', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$this->load->model('Consult_Details');
				if ($type == "Add") 
					$id = 0;
				if ($type == "Edit") 				
					$this->Consult_Details->id = $id;

				$this->Consult_Details->resident_id = $resident_id;
				$this->Consult_Details->room_id 	= $room_id;
				$this->Consult_Details->consult_id 	= $consult_id;
				$this->Consult_Details->reason_id	= $reason_id;
				$this->Consult_Details->location_id	= $location_id;
				$this->Consult_Details->status_id 	= $status_id;
				$this->Consult_Details->user_id 	= $this->session->userdata('id');
				$this->Consult_Details->transportation_status = $transportation_status;
				$this->Consult_Details->date 		= date('Y-m-d',strtotime($date));
				$this->Consult_Details->time 		= $time;
				$this->Consult_Details->others 		= $others;				
				$this->Consult_Details->date_entry	= date('Y-m-d H:i:s');
				$this->Consult_Details->accompanied_by	= $accompanied_by;
				$this->Consult_Details->active		= 1;
				$this->Consult_Details->save($id);	

				if ($type == "Add") 
				{
					$commandText = "select id from consult_details order by id desc limit 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$consult_id = $query_result[0]->id;	
				}
				else
					$consult_id = $id;

				#consult_materials
				$commandText = "delete from consult_materials where consult_id = $consult_id";
				$result = $this->db->query($commandText);

				$this->load->model('consult_materials');
				$materials=explode(',',$materials);
				foreach($materials as $key => $value) 
				{
					$this->consult_materials->consult_id	= $consult_id;
					$this->consult_materials->material_id 	= $value;
					$this->consult_materials->save(0);	
				}

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'consult_details', $type, $this->modulename('Label'));
			}
			
			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function serviceview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');
			$resident_id = $this->input->post('resident_id');
			$type = $this->input->post('type');
			
			if($type == 'Edit')
				$commandText = "SELECT 	
									a.id,	
									a.date,
									a.time,
									a.others,
									a.transportation_status,
									a.accompanied_by,
									c.id AS room_id, 
									d.id AS consult_id, 
									e.id AS reason_id, 
									f.id AS location_id, 
									g.id AS status_id, 
									h.material_id,
									c.description AS room_desc,
									d.description AS consult_desc,
									e.description AS reason_desc,
									f.description AS location_desc,
									g.description AS status_desc,
									h.material_desc
								FROM consult_details a	
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c ON a.room_id = c.id
									LEFT JOIN consults d ON a.consult_id = d.id
									LEFT JOIN reasons e ON a.reason_id = e.id
									LEFT JOIN locations f ON a.location_id = f.id
									LEFT JOIN status g ON a.status_id = g.id
									LEFT JOIN (SELECT 
											a.consult_id,
											GROUP_CONCAT(material_id) material_id,
											GROUP_CONCAT(b.description) material_desc	
									   FROM consult_materials a
										JOIN materials b ON a.material_id = b.id
									   WHERE a.consult_id = $id) h ON a.id = h.consult_id
								WHERE a.id = $id";
			else 
				$commandText = "SELECT 
									a.room_id,
									b.description room_desc
								FROM residents_profile a 
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
								WHERE a.resident_id = $resident_id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				if($type == 'Edit')
				{
					$record['id'] 			= $value->id;					
					$record['date']			= $value->date;	
					$record['time']			= $value->time;	
					$record['others']		= $value->others;	
					$record['transportation_status'] = $value->transportation_status;
					$record['accompanied_by'] = $value->accompanied_by;					
					$record['consult_id']	= $value->consult_id;	
					$record['reason_id']	= $value->reason_id;	
					$record['location_id']	= $value->location_id;	
					$record['status_id']	= $value->status_id;	
					$record['material_id']	= $value->material_id;	
					$record['consult_desc']	= $value->consult_desc;
					$record['reason_desc']	= $value->reason_desc;
					$record['location_desc']= $value->location_desc;
					$record['status_desc']	= $value->status_desc;					
					$record['material_desc']= $value->material_desc;					
				}
				$record['room_id']		= $value->room_id;	
				$record['room_desc']	= $value->room_desc;
			}

			$commandText = "SELECT medicare, medicaid FROM residents_profile WHERE resident_id = $resident_id";
			$result = $this->db->query($commandText);
			$query_result = $result->result();
			foreach($query_result as $key => $value) 
			{
				$record['medicare']	= $value->medicare;
				$record['medicaid']		= $value->medicaid;
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function consultlist()
	{ 
		try 
		{			
			$query 		= addslashes(strip_tags(trim($_GET['query'])));
			$resident_id = $_GET['resident_id'];
			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));
			$type 		= $_GET['type'];

			die(json_encode($this->generateconsultlist($query, $resident_id, $date_from, $date_to, $type, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateconsultlist($query, $resident_id, $date_from, $date_to, $type, $transaction_type){
		try 
		{	
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$dateCondition = "";
			if($type == 'Name') 
			{
				$type = ' a.resident_id = '.$resident_id.' and ';
				$dateCondition = " 
								AND a.date >= (SELECT admission_date FROM residents_medical_records WHERE resident_id = ".$resident_id." AND STATUS = 1 ORDER BY admission_date ASC LIMIT 1)							
								AND a.date <= (SELECT IF(discharge_date = NULL, discharge_date, ADDDATE(CURDATE(), 180)) AS discharge_date FROM residents_medical_records WHERE resident_id = ".$resident_id." AND STATUS = 1 ORDER BY discharge_date ASC LIMIT 1)";
			}
			else $type = null;

			$commandText = "SELECT 	
								a.id,	
								a.date,
								a.time,								
								a.others,
								a.transportation_status,
								a.date_entry,
								CONCAT (b.lname, ', ', b.fname, ' ', b.mname) resident_name,
								c.description AS room,
								d.description AS consult,
								e.description AS reason,
								f.description AS location,
								g.description AS status,
								i.lname,
								i.fname,
								i.mname
							FROM consult_details a
								LEFT JOIN residents b ON a.resident_id = b.id
								LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c ON a.room_id = c.id
								LEFT JOIN consults d ON a.consult_id = d.id
								LEFT JOIN reasons e ON a.reason_id = e.id
								LEFT JOIN locations f ON a.location_id = f.id
								LEFT JOIN status g ON a.status_id = g.id
								LEFT JOIN users h on a.user_id = h.id
								LEFT JOIN staff i on h.staff_id = i.id
							WHERE $type
								(a.date between '$date_from' and '$date_to') and 
								(
									c.description like '%$query%' or
									d.description like '%$query%' or
									e.description like '%$query%' or
									f.description like '%$query%' or
									g.description like '%$query%' or
									transportation_status like '%$query%'
								)
								AND a.active = 1
								$dateCondition
							ORDER BY b.lname ASC, b.fname ASC, a.date ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result();  			

			if($type)
			{
				$data['metaData']['columns'][] = array('dataIndex' => 'id', 'hidden' => true);
				$data['metaData']['columns'][] = array('text' => 'Rm #', 'dataIndex' => 'room', 'width' => '15%');
				$data['metaData']['columns'][] = array('text' => 'Date', 'dataIndex' => 'date', 'width' => '10%');				
				$data['metaData']['columns'][] = array('text' => 'Time', 'dataIndex' => 'time', 'width' => '9%', 'align' => 'right');
				$data['metaData']['columns'][] = array('dataIndex' => 'name', 'hidden' => true);
				$data['metaData']['columns'][] = array('text' => 'Consult', 'dataIndex' => 'consult', 'width' => '13%');
				$data['metaData']['columns'][] = array('text' => 'Reason', 'dataIndex' => 'reason', 'width' => '22%');
				$data['metaData']['columns'][] = array('text' => 'Location', 'dataIndex' => 'location', 'width' => '9%');
				$data['metaData']['columns'][] = array('text' => 'Transport Status', 'dataIndex' => 'transportation_status', 'width' => '15%');				
				$data['metaData']['columns'][] = array('text' => 'Other Instructions', 'dataIndex' => 'others', 'width' => '15%');			
				$data['metaData']['columns'][] = array('text' => 'Status', 'dataIndex' => 'status', 'width' => '12%');				
				$data['metaData']['columns'][] = array('text' => 'Input Details (Time/Inputted by)', 'dataIndex' => 'date_entry', 'width' => '35%');
			}
			else
			{
				$data['metaData']['columns'][] = array('dataIndex' => 'id', 'hidden' => true);
				$data['metaData']['columns'][] = array('text' => 'Rm #', 'dataIndex' => 'room', 'width' => '12%');
				$data['metaData']['columns'][] = array('text' => 'Resident Name', 'dataIndex' => 'name', 'width' => '18%');
				$data['metaData']['columns'][] = array('text' => 'Date', 'dataIndex' => 'date', 'width' => '8%');				
				$data['metaData']['columns'][] = array('text' => 'Time', 'dataIndex' => 'time', 'width' => '7%', 'align' => 'right');
				$data['metaData']['columns'][] = array('text' => 'Consult', 'dataIndex' => 'consult', 'width' => '9%');
				$data['metaData']['columns'][] = array('text' => 'Reason', 'dataIndex' => 'reason', 'width' => '16%');
				$data['metaData']['columns'][] = array('text' => 'Location', 'dataIndex' => 'location', 'width' => '7%');
				$data['metaData']['columns'][] = array('text' => 'Transport Status', 'dataIndex' => 'transportation_status', 'width' => '11%');				
				$data['metaData']['columns'][] = array('text' => 'Other Instructions', 'dataIndex' => 'others', 'width' => '11%');			
				$data['metaData']['columns'][] = array('text' => 'Status', 'dataIndex' => 'status', 'width' => '9%');				
				$data['metaData']['columns'][] = array('text' => 'Input Details (Time/Inputted by)', 'dataIndex' => 'date_entry', 'width' => '25%');
				}			

			$data['metaData']['fields'][] = array('name' => 'id', 'type' => 'int');
			$data['metaData']['fields'][] = array('name' => 'date');
			$data['metaData']['fields'][] = array('name' => 'date_entry');
			$data['metaData']['fields'][] = array('name' => 'time');
			$data['metaData']['fields'][] = array('name' => 'room');
			$data['metaData']['fields'][] = array('name' => 'name');
			$data['metaData']['fields'][] = array('name' => 'consult');
			$data['metaData']['fields'][] = array('name' => 'reason');
			$data['metaData']['fields'][] = array('name' => 'location');
			$data['metaData']['fields'][] = array('name' => 'others');
			$data['metaData']['fields'][] = array('name' => 'status');
			$data['metaData']['fields'][] = array('name' => 'transportation_status');

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				if($value->status == 'Completed') $status = "<font color='green'><b>Completed</b></font>";
				else if($value->status == 'Pending') $status = "<font color='red'>Pending</font>";
				else if($value->status == 'Cancelled') $status = "<font color='gray'>Cancelled</font>";
				else $status = $value->status;

				if($value->transportation_status == 'Pending') $transportation_status = "<font color='red'>Pending</font>";
				else $transportation_status = "<font color='green'>".$value->transportation_status."</font>";

				$data['data'][] = array(
					'id' 		=> $value->id,
					'date' 		=> date('m/d/Y',strtotime($value->date)),
					'date_entry'=> date('m/d/Y h:i:sa',strtotime($value->date_entry)).' by:'.strtoupper($value->lname).'.'.strtoupper($value->fname[0]),
					'time' 		=> $value->time,
					'room' 		=> $value->room,
					'name' 		=> strtoupper($value->resident_name),
					'consult' 	=> $value->consult,
					'reason' 	=> $value->reason,
					'location' 	=> $value->location,
					'others' 	=> $value->others,
					'status' 	=> $status,
					'ostatus' 	=> $value->status,
					'otransportation_status' => $value->transportation_status,
				'transportation_status' => $transportation_status);
			}

			$data['count'] = count($query_result);

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{
		$this->load->model('Session');$this->Session->Validate();

		$query 		= addslashes(strip_tags(trim($this->input->post('query'))));
		$resident_id = $this->input->post('resident_id');
		$rName = $this->input->post('rName');		
		$date_from 	= date('Y-m-d',strtotime($this->input->post('date_from')));
		$date_to 	= date('Y-m-d',strtotime($this->input->post('date_to')));
		$type 		= $this->input->post('type');
		$filetype 	= $this->input->post('filetype');

		$response = array();
        $response['success'] = true;

        if($filetype == "PDF")
        	$response['filename'] = $this->exportpdfConsultList($this->generateconsultlist($query, $resident_id, $date_from, $date_to, $type, 'Report'), $query, $date_from, $date_to);
        else         	
			$response['filename'] = $this->exportexcelConsultList($this->generateconsultlist($query, $resident_id, $date_from, $date_to, $type, 'Report'), $query, $date_from, $date_to);

		if($type == 'Name')
        {
        	// $this->load->model('Logs'); $this->Logs->audit_logs(0, 'immunizations', 'Report-'.$filetype, $this->modulename('Label').' ('.$rName.' From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');
        	$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')." (Resident: ".$rName." From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'consult', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
        }
		else
		{
		    // $this->load->model('Logs'); $this->Logs->audit_logs(0, 'immunizations', 'Report-'.$filetype, $this->modulename('Label').'-All (From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');    
		    $commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')."-All (From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'consult', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
		}

		die(json_encode($response));
	}

	public function exportexcelConsultList($data, $query, $date_from, $date_to)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ConsultList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ConsultList")
					      ->setSubject("Report")
					      ->setDescription("Generating ConsultList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B9:U10')->getFont()->setBold(true);

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'Consultation Details');
			###DATE

			if(!$query) $query = 'NULL';

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Searched by:")		
					      ->setCellValue("B6", "Date From:")
					      ->setCellValue("B7", "Date To:")
					      ->setCellValue("C5", $query)
					      ->setCellValue("C6", date('m/d/Y',strtotime($date_from)))
					      ->setCellValue("C7", date('m/d/Y',strtotime($date_to)))
					      ->setCellValue("B9", "Room No.")
					      ->setCellValue("C9", "Date")
					      ->setCellValue("D9", "Time")
					      ->setCellValue("E9", "Consult")
					      ->setCellValue("F9", "Reason")
					      ->setCellValue("G9", "Location")
					      ->setCellValue("H9", "Status")
					      ->setCellValue("I9", "Transport Status")
					      ->setCellValue("J9", "Others");

			$initialValue = 11;
			$name = '';

			for ($i = 0; $i<$data['count'];$i++)
			{
				if($data['data'][$i]['name'] != $name)
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+$initialValue), $data['data'][$i]['name']);
					$name = $data['data'][$i]['name'];
					$initialValue++;
				}

				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+$initialValue), $data['data'][$i]['room'])
						      ->setCellValue("C".($i+$initialValue), $data['data'][$i]['date'])
						      ->setCellValue("D".($i+$initialValue), $data['data'][$i]['time'])
						      ->setCellValue("E".($i+$initialValue), $data['data'][$i]['consult'])
						      ->setCellValue("F".($i+$initialValue), $data['data'][$i]['reason'])
						      ->setCellValue("G".($i+$initialValue), $data['data'][$i]['location'])
						      ->setCellValue("H".($i+$initialValue), $data['data'][$i]['ostatus'])
						      ->setCellValue("I".($i+$initialValue), $data['data'][$i]['otransportation_status'])
						      ->setCellValue("J".($i+$initialValue), $data['data'][$i]['others']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($initialValue+$i+8), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($initialValue+$i+9), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfConsultList($data, $query, $date_from, $date_to)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ConsultList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ConsultList');
			$pdf->SetSubject('ConsultList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('L', 'LETTER');

			if(!$query) $query = 'NULL';

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Consultation Details</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>			

					<table>
						<tr style="font-size:24px;">
						  <td width="10%">Searched by</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$query.'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date From</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_from)).'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date To</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_to)).'</td>
						</tr>
					</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:24px;">
					  <td width="10%" style="padding: 10px;" align="left"><b>Room</b></td>
					  <td width="8%" style="padding: 10px;" align="left"><b>Date</b></td>
					  <td width="8%" style="padding: 10px;" align="left"><b>Time</b></td>
					  <td width="10%" style="padding: 10px;" align="left"><b>Consult</b></td>
					  <td width="15%" style="padding: 10px;" align="left"><b>Reason</b></td>
					  <td width="10%" style="padding: 10px;" align="left"><b>Location</b></td>
					  <td width="8%" style="padding: 10px;" align="left"><b>Status</b></td>
					  <td width="10%" style="padding: 10px;" align="left"><b>Transport Status</b></td>
					  <td width="20%" style="padding: 10px;" align="left"><b>Others</b></td>
					</tr>';

			$name = '';
			for ($i = 0; $i<$data['count'];$i++)
			{
				if($data['data'][$i]['name'] != $name)
				{
					$html .= '<tr style="font-size:24px;">
						  <td colspan="9" style="padding: 10px;" align="left"><b>'.$data['data'][$i]['name'].'</b></td>
						</tr>';
					$name = $data['data'][$i]['name'];
				}

				$html .= '<tr style="font-size:24px;">
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['room'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['date'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['time'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['consult'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['reason'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['location'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['status'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['transportation_status'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['others'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}