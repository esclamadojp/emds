<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class exam_courses_resources extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'exam_courses_resources';
		else 
			return 'Exam Courses Resources';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));  
	}	
}