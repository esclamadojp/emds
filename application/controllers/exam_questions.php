<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exam_Questions extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'exam_questions';
		else 
			return 'Exam Questions';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));  
	}	
}