<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exams extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'exams';
		else 
			return 'Exams';
	} 

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));
	}

	public function examlist()
	{ 
		try 
		{						
			die(json_encode($this->generateexamlist(addslashes(strip_tags(trim($_GET['query']))))));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateexamlist($query)
	{	
		#update session
		$this->load->model('Session');$this->Session->Validate();

		$limit = $_GET['limit'];
		$start = $_GET['start'];
		$limitQuery = " LIMIT $start, $limit";
		
		$commandText = "SELECT 
							a.*,
							b.description,
							IF(a.raw_score IS NULL, NULL, CONCAT(a.raw_score, ' / ', a.total_score)) AS score
						FROM exam_items a
							JOIN exam_courses b ON a.course_id = b.id
						ORDER BY date_posted DESC
						$limitQuery";		
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		$commandText = "SELECT count(a.id) AS count
						FROM exam_items a
							JOIN exam_courses b ON a.course_id = b.id
						ORDER BY date_posted DESC";
		$result = $this->db->query($commandText);
		$query_count = $result->result(); 

		if(count($query_result) == 0) 
		{
			$data["totalCount"] = 0;
			$data["data"] 		= array();
			die(json_encode($data));
		}	

		foreach($query_result as $key => $value) 
		{	
			if($value->status == "Scheduled")
				$status = '<b>'.$value->status.'</b>';
			if($value->status == "Active")
				$status = '<font color=blue>'.$value->status.'</font>';
			if($value->status == "Passed")
				$status = '<font color=green>'.$value->status.'</font>';
			if($value->status == "Failed")
				$status = '<font color=red>'.$value->status.'</font>';

			$data['data'][] = array(
				'id' 		=> $value->id,
				'course' 	=> $value->description,
				'items' 	=> $value->items,
				'date_active' 	=> date('m/d/Y',strtotime($value->active_from)).' - '.date('m/d/Y',strtotime($value->active_to)),
				'date_posted' 	=> date('m/d/Y',strtotime($value->date_posted)),
				'date_taken' 	=> date('m/d/Y',strtotime($value->date_taken)),
				'score' 	=> $value->score,
				'mystatus' 	=> $status,
				'status' 	=> $value->status);
		}			

		$data['totalCount'] = $query_count[0]->count;
		return $data;
	}
}