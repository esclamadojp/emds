<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Immunization extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'immunization';
		else 
			return 'Immunization';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));  
	}	 

	public function namelist()
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$query = addslashes(strip_tags(trim($_GET['query'])));
			$type = $_GET['type'];

			if($type == 'Resident') $table = ' residents WHERE status = true and';
			else $table = ' staff WHERE ';
			$commandText = "SELECT * FROM 
							$table
								(fname like '%$query%' or 
							    mname like '%$query%' or
							    lname like '%$query%')
							AND active = 1							
							ORDER BY lname ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				if($type == 'Resident'){
					if($value->sex == "Male") $extension = "<font color=blue size=1.5> (Male), ".$value->age."yrs.</font>";
					else $extension = "<font color=green size=1.5> (Female), ".$value->age."yrs.</font>";
				}
				else $extension = "";

				$names[] = array(
					'id' 	=> $value->id,
					'name' 	=> strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname).$extension,
					'text' 	=> strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname).$extension,
					'leaf' 	=> true);
			}

			$record = array();
			$record['id'] 		= 0;
			$record['text'] 	= 'Names';
			$record['cls'] 		= 'folder';
			$record['expanded'] = true;
			$record['children'] = $names;

			$data = array();
			$data[] = $record;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function immunizationlist()
	{ 
		try 
		{			
			$query 		= addslashes(strip_tags(trim($_GET['query'])));
			$name_id 	= $_GET['name_id'];
			$name_type 	= $_GET['name_type'];
			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));			
			$type 		= $_GET['type'];

			die(json_encode($this->generateimmunizationlist($query, $name_id, $name_type, $date_from, $date_to, $type, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateimmunizationlist($query, $name_id, $name_type, $date_from, $date_to, $type, $transaction_type)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$dateCondition = "";
			if($name_type == 'Resident' & $type == 'Name')
				$dateCondition = " 
								AND a.date >= (SELECT admission_date FROM residents_medical_records WHERE resident_id = ".$name_id." AND STATUS = 1 ORDER BY admission_date ASC LIMIT 1)							
								AND a.date <= (SELECT IF(discharge_date = NULL, discharge_date, ADDDATE(CURDATE(), 180)) AS discharge_date FROM residents_medical_records WHERE resident_id = ".$name_id." AND STATUS = 1 ORDER BY discharge_date ASC LIMIT 1)";
			
			if($name_type == 'Resident')
				$columnName = "(SELECT CONCAT (lname, ', ', fname, ' ', mname) name FROM residents WHERE id = a.name_id) as name";
			else
				$columnName = "(SELECT CONCAT (lname, ', ', fname, ' ', mname) NAME FROM staff WHERE id = a.name_id) as name";
			if($type == 'Name') $type = ' a.name_id = '.$name_id.' and ';
			else $type = null;

			$commandText = "SELECT 
								a.id,
								a.date,
								a.administered,
								a.result,
								a.brand,
								a.expiration_date,
								a.confirmation_date,
								a.confirmation_result,
								a.titer_result,
								a.name_type,
								b.description AS rooms_desc,
								c.description AS immunization_desc,
								d.description AS induration_desc,
								e.description AS administered_site_desc,
								f.description AS confirmation_test_desc,
								g.description AS reaction_desc,
								h.description AS intervention_desc,
								CONCAT(i.lname,', ',i.fname) AS administered_name,
								CONCAT(j.lname,', ',j.fname) AS read_name,
								$columnName
							FROM immunizations_details a 
								LEFT JOIN (SELECT a.id, CONCAT(alias,'-',b.description,'-',a.description) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
								LEFT JOIN immunizations c ON a.immunization_id = c.id
								LEFT JOIN indurations d ON a.induration_id = d.id
								LEFT JOIN administered_sites e ON a.administered_site_id = e.id
								LEFT JOIN confirmation_tests f ON a.confirmation_test_id = f.id
								LEFT JOIN reactions g ON a.reaction_id = g.id
								LEFT JOIN interventions h ON a.intervention_id = h.id
								LEFT JOIN staff i ON a.administered_by = i.id
								LEFT JOIN staff j ON a.read_by = j.id
							WHERE $type
								a.name_type = '$name_type'
								AND (a.date between '$date_from' and '$date_to') 
								AND 
								(
									b.description like '%$query%' or
									c.description like '%$query%' or
									d.description like '%$query%' or
									e.description like '%$query%' or
									f.description like '%$query%' or
									g.description like '%$query%' or
									h.description like '%$query%'
								)
								AND a.active = 1	
								$dateCondition							
							ORDER BY a.date ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result();  			

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				if($value->confirmation_result == 'Pending')
					$confirmation_result = '<font color=red><b>Pending</b></font>';
				else $confirmation_result = $value->confirmation_result;
				
				$html = '<table>';
				if(!$type)
				if($value->name){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>'.$value->name_type.' Name</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.strtoupper($value->name).'<b></font></td>';
					$html .= '</tr>';
				}
				if ($name_type == "Resident" & $value->administered == "Inhouse") 
				if($value->rooms_desc){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Room</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->rooms_desc.'<b></font></td>';
					$html .= '</tr>';
				}
				if($value->administered_name){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Administered By</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.strtoupper($value->administered_name).'<b></font></td>';
					$html .= '</tr>';
				}
				if($value->brand){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Brand</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->brand.'</font></td>';
					$html .= '</tr>';
				}
				if($value->expiration_date){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Expiration Date</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.date('m/d/Y',strtotime($value->expiration_date)).'</font></td>';
					$html .= '</tr>';
				}
				if($value->result){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>48-72hrs. Result</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->result.'<b></font></td>';
					$html .= '</tr>';
				}
				if($value->induration_desc){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Induration</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->induration_desc.'</font></td>';
					$html .= '</tr>';
				}
				if($value->read_name){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Read By</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.strtoupper($value->read_name).'<b></font></td>';
					$html .= '</tr>';
				}
				if($value->confirmation_test_desc){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Confirmation Test</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->confirmation_test_desc.'</font></td>';
					$html .= '</tr>';
				}
				if($value->confirmation_date){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Confirmation Date</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.date('m/d/Y',strtotime($value->confirmation_date)).'</font></td>';
					$html .= '</tr>';
				}
				if($value->confirmation_result){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Confirmation Result</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$confirmation_result.'</font></td>';
					$html .= '</tr>';
				}																
				if($value->administered_site_desc){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Site Administered</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->administered_site_desc.'</font></td>';
					$html .= '</tr>';
				}				
				if($value->reaction_desc){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Reaction</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->reaction_desc.'</font></td>';
					$html .= '</tr>';
				}
				if($value->intervention_desc){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Intervention</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->intervention_desc.'</font></td>';
					$html .= '</tr>';
				}
				if($value->titer_result){
					$html .= '<tr>';
					$html .= '<td valign="top"><font size=2><b>Titer Result</b></font></td>';
					$html .= '<td valign="top"><font size=2><b>:</b></font></td>';
					$html .= '<td valign="top"><font size=2>'.$value->titer_result.'</font></td>';
					$html .= '</tr>';
				}

				$html .= '</table>';

				$data['data'][] = array(
					'id' 			=> $value->id,
					'date' 			=> date('m/d/Y',strtotime($value->date)),	
					'immunization_desc'	=> $value->immunization_desc,
					'administered' => $value->administered,
					'html' 			=> $html,
					'name_type'		=> $value->name_type,
					'name' 			=> $value->name,
					'rooms_desc' 	=> $value->rooms_desc,
					'result' 		=> $value->result,
					'induration_desc' => $value->induration_desc,
					'confirmation_test_desc' => $value->confirmation_test_desc,
					'confirmation_date' => $value->confirmation_date,
					'confirmation_result' => $value->confirmation_result,
					'brand' 		=> $value->brand,
					'expiration_date' => $value->expiration_date,
					'administered_site_desc' => $value->administered_site_desc,
					'administered_name' => $value->administered_name,
					'read_name' 	=> $value->read_name,
					'reaction_desc' => $value->reaction_desc,
					'intervention_desc' => $value->intervention_desc,
					'titer_result' 	=> $value->titer_result);
			}

			$data['count'] = count($query_result);

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function immunizationcrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$immunization_cat = $this->input->post('immunization_cat');
			$name_id		= $this->input->post('name_id');
			$rooms			= $this->input->post('rooms');
			$immunizations	= $this->input->post('immunizations');
			$indurations	= $this->input->post('indurations');
			$administered_sites	= $this->input->post('administered_sites');
			$confirmation_tests	= $this->input->post('confirmation_tests');
			$reactions		= $this->input->post('reactions');
			$interventions	= $this->input->post('interventions');
			$administered_by= $this->input->post('administered_by');
			$read_by		= $this->input->post('read_by');
			$name_type		= $this->input->post('name_type');
			$date			= date('Y-m-d',strtotime($this->input->post('date')));
			$administered	= $this->input->post('administered');
			$result			= $this->input->post('result');
			$brand			= strip_tags(trim($this->input->post('brand')));
			$expiration_date = $this->input->post('expiration_date');
			$confirmation_date = $this->input->post('confirmation_date');
			$confirmation_result = $this->input->post('confirmation_result');
			$titer_result	= strip_tags(trim($this->input->post('titer_result')));
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "UPDATE immunizations_details set active = 0 where id = $id";
				$result = $this->db->query($commandText);
				
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('".$this->modulename('Label').'('.$name_type.')'."', $id, 'immunizations_details', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$this->load->model('Immunizations_Details');
				if ($type == "Add") 
					$id = 0;
				if ($type == "Edit") 				
					$this->Immunizations_Details->id = $id;

				if($this->input->post('expiration_date')) $expiration_date = date('Y-m-d',strtotime($this->input->post('expiration_date')));
				else $expiration_date = null;

				if($this->input->post('confirmation_date')) $confirmation_date = date('Y-m-d',strtotime($this->input->post('confirmation_date')));
				else $confirmation_date = null;

				$this->Immunizations_Details->name_id 				= $name_id;

				if ($name_type == "Resident") 
					$this->Immunizations_Details->room_id 			= $rooms;

				$this->Immunizations_Details->immunization_id 		= $immunizations;
				$this->Immunizations_Details->induration_id 		= $indurations;
				$this->Immunizations_Details->administered_site_id 	= $administered_sites;
				$this->Immunizations_Details->confirmation_test_id 	= $confirmation_tests;
				$this->Immunizations_Details->reaction_id 			= $reactions;
				$this->Immunizations_Details->intervention_id 		= $interventions;
				$this->Immunizations_Details->administered_by 		= $administered_by;
				$this->Immunizations_Details->read_by 				= $read_by;
				$this->Immunizations_Details->name_type 			= $name_type;
				$this->Immunizations_Details->date 					= $date;
				$this->Immunizations_Details->administered 			= $administered;
				$this->Immunizations_Details->result 				= $result;
				$this->Immunizations_Details->brand 				= $brand;
				$this->Immunizations_Details->expiration_date 		= $expiration_date;
				$this->Immunizations_Details->confirmation_date 	= $confirmation_date;
				$this->Immunizations_Details->confirmation_result 	= $confirmation_result;
				$this->Immunizations_Details->titer_result 			= $titer_result;
				$this->Immunizations_Details->active 				= 1;
				$this->Immunizations_Details->save($id);	

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'immunizations_details', $type, $this->modulename('Label').'('.$name_type.')');
			}
						
			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function immunizationview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');
			$name_id = $this->input->post('name_id');
			$name_type = $this->input->post('name_type');
			$type = $this->input->post('type');
			
			if($type == 'Edit'){

				if($name_type == 'Resident')
					$commandText = "SELECT 
										a.id,
										a.date,
										a.administered,
										a.result,
										a.brand,
										a.expiration_date,
										a.confirmation_date,
										a.confirmation_result,
										a.titer_result,
										a.name_type,
										a.room_id,
										a.immunization_id,
										a.induration_id,
										a.administered_site_id,
										a.confirmation_test_id,
										a.reaction_id,
										a.intervention_id,
										a.administered_by AS administered_by_id,
										a.read_by AS read_by_id,
										c.cat_id,
										b.description AS room_desc,
										c.description AS immunization_desc,
										d.description AS induration_desc,
										e.description AS administered_site_desc,
										f.description AS confirmation_test_desc,
										g.description AS reaction_desc,
										h.description AS intervention_desc,
										CONCAT(i.lname,', ',i.fname) AS administered_name,
										CONCAT(j.lname,', ',j.fname) AS read_name
									FROM immunizations_details a 
										LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
										LEFT JOIN immunizations c ON a.immunization_id = c.id
										LEFT JOIN indurations d ON a.induration_id = d.id
										LEFT JOIN administered_sites e ON a.administered_site_id = e.id
										LEFT JOIN confirmation_tests f ON a.confirmation_test_id = f.id
										LEFT JOIN reactions g ON a.reaction_id = g.id
										LEFT JOIN interventions h ON a.intervention_id = h.id
										LEFT JOIN staff i ON a.administered_by = i.id
										LEFT JOIN staff j ON a.read_by = j.id
									WHERE a.id = $id";
				else
					$commandText = "SELECT 
										a.id,
										a.date,
										a.administered,
										a.result,
										a.brand,
										a.expiration_date,
										a.confirmation_date,
										a.confirmation_result,
										a.titer_result,
										a.name_type,								
										a.immunization_id,
										a.induration_id,
										a.administered_site_id,
										a.confirmation_test_id,
										a.reaction_id,
										a.intervention_id,
										a.administered_by AS administered_by_id,
										a.read_by AS read_by_id,
										c.cat_id,
										b.description AS department,
										c.description AS immunization_desc,
										d.description AS induration_desc,
										e.description AS administered_site_desc,
										f.description AS confirmation_test_desc,
										g.description AS reaction_desc,
										h.description AS intervention_desc,
										CONCAT(i.lname,', ',i.fname) AS administered_name,
										CONCAT(j.lname,', ',j.fname) AS read_name
									FROM immunizations_details a 
										LEFT JOIN staff a1 ON a1.id = a.name_id
										LEFT JOIN departments b ON a1.department_id = b.id
										LEFT JOIN immunizations c ON a.immunization_id = c.id
										LEFT JOIN indurations d ON a.induration_id = d.id
										LEFT JOIN administered_sites e ON a.administered_site_id = e.id
										LEFT JOIN confirmation_tests f ON a.confirmation_test_id = f.id
										LEFT JOIN reactions g ON a.reaction_id = g.id
										LEFT JOIN interventions h ON a.intervention_id = h.id
										LEFT JOIN staff i ON a.administered_by = i.id
										LEFT JOIN staff j ON a.read_by = j.id
									WHERE a.id = $id";			
			}
			else 
			{
				if($name_type == 'Resident')
					$commandText = "SELECT 
										a.room_id,
										b.description room_desc
									FROM residents_profile a JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
									WHERE a.resident_id = $name_id";
				else 
					$commandText = "SELECT b.description AS department
									FROM staff a JOIN departments b ON a.department_id = b.id
									WHERE a.id = $name_id";
			}
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				if($type == 'Edit')
				{
					$record['id'] 				= $value->id;					
					$record['date']				= $value->date;
					$record['administered']		= $value->administered;
					$record['result']			= $value->result;
					$record['brand']			= $value->brand;
					$record['expiration_date']	= $value->expiration_date;
					$record['confirmation_date'] = $value->confirmation_date;
					$record['confirmation_result'] = $value->confirmation_result;
					$record['titer_result']		= $value->titer_result;
					$record['name_type']		= $value->name_type;
					$record['immunization_id']	= $value->immunization_id;
					$record['induration_id']	= $value->induration_id;
					$record['administered_site_id']	= $value->administered_site_id;
					$record['confirmation_test_id']	= $value->confirmation_test_id;
					$record['reaction_id']		= $value->reaction_id;
					$record['intervention_id']	= $value->intervention_id;
					$record['administered_by_id']= $value->administered_by_id;
					$record['read_by_id']		= $value->read_by_id;
					$record['immunization_desc']= $value->immunization_desc;
					$record['induration_desc']	= $value->induration_desc;
					$record['administered_site_desc'] = $value->administered_site_desc;
					$record['confirmation_test_desc'] = $value->confirmation_test_desc;
					$record['reaction_desc']	= $value->reaction_desc;
					$record['intervention_desc']= $value->intervention_desc;
					$record['administered_name']= strtoupper($value->administered_name);
					$record['read_name']		= strtoupper($value->read_name);
					$record['cat_id']			= $value->cat_id;
				}
				if($name_type == 'Resident'){
					$record['room_id']		= $value->room_id;	
					$record['room_desc']	= $value->room_desc;
				}
				else
					$record['department']	= $value->department;	
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{		
		$this->load->model('Session');$this->Session->Validate();

		$query 		= addslashes(strip_tags(trim($this->input->post('query'))));
		$name_id 	= $this->input->post('name_id');
		$rsname 	= $this->input->post('rsname');
		$name_type 	= $this->input->post('name_type');
		$date_from 	= date('Y-m-d',strtotime($this->input->post('date_from')));
		$date_to 	= date('Y-m-d',strtotime($this->input->post('date_to')));
		$type 		= $this->input->post('type');
		$filetype 	= $this->input->post('filetype');

		$response = array();
        $response['success'] = true;

        if($filetype == "PDF")
        	$response['filename'] = $this->exportpdfImmunizationList($this->generateimmunizationlist($query, $name_id, $name_type, $date_from, $date_to, $type, 'Report'), $query, $date_from, $date_to, $type);
        else         	
			$response['filename'] = $this->exportexcelImmunizationList($this->generateimmunizationlist($query, $name_id, $name_type, $date_from, $date_to, $type, 'Report'), $query, $date_from, $date_to, $type);

        if($type == 'Name')
        {
        	// $this->load->model('Logs'); $this->Logs->audit_logs(0, 'immunizations', 'Report-'.$filetype, $this->modulename('Label').' ('.$name_type.': '.$rsname.' From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');
        	$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')."  (".$name_type.": ".$rsname." From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'immunizations', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
        }
		else
		{
		    // $this->load->model('Logs'); $this->Logs->audit_logs(0, 'immunizations', 'Report-'.$filetype, $this->modulename('Label').'-All '.$name_type.' (From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');    
		    $commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')."-All  ".$name_type." (From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'immunizations', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
		}

		die(json_encode($response));
	}

	public function exportexcelImmunizationList($data, $query, $date_from, $date_to, $type)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ImmunizationList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ImmunizationList")
					      ->setSubject("Report")
					      ->setDescription("Generating ImmunizationList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B10:D10')->getFont()->setBold(true);

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'Immunization Details');
			###DATE

			if(!$query) $query = 'NULL';

			if($type != 'All')
				$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B8", $data['data'][0]['name_type'])		
					      ->setCellValue("C8", strtoupper($data['data'][0]['name']));

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Searched by:")		
					      ->setCellValue("B6", "Date From:")
					      ->setCellValue("B7", "Date To:")
					      ->setCellValue("C5", $query)
					      ->setCellValue("C6", date('m/d/Y',strtotime($date_from)))
					      ->setCellValue("C7", date('m/d/Y',strtotime($date_to)))
					      ->setCellValue("B10", "Date")
					      ->setCellValue("C10", "Immunization")
					      ->setCellValue("D10", "Administered");

			$initialValue = 11;

			for ($i = 0; $i<$data['count'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+$initialValue), $data['data'][$i]['date'])
						      ->setCellValue("C".($i+$initialValue), $data['data'][$i]['immunization_desc'])
						      ->setCellValue("D".($i+$initialValue), $data['data'][$i]['administered']);

				if($type == 'All')
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), $data['data'][$i]['name_type'].': '.strtoupper($data['data'][$i]['name']));
				}
				if ($data['data'][$i]['name_type'] == "Resident" & $data['data'][$i]['administered'] == "Inhouse") 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Room: '.$data['data'][$i]['rooms_desc']);
				}
				if ($data['data'][$i]['result']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), '48-72hrs. Result: '.$data['data'][$i]['result']);
				}
				if ($data['data'][$i]['induration_desc']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Induration: '.$data['data'][$i]['induration_desc']);
				}
				if ($data['data'][$i]['confirmation_test_desc']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Confirmation Test: '.$data['data'][$i]['confirmation_test_desc']);
				}
				if ($data['data'][$i]['confirmation_date']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Confirmation Date: '.$data['data'][$i]['confirmation_date']);
				}
				if ($data['data'][$i]['confirmation_result']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Confirmation Result: '.$data['data'][$i]['confirmation_result']);
				}
				if ($data['data'][$i]['brand']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Brand: '.$data['data'][$i]['brand']);
				}
				if ($data['data'][$i]['expiration_date']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Expiration Date: '.$data['data'][$i]['expiration_date']);
				}
				if ($data['data'][$i]['administered_site_desc']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Site Administered: '.$data['data'][$i]['administered_site_desc']);
				}
				if ($data['data'][$i]['administered_name']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Administered By: '.strtoupper($data['data'][$i]['administered_name']));
				}
				if ($data['data'][$i]['reaction_desc']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Reaction: '.$data['data'][$i]['reaction_desc']);
				}
				if ($data['data'][$i]['intervention_desc']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Intervention: '.$data['data'][$i]['intervention_desc']);
				}
				if ($data['data'][$i]['titer_result']) 
				{
					$initialValue++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".($i+$initialValue), 'Titer Result: '.$data['data'][$i]['titer_result']);
				}
				$initialValue++;
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($initialValue+$i+8), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($initialValue+$i+9), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfImmunizationList($data, $query, $date_from, $date_to, $type)
	{
		try 
		{			
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ImmunizationList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ImmunizationList');
			$pdf->SetSubject('ImmunizationList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');

			if(!$query) $query = 'NULL';

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Immunization Details</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table><br><br>			

					<table>
						<tr style="font-size:24px;">
						  <td width="10%">Searched by</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$query.'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date From</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_from)).'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date To</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_to)).'</td>
						</tr>';
			
			if($type != 'All')
			$html .= '<tr style="font-size:24px;">
						  <td >'.$data['data'][0]['name_type'].'</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][0]['name']).'</td>
						</tr>';

			$html .= '</table>

					<br>

					<table>
					<tr style="font-size:24px;">
					  <td width="10%" style="padding: 10px;" align="left"><b>Date</b></td>
					  <td width="30%" style="padding: 10px;" align="left"><b>Immunization</b></td>
					  <td width="30%" style="padding: 10px;" align="left"><b>Administered</b></td>
					</tr>';

			for ($i = 0; $i<$data['count'];$i++)
			{
				$html .= '<tr style="font-size:24px;">
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['date'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['immunization_desc'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['administered'].'</td>
					</tr>';

				$html .= '<tr style="font-size:24px;">
					  <td style="padding: 10px;" align="left"></td>
					  <td colspan="2" style="padding: 10px;" align="left"><table>';

				$html .='<tr style="font-size:24px;">
						  <td width="25%" style="padding: 10px;" align="left"></td>
						  <td width="2%" style="padding: 10px;" align="left"></td>
						  <td width="73%" style="padding: 10px;" align="left"></td>
						</tr>';

				if($type == 'All')
				$html .='<tr style="font-size:24px;">
						  <td >'.$data['data'][$i]['name_type'].'</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['name']).'</td>
						</tr>';

				if($data['data'][$i]['name_type'] == "Resident" & $data['data'][$i]['administered'] == "Inhouse")
				$html .='<tr style="font-size:24px;">
						  <td >Room</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['rooms_desc']).'</td>
						</tr>';

				if ($data['data'][$i]['result']) 
				$html .='<tr style="font-size:24px;">
						  <td >48-72hrs. Result</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['result']).'</td>
						</tr>';

				if ($data['data'][$i]['induration_desc']) 
				$html .='<tr style="font-size:24px;">
						  <td >Induration</td>
						  <td >:</td>
						  <td >'.htmlspecialchars($data['data'][$i]['induration_desc']).'</td>
						</tr>';

				if ($data['data'][$i]['confirmation_test_desc']) 
				$html .='<tr style="font-size:24px;">
						  <td >Confirmation Test</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['confirmation_test_desc']).'</td>
						</tr>';

				if ($data['data'][$i]['confirmation_date']) 
				$html .='<tr style="font-size:24px;">
						  <td >Confirmation Date</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['confirmation_date']).'</td>
						</tr>';

				if ($data['data'][$i]['confirmation_result']) 
				$html .='<tr style="font-size:24px;">
						  <td >Confirmation Result</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['confirmation_result']).'</td>
						</tr>';

				if ($data['data'][$i]['brand']) 
				$html .='<tr style="font-size:24px;">
						  <td >Brand</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['brand']).'</td>
						</tr>';

				if ($data['data'][$i]['expiration_date']) 
				$html .='<tr style="font-size:24px;">
						  <td >Expiration Date</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['expiration_date']).'</td>
						</tr>';

				if ($data['data'][$i]['administered_site_desc']) 
				$html .='<tr style="font-size:24px;">
						  <td >Site Administered</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['administered_site_desc']).'</td>
						</tr>';

				if ($data['data'][$i]['administered_name']) 
				$html .='<tr style="font-size:24px;">
						  <td >Administered By</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['administered_name']).'</td>
						</tr>';

				if ($data['data'][$i]['reaction_desc']) 
				$html .='<tr style="font-size:24px;">
						  <td >Reaction</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['reaction_desc']).'</td>
						</tr>';

				if ($data['data'][$i]['intervention_desc']) 
				$html .='<tr style="font-size:24px;">
						  <td >Intervention</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['intervention_desc']).'</td>
						</tr>';

				if ($data['data'][$i]['titer_result']) 
				$html .='<tr style="font-size:24px;">
						  <td >Titer Result</td>
						  <td >:</td>
						  <td >'.strtoupper($data['data'][$i]['titer_result']).'</td>
						</tr>';

			  	$html .= '</table></td></tr><br>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}