<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Infection_Control extends CI_Controller {
	/**
	*/	
	private function modulename($type)
	{		
		if($type == 'link')
			return 'infection_control';
		else 
			return 'Infection Control';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));  
	}	

	public function residentcaseList()
	{ 
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$status = $_GET['status'];

			die(json_encode($this->generateresidentcaseList($query, $status, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateresidentcaseList($query, $status, $transaction_type)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$limitQuery = "";
			if($transaction_type == 'Grid')
			{
				$limit = $_GET['limit'];
				$start = $_GET['start'];
				$limitQuery = " LIMIT $start, $limit";
			}

			if($status == 1) $status = '';
			else if($status == 2) $status = ' a.status = true and ';
			else $status = ' a.status = false and ';

			$commandText = "SELECT 
								a.*, 
								c.description room,
								c.details
								FROM residents a 
									LEFT JOIN (SELECT a.* FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1) b on a.id = b.resident_id
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description, CONCAT(c.description,'-',b.description,'-',a.description) AS details FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c on b.room_id = c.id
							WHERE
							    $status (a.fname like '%$query%' or 
							    		 a.mname like '%$query%' or
							    		 a.lname like '%$query%' or
							    		 a.age like '%$query%' or
							    		 a.sex like '%$query%' or
							    		 c.description like '%$query%') AND active = 1 
							ORDER BY lname ASC
							$limitQuery";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$commandText = "SELECT count(*) as count
							FROM residents a 
								LEFT JOIN (SELECT a.* FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1) b on a.id = b.resident_id
								LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description, CONCAT(c.description,'-',b.description,'-',a.description) AS details FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c on b.room_id = c.id
							WHERE
							    $status (a.fname like '%$query%' or 
							    		 a.mname like '%$query%' or
							    		 a.lname like '%$query%' or
							    		 a.age like '%$query%' or
							    		 a.sex like '%$query%' or
							    		 c.description like '%$query%') AND active = 1";
			$result = $this->db->query($commandText);
			$query_count = $result->result(); 

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,
					'name' 			=> strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname),
					'age' 			=> $value->age,
					'room' 			=> $value->room,
					'details'		=> $value->details,
					'sex' 			=> $value->sex);
			}

			$data['totalCount'] = $query_count[0]->count;
			return $data;

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function residentcasegrid()
	{
		try 
		{
			$id = $_GET['id'];
			die(json_encode($this->generateresidentcasegrid($id, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateresidentcasegrid($id, $transaction_type)
	{
		try 
		{			
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$commandText = "SELECT 
								a.id,
								a.date AS hdate,
								a.date_resolved,
								b.description AS room,
								c.description AS infection,
								(SELECT GROUP_CONCAT(di.description) FROM resident_case_diagnosis rcd LEFT JOIN diagnosis di ON rcd.diagnose_id = di.id  WHERE header_case_id = a.id) diagnosis,
								(SELECT 	
									GROUP_CONCAT(b.description)
								FROM resident_case_sign_symtoms a LEFT JOIN signs_symptoms b ON a.ss_id = b.id
								WHERE a.details_case_id = d.id) AS symptoms,	
								(SELECT 
									GROUP_CONCAT(CONCAT(b1.description, ' ', c1.description, ' ', d1.description, ' ',e1.description, ', ', f1.description, g1.description) SEPARATOR '<br>')
								FROM resident_case_medications a1 
									LEFT JOIN medications b1 ON a1.medication_id = b1.id
									LEFT JOIN dosages c1 ON a1.dosage_id = c1.id
									LEFT JOIN tablets d1 ON a1.tablet_id = d1.id
									LEFT JOIN routes e1 ON a1.route_id = e1.id
									LEFT JOIN frequencies f1 ON a1.frequency_id = f1.id
									LEFT JOIN durations g1 ON a1.duration_id = g1.id	
								WHERE a1.details_case_id = d.id 
								ORDER BY a1.id ASC) AS medication,
								d.id AS detail_id,
								d.date,
								d.lab_report,
								d.microorganism,								
								d.case_type,
								d.other,
								e.description AS conditions,
								f.description AS physician,
								g.description AS precaution,
								i.description AS test,
								j.description AS consults
							FROM resident_case_header a 
								LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',c.description) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
								LEFT JOIN infections c ON a.infection_id = c.id
								LEFT JOIN resident_case_details d ON a.id = d.header_case_id
								LEFT JOIN conditions e ON d.condition_id = e.id
								LEFT JOIN physicians f ON d.physician_id = f.id
								LEFT JOIN transmission_precautions g ON d.precaution_id = g.id
								LEFT JOIN tests i ON d.test_id = i.id
								LEFT JOIN consults j ON d.consult_id = j.id
							WHERE a.resident_id = $id 
								AND a.active = 1 
								AND d.active = 1
								AND a.date >= (SELECT admission_date FROM residents_medical_records WHERE resident_id = $id AND STATUS = 1 ORDER BY admission_date ASC LIMIT 1)							
								AND a.date <= (SELECT IF(discharge_date = NULL, discharge_date, ADDDATE(CURDATE(), 180)) AS discharge_date FROM residents_medical_records WHERE resident_id = $id AND STATUS = 1 ORDER BY discharge_date ASC LIMIT 1)
							ORDER BY a.date DESC, d.date ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			$xCount = 1;
			foreach($query_result as $key => $value) 
			{	
				if($value->date_resolved)
					$date_resolved = '<font color=green><b> Resolved Date: '.date("m/d/Y",strtotime($value->date_resolved)).'</b><font>';
				else
					$date_resolved = '';
				if($value->case_type == 'NEW') {
					$title = $xCount.'.) <b>'.strtoupper($value->diagnosis).'</b> - '.$value->infection;
					$xCount++;
					$casetype = '<font color=red><b>NEW CASE</b><font>'.$date_resolved; 
				}
				else $casetype = 'Current / Same Case';

				$html = '<table>';
				if($value->case_type == 'NEW'){
					$html .= '<tr>
						<td valign="top"><font size=2><b>Diagnosis</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->diagnosis.'</font></td>
					</tr>
					<tr>
						<td><font size=2><b>Room</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->room.'</font></td>
					</tr>';
				}
				if($value->symptoms){
					$html .= '<tr>
						<td valign="top"><font size=2><b>Signs/Symptoms</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->symptoms.'</font></td>
					</tr>';
				}
				if($value->lab_report){
					$html .= '<tr>
						<td><font size=2><b>Lab Report</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.date('m/d/Y',strtotime($value->lab_report)).'</font></td>
					</tr>';
				}
				if($value->test){
					$html .= '<tr>
						<td><font size=2><b>Test</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->test.'</font></td>
					</tr>';
				}
				if($value->microorganism){
					$html .= '<tr>
						<td valign="top"><font size=2><b>Microorganism</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->microorganism.'</font></td>
					</tr>';
				}
				if($value->medication){
					$html .= '<tr>
						<td valign="top"><font size=2><b>Medication</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->medication.'</font></td>
					</tr>';
				}
				if($value->conditions){
					$html .= '<tr>
						<td><font size=2><b>Condition</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->conditions.'</font></td>
					</tr>';
				}
				if($value->precaution){
					$html .= '<tr>
						<td><font size=2><b>Precaution</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->precaution.'</font></td>
					</tr>';
				}
				if($value->consults){
					$html .= '<tr>
						<td><font size=2><b>Consult</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->consults.'</font></td>
					</tr>';
				}
				if($value->physician){
					$html .= '<tr>
						<td><font size=2><b>Physician</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->physician.'</font></td>
					</tr>';
				}
				if($value->other){
					$html .= '<tr>
						<td valign="top"><font size=2><b>Other</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$value->other.'</font></td>
					</tr>';
				}
				$html .= '</table>';


				$data['data'][] = array(
					'id' 			=> $value->detail_id,
					'title' 		=> $title,
					'diagnosis' 	=> $value->diagnosis,
					'infection' 	=> $value->infection,
					'room' 			=> $value->room,
					'symptoms' 		=> $value->symptoms,
					'lab_report' 	=> $value->lab_report,
					'test' 			=> $value->test,
					'microorganism' => $value->microorganism,
					'medication' 	=> $value->medication,
					'conditions' 	=> $value->conditions,
					'precaution' 	=> $value->precaution,
					'consults' 		=> $value->consults,
					'physician' 	=> $value->physician,
					'other' 		=> $value->other,
					'date_resolved' => $value->date_resolved,
					'date' 			=> date('m/d/Y',strtotime($value->date)),
					'case_type' 	=> $casetype,
					'case_type_orig'=> $value->case_type,					
					'html' 			=> $html);
			}

			$data['totalCount'] = count($query_result);

			return $data;			
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function crud(){
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$room_id		= $this->input->post('room_no');
			$date			= $this->input->post('date');
			$case_type_id	= $this->input->post('case_type_id');
			$resident_id	= $this->input->post('resident_id');
			$infection_id	= $this->input->post('infections');
			$condition_id	= $this->input->post('conditions');
			$physician_id	= $this->input->post('physicians');
			$precaution_id	= $this->input->post('transmission_precautions');
			$test_id		= $this->input->post('tests');
			$consult_id		= $this->input->post('consults');			
			$lab_report		= $this->input->post('lab_report');
			$microorganism	= strip_tags(trim($this->input->post('microorganism')));			
			$other			= strip_tags(trim($this->input->post('other')));
			$case_type		= $this->input->post('case_type');
			$diagnosis		= $this->input->post('diagnosis');
			$signs_symptoms	= $this->input->post('signs_symptoms');					
			$date_resolved	= $this->input->post('date_resolved');
			$type			= $this->input->post('type');

			if ($case_type == 1) $case_type = 'NEW'; else $case_type = 'Current/Same Case';
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "SELECT b.* FROM resident_case_details a JOIN resident_case_header b ON a.header_case_id = b.id WHERE a.id = $id AND b.date_resolved IS NULL";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 
				if(count($query_result) == 0) 
				{
					$data = array("success"=> false, "data"=>"Cannot delete resolved case record.");
					die(json_encode($data));
				}

				$commandText = "SELECT a.header_case_id as id, a.case_type FROM resident_case_details a WHERE a.id = $id";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 
				$header_case_id = $query_result[0]->id;
				$case_type = $query_result[0]->case_type;

				if ($case_type == 'NEW')
				{
					$commandText = "select * from resident_case_details where header_case_id = $header_case_id AND active = 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					
					if(count($query_result) > 1) 
					{
						$data = array("success"=> false, "data"=>"Cannot delete record with Current / Same case/s.");
						die(json_encode($data));
					}
					
					$commandText = "UPDATE resident_case_header set active = 0 where id = $header_case_id";
					$result = $this->db->query($commandText);
				}

				$commandText = "UPDATE resident_case_details set active = 0 where id = $id";
				$result = $this->db->query($commandText);

				$this->load->library('session');		
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('".$this->modulename('Label')."', $id, 'resident_case_details', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}
			else
			{				
				if ($type == "Add") 
				{

					if ($case_type == 'NEW') 
					{
						if ($date_resolved)
							$date_resolved = date('Y-m-d',strtotime($date_resolved));
						else
							$date_resolved = null;

						$this->load->model('Resident_Case_Header');
						$this->Resident_Case_Header->room_id 		= $room_id;
						$this->Resident_Case_Header->resident_id 	= $resident_id;
						$this->Resident_Case_Header->infection_id	= $infection_id;
						$this->Resident_Case_Header->date			= date('Y-m-d',strtotime($date));
						$this->Resident_Case_Header->date_resolved	= $date_resolved;
						$this->Resident_Case_Header->active			= 1;
						$this->Resident_Case_Header->save(0);

						$commandText = "select id from resident_case_header order by id desc limit 1";
						$result = $this->db->query($commandText);
						$query_result = $result->result(); 
						$header_case_id = $query_result[0]->id;
					}
					else
					{
						$datecmp = date('Y-m-d',strtotime($date));
						$commandText = "SELECT * FROM resident_case_header WHERE id = $case_type_id AND DATE <= '$datecmp'";
						$result = $this->db->query($commandText);
						$query_result = $result->result(); 
						if(count($query_result) == 0) 
						{
							$data = array("success"=> false, "data"=>"Date should be ON or AFTER the <font color=red>NEW CASE</font> date.");
							die(json_encode($data));
						}

						$header_case_id = $case_type_id;
					}

					if ($lab_report) $lab_report = date('Y-m-d',strtotime($lab_report));
					else $lab_report = null;

					$this->load->model('Resident_Case_Details');
					$this->Resident_Case_Details->header_case_id= $header_case_id;
					$this->Resident_Case_Details->condition_id 	= $condition_id;
					$this->Resident_Case_Details->physician_id	= $physician_id;
					$this->Resident_Case_Details->precaution_id	= $precaution_id;
					$this->Resident_Case_Details->test_id		= $test_id;
					$this->Resident_Case_Details->consult_id	= $consult_id;
					$this->Resident_Case_Details->date			= date('Y-m-d',strtotime($date));
					$this->Resident_Case_Details->lab_report	= $lab_report;
					$this->Resident_Case_Details->microorganism	= $microorganism;
					$this->Resident_Case_Details->case_type		= $case_type;
					$this->Resident_Case_Details->other			= $other;
					$this->Resident_Case_Details->active		= 1;
					$this->Resident_Case_Details->save(0);

					$commandText = "select id from resident_case_details order by id desc limit 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$details_case_id = $query_result[0]->id;					
				}
				else
				{
					$commandText = "SELECT header_case_id FROM resident_case_details WHERE id = $id";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$header_case_id = $query_result[0]->header_case_id;

					if ($case_type == 'NEW') 
					{
						if($date_resolved)
							$commandText = "UPDATE resident_case_header SET date_resolved = '".date('Y-m-d',strtotime($date_resolved))."', date = '".date('Y-m-d',strtotime($date))."' WHERE id = $header_case_id";
						else
							$commandText = "UPDATE resident_case_header SET date_resolved = null, date = '".date('Y-m-d',strtotime($date))."' WHERE id = $header_case_id";
						$result = $this->db->query($commandText);
					}

					if ($lab_report) $lab_report = date('Y-m-d',strtotime($lab_report));
					else $lab_report = null;

					$this->load->model('Resident_Case_Details');
					$this->Resident_Case_Details->id			= $id;
					$this->Resident_Case_Details->header_case_id= $header_case_id;
					$this->Resident_Case_Details->condition_id 	= $condition_id;
					$this->Resident_Case_Details->physician_id	= $physician_id;
					$this->Resident_Case_Details->precaution_id	= $precaution_id;
					$this->Resident_Case_Details->test_id		= $test_id;
					$this->Resident_Case_Details->consult_id	= $consult_id;
					$this->Resident_Case_Details->date			= date('Y-m-d',strtotime($date));
					$this->Resident_Case_Details->lab_report	= $lab_report;
					$this->Resident_Case_Details->microorganism	= $microorganism;
					$this->Resident_Case_Details->case_type		= $case_type;
					$this->Resident_Case_Details->other			= $other;
					$this->Resident_Case_Details->active 		= 1;
					$this->Resident_Case_Details->save($id);

					$commandText = "delete from resident_case_sign_symtoms where details_case_id = $id";
					$result = $this->db->query($commandText);					

					$commandText = "delete from resident_case_medications where details_case_id = $id";
					$result = $this->db->query($commandText);

					$details_case_id = $id;
				}

				#diagnosis
				if ($case_type == 'NEW') 
				{						
					$commandText = "delete from resident_case_diagnosis where header_case_id = $header_case_id";
					$result = $this->db->query($commandText);

					$this->load->model('Resident_Case_Diagnosis');
					$diagnosis=explode(',',$diagnosis);
					foreach($diagnosis as $key => $value) 
					{
						$this->Resident_Case_Diagnosis->header_case_id	= $header_case_id;
						$this->Resident_Case_Diagnosis->diagnose_id 	= $value;
						$this->Resident_Case_Diagnosis->save(0);	
					}
				}

				#add/update signs&symptoms
				$this->load->model('Resident_Case_Sign_Symtoms');
				$signs_symptoms=explode(',',$signs_symptoms);
				foreach($signs_symptoms as $key => $value) 
				{
					$this->Resident_Case_Sign_Symtoms->details_case_id	= $details_case_id;
					$this->Resident_Case_Sign_Symtoms->ss_id 	= $value;
					$this->Resident_Case_Sign_Symtoms->save(0);	
				}

				#add/update medications
				$this->load->model('Resident_Case_Medications');				
				$medications=explode(',',$this->input->post('medications'));
				$dosages	=explode(',',$this->input->post('dosages'));
				$tablets	=explode(',',$this->input->post('tablets'));
				$routes		=explode(',',$this->input->post('routes'));
				$frequencies=explode(',',$this->input->post('frequencies'));
				$durations	=explode(',',$this->input->post('durations'));
				$x=0;
				foreach($medications as $key) 
				{
					if($medications[$x] != null)
					{
						$this->Resident_Case_Medications->details_case_id	= $details_case_id;
						$this->Resident_Case_Medications->medication_id 	= $medications[$x];
						$this->Resident_Case_Medications->dosage_id 		= $dosages[$x];
						$this->Resident_Case_Medications->tablet_id 		= $tablets[$x];
						$this->Resident_Case_Medications->route_id 			= $routes[$x];
						$this->Resident_Case_Medications->frequency_id 		= $frequencies[$x];
						$this->Resident_Case_Medications->duration_id 		= $durations[$x];
						$this->Resident_Case_Medications->save(0);	
					}
					$x++;
				}

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'resident_case_details', $type, $this->modulename('Label'));
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function view()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');
			$resident_id = $this->input->post('resident_id');
			$type = $this->input->post('type');
			
			if($type == 'Edit')
				$commandText = "SELECT 
									a.id,
									a.date,									
									a.lab_report,
									a.microorganism,	
									a.case_type,
									a.other,
									a.condition_id,
									a.physician_id,
									a.precaution_id AS tp_id,
									a.test_id,
									a.consult_id,
									b.id AS case_type_id, 
									b.date AS case_date,
									b.date_resolved,
									b.infection_id,	
									b.room_id,
									c.description AS room_desc,
									d.diagnose_id,
									d.diagnose_desc,
									e.description AS infection_desc,
									f.ss_id,
									f.ss_desc,	
									g.description AS condition_desc,
									h.description AS physician_desc,
									i.description AS tp_desc,
									j.description AS test_desc,
									k.description AS consult_desc
								FROM resident_case_details a
									LEFT JOIN resident_case_header b ON a.header_case_id = b.id
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) c ON b.room_id = c.id
									LEFT JOIN (SELECT 	
											a1.header_case_id,
											GROUP_CONCAT(diagnose_id) diagnose_id,
											GROUP_CONCAT(b1.description) diagnose_desc	
										FROM resident_case_diagnosis a1
											JOIN diagnosis b1 ON a1.diagnose_id = b1.id
										WHERE a1.header_case_id = (SELECT header_case_id FROM resident_case_details WHERE id = $id)) d ON a.header_case_id = d.header_case_id
									LEFT JOIN infections e ON b.infection_id = e.id
									LEFT JOIN (SELECT 	
											a2.details_case_id,
											GROUP_CONCAT(ss_id) ss_id,
											GROUP_CONCAT(b2.description) ss_desc	
										FROM resident_case_sign_symtoms a2
											JOIN signs_symptoms b2 ON a2.ss_id = b2.id
										WHERE a2.details_case_id = $id) f ON a.id = f.details_case_id
									LEFT JOIN conditions g ON a.condition_id = g.id
									LEFT JOIN physicians h ON a.physician_id = h.id
									LEFT JOIN transmission_precautions i ON a.precaution_id = i.id
									LEFT JOIN tests j ON a.test_id = j.id
									LEFT JOIN consults k ON a.consult_id = k.id
								WHERE a.id = $id";
			else 
				$commandText = "SELECT 
									a.room_id,
									b.description room_desc
								FROM (SELECT a.* FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1) a 
									LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
								WHERE a.resident_id = $resident_id"; 
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				if($type == 'Edit')
				{
					if ($value->case_type == "NEW") $case_type = 1; else $case_type = 2;
					$case_type_desc = date('m/d/Y',strtotime($value->date)).' '.$value->diagnose_desc.' - '.$value->infection_desc;

					$record['id'] 				= $value->id;
					$record['case_type'] 		= $case_type;
					$record['date'] 			= $value->date;
					$record['infection_id'] 	= $value->infection_id;
					$record['infection_desc'] 	= $value->infection_desc;
					$record['ss_id'] 			= $value->ss_id;
					$record['ss_desc'] 			= $value->ss_desc;
					$record['lab_report'] 		= $value->lab_report;
					$record['test_id'] 			= $value->test_id;
					$record['test_desc'] 		= $value->test_desc;
					$record['microorganism'] 	= $value->microorganism;
					$record['condition_id'] 	= $value->condition_id;
					$record['condition_desc'] 	= $value->condition_desc;
					$record['tp_id'] 			= $value->tp_id;
					$record['tp_desc'] 			= $value->tp_desc;
					$record['consult_id'] 		= $value->consult_id;
					$record['consult_desc'] 	= $value->consult_desc;
					$record['physician_id'] 	= $value->physician_id;
					$record['physician_desc'] 	= $value->physician_desc;
					$record['other'] 			= $value->other;
					$record['case_type_id'] 	= $value->case_type_id;
					$record['case_type_desc'] 	= $case_type_desc;
					$record['date_resolved'] 	= $value->date_resolved;
					$record['resolved_date'] 	= date('m/d/Y',strtotime($value->date_resolved));					
					$record['diagnose_id'] 		= $value->diagnose_id;
					$record['diagnose_desc']	= $value->diagnose_desc;

					$commandText = "SELECT 
										a.medication_id,
										a.dosage_id,
										a.tablet_id,
										a.route_id,
										a.frequency_id,
										a.duration_id,
										b.description AS medication_desc,
										c.description AS dosage_desc,
										d.description AS tablet_desc,
										e.description AS route_desc,
										f.description AS frquency_desc,
										g.description AS duration_desc
									FROM resident_case_medications a
										LEFT JOIN medications b ON a.medication_id = b.id
										LEFT JOIN dosages c ON a.dosage_id = c.id
										LEFT JOIN tablets d ON a.tablet_id = d.id
										LEFT JOIN routes e ON a.route_id = e.id
										LEFT JOIN frequencies f ON a.frequency_id = f.`id`
										LEFT JOIN durations g ON a.duration_id = g.id
									WHERE a.details_case_id = $id";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					foreach($query_result as $key => $val) 
					{	
						$data['medicationID'][] = $val->medication_id;
						$data['dosageID'][] 	= $val->dosage_id;
						$data['tabletID'][] 	= $val->tablet_id;
						$data['routeID'][] 		= $val->route_id;
						$data['frequencyID'][] 	= $val->frequency_id;
						$data['durationID'][] 	= $val->duration_id;

						$data['medication'][] 	= $val->medication_desc;
						$data['dosage'][] 		= $val->dosage_desc;
						$data['tablet'][] 		= $val->tablet_desc;
						$data['route'][] 		= $val->route_desc;
						$data['frequency'][] 	= $val->frquency_desc;
						$data['duration'][] 	= $val->duration_desc;
					}
				}

				$record['room_id']		= $value->room_id;	
				$record['room_desc']	= $value->room_desc;
			}

			$data['totalCount'] = count($query_result);
			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function casetypelist()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$resident_id = $_GET['resident_id'];

			$commandText = "SELECT 
								a.id,
								a.date,
								b.description AS infection,
								(SELECT GROUP_CONCAT(di.description) FROM resident_case_diagnosis rcd JOIN diagnosis di ON rcd.diagnose_id = di.id  WHERE header_case_id = a.id) diagnosis
							FROM resident_case_header a JOIN infections b ON a.infection_id = b.id
							WHERE a.resident_id = $resident_id AND a.date_resolved IS NULL AND a.active = 1
							ORDER BY a.date DESC";					
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$description = date('m/d/Y',strtotime($value->date)).' '.$value->diagnosis.' - '.$value->infection;
				$data['data'][] = array(
					'id' 			=> $value->id,
					'description' 	=> $description);
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{
		$this->load->library('session');

		$query 	= addslashes(strip_tags(trim($this->input->post('query'))));
		$type 	=  $this->input->post('type');
		$report	=  $this->input->post('report');

		$response = array();
        $response['success'] = true;

        if($report == 'ResidentsList')
        {
        	$status =  $this->input->post('status');
	        if($type == "PDF")
	        	$response['filename'] = $this->exportpdfResidentsList($this->generateresidentcaseList($query, $status, 'Report'));
	        else         	
				$response['filename'] = $this->exportexcelResidentsList($this->generateresidentcaseList($query, $status, 'Report'));
			// $this->load->model('Logs'); $this->Logs->audit_logs(0, 'residents', 'Report-'.$type, 'Residents List');        	
			$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, 'Residents List', 'residents', ".$this->session->userdata('id').", 'Report-".$type."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
		}
		if($report == 'CaseList')
		{
			$id 	=  $this->input->post('id');
			$name 	=  $this->input->post('name');
			$age 	=  $this->input->post('age');
			$sex 	=  $this->input->post('sex');
        	$response['filename'] = $this->exportpdfCaseList($this->generateresidentcasegrid($id, 'Report'), $name, $age, $sex);
			// $this->load->model('Logs'); $this->Logs->audit_logs(0, 'residents_case', 'Report', $this->modulename('Label').' Cases ('.strtoupper($name).')');        	
			$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')."  Cases (".strtoupper($name).")', 'residents_case', ".$this->session->userdata('id').", 'Report-".$type."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
		}
		die(json_encode($response));
	}

	public function exportexcelResidentsList($data)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ResidentsList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ResidentsList")
					      ->setSubject("Report")
					      ->setDescription("Generating ResidentsList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B7:F7')->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getStyle('B7:B8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('E7:E8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			#Duplicate Cell Styles
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('B8'), 'B8:B'.($data['totalCount']+7));
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('E8'), 'E8:E'.($data['totalCount']+7));

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B5", 'Resident\'s List');
			###DATE

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B7", "No.")		
					      ->setCellValue("C7", "Name")
					      ->setCellValue("D7", "Room")
					      ->setCellValue("E7", "Age")
					      ->setCellValue("F7", "Sex");

			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+8), ($i+1))		
						      ->setCellValue("C".($i+8), $data['data'][$i]['name'])
						      ->setCellValue("D".($i+8), $data['data'][$i]['details'])
						      ->setCellValue("E".($i+8), $data['data'][$i]['age'])
						      ->setCellValue("F".($i+8), $data['data'][$i]['sex']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+15), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+16), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfResidentsList($data)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ResidentsList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ResidentsList');
			$pdf->SetSubject('ResidentsList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
			
			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Resident\'s List</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>			
			<table border=1>
				<tr style="font-weight:bold;font-size:24px;">
				  <td width="5%"  style="background: black; padding: 10px;" align="center"><font face="Arial" >No.</font></td>
				  <td width="25%"  style="background: black; padding: 10px;" align="left"><font face="Arial" >Name</font></td>
				  <td width="20%"  style="background: black; padding: 10px;" align="left"><font face="Arial" >Room</font></td>
				  <td width="7%"  style="background: black; padding: 10px;" align="center"><font face="Arial" >Age</font></td>
				  <td width="7%"  style="background: black; padding: 10px;" align="left"><font face="Arial" >Sex</font></td>
				</tr>';

			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				if($i%2 == 0)
				{
					$html .= '<tr style="background-color:#f7f6f6;font-size:24px;">
					  <td align="center"><font face="Arial" >'.($i+1).'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['name'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['details'].'</font></td>
					  <td align="center"><font face="Arial" >'.$data['data'][$i]['age'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['sex'].'</font></td>
					</tr>';
				}
				else
				{
					$html .= '<tr style="font-size:24px;">
					  <td align="center"><font face="Arial" >'.($i+1).'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['name'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['details'].'</font></td>
					  <td align="center"><font face="Arial" >'.$data['data'][$i]['age'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['sex'].'</font></td>
					</tr>';
				}
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfCaseList($data, $name, $age, $sex)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ResidentsList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ResidentsList');
			$pdf->SetSubject('ResidentsList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
			
			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Infection Control Result</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>';

			$html .= $name.' | '.$age.'yrs. | '.$sex.'<br>';
			
			$html .= '<table>';
			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				if($data['data'][$i]['case_type_orig'] == 'NEW')
				{
					$html .= '<tr>
					<td colspan="3"></td>
					</tr>
						<tr style="font-size:24px;">
						<td colspan="3" valign="top"><font color="red"><b>'.$data['data'][$i]['date'].' | NEW CASE - '.$data['data'][$i]['infection'].'</b></font></td>
					</tr>';
					
					if($data['data'][$i]['date_resolved']){
						$html .= '<tr style="font-size:20px;">
							<td valign="top" width="12%"><font size=2 color="green"><b>Resolved Date</b></font></td>
							<td valign="top" width="1%"><font size=2 color="green"><b>:</b></font></td>
							<td><font size=2 width="85%" color="green">'.$data['data'][$i]['date_resolved'].'</font></td>
						</tr>';
					}

					$html .= '<tr style="font-size:20px;">
						<td valign="top" width="12%"><b>Diagnosis</b></td>
						<td valign="top" width="1%"><b>:</b></td>
						<td	width="85%">'.$data['data'][$i]['diagnosis'].'</td>
					</tr>
						<tr style="font-size:20px;">
						<td><b>Room</b></td>
						<td><b>:</b></td>
						<td>'.$data['data'][$i]['room'].'</td>
					</tr>';
				}
				else
				{
					$html .= '<tr style="font-size:24px;">
						<br><td colspan="3" valign="top"><font><b>'.$data['data'][$i]['date'].' | Current / Same Case - '.$data['data'][$i]['infection'].'</b></font></td>
					</tr>';
				}
				if($data['data'][$i]['symptoms']){
					$html .= '<tr style="font-size:20px;">
						<td valign="top"><font size=2><b>Signs/Symptoms</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['symptoms'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['lab_report']){
					$html .= '<tr style="font-size:20px;">
						<td><font size=2><b>Lab Report</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.date('m/d/Y',strtotime($data['data'][$i]['lab_report'])).'</font></td>
					</tr>';
				}
				if($data['data'][$i]['test']){
					$html .= '<tr style="font-size:20px;">
						<td><font size=2><b>Test</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['test'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['microorganism']){
					$html .= '<tr style="font-size:20px;">
						<td valign="top"><font size=2><b>Microorganism</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['microorganism'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['medication']){
					$html .= '<tr style="font-size:20px;">
						<td valign="top"><font size=2><b>Medication</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['medication'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['conditions']){
					$html .= '<tr style="font-size:20px;">
						<td><font size=2><b>Condition</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['conditions'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['precaution']){
					$html .= '<tr style="font-size:20px;">
						<td><font size=2><b>Precaution</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['precaution'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['consults']){
					$html .= '<tr style="font-size:20px;">
						<td><font size=2><b>Consult</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['consults'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['physician']){
					$html .= '<tr style="font-size:20px;">
						<td><font size=2><b>Physician</b></font></td>
						<td><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['physician'].'</font></td>
					</tr>';
				}
				if($data['data'][$i]['other']){
					$html .= '<tr style="font-size:20px;">
						<td valign="top"><font size=2><b>Other</b></font></td>
						<td valign="top"><font size=2><b>:</b></font></td>
						<td><font size=2>'.$data['data'][$i]['other'].'</font></td>
					</tr>';
				}
			}
			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function residentslist()
	{ 
		try 
		{
			$record = array();
			$record['diagnosis']	= $this->input->post('diagnosis');
			$record['signs_symptoms']	= $this->input->post('signs_symptoms');
			$record['isolation']	= $this->input->post('isolation');
			$record['building']		= $this->input->post('building');
			$record['floor']		= $this->input->post('floor');
			$record['start_date']	= $this->input->post('start_date');
			$record['end_date']		= $this->input->post('end_date');
			
			die(json_encode($this->generateresidentslist($record)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateresidentslist($record)
	{	
		$header = array();	
		
		$diagnosis = "";
		$header['diagnosis_desc'] = "-";
		if($record['diagnosis'])
		{
			$diagnosis = " AND d.diagnose_id IN (".$record['diagnosis'].") ";			
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM diagnosis WHERE id IN (".$record['diagnosis'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['diagnosis_desc'] = $query_result[0]->description;	
		}

		$signs_symptoms = "";
		$header['signs_symptoms_desc'] = "-";
		if($record['signs_symptoms']){
			$signs_symptoms = " AND e.ss_id IN (".$record['signs_symptoms'].") ";
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM signs_symptoms WHERE id IN (".$record['signs_symptoms'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['signs_symptoms_desc'] = $query_result[0]->description;	
		}

		$isolation = "";
		$header['isolation_desc'] = "-";
		if($record['isolation']){
			$isolation = " AND j.id IN (".$record['isolation'].") ";
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM transmission_precautions_category WHERE id IN (".$record['isolation'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['isolation_desc'] = $query_result[0]->description;	
		}

		$building = "";
		$header['buildings_desc'] = "-";
		if($record['building']){
			$building = " AND h.id = ".$record['building']." ";		
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM buildings WHERE id IN (".$record['building'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['buildings_desc'] = $query_result[0]->description;	
		}

		$floor = "";
		$header['floor_desc'] = "-";
		if($record['floor']){
			$floor = " AND g.id = ".$record['floor']." ";		
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM floors WHERE id IN (".$record['floor'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['floor_desc'] = $query_result[0]->description;	
		}

		$start_date = "";
		$header['start_date'] = "-";
		if($record['start_date']){
			$start_date = " AND b.date >= '".$record['start_date']."' ";		
			$header['start_date'] = date('m/d/Y',strtotime($record['start_date']));
		}

		$end_date = "";
		$header['end_date'] = "-";
		if($record['end_date']){
			$end_date = " AND b.date <= '".$record['end_date']."'";		
			$header['end_date'] = date('m/d/Y',strtotime($record['end_date']));
		}

		$commandText = "SELECT 
							CONCAT(lname, ', ', fname) resident_name, 
							age, 
							sex
						FROM residents a
							LEFT JOIN resident_case_header b ON a.id = b.resident_id
							LEFT JOIN resident_case_details c ON b.id = c.header_case_id
							LEFT JOIN resident_case_diagnosis d ON b.id = d.header_case_id
							LEFT JOIN resident_case_sign_symtoms e ON c.id = e.details_case_id	
							LEFT JOIN rooms f ON b.room_id = f.id
							LEFT JOIN floors g ON f.cat_id = g.id
							LEFT JOIN buildings h ON g.cat_id = h.id
							LEFT JOIN transmission_precautions i ON c.precaution_id = i.id
							LEFT JOIN transmission_precautions_category j ON i.cat_id = j.id
						WHERE b.active = 1 AND c.active = 1
							$diagnosis
							$signs_symptoms
							$isolation
							$building
							$floor
							$start_date
							$end_date
						GROUP BY resident_name
						ORDER BY lname ASC, fname ASC";		
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		foreach($query_result as $key => $value) 
		{	
			$data['data'][] 	= array(
				'resident_name' => strtoupper($value->resident_name),
				'age' 			=> $value->age,
				'sex' 			=> $value->sex);							
		}			

		if(count($query_result) == 0) 
			$data["data"] = array();
		
		$data['totalCount'] = count($query_result);
		$data['header'] = $header;		
		$data['success'] = true;

		return $data;
	}

	public function exportadvancedsearchdoc()
	{ 
		$this->load->library('session');
		$filetype 	= $this->input->post('filetype');

		$record = array();
		$record['diagnosis']	= $this->input->post('diagnosis');
		$record['signs_symptoms'] = $this->input->post('signs_symptoms');
		$record['isolation'] 	= $this->input->post('isolation');
		$record['building']		= $this->input->post('building');
		$record['floor']		= $this->input->post('floor');
		$record['start_date']	= $this->input->post('start_date');
		$record['end_date']		= $this->input->post('end_date');

		$response = array();
        $response['success'] = true;

        if($filetype == "PDF")
        	$response['filename'] = $this->exportpdfAdvancedSearchResidentsList($this->generateresidentslist($record));
        else         	
			$response['filename'] = $this->exportexcelAdvancedSearchResidentsList($this->generateresidentslist($record));
		die(json_encode($response));
	}

	public function exportexcelAdvancedSearchResidentsList($record)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ResidentProfileList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ResidentProfileList")
					      ->setSubject("Report")
					      ->setDescription("Generating ResidentProfileList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B13:D13')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C13:D14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			#Duplicate
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('C14'), 'C14:D'.($record['totalCount']+14));

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'Infection Control - Residents List');
			
			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Diagnosis:")		
					      ->setCellValue("B6", "Signs & Symptoms:")
					      ->setCellValue("B7", "Isolation:")
					      ->setCellValue("B8", "Building:")
					      ->setCellValue("B9", "Floor:")
					      ->setCellValue("B10", "Case (From):")
					      ->setCellValue("B11", "Case (To):")
					      ->setCellValue("C5", $record['header']['diagnosis_desc'])		
					      ->setCellValue("C6", $record['header']['signs_symptoms_desc'])		
					      ->setCellValue("C7", $record['header']['isolation_desc'])		
					      ->setCellValue("C8", $record['header']['buildings_desc'])		
					      ->setCellValue("C9", $record['header']['floor_desc'])		
					      ->setCellValue("C10", $record['header']['start_date'])		
					      ->setCellValue("C11", $record['header']['end_date'])		
					      ->setCellValue("B13", "Name")
					      ->setCellValue("C13", "Age")
					      ->setCellValue("D13", "Sex");

			for ($i = 0; $i<$record['totalCount'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+14), $record['data'][$i]['resident_name'])
						      ->setCellValue("C".($i+14), $record['data'][$i]['age'])
						      ->setCellValue("D".($i+14), $record['data'][$i]['sex']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+20), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+21), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfAdvancedSearchResidentsList($record)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ResidentProfileList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ResidentProfileList');
			$pdf->SetSubject('ResidentProfileList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Infection Control - Residents List</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>
					<table>
						<tr style="font-size:24px;">
						  <td width="16%">Diagnosis</td>
						  <td width="1%" >:</td>
						  <td width="80%">'.$record['header']['diagnosis_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Signs & Symptoms</td>
						  <td >:</td>
						  <td >'.$record['header']['signs_symptoms_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Isolation</td>
						  <td >:</td>
						  <td >'.$record['header']['isolation_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Building</td>
						  <td >:</td>
						  <td >'.$record['header']['buildings_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Floor</td>
						  <td >:</td>
						  <td >'.$record['header']['floor_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Case (From)</td>
						  <td >:</td>
						  <td >'.$record['header']['start_date'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Case (To)</td>
						  <td >:</td>
						  <td >'.$record['header']['end_date'].'</td>
						</tr>';

			$html .= '</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:24px;">
					  <td width="40%" style="padding: 10px;" align="left"><b>Name</b></td>
					  <td width="10%" style="padding: 10px;" align="center"><b>Age</b></td>
					  <td width="10%" style="padding: 10px;" align="center"><b>Sex</b></td>
					</tr>';

			for ($i = 0; $i<$record['totalCount'];$i++)
			{
				$html .= '<tr style="font-size:24px;">
					  <td style="padding: 10px;" align="left">'.$record['data'][$i]['resident_name'].'</td>
					  <td style="padding: 10px;" align="center">'.$record['data'][$i]['age'].'</td>
					  <td style="padding: 10px;" align="center">'.$record['data'][$i]['sex'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function isolation()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));

			$commandText = "SELECT * from transmission_precautions_category where description like '%$query%' and infection_control = 1 order by description asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,						
					'description' 	=> $value->description);
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}