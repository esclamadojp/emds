<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class InService extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'inservice';
		else 
			return 'In-Service';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));  
	}	

	public function namelist()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$query = addslashes(strip_tags(trim($_GET['query'])));

			$commandText = "SELECT * 
							FROM staff 
							WHERE 
								(fname like '%$query%' or 
							    mname like '%$query%' or
							    lname like '%$query%')
							AND active = 1
							ORDER BY lname ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$names[] = array(
					'id' 	=> $value->id,
					'text' 	=> strtoupper($value->lname).", ".strtoupper($value->fname)." ".strtoupper($value->mname),
					'leaf' 	=> true);
			}

			$record = array();
			$record['id'] 		= 0;
			$record['text'] 	= 'Names';
			$record['cls'] 		= 'folder';
			$record['expanded'] = true;
			$record['children'] = $names;

			$data = array();
			$data[] = $record;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function servicelist()
	{ 
		try 
		{			
			$query 		= addslashes(strip_tags(trim($_GET['servicequery'])));
			$staff_id 	= $_GET['staffID'];
			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));
			$type 		= $_GET['searchServiceType'];

			die(json_encode($this->generateservicelist($query, $staff_id, $date_from, $date_to, $type, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateservicelist($query, $staff_id, $date_from, $date_to, $type, $transaction_type)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			if($type == 'Name')
			$commandText = "SELECT 
								b.id,
								b.topic_id,
								b.servicedate,	
								b.duration,
								b.instructor,	
								c.description AS shift,
								d.description AS topic,
								CONCAT (e.lname, ', ', e.fname) staff_name
							FROM inservice_details_staff a								
								LEFT JOIN inservice_details b ON a.service_id = b.id
								LEFT JOIN shifts c ON b.shift_id = c.id
								LEFT JOIN topics d ON b.topic_id = d.id
								LEFT JOIN staff e ON a.staff_id = e.id
							WHERE a.staff_id = $staff_id
								and (b.servicedate between '$date_from' and '$date_to') 
								and (c.description like '%$query%' or d.description like '%$query%')
								and b.active = 1
							ORDER BY b.servicedate DESC";
			else
			$commandText = "SELECT 
								b.id,	
								b.topic_id,
								b.servicedate,	
								b.duration,
								b.instructor,	
								c.description AS shift,
								d.description AS topic	
							FROM inservice_details b 
								LEFT JOIN shifts c ON b.shift_id = c.id
								LEFT JOIN topics d ON b.topic_id = d.id
							WHERE 
								(b.servicedate between '$date_from' and '$date_to') 
								and (c.description like '%$query%' or d.description like '%$query%')
								and b.active = 1
							ORDER BY b.servicedate DESC";							

			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				if($type == 'Name') $staff_name = $value->staff_name;
				else $staff_name = "";

				$data['data'][] = array(
					'id' 			=> $value->id,
					'staff_name'	=> $staff_name,
					'topic_id' 		=> $value->topic_id,
					'servicedate' 	=> date('m/d/Y',strtotime($value->servicedate)),
					'duration' 		=> $value->duration,
					'instructor' 	=> $value->instructor,
					'shift' 		=> $value->shift,
					'topic' 		=> $value->topic);
			}

			$data['count'] = count($query_result);
			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function inservicecrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$staff_id		= $this->input->post('staff_id');
			$servicedate	= date('Y-m-d',strtotime($this->input->post('servicedate')));
			$shifts			= $this->input->post('shifts');
			$staff			= $this->input->post('staff');
			$topic			= strip_tags(trim($this->input->post('topics')));
			$duration		= strip_tags(trim($this->input->post('duration')));
			$instructor		= strip_tags(trim($this->input->post('instructor')));
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "UPDATE inservice_details set active = 0 where id = $id";
				$result = $this->db->query($commandText);
				
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('".$this->modulename('Label')."', $id, 'inservice_details', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}
			else
			{		
				$this->load->model('Inservice_Details');									
				if ($type == "Add") 
					$id = 0;
				else
					$this->Inservice_Details->id = $id;
				
				$this->Inservice_Details->shift_id 		= $shifts;
				$this->Inservice_Details->topic_id		= $topic;
				$this->Inservice_Details->servicedate	= $servicedate;
				$this->Inservice_Details->duration 		= $duration;
				$this->Inservice_Details->instructor 	= $instructor;
				$this->Inservice_Details->active 		= 1;
				$this->Inservice_Details->save($id);

				if ($type == "Add")
				{
					$commandText = "select id from inservice_details order by id desc limit 1";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$service_id = $query_result[0]->id;
				}	
				else 
					$service_id = $id;

				$commandText = "DELETE FROM inservice_details_staff WHERE service_id = $service_id";
				$result = $this->db->query($commandText);

				$this->load->model('Inservice_Details_Staff');
				$this->Inservice_Details_Staff->service_id 	= $service_id;
				$this->Inservice_Details_Staff->staff_id 	= $staff_id;
				$this->Inservice_Details_Staff->save(0);

				$staff	=explode(',',$staff);				
				$staffIDS =implode(',',$staff);				
				foreach($staff as $key => $value) 
				{
					if($value)
					{
						$this->load->model('Inservice_Details_Staff');
						$this->Inservice_Details_Staff->service_id 	= $service_id;
						$this->Inservice_Details_Staff->staff_id 	= $value;
						$this->Inservice_Details_Staff->save(0);	
					}
				}

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'inservice_details', $type, $this->modulename('Label'));
			}
			
			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function inserviceview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');
			$staff_id = $this->input->post('staff_id');
			
			$commandText = "SELECT 
								a.id,
								a.shift_id,
								a.topic_id,
								a.servicedate,
								a.duration,
								a.instructor,
								b.description AS shift_desc,
								c.description AS topic_desc		
							FROM inservice_details a
								LEFT JOIN shifts b ON a.shift_id = b.id
								LEFT JOIN topics c ON a.topic_id = c.id
							WHERE a.id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['id'] 			= $value->id;		
				$record['servicedate']	= $value->servicedate;	
				$record['duration']		= $value->duration;	
				$record['instructor']	= $value->instructor;	
				$record['shift_id']		= $value->shift_id;	
				$record['shift_desc']	= $value->shift_desc;
				$record['topic_id']		= $value->topic_id;	
				$record['topic_desc']	= $value->topic_desc;	
			}

			$servicedate= $record['servicedate'];
			$shift_id 	= $record['shift_id'];
			$topic_id 	= $record['topic_id'];

			$commandText = "SELECT 
								a.staff_id,
								CONCAT(lname,', ',fname) AS staff_name
							FROM inservice_details_staff a
								LEFT JOIN staff b ON a.staff_id = b.id
							WHERE staff_id <> '$staff_id' and service_id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $val) 
			{	
				$data['staff_id'][] 	= $val->staff_id;
				$data['staff_name'][] 	= strtoupper($val->staff_name);
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function topicsview()
	{
		try 
		{
			$id = $this->input->post('id');
			die(json_encode($this->generatetopicsview($id)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generatetopicsview($id)
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$commandText = "SELECT 
								b.id,	
								b.topic_id,
								b.servicedate,	
								b.duration,
								b.instructor,	
								c.description AS shift,
								d.description AS topic	
							FROM inservice_details b 
								LEFT JOIN shifts c ON b.shift_id = c.id
								LEFT JOIN topics d ON b.topic_id = d.id
							WHERE b.id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['duration']		= $value->duration;
				$record['servicedate']	= date('m/d/Y',strtotime($value->servicedate));
				$record['instructor'] 	= $value->instructor;
				$record['shift'] 		= $value->shift;
				$record['topic'] 		= strtoupper($value->topic);
			}
			$data['header'] = $record;

			#participants
			$commandText = "SELECT 
								a.staff_id,
								CONCAT(lname, ', ', fname, ' ', mname) as name,
								c.description AS department,
								d.description AS positions
							FROM inservice_details_staff a 
								LEFT JOIN staff b ON a.staff_id = b.id
								LEFT JOIN departments c ON b.department_id = c.id
								LEFT JOIN positions  d ON b.position_id = d.id
							WHERE a.service_id = $id
							ORDER BY lname ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$data['participants'][] = array(
					'name' 			=> strtoupper($value->name),
					'department' 	=> $value->department,
					'positions' 	=> $value->positions);
			}

			$data['count'] = count($query_result);
			$data['success'] = true;

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function staffname()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$staff_id = $_GET['staff_id'];

			$commandText = "SELECT id, CONCAT(lname,', ',fname) as description FROM staff where id <> $staff_id and (lname like '%$query%' or fname like '%$query%') order by lname asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,						
					'description' 	=> strtoupper($value->description));
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{		
		$this->load->model('Session');$this->Session->Validate();

		if($this->input->post('searchServiceType') == 'List')
			$id 		= $this->input->post('id');
		else
		{
			$query 		= addslashes(strip_tags(trim($this->input->post('servicequery'))));
			$staff_id 	= $this->input->post('staffID');
			$sName 		= $this->input->post('sName');
			$date_from 	= date('Y-m-d',strtotime($this->input->post('date_from')));
			$date_to 	= date('Y-m-d',strtotime($this->input->post('date_to')));
		}

		$type 		= $this->input->post('searchServiceType');
		$filetype 	= $this->input->post('filetype');


		$response = array();
        $response['success'] = true;

		if($this->input->post('searchServiceType') == 'List')
        {
	        if($filetype == "PDF")
	        	$response['filename'] = $this->exportpdfParticipantsList($this->generatetopicsview($id));
	        else         	
				$response['filename'] = $this->exportexcelParticipantsList($this->generatetopicsview($id));
			// $this->load->model('Logs'); $this->Logs->audit_logs($id, 'inservice_details', 'Report-'.$filetype, $this->modulename('Label').' (Participants)');
			$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values ($id, '".$this->modulename('Label')." (Participants)', 'inservice_details', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
        }
        else
        {
	        if($filetype == "PDF")
	        	$response['filename'] = $this->exportpdfServiceList($this->generateservicelist($query, $staff_id, $date_from, $date_to, $type, 'Report'), $query, $date_from, $date_to);
	        else         	
				$response['filename'] = $this->exportexcelServiceList($this->generateservicelist($query, $staff_id, $date_from, $date_to, $type, 'Report'), $query, $date_from, $date_to);
			
			if($type == 'Name')
	        {
	        	// $this->load->model('Logs'); $this->Logs->audit_logs(0, 'inservice', 'Report-'.$filetype, $this->modulename('Label').' (Staff: '.$sName.' From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');
				$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')." (Staff: ".$sName." From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'inservice', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
	        }
			else
			{
			    // $this->load->model('Logs'); $this->Logs->audit_logs(0, 'inservice', 'Report-'.$filetype, $this->modulename('Label').'-All (From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');    
			    $commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')."-All (From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'inservice', ".$this->session->userdata('id').", 'Report-".$filetype."', '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}
				
        }
		die(json_encode($response));
	}

	public function exportexcelServiceList($data, $query, $date_from, $date_to)
	{
		try 
		{

		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ServiceList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ServiceList")
					      ->setSubject("Report")
					      ->setDescription("Generating ServiceList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B9:U10')->getFont()->setBold(true);

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'In-Service Details');
			###DATE

			if(!$query) $query = 'NULL';

			$headerValue = 9;
			if($data['data'][0]['staff_name'])
			{
				$headerValue = 10;
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B8", 'Staff Name');
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C8", $data['data'][0]['staff_name']);
			}

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Searched by:")		
					      ->setCellValue("B6", "Date From:")
					      ->setCellValue("B7", "Date To:")
					      ->setCellValue("C5", $query)
					      ->setCellValue("C6", date('m/d/Y',strtotime($date_from)))
					      ->setCellValue("C7", date('m/d/Y',strtotime($date_to)))
					      ->setCellValue("B".$headerValue, "Date")
					      ->setCellValue("C".$headerValue, "Topic")
					      ->setCellValue("D".$headerValue, "Shift")
					      ->setCellValue("E".$headerValue, "Duration")
					      ->setCellValue("F".$headerValue, "Instructor / By");		

			for ($i = 0; $i<$data['count'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+$headerValue+2), $data['data'][$i]['servicedate'])
						      ->setCellValue("C".($i+$headerValue+2), $data['data'][$i]['topic'])
						      ->setCellValue("D".($i+$headerValue+2), $data['data'][$i]['shift'])
						      ->setCellValue("E".($i+$headerValue+2), $data['data'][$i]['duration'])
						      ->setCellValue("F".($i+$headerValue+2), $data['data'][$i]['instructor']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+19), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+20), 'Date Printed: '.date('m/d/Y h:i:sa'));


			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";

			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfServiceList($data, $query, $date_from, $date_to)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ServiceList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ServiceList');
			$pdf->SetSubject('ServiceList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');

			if(!$query) $query = 'NULL';

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:12px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >In-Service Details</font></td>
				</tr>
				<tr style="font-size:12px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:12px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table><br><br>

					<table>
						<tr style="font-size:12px;">
						  <td width="15%">Searched by</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$query.'</td>
						</tr>
						<tr style="font-size:12px;">
						  <td>Date From</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_from)).'</td>
						</tr>
						<tr style="font-size:12px;">
						  <td>Date To</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_to)).'</td>
						</tr>';

			if($data['data'][0]['staff_name'])						
			$html .= '<tr style="font-size:12px;">
						  <td>Staff Name</td>
						  <td>:</td>
						  <td>'.$data['data'][0]['staff_name'].'</td>
						</tr>';

			$html .= '</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:12px;">
					  <td width="15%" style="padding: 10px;" align="left"><b>Date</b></td>
					  <td width="30%" style="padding: 10px;" align="left"><b>Topic</b></td>
					  <td width="15%" style="padding: 10px;" align="left"><b>Shift</b></td>
					  <td width="15%" style="padding: 10px;" align="left"><b>Duration</b></td>
					  <td width="25%" style="padding: 10px;" align="left"><b>Instructor / By</b></td>
					</tr>';

			for ($i = 0; $i<$data['count'];$i++)
			{
				$html .= '<tr style="font-size:12px;">
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['servicedate'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['topic'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['shift'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['duration'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['data'][$i]['instructor'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:8px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportexcelParticipantsList($data)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ParticipantsList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ParticipantsList")
					      ->setSubject("Report")
					      ->setDescription("Generating ParticipantsList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B11:D11')->getFont()->setBold(true);

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'In-Service Participants');
			###DATE

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Date:")		
					      ->setCellValue("B6", "Shift:")
					      ->setCellValue("B7", "Topic:")
					      ->setCellValue("B8", "Duration:")
					      ->setCellValue("B9", "Instructor / By:")
					      ->setCellValue("B11", "Name")
					      ->setCellValue("C11", "Department")
					      ->setCellValue("D11", "Position")
					      ->setCellValue("C5", $data['header']['servicedate'])
					      ->setCellValue("C6", $data['header']['shift'])
					      ->setCellValue("C7", $data['header']['topic'])
					      ->setCellValue("C8", $data['header']['duration'])
					      ->setCellValue("C9", $data['header']['instructor']);

			for ($i = 0; $i<$data['count'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+12), $data['participants'][$i]['name'])
						      ->setCellValue("C".($i+12), $data['participants'][$i]['department'])
						      ->setCellValue("D".($i+12), $data['participants'][$i]['positions']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+19), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+20), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfParticipantsList($data)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ParticipantsList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ParticipantsList');
			$pdf->SetSubject('ParticipantsList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:12px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >In-Service Participants</font></td>
				</tr>
				<tr style="font-size:12px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:12px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table><br><br>			

					<table>
						<tr style="font-size:12px;">
						  <td width="10%">Date</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$data['header']['servicedate'].'</td>
						</tr>
						<tr style="font-size:12px;">
						  <td width="10%">Shift</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$data['header']['shift'].'</td>
						</tr>
						<tr style="font-size:12px;">
						  <td width="10%">Topic</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$data['header']['topic'].'</td>
						</tr>
						<tr style="font-size:12px;">
						  <td width="10%">Duration</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$data['header']['duration'].'</td>
						</tr>
						<tr style="font-size:12px;">
						  <td width="10%">Instructor / By</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$data['header']['instructor'].'</td>
						</tr>

					</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:12px;">
					  <td width="30%" style="padding: 10px;" align="left"><b>Name</b></td>
					  <td width="25%" style="padding: 10px;" align="left"><b>Department</b></td>
					  <td width="25%" style="padding: 10px;" align="left"><b>Position</b></td>
					</tr>';

			for ($i = 0; $i<$data['count'];$i++)
			{
				$html .= '<tr style="font-size:12px;">
					  <td style="padding: 10px;" align="left">'.$data['participants'][$i]['name'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['participants'][$i]['department'].'</td>
					  <td style="padding: 10px;" align="left">'.$data['participants'][$i]['positions'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:8px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}