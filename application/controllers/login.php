<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'login';
		else 
			return 'Login';
	}

	public function index(){
		$this->load->helper('common_helper');
		$this->load->view($this->modulename('link').'/index');
		$this->load->view('templates/footer');
	}

	public function userauthentication() 
	{
		try 
		{
			$user_name	= addslashes(strip_tags(trim($this->input->post('user_name'))));
			$password	= addslashes(strip_tags(trim($this->input->post('password'))));
			$type		= $this->input->post('type');
			
			if ($type == "Staff")
			{
				// $this->load->model('Cipher');
				// $this->Cipher->secretpassphrase();			
				// $encryptedtext = $this->Cipher->encrypt($password);
				$encryptedtext = $password;

				$commandText = "select 	
									a.id,
									a.admin,							    
									a.staff_id,
								    b.department_id,
								    c.description as department_description,
								    b.fname,
								    b.mname,
								    b.lname
								from (users a join staff b on a.staff_id = b.id)
								    join departments c on b.department_id = c.id 
								where a.username = '".addslashes($user_name)."' 
								    and a.password = '$encryptedtext'";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 

				if(count($query_result) == 0) 
				{
					$this->load->library('session');
					$commandText = "insert into audit_logs (transaction_id, transaction_type, query_type, date_created, time_created) values (0, 'Failed Attempt! (Username:".addslashes($user_name).")', 'Login', '".date('Y-m-d')."', '".date('H:i:s')."')";
					$result = $this->db->query($commandText);
					$data = array("success"=> false, "data"=>"User name not found!");
					die(json_encode($data));
				}

				#set session
				$this->load->library('session');
				if($query_result[0]->admin)
					$admin = " <font color=white>(<font color=red><b>Administrator</b></font>)</font>";
				else 
					$admin = null;		

				$name = strtoupper($query_result[0]->fname)." ".strtoupper($query_result[0]->mname)." ".strtoupper($query_result[0]->lname);
				$newdata = array(
					'id'			=> $query_result[0]->id,
					'admin'			=> $admin,
					'staff_id'		=> $query_result[0]->staff_id,
					'name'  		=> $name,
					'department_id'	=> $query_result[0]->department_id,
					'department_description'=> $query_result[0]->department_description,
					'type'			=> $type,
					'logged_in' 	=> TRUE,
					'time' 			=> date('Y-m-d H:i:s')
				);
				$this->session->set_userdata($newdata);

				$route = "thumbnailmenu";	

				$commandText = "insert into audit_logs (transaction_id, transaction_type, created_by, query_type, date_created, time_created) values (0, 'Successfully Login!', ".$this->session->userdata('id').", 'Login', '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}

			$arr = array();  
			$arr['success'] = true;
			$arr['data'] = $route;
			$arr['name'] = strtoupper($query_result[0]->fname)." ".strtoupper($query_result[0]->mname)." ".strtoupper($query_result[0]->lname);
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}
}