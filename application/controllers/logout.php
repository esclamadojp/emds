<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	/**
	*/
	public function index(){
		$this->load->library('session');
		$this->logs($this->session->userdata('id'));		
		$this->session->sess_destroy();		
		header("Location:".base_url().'index.php/login');
        exit();
	}

	public function terminateSession()
	{		
		$id	= $this->input->post('id');
		$this->logs($id);
		$data = array("success"=> true);
		die(json_encode($data));
	}

	public function logs($id)
	{
		$commandText = "insert into audit_logs (transaction_id, transaction_type, created_by, query_type, date_created, time_created) values (0, 'Session Terminated!', ".$id.", 'Logout', '".date('Y-m-d')."', '".date('H:i:s')."')";
		$result = $this->db->query($commandText);		
	}
}