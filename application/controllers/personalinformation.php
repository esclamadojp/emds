<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personalinformation extends CI_Controller {
	/**
	*/
	private function modulename()
	{
		return 'personalinformation';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename());
	}

	public function view()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			

            if ($this->session->userdata('type') == 'Residents')
            {
            	$student_id = $this->session->userdata('student_id');
				$commandText = "";
            }
            else
            {
            	$staff_id = $this->session->userdata('staff_id');
				$commandText = "SELECT 
									a.*,	
									a.department_id,
									a.position_id,
									b.description AS department_desc,
									c.description AS position_desc	
								FROM staff a
									LEFT JOIN departments b ON a.department_id = b.id
									LEFT JOIN positions c ON a.position_id = c.id
								WHERE a.id = '$staff_id'";
            }
							   
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			foreach($query_result as $key => $value) 
			{	
				if ($this->session->userdata('type') == 'Residents')
				{
				}
				else
				{
					$record['id']				= $value->id;
					$record['department_id']	= $value->department_id;
					$record['position_id'] 		= $value->position_id;
					$record['department_desc']	= $value->department_desc;
					$record['position_desc'] 	= $value->position_desc;
					$record['name'] 			= strtoupper($value->fname)." ".strtoupper($value->mname)." ".strtoupper($value->lname);
					$record['type'] 			= 'Staff';
				}
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function usercrud() 
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->session->userdata('id');

			$password	= strip_tags(trim($this->input->post('password')));
			$password2	= strip_tags(trim($this->input->post('password2')));
			
			if($password != $password2)	
			{
				$data = array("success"=> false, "data"=>"Password mismatch!");
				die(json_encode($data));
			}

			if ($this->session->userdata('type') == 'Residents')
			{
			}
			else
			{
				$this->load->model('Cipher');
				$this->Cipher->secretpassphrase();
				$encryptedtext = $this->Cipher->encrypt($password);

				$commandText = "update users set password = '$encryptedtext' where id = $id";
				$result = $this->db->query($commandText);
			}
			

			$arr = array();  
			$arr['success'] = true;
			$arr['data'] = "Successfully Updated";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function userview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id = $this->session->userdata('id');
		
			if ($this->session->userdata('type') == 'Residents')
			{	
			}   
			else
			{
				$commandText = "select 
							    a.id,
							    a.username,
							    a.password
							from users a LEFT JOIN staff b on a.staff_id = b.id
							where a.id = $id";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 

				$data = array();
				$record = array();

				$this->load->model('Cipher');
				$this->Cipher->secretpassphrase();			

				foreach($query_result as $key => $value) 
				{	
					$decryptedtext = $this->Cipher->decrypt($value->password);
					
					$record['id'] 			= $value->id;					
					$record['user_name']	= $value->username;	
					$record['password']		= $decryptedtext;	
				}
			}         		

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}		
}