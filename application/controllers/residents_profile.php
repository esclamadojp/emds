<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Residents_Profile extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'residents_profile';
		else 
			return 'Residents Profile';
	} 

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));
	}

	public function residentslist()
	{ 
		try 
		{
			$record = array();
			$record['diagnosis']	= $this->input->post('diagnosis');
			$record['allergies']	= $this->input->post('allergies');
			$record['census']		= $this->input->post('census');
			$record['building']		= $this->input->post('building');
			$record['floor']		= $this->input->post('floor');
			$record['start_date']	= $this->input->post('start_date');
			$record['end_date']		= $this->input->post('end_date');
			$record['medicare']		= $this->input->post('medicare');
			$record['medicaid']		= $this->input->post('medicaid');
			
			die(json_encode($this->generateresidentslist($record)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateresidentslist($record)
	{	
		#update session
		$this->load->model('Session');$this->Session->Validate();

		$header = array();	
		
		$diagnosis = "";
		$header['diagnosis_desc'] = "-";
		if($record['diagnosis'])
		{
			$diagnosis = " AND c.diagnose_id IN (".$record['diagnosis'].") ";			
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM diagnosis WHERE id IN (".$record['diagnosis'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['diagnosis_desc'] = $query_result[0]->description;	
		}

		$allergies = "";
		$header['allergies_desc'] = "-";
		if($record['allergies']){
			$allergies = " AND d.allergy_id IN (".$record['allergies'].") ";
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM allergies WHERE id IN (".$record['allergies'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['allergies_desc'] = $query_result[0]->description;	
		}

		$census = "";
		$header['census_desc'] = "-";
		if($record['census']){
			$census = " AND h.census_id = ".$record['census']." ";		
			$commandText = "SELECT description FROM resident_census WHERE id IN (".$record['census'].")";						
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['census_desc'] = $query_result[0]->description;	
		}

		$building = "";
		$header['buildings_desc'] = "-";
		if($record['building']){
			$building = " AND g.id = ".$record['building']." ";		
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM buildings WHERE id IN (".$record['building'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['buildings_desc'] = $query_result[0]->description;	
		}

		$floor = "";
		$header['floor_desc'] = "-";
		if($record['floor']){
			$floor = " AND f.id = ".$record['floor']." ";		
			$commandText = "SELECT GROUP_CONCAT(description SEPARATOR ' and ') AS description FROM floors WHERE id IN (".$record['floor'].")";			
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 
			$header['floor_desc'] = $query_result[0]->description;	
		}

		$start_date = "";
		$header['start_date'] = "-";
		if($record['start_date']){
			$start_date = " AND b.latest_admission_date >= '".$record['start_date']."' ";		
			$header['start_date'] = date('m/d/Y',strtotime($record['start_date']));
		}

		$end_date = "";
		$header['end_date'] = "-";
		if($record['end_date']){
			$end_date = " AND b.latest_admission_date <= '".$record['end_date']."'";		
			$header['end_date'] = date('m/d/Y',strtotime($record['end_date']));
		}

		$medicare = "";
		$header['medicares_desc'] = "-";
		if($record['medicare']){
			$medicare = " AND b.medicare LIKE '%".$record['medicare']."%' ";		
			$header['medicares_desc'] = $record['medicare'];	
		}

		$medicaid = "";
		$header['medicaids_desc'] = "-";
		if($record['medicaid']){
			$medicaid = " AND b.medicaid LIKE '%".$record['medicaid']."%' ";
			$header['medicaids_desc'] = $record['medicaid'];	
		}

		$commandText = "SELECT 
							CONCAT(lname, ', ', fname) resident_name, 
							age, 
							sex
						FROM residents a
							LEFT JOIN residents_profile b ON a.id = b.resident_id
							LEFT JOIN residents_profile_diagnosis c ON b.id = c.profile_id
							LEFT JOIN residents_profile_allergies d ON b.id = d.profile_id	
							LEFT JOIN rooms e ON b.room_id = e.id
							LEFT JOIN floors f ON e.cat_id = f.id
							LEFT JOIN buildings g ON f.cat_id = g.id	
							LEFT JOIN resident_profile_census h ON b.id = h.profile_id
						WHERE a.active = 1
							$diagnosis
							$allergies
							$census
							$building
							$floor
							$start_date
							$end_date
							$medicare
							$medicaid
						GROUP BY resident_name
						ORDER BY lname ASC, fname ASC";		
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		foreach($query_result as $key => $value) 
		{	
			$data['data'][] 	= array(
				'resident_name' => strtoupper($value->resident_name),
				'age' 			=> $value->age,
				'sex' 			=> $value->sex);							
		}			

		if(count($query_result) == 0) 
			$data["data"] = array();
		
		$data['count'] = count($query_result);
		$data['header'] = $header;		
		$data['success'] = true;

		return $data;
	}

	public function exportdocument()
	{		
		$filetype 	= $this->input->post('filetype');

		$record = array();
		$record['diagnosis']	= $this->input->post('diagnosis');
		$record['allergies']	= $this->input->post('allergies');
		$record['census']		= $this->input->post('census');
		$record['building']		= $this->input->post('building');
		$record['floor']		= $this->input->post('floor');
		$record['start_date']	= $this->input->post('start_date');
		$record['end_date']		= $this->input->post('end_date');
		$record['medicare']		= $this->input->post('medicare');
		$record['medicaid']		= $this->input->post('medicaid');

		$response = array();
        $response['success'] = true;

        if($filetype == "PDF")
        	$response['filename'] = $this->exportpdfResidentsList($this->generateresidentslist($record));
        else         	
			$response['filename'] = $this->exportexcelResidentsList($this->generateresidentslist($record));
		die(json_encode($response));
	}

	public function exportexcelResidentsList($record)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "ResidentProfileList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("ResidentProfileList")
					      ->setSubject("Report")
					      ->setDescription("Generating ResidentProfileList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B15:D15')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('C15:D16')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			#Duplicate
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('C16'), 'C16:D'.($record['count']+16));

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'Resident\'s Profile - Residents List');
			
			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Diagnosis:")		
					      ->setCellValue("B6", "Allergies:")
					      ->setCellValue("B7", "Census:")
					      ->setCellValue("B8", "Building:")
					      ->setCellValue("B9", "Floor:")
					      ->setCellValue("B10", "Admission (From):")
					      ->setCellValue("B11", "Admission (To):")
					      ->setCellValue("B12", "Insurance 1(Medicare):")
					      ->setCellValue("B13", "Insurance 2(Medicaid):")
					      ->setCellValue("C5", $record['header']['diagnosis_desc'])		
					      ->setCellValue("C6", $record['header']['allergies_desc'])		
					      ->setCellValue("C7", $record['header']['census_desc'])		
					      ->setCellValue("C8", $record['header']['buildings_desc'])		
					      ->setCellValue("C9", $record['header']['floor_desc'])		
					      ->setCellValue("C10", $record['header']['start_date'])		
					      ->setCellValue("C11", $record['header']['end_date'])		
					      ->setCellValue("C12", $record['header']['medicares_desc'])
					      ->setCellValue("C13", $record['header']['medicaids_desc'])
					      ->setCellValue("B15", "Name")
					      ->setCellValue("C15", "Age")
					      ->setCellValue("D15", "Sex");

			for ($i = 0; $i<$record['count'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+16), $record['data'][$i]['resident_name'])
						      ->setCellValue("C".($i+16), $record['data'][$i]['age'])
						      ->setCellValue("D".($i+16), $record['data'][$i]['sex']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+24), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+25), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfResidentsList($record)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "ResidentProfileList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ResidentProfileList');
			$pdf->SetSubject('ResidentProfileList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Resident\'s Profile - Residents List</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>
					<table>
						<tr style="font-size:24px;">
						  <td width="16%">Diagnosis</td>
						  <td width="1%" >:</td>
						  <td width="80%">'.$record['header']['diagnosis_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Allergies</td>
						  <td >:</td>
						  <td >'.$record['header']['allergies_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Census</td>
						  <td >:</td>
						  <td >'.$record['header']['census_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Building</td>
						  <td >:</td>
						  <td >'.$record['header']['buildings_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Floor</td>
						  <td >:</td>
						  <td >'.$record['header']['floor_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Admission (From)</td>
						  <td >:</td>
						  <td >'.$record['header']['start_date'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Admission (To)</td>
						  <td >:</td>
						  <td >'.$record['header']['end_date'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Insurance 1(Medicare)</td>
						  <td >:</td>
						  <td >'.$record['header']['medicares_desc'].'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Insurance 2(Medicaid)</td>
						  <td >:</td>
						  <td >'.$record['header']['medicaids_desc'].'</td>
						</tr>';

			$html .= '</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:24px;">
					  <td width="40%" style="padding: 10px;" align="left"><b>Name</b></td>
					  <td width="10%" style="padding: 10px;" align="center"><b>Age</b></td>
					  <td width="10%" style="padding: 10px;" align="center"><b>Sex</b></td>
					</tr>';

			for ($i = 0; $i<$record['count'];$i++)
			{
				$html .= '<tr style="font-size:24px;">
					  <td style="padding: 10px;" align="left">'.$record['data'][$i]['resident_name'].'</td>
					  <td style="padding: 10px;" align="center">'.$record['data'][$i]['age'].'</td>
					  <td style="padding: 10px;" align="center">'.$record['data'][$i]['sex'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generatecareplan($id, $resident_id)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$commandText = "SELECT 
								a.*,
								IF(created_by = 0, 'Default', CONCAT(fname, ' ', mname, ' ', lname))  AS staff,
								IF(a.status = 1, 'Active', 'Discontinued') AS gic_status
							FROM residents_profile_care_plan a
								LEFT JOIN users b ON a.created_by = b.id
								LEFT JOIN staff c ON b.staff_id = c.id
							WHERE a.diagnose_id = $id 
								AND profile_id = (SELECT a.id FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1 AND b.resident_id = $resident_id)
							ORDER BY a.date ASC";	
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{								
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
				'id' 			=> $value->id,
				'date' 			=> date('m/d/Y',strtotime($value->date)),
				'description' 	=> $value->remarks,
				'created_by' 	=> strtoupper($value->staff),				
				'date_created' 	=> date('m/d/Y',strtotime($value->date_created)),
				'type' 			=> $value->type,
				'status' 		=> $value->gic_status);
			}	

			$data["success"] = true;
			$data["count"] = count($query_result);

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function careplandocument()
	{		
		$type 	= $this->input->post('type');

		$response = array();
        $response['success'] = true;

        if($type == "PDF")
        	$response['filename'] = $this->exportpdfCarePlan($this->generatecareplan($this->input->post('id'), $this->input->post('resident_id')));
        else         	
			$response['filename'] = $this->exportexcelCarePlan($this->generatecareplan($this->input->post('id'), $this->input->post('resident_id')));
		die(json_encode($response));
	}

	public function exportpdfCarePlan($record)
	{
		try 
		{
			//print"<pre>";print_r($record);die();
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "CarePlan".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('CarePlan');
			$pdf->SetSubject('CarePlan');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');

			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Resident\'s Profile - Residents List</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table>
			<br><br>
					<table>
						<tr style="font-size:24px;">
						  <td width="16%">Resident Name</td>
						  <td width="1%" >:</td>
						  <td width="80%">test name</td>
						</tr>
						<tr style="font-size:24px;">
						  <td >Problem</td>
						  <td >:</td>
						  <td >test problem</td>
						</tr>';

			$html .= '</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:24px;">
					  <td colspan=2 style="padding: 10px;" align="left"><b>GOAL/S</b></td>
					</tr>';

			for ($i = 0; $i<$record['count'];$i++)
			{
				if($record['data'][$i]['type'] == 'GOAL')
				$html .= '<tr style="font-size:24px;">
					  <td width="10%" style="padding: 10px;" align="left">'.$record['data'][$i]['date'].'</td>
					  <td width="78%" style="padding: 10px;" align="left">'.$record['data'][$i]['description'].'</td>
					  <td width="12%" style="padding: 10px;" align="left">'.$record['data'][$i]['status'].'</td>
					</tr>';
			}

			$html .= '</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:24px;">
					  <td colspan=4 style="padding: 10px;" align="left"><b>INTERVENTION/S</b></td>
					</tr>';

			for ($i = 0; $i<$record['count'];$i++)
			{
				if($record['data'][$i]['type'] == 'INTERVENTION')
				$html .= '<tr style="font-size:24px;">
					  <td width="10%" style="padding: 10px;" align="left">'.$record['data'][$i]['date'].'</td>
					  <td width="48%" style="padding: 10px;" align="left">'.$record['data'][$i]['description'].'</td>
					  <td width="20%" style="padding: 10px;" align="left">'.$record['data'][$i]['created_by'].'</td>
					  <td width="10%" style="padding: 10px;" align="left">'.$record['data'][$i]['date_created'].'</td>
					  <td width="12%" style="padding: 10px;" align="left">'.$record['data'][$i]['status'].'</td>
					</tr>';
			}

			$html .= '</table>

					<br>

					<table border="1" cellpadding="2">
					<tr style="font-size:24px;">
					  <td colspan=4 style="padding: 10px;" align="left"><b>COMMENT/S</b></td>
					</tr>';

			for ($i = 0; $i<$record['count'];$i++)
			{
				if($record['data'][$i]['type'] == 'COMMENT')
				$html .= '<tr style="font-size:24px;">
					  <td width="10%" style="padding: 10px;" align="left">'.$record['data'][$i]['date'].'</td>
					  <td width="48%" style="padding: 10px;" align="left">'.$record['data'][$i]['description'].'</td>
					  <td width="20%" style="padding: 10px;" align="left">'.$record['data'][$i]['created_by'].'</td>
					  <td width="10%" style="padding: 10px;" align="left">'.$record['data'][$i]['date_created'].'</td>
					  <td width="12%" style="padding: 10px;" align="left">'.$record['data'][$i]['status'].'</td>
					</tr>';
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function medicalrecords(){ 
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$resident_id = $_GET['resident_id'];

			$commandText = "SELECT *, IF(status=1, 'Active', 'Closed') status_label FROM residents_medical_records WHERE resident_id = $resident_id and active = 1 ORDER BY record_no ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result();  

			foreach($query_result as $key => $value) 
			{	
				if($value->discharge_date)
					$discharge_date = date('m/d/Y',strtotime($value->discharge_date));
				else $discharge_date = null;

				if($value->status_label == 'Active')
					$status = '<font color=green>Active</font>';
				else 
					$status = '<font color=gray>Closed</font>';

				$data['data'][] 	= array(
					'id' 			=> $value->id,
					'record_no' 	=> $value->record_no,
					'admission_date' => date('m/d/Y',strtotime($value->admission_date)),					
					'discharge_date' => $discharge_date,
					'status' 		=> $status);					
			}

			$data['count'] = count($query_result);

			die(json_encode($data));

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function crud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$resident_id	= $this->input->post('resident_id');
			$record_no		= $this->input->post('record_no');
			$admission_date = $this->input->post('admission_date');
			$discharge_date = $this->input->post('discharge_date');
			$status			= $this->input->post('status');
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "update residents_medical_records set active = 0 where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$this->load->model('residents_medical_records');
				if ($type == "Add")
				{
					$commandText = "select * from residents_medical_records where record_no = '$record_no' and resident_id = $resident_id";
					$result = $this->db->query($commandText);
					$query_result = $result->result();
					$id = 0;
				}
				if ($type == "Edit") 				
				{
					$commandText = "select * from residents_medical_records where record_no = '$record_no' and resident_id = $resident_id and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result = $result->result();
					$this->residents_medical_records->id = $id;
				}
				
				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Record number already exist.");
					die(json_encode($data));
				}

				if($discharge_date)
					$discharge_date = date('Y-m-d',strtotime($discharge_date));
				else 
					$discharge_date = null;

				$commandText = "UPDATE residents_medical_records SET STATUS = 2 WHERE resident_id = $resident_id";
				$result = $this->db->query($commandText);
				
				$this->residents_medical_records->resident_id 		= $resident_id;
				$this->residents_medical_records->record_no 		= $record_no;
				$this->residents_medical_records->admission_date 	= date('Y-m-d',strtotime($admission_date));
				$this->residents_medical_records->discharge_date	= $discharge_date;
				$this->residents_medical_records->status			= $status;
				$this->residents_medical_records->active			= 1;
				$this->residents_medical_records->save($id);

				if ($type == "Add")
				{
					$commandText = "INSERT INTO residents_profile (resident_id, record_no) VALUES ($resident_id, $record_no)"; 
					$result = $this->db->query($commandText);
				}

				#update residents_profile
				$commandText = "UPDATE residents_profile 
								SET 
									original_admission_date = (SELECT admission_date FROM residents_medical_records WHERE resident_id = $resident_id ORDER BY admission_date ASC LIMIT 1),
									latest_admission_date = (SELECT admission_date FROM residents_medical_records WHERE resident_id = $resident_id ORDER BY admission_date DESC LIMIT 1),
									latest_discharge_date = (SELECT discharge_date FROM residents_medical_records WHERE resident_id = $resident_id ORDER BY discharge_date DESC LIMIT 1)
								WHERE resident_id = $resident_id and record_no = $record_no";
				$result = $this->db->query($commandText);

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'residents_medical_records', $type, $this->modulename('Label').' (Medical Record)');
			}
			
			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}

	public function view()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');

			$commandText = "select * from residents_medical_records where id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['id'] 			= $value->id;					
				$record['record_no'] 	= $value->record_no;					
				$record['admission_date'] 	= $value->admission_date;
				$record['discharge_date'] 	= $value->discharge_date;
				$record['status'] 	= $value->status;
			}

			$data['data'] = $record;
			$data['success'] = true;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function imageupload() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();
			$resident_id = $this->input->post('resident_id');

			$name 	= $_FILES['form-file']['name'];       
	        $source = $_FILES['form-file']['tmp_name'];    
	        $size 	= $_FILES['form-file']['size'];

	        $path = "profile_pic/";
	        $valid_formats = array("jpg", "png", "gif", "bmp");

	        $arr = array();  
			list($txt, $ext) = explode(".", $name);
			if(in_array($ext,$valid_formats))
			{
				if($size<(250*1024))
				{
					if( file_exists ($path.$name))
						unlink($path.$name);
					
					$commandText = "DELETE FROM residents_image WHERE src='$name' AND resident_id = $resident_id";
					$result = $this->db->query($commandText);						

					$this->load->model('Residents_Image');
					$this->Residents_Image->resident_id = $resident_id;
					$this->Residents_Image->src 		= $name;
					$this->Residents_Image->save(0);

					$arr['success'] = true;
					$arr['imagename'] = $name;
					$arr['data'] = 'Upload '.$name . ' success!';
					move_uploaded_file($source,$path.$name);
				}
				else
				{
					$arr['success'] = false;
					$arr['data'] = 'File '. $name . ' Size exceeded!. Not more than 256kb.';
				}
			}
			else
			{
				$arr['success'] = false;
				$arr['data'] = 'File '. $name . ' Invalid format!';
			}	

			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}

	public function diagnosis_care_plan()
	{
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));
			$resident_id = $_GET['resident_id'];

			$commandText = "SELECT 
								b.id,
								b.description
							FROM residents_profile_diagnosis a
								LEFT JOIN diagnosis b ON a.diagnose_id = b.id
							WHERE profile_id = (SELECT a.id FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1 AND b.resident_id = $resident_id)
								AND b.care_plan = 1 
								AND b.description like '%$query%' 
							ORDER BY b.description ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["count"] = 0;
				$data["data"] = array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,						
					'description' 	=> $value->description);
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function gicList()
	{ 
		try 
		{			
			die(json_encode($this->generateregicList($_GET['id'], $_GET['resident_id'], $_GET['type'], $_GET['status'])));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateregicList($id, $resident_id, $type, $status)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$filterStatus = '';
			if($status == 2)
				$filterStatus = ' AND a.status = 1 ';
			if($status == 3) 
				$filterStatus = ' AND a.status = 2 ';

			$commandText = "SELECT 
								a.*,
								IF(created_by = 0, 'Default', CONCAT(fname, ' ', mname, ' ', lname))  AS staff,
								IF(a.status = 1, '<font color=green>Active</font>', '<font color=red>Discontinued</font>') AS gic_status
							FROM residents_profile_care_plan a 
								LEFT JOIN users b ON a.created_by = b.id
								LEFT JOIN staff c ON b.staff_id = c.id
							WHERE a.diagnose_id = $id 
								AND profile_id = (SELECT a.id FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1 AND b.resident_id = $resident_id)
								AND a.type = '$type' $filterStatus
							ORDER BY a.date ASC";	
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{								
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
				'id' 			=> $value->id,
				'date' 			=> date('m/d/Y',strtotime($value->date)),
				'description' 	=> $value->remarks,
				'created_by' 	=> strtoupper($value->staff),				
				'date_created' 	=> date('m/d/Y',strtotime($value->date_created)),
				'status' 		=> $value->gic_status);
			}	

			$data["success"] = true;
			$data["count"] = count($query_result);

			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function giccrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$diagnosis_id	= $this->input->post('diagnosis_id');
			$resident_id	= $this->input->post('resident_id');
			$date 			= date('Y-m-d',strtotime($this->input->post('date')));
			$remarks		= strip_tags(trim($this->input->post('remarks')));
			$ttype			= $this->input->post('ttype');
			$status			= $this->input->post('gic_status');
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "delete from residents_profile_care_plan where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$commandText = "SELECT a.id FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1 AND b.resident_id = $resident_id";
				$result = $this->db->query($commandText);
				$query_profile_id = $result->result(); 
				$profile_id = $query_profile_id[0]->id;

				$this->load->model('residents_profile_care_plan');

				if ($type == "Add")
					$id = 0;
				if ($type == "Edit") 				
					$this->residents_profile_care_plan->id = $id;
				
				$this->residents_profile_care_plan->profile_id 				= $profile_id;
				$this->residents_profile_care_plan->diagnose_id 			= $diagnosis_id;
				$this->residents_profile_care_plan->remarks 				= $remarks;
				$this->residents_profile_care_plan->date 					= $date;
				$this->residents_profile_care_plan->created_by				= $this->session->userdata('id');
				$this->residents_profile_care_plan->date_created			= date('Y-m-d');
				$this->residents_profile_care_plan->type					= $ttype;
				$this->residents_profile_care_plan->status					= $status;
				$this->residents_profile_care_plan->save($id);

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'residents_profile_care_plan', $type, $this->modulename('Label').' CARE PLAN');
			}
			
			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}

	public function gicView()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');

			$commandText = "select * from residents_profile_care_plan where id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['remarks'] 	= $value->remarks;					
				$record['date'] 	= $value->date;
				$record['gic_status'] = $value->status;
			}

			$data['data'] = $record;
			$data['success'] = true;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function generatedata()
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			#adl
			$commandText = "SELECT 
								b.description,
								b.independent AS findependent,
								b.assist AS fassist,
								b.dependent AS fdependent,
								COUNT(a.independent) AS independent,
								COUNT(a.assist) AS assist,
								COUNT(a.dependent) AS dependent
							FROM adl b
								LEFT JOIN (SELECT * FROM residents_profile_adl) a ON a.adl_id = b.id
							GROUP BY b.description
							ORDER BY a.id ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$data['description'][] 	= $value->description;
				$data['independent'][] 	= $value->independent;
				$data['assist'][] 		= $value->assist;
				$data['dependent'][] 	= $value->dependent;
				$data['findependent'][] = $value->findependent;
				$data['fassist'][] 		= $value->fassist;
				$data['fdependent'][] 	= $value->fdependent;
			}
			$data['adl_count'] = count($query_result);

			#census
			$commandText = "SELECT 
								a.*,								
								b.count_census
							FROM resident_census a 
								LEFT JOIN (SELECT *, COUNT(census_id) AS count_census FROM resident_profile_census GROUP BY census_id) b ON a.id = b.census_id							
							ORDER BY a.id ASC";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$data['census_description'][] = $value->description;
				$data['census_type'][] = $value->type;
				$data['count_census'][] = $value->count_census;				
			}
			$data['census_count'] = count($query_result);

			$data['success'] = true;
			
			return $data;
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportcensus()
	{
		$this->load->model('Session');$this->Session->Validate();
		
		$response = array();
        $response['success'] = true;
 
        // $this->load->model('Logs'); $this->Logs->audit_logs($id, 'residents_profile', 'Report', 'Resident Profile');
        $commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, 'Resident Census and Condition of Residents', 'residents_profile', ".$this->session->userdata('id').", 'Report', '".date('Y-m-d')."', '".date('H:i:s')."')";
		$result = $this->db->query($commandText);

    	$response['filename'] = $this->exportpdfResidentCensus_ConditionofResidents($this->generatedata());
		die(json_encode($response));
	}

	public function exportpdfResidentCensus_ConditionofResidents($data)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$pdf->SetFont('freeserif', '', 12, '', false);
			$fDate = date("Ymd_His"); 
			$filename = "ResidentCensus_ConditionofResidents".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('ResidentProfile');
			$pdf->SetSubject('ResidentProfile');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
			
			$html = '

			<div style="font-size:24px;">
				DEPARTMENT OF HEALTH AND HUMAN SERVICES<br>
				CENTERS FOR MEDICARE & MEDICAID SERVICES
			</div>

			<table cellpadding="2">
				<tr style="font-size:40px;">
				  <td colspan="5" border="1" align="center">RESIDENT CENSUS AND CONDITIONS OF RESIDENT</td>
				</tr>

				<tr style="font-size:24px;">
				  <td border="1">Provider No.<br>...</td>
				  <td border="1">Medicare (F75)<br></td>
				  <td border="1">Medicaid (F76)<br></td>
				  <td border="1">Other (F77)<br></td>
				  <td border="1">Total Residents (F78)<br>...</td>
				</tr>

				<tr style="font-size:24px;">
				  <td width="20%" border="1" colspan="2"><b>ADL</b></td>
				  <td width="27%" border="1"><b>Independent</b></td>
				  <td width="27%" border="1"><b>Assist of One or Two Staff</b></td>
				  <td width="26%" border="1"><b>Dependent</b></td>
				</tr>';

			for ($i=0; $i < $data['adl_count']; $i++) 
			{ 
				$independent = "";
				if($data['independent'][$i]) $independent = '('.$data['independent'][$i].')';

				$assist = "";
				if($data['assist'][$i]) $assist = '('.$data['assist'][$i].')';

				$dependent = "";
				if($data['dependent'][$i]) $dependent = '('.$data['dependent'][$i].')';

				$html .='
					<tr style="font-size:24px;">
					  <td width="20%" border="1" colspan="2">'.$data['description'][$i].' </td>
					  <td width="27%" border="1">'.$data['findependent'][$i].' '.$independent.'</td>
					  <td width="27%" border="1">'.$data['fassist'][$i].' '.$assist.'</td>
					  <td width="26%" border="1">'.$data['fdependent'][$i].' '.$dependent.'</td>
					</tr>';
			}

			$html .='
			</table>

			<table cellpadding="4">
				<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>A. Bowel/Bladder Status</b><br></td>
							</tr>';

						$censusCount = 0;
						while($data['census_type'][$censusCount] == 'A')
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>B. Mobility</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'B')
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
			  	</tr>

			  	<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>C. Mental Status<br>F108-114 - indicate the number of residents with:</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'C')
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>D. Skin Integrity<br>F115-118 - indicate the number of residents with:</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'D')
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
			  	</tr>

			  	<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>E. Special Care<br>F119-132 - indicate the number of residents receiving:</b><br></td>
							</tr>';
						
						$censusCountTemp = $censusCount;
						$x = 0;
						while($data['census_type'][$censusCountTemp] == 'E'){
							$x++; $censusCountTemp++;
						}
						$x /=2; 
						
						while($data['census_type'][$censusCount] == 'E' & $x>0)
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
							$x--;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'E')
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
			  	</tr>		  	
			  	<tr>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>F. Medications<br>F133-139 - indicate the number of residents receiving:</b><br></td>
							</tr>';
						
						while($data['census_type'][$censusCount] == 'F')
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
						}
						
						$html .='</table>
					  </td>
					  <td width="50%" border="1">
					  	<table>
							<tr style="font-size:30px;">
							  <td colspan="2"><b>G. Other</b><br></td>
							</tr>';

						while($data['census_type'][$censusCount] == 'G')
						{
							$census = "";
							if($data['count_census'][$censusCount]) $census = '('.$data['count_census'][$censusCount].')';

							$html .= '<tr style="font-size:24px;">
							  <td width="5%">'.$census.'</td>
							  <td width="95%">'.$data['census_description'][$censusCount].'</td>
							</tr>';	
							$censusCount++;
							if($censusCount == $data['census_count']) break;
						}
						
						$html .='</table>
					  </td>
			  	</tr>

			  	<tr><td></td><td></td></tr>

			</table>

			<table cellpadding="4">
				<tr style="font-size:24px;">
					<td border="1" width="50%">Signature of Person Completing the Form<br><br><br></td>
					<td border="1" width="30%">Title</td>
					<td border="1" width="20%">Date</td>
				</tr>
			</table>

			<div style="font-size:30px;">TO BE COMPLETED BY SURVEY TEAM</div>

			<table>
				<tr style="font-size:24px;">
					<td width="5%">F146</td>
					<td width="50%">Was ombudsman office notified prior to survey?</td>
					<td width="10%">___ YES</td>
					<td width="10%">___ NO</td>
				</tr>
				<tr style="font-size:24px;">
					<td>F147</td>
					<td>Was ombudsman present during any portion of the survey?</td>
					<td>___ YES</td>
					<td>___ NO</td>
				</tr>
				<tr style="font-size:24px;">
					<td>F148</td>
					<td>Medication error rate _______%</td>
					<td>___ YES</td>
					<td>___ NO</td>
				</tr>
			</table>

			<br><br><br>';

			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	
}