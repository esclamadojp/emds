<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userinformation extends CI_Controller {
	/**
	*/
	public function index(){
		$this->load->model('Page');
        $this->Page->set_page('userinformation');
	}

	public function stafflist()
	{ 
		try 
		{
			$query = addslashes(strip_tags(trim($_GET['query'])));

			die(json_encode($this->generatestafflist($query, 'Grid')));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generatestafflist($query, $transaction_type)
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
				
			$limitQuery = "";
			if($transaction_type == 'Grid')
			{
				$limit = $_GET['limit'];
				$start = $_GET['start'];
				$limitQuery = " LIMIT $start, $limit";
			}

			$commandText = "SELECT 
								a.id,
								CONCAT(lname, ', ', fname, ' ',mname) AS name,
								b.description AS department_desc,
								c.description AS position_desc	
							FROM staff a
								LEFT JOIN departments b ON a.department_id = b.id
								LEFT JOIN positions c ON a.position_id = c.id
							WHERE 
							(
								a.fname like '%$query%' or 
					    		a.mname like '%$query%' or
					    		a.lname like '%$query%' or
					    		b.description like '%$query%' or
					    		c.description like '%$query%'
				    		 ) and a.active = 1
							ORDER BY lname ASC
							$limitQuery";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$commandText = "SELECT count(a.id) AS count
							FROM staff a
								LEFT JOIN departments b ON a.department_id = b.id
								LEFT JOIN positions c ON a.position_id = c.id
							WHERE 
							(
								a.fname like '%$query%' or 
					    		a.mname like '%$query%' or
					    		a.lname like '%$query%' or
					    		b.description like '%$query%' or
					    		c.description like '%$query%'
				    		 ) and a.active = 1";
			$result = $this->db->query($commandText);
			$query_count = $result->result(); 

			if(count($query_result) == 0 & $transaction_type == 'Report') 
			{
				$data = array("success"=> false, "data"=>'No records found!');
				die(json_encode($data));
			}	
			if(count($query_result) == 0 & $transaction_type == 'Grid') 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}	

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 				=> $value->id,
					'name'				=> strtoupper($value->name),
					'position_desc' 	=> $value->position_desc,
					'department_desc'	=> $value->department_desc);
			}

			$data['totalCount'] = $query_count[0]->count;
			return $data;

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function exportdocument()
	{
		$query 	= addslashes(strip_tags(trim($this->input->post('query'))));
		$type 	=  $this->input->post('type');

		$response = array();
        $response['success'] = true;

        if($type == "PDF")
        	$response['filename'] = $this->exportpdfStaffList($this->generatestafflist($query, 'Report'));
        else         	
			$response['filename'] = $this->exportexcelStaffList($this->generatestafflist($query, 'Report'));
		$this->load->model('Logs'); $this->Logs->audit_logs(0, 'staff', 'Report-'.$type, 'Staff List');        	
		die(json_encode($response));
	}

	public function exportexcelStaffList($data)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "StaffList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("StaffList")
					      ->setSubject("Report")
					      ->setDescription("Generating StaffList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B7:F7')->getFont()->setBold(true);

			#Duplicate Cell Styles
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('B8'), 'B8:B'.($data['totalCount']+7));
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('E8'), 'E8:E'.($data['totalCount']+7));

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B5", 'Staff List');
			###DATE

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B7", "No.")		
					      ->setCellValue("C7", "Name")
					      ->setCellValue("D7", "Position")
					      ->setCellValue("E7", "Department");

			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+8), ($i+1))		
						      ->setCellValue("C".($i+8), $data['data'][$i]['name'])
						      ->setCellValue("D".($i+8), $data['data'][$i]['position_desc'])
						      ->setCellValue("E".($i+8), $data['data'][$i]['department_desc']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($i+15).':L11'.($i+16))->getFont()->setSize(8); 
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+15), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+16), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfStaffList($data)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "StaffList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('StaffList');
			$pdf->SetSubject('StaffList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
		
			$html  = 'Staff List<br><br>
			<table border=1>
				<tr style="font-weight:bold;font-size:24px;">
				  <td width="5%"  style="background: black; padding: 10px;" align="center"><font face="Arial" >No.</font></td>
				  <td width="25%"  style="background: black; padding: 10px;" align="left"><font face="Arial" >Name</font></td>
				  <td width="20%"  style="background: black; padding: 10px;" align="left"><font face="Arial" >Position</font></td>
				  <td width="20%"  style="background: black; padding: 10px;" align="left"><font face="Arial" >Department</font></td>
				</tr>';

			for ($i = 0; $i<$data['totalCount'];$i++)
			{
				if($i%2 == 0)
				{
					$html .= '<tr style="background-color:#f7f6f6;font-size:24px;">
					  <td align="center"><font face="Arial" >'.($i+1).'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['name'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['position_desc'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['department_desc'].'</font></td>
					</tr>';
				}
				else
				{
					$html .= '<tr style="font-size:24px;">
					  <td align="center"><font face="Arial" >'.($i+1).'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['name'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['position_desc'].'</font></td>
					  <td align="left"><font face="Arial" >'.$data['data'][$i]['department_desc'].'</font></td>
					</tr>';
				}
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}