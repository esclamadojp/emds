<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermanagement extends CI_Controller {
	/**
	*/
	public function index(){
		$this->load->model('Page');
        $this->Page->set_page('usermanagement');
	}

	public function userslist()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$query = addslashes(strip_tags(trim($_GET['query'])));

			$limit = $_GET['limit'];
			$start = $_GET['start'];

			$commandText = "select 
							    a.id,
							    a.username,
							    a.staff_id,
							    lname,
							    mname,
							    fname,
							    concat(lname, ', ', fname, ' ', mname) AS name
							from users a LEFT JOIN staff b on a.staff_id = b.id
							where a.username like '%$query%' 
							    or a.staff_id like '%$query%'
							    or b.fname like '%$query%'
							    or b.mname like '%$query%'
							    or b.lname like '%$query%'  
							order by a.id desc
							LIMIT $start, $limit";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$commandText = "select count(a.id) as count
							from users a LEFT JOIN staff b on a.staff_id = b.id
							where a.username like '%$query%' 
							    or a.staff_id like '%$query%'
							    or b.fname like '%$query%'
							    or b.mname like '%$query%'
							    or b.lname like '%$query%'";
			$result = $this->db->query($commandText);
			$query_count = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 		=> $value->id,
					'username' 	=> $value->username,
					'module_users'=> strtoupper($value->name),
					'name' 		=> strtoupper($value->name));
			}

			$data['totalCount'] = $query_count[0]->count;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function usercrud() 
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id			= $this->input->post('id');
			$user_name	= strip_tags(trim($this->input->post('user_name')));
			$password	= strip_tags(trim($this->input->post('password')));
			$password2	= strip_tags(trim($this->input->post('password2')));
			$staff_id	= $this->input->post('staff_id');
			$admin		= $this->input->post('admin');
			$type		= $this->input->post('type');
			
			if ($type == "Delete")
			{
				$commandText = "delete from users where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{			
				if($password != $password2)	
				{
					$data = array("success"=> false, "data"=>"Password mismatch!");
					die(json_encode($data));
				}

				$this->load->model('Users');
				if ($type == "Add") 
				{
					$commandText = "select * from users where username = '".addslashes($user_name)."'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$id = 0;
				}
				if ($type == "Edit") 				
				{
					$commandText = "select * from users where username = '".addslashes($user_name)."' and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$this->Users->id = $id;
				}

				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"User name already exist.");
					die(json_encode($data));
				}

				$this->load->model('Cipher');
				$this->Cipher->secretpassphrase();
				$encryptedtext = $this->Cipher->encrypt($password);

				$this->Users->username 		= $user_name;
				$this->Users->password 		= $encryptedtext;
				$this->Users->staff_id 		= $staff_id;
				$this->Users->admin 		= $admin;
				$this->Users->save($id);	

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'users', $type, '(User)');
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function userview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id = $this->input->post('id');
			
			$commandText = "select 
							    a.*,
							    concat(lname, ', ', fname, ' ', mname) AS name     
							from users a LEFT JOIN staff b on a.staff_id = b.id
							where a.id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			$this->load->model('Cipher');
			$this->Cipher->secretpassphrase();			

			foreach($query_result as $key => $value) 
			{	
				$decryptedtext = $this->Cipher->decrypt($value->password);
				
				$record['id'] 			= $value->id;					
				$record['user_name']	= $value->username;	
				$record['password']		= $decryptedtext;	
				$record['password2']	= $decryptedtext;	
				$record['staff_id'] 	= $value->staff_id;	
				$record['admin'] 		= $value->admin;	
				$record['name']			= strtoupper($value->name);
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}					

	public function modulelist()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$commandText = "select * from modules where type = 'main' order by sno asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}

			foreach($query_result as $key => $value) 
			{	
				$data['data'][] = array(
					'id' 			=> $value->id,
					'sno' 			=> $value->sno,
					'module_name' 	=> $value->module_name);
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function modulecrud() 
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id			= $this->input->post('id');
			$module_name= strip_tags(trim($this->input->post('module_name')));
			$sno		= $this->input->post('sno');
			$type		= $this->input->post('type');
			
			if ($type == "Delete")
			{
				$commandText = "select * from modules where parent_id = '$id'";
				$result = $this->db->query($commandText);
				$query_result = $result->result(); 
				
				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Cannot delete record with dependence.");
					die(json_encode($data));
				}
				
				$commandText = "delete from modules where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$this->load->model('Modules');
				if ($type == "Add") 
				{
					$commandText = "select * from modules where module_name = '".addslashes($module_name)."'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$commandText = "select * from modules where sno = '$sno' and type = 'main'";
					$result = $this->db->query($commandText);
					$query_result1 = $result->result(); 

					$id = 0;
				}
				if ($type == "Edit") 				
				{
					$commandText = "select * from modules where module_name = '".addslashes($module_name)."' and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$commandText = "select * from modules where sno = '$sno' and type = 'main' and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result1 = $result->result(); 

					$this->Modules->id = $id;
				}

				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Module name already exist.");
					die(json_encode($data));
				}

				if(count($query_result1) > 0) 
				{
					$data = array("success"=> false, "data"=>"Order number already exist.");
					die(json_encode($data));
				}

				$this->Modules->parent_id 	= null;
				$this->Modules->sno 		= $sno;
				$this->Modules->module_name = $module_name;
				$this->Modules->link 		= null;
				$this->Modules->type 		= 'main';
				$this->Modules->save($id);	

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'modules', $type, '(Module)');
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function moduleview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id = $this->input->post('id');

			$commandText = "select * from modules where id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['id'] 			= $value->id;					
				$record['sno'] 			= $value->sno;					
				$record['module_name'] 	= $value->module_name;
			}

			$data['data'] = $record;
			$data['success'] = true;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function submodulelist()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$module_id = $_GET['module_id'];

			$commandText = "select * from modules where parent_id = '$module_id' order by sno asc";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) == 0) 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}

			foreach($query_result as $key => $value) 
			{	
				if ($value->thumbnail == 1) $thumbnail = '<font color="green">Yes</font>';
				else $thumbnail = '<font color="red">No</font>';

				if ($value->menu == 1) $menu = '<font color="green">Yes</font>';
				else $menu = '<font color="red">No</font>';

				$data['data'][] = array(
					'id' 			=> $value->id,
					'sno' 			=> $value->sno,
					'parent_id' 	=> $value->parent_id,
					'module_name' 	=> $value->module_name,
					'link' 			=> $value->link,
					'icon' 			=> $value->icon,
					'thumbnail'		=> $value->thumbnail,
					'menu' 			=> $value->menu);
			}

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	

	public function submodulecrud() 
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id			= $this->input->post('id');
			$parent_id	= $this->input->post('parent_id');
			$sno		= $this->input->post('sno');
			$module_name= strip_tags(trim($this->input->post('module_name')));
			$link		= strip_tags(trim($this->input->post('link')));
			$icon		= strip_tags(trim($this->input->post('icon')));
			$menu		= $this->input->post('ckmenu');
			$thumbnail	= $this->input->post('ckthumbnail');
			$type		= $this->input->post('type');
			
			if ($type == "Delete")
			{
				$commandText = "delete from modules where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$this->load->model('Modules');
				if ($type == "Add") 
				{
					$commandText = "select * from modules where module_name = '".addslashes($module_name)."'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$commandText = "select * from modules where link = '".addslashes($link)."'";
					$result = $this->db->query($commandText);
					$query_result1 = $result->result(); 

					$commandText = "select * from modules where sno = $sno and parent_id = $parent_id";
					$result = $this->db->query($commandText);
					$query_result2 = $result->result(); 

					$id = 0;
				}
				if ($type == "Edit") 				
				{
					$commandText = "select * from modules where module_name = '".addslashes($module_name)."' and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 

					$commandText = "select * from modules where link = '".addslashes($link)."' and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result1 = $result->result(); 

					$commandText = "select * from modules where sno = $sno and parent_id = $parent_id and id <> '$id'";
					$result = $this->db->query($commandText);
					$query_result2 = $result->result(); 

					$this->Modules->id = $id;
				}

				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"Sub module name already exist.");
					die(json_encode($data));
				}

				if(count($query_result1) > 0) 
				{
					$data = array("success"=> false, "data"=>"Link name already exist.");
					die(json_encode($data));
				}

				if(count($query_result2) > 0) 
				{
					$data = array("success"=> false, "data"=>"Order number already exist.");
					die(json_encode($data));
				}

				$this->Modules->parent_id 	= $parent_id;
				$this->Modules->sno 		= $sno;
				$this->Modules->module_name = $module_name;
				$this->Modules->link 		= $link;
				$this->Modules->icon 		= $icon;
				$this->Modules->type 		= 'sub';
				if($menu) $this->Modules->menu 		= $menu;				
				if($thumbnail) $this->Modules->thumbnail	= $thumbnail;				
				$this->Modules->save($id);	

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'modules', $type, '(Sub Module)');
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function submoduleview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id = $this->input->post('id');

			$commandText = "select * from modules where id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$data = array();
			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['id'] 			= $value->id;					
				$record['sno'] 			= $value->sno;
				$record['module_name'] 	= $value->module_name;
				$record['link'] 		= $value->link;
				$record['icon'] 		= $value->icon;
				$record['ckmenu'] 		= $value->menu;
				$record['ckthumbnail']	= $value->thumbnail;
			}

			$data['data'] = $record;
			$data['success'] = true;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}		

	public function moduleuserslist()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$query = addslashes(strip_tags(trim($_GET['query'])));
			$module_id = $_GET['module_id'];

			$limit = $_GET['limit'];
			$start = $_GET['start'];

			$commandText = "select 
							    a.*,
							    b.username,
							    concat(lname, ', ', fname, ' ', mname) AS name
							from (modules_users a LEFT JOIN users b on a.user_id = b.id)
							    LEFT JOIN staff c on c.id = b.staff_id
							where (b.username like '%$query%' 
							    or c.id like '%$query%'
							    or c.fname like '%$query%'
							    or c.mname like '%$query%'
							    or c.lname like '%$query%')
							    and a.module_id = $module_id 
							order by lname asc, fname asc
							LIMIT $start, $limit";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$commandText = "select count(*) as count
							from (modules_users a LEFT JOIN users b on a.user_id = b.id)
							    LEFT JOIN staff c on c.id = b.staff_id
							where (b.username like '%$query%' 
							    or c.id like '%$query%'
							    or c.fname like '%$query%'
							    or c.mname like '%$query%'
							    or c.lname like '%$query%')
							    and a.module_id = $module_id";
			$result = $this->db->query($commandText);
			$query_count = $result->result();

			if(count($query_result) == 0) 
			{
				$data["totalCount"] = 0;
				$data["data"] 		= array();
				die(json_encode($data));
			}

			foreach($query_result as $key => $value) 
			{	
				if($value->uadd) $add = '<font color="green">Yes</font>'; else $add = '<font color="red">No</font>';
				if($value->uedit) $edit = '<font color="green">Yes</font>'; else $edit = '<font color="red">No</font>';
				if($value->udelete) $delete = '<font color="green">Yes</font>'; else $delete = '<font color="red">No</font>';

				$data['data'][] = array(
					'id' 		=> $value->id,
					'username' 	=> $value->username,
					'name' 		=> strtoupper($value->name),
					'add' 		=> $value->uadd,
					'edit' 		=> $value->uedit,
					'delete' 	=> $value->udelete);
			}

			$data['totalCount'] = $query_count[0]->count;

			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function moduleusercrud() 
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id			= $this->input->post('id');
			$module_id	= $this->input->post('module_id');
			$user_id	= $this->input->post('user_id');
			$uadd		= $this->input->post('ckadd');
			$uedit		= $this->input->post('ckedit');
			$udelete	= $this->input->post('ckdelete');
			$type		= $this->input->post('type');

			if ($type == "Delete")
			{
				$commandText = "delete from modules_users where id = $id";
				$result = $this->db->query($commandText);
			}
			else
			{					
				$this->load->model('Modules_Users');
				if ($type == "Add") 
				{
					$commandText = "select * from modules_users where module_id = $module_id and user_id = $user_id";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$id = 0;
				}
				if ($type == "Edit") 				
				{
					$commandText = "select * from modules_users where module_id = $module_id and user_id = $user_id and id <> $id";
					$result = $this->db->query($commandText);
					$query_result = $result->result(); 
					$this->Modules_Users->id = $id;
				}	

				if(count($query_result) > 0) 
				{
					$data = array("success"=> false, "data"=>"User already exist.");
					die(json_encode($data));
				}

				$this->Modules_Users->module_id = $module_id;
				$this->Modules_Users->user_id 	= $user_id;
				if($uadd) $this->Modules_Users->uadd 		= $uadd;
				if($uedit) $this->Modules_Users->uedit		= $uedit;
				if($udelete) $this->Modules_Users->udelete 	= $udelete;
				$this->Modules_Users->save($id);	

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'modules_users', $type, '(Module User)');
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function moduleuserview()
	{
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();
			
			$id = $this->input->post('id');
			
			$commandText = "SELECT 
							    a.*,
							    b.id AS user_id,
							    b.username,
							    b.staff_id, 
							    CONCAT(lname, ', ', fname, ' ', mname) AS name							    
							FROM modules_users a 
							    LEFT JOIN users b ON a.user_id = b.id
							    LEFT JOIN staff c ON c.id = b.staff_id
							WHERE a.id = $id";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			$record = array();

			foreach($query_result as $key => $value) 
			{	
				$record['user_id']	= $value->user_id;	
				$record['user_name'] = strtoupper($value->name);	
				$record['ckadd']	= $value->uadd;	
				$record['ckedit']	= $value->uedit;	
				$record['ckdelete']	= $value->udelete;	
			}

			$data['data'] = $record;
			$data['success'] = true;
			die(json_encode($data));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}	
}