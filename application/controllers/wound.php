<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wound extends CI_Controller {
	/**
	*/
	private function modulename($type)
	{		
		if($type == 'link')
			return 'wound';
		else 
			return 'Wound';
	}

	public function index(){
		$this->load->model('Page');
        $this->Page->set_page($this->modulename('link'));  
	}

	public function woundlist(){ 
		try 
		{			
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$query 	= addslashes(strip_tags(trim($_GET['query'])));
			$resident_id = $_GET['resident_id'];
			$date_from 	= date('Y-m-d',strtotime($_GET['date_from']));
			$date_to 	= date('Y-m-d',strtotime($_GET['date_to']));
			$type 	= $_GET['type'];

			$dateCondition = "";
			if($type == 'Name') 
			{
				$type = ' b.resident_id = '.$resident_id.' and ';
				$dateCondition = " 
								AND a.date >= (SELECT admission_date FROM residents_medical_records WHERE resident_id = ".$resident_id." AND STATUS = 1 ORDER BY admission_date ASC LIMIT 1)							
								AND a.date <= (SELECT IF(discharge_date = NULL, discharge_date, ADDDATE(CURDATE(), 180)) AS discharge_date FROM residents_medical_records WHERE resident_id = ".$resident_id." AND STATUS = 1 ORDER BY discharge_date ASC LIMIT 1)";
			}
			else $type = null;

			$commandText = "SELECT  
								a.id,
								b.resident_id,
								b.id AS wound_id,
								a.date,
								a.healed_date,
								a.wound_no,
								a.healed,
								CONCAT (c.lname, ', ', c.fname) AS name,
								d.description AS wound_type_desc,
								e.description AS infection_desc,
								f.description AS site_desc,
								g.description AS room,								
								h.description AS orig_stage_desc,
								i.description AS high_stage_desc,
								j.description AS stage_desc,
								a.treatment
							FROM wound_details a
								LEFT JOIN wound_header b ON a.wound_id = b.id
								LEFT JOIN residents c ON b.resident_id = c.id	
								LEFT JOIN wound_types d ON b.wound_type_id = d.id
								LEFT JOIN infections e ON b.infection_id = e.id
								LEFT JOIN sites f ON b.site_id = f.id
								LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) g ON a.room_id = g.id
								LEFT JOIN stages h ON a.orig_stage_id = h.id
								LEFT JOIN stages i ON a.high_stage_id = i.id
								LEFT JOIN stages j ON a.stage_id = j.id
							WHERE $type
								(a.date between '$date_from' and '$date_to') and 
								(
									d.description like '%$query%' or
									e.description like '%$query%' or
									f.description like '%$query%' or
									g.description like '%$query%' or
									h.description like '%$query%' or
									i.description like '%$query%' or 
									j.description like '%$query%'
								)
								AND a.active = 1 and b.active = 1
								$dateCondition
							ORDER BY wound_no ASC, a.date DESC";
			$result = $this->db->query($commandText);
			$query_result = $result->result();  

			$xCount = 1; 

			foreach($query_result as $key => $value) 
			{	
				if ($value->healed)
				{
					$wound_no 	= '<font color="gray">'.$value->wound_no.'</font>';
					$room 		= '<font color="gray">'.$value->room.'</font>';
					$healed 	= '<font color="gray">YES</font>';
					$ohealed 	= 'Yes';
					$healed_date = '<font color="gray">'.date('m/d/Y',strtotime($value->healed_date)).'</font>';
					$date 		= '<font color="gray">'.date('m/d/Y',strtotime($value->date)).'</font>';
					$orig_stage_desc = '<font color="gray">'.$value->orig_stage_desc.'</font>';
					$high_stage_desc = '<font color="gray">'.$value->high_stage_desc.'</font>';					
					$stage_desc = '<font color="gray">'.$value->stage_desc.'</font>';
					$healedLabel = '<font color=green> <b>Healed</b> ('.date('m/d/Y',strtotime($value->healed_date)).')</font>';
					
				}	
				else
				{
					$wound_no 	= $value->wound_no;
					$room 		= $value->room;
					$healed 	= 'NO';
					$ohealed 	= 'No';
					$healed_date = null;
					$date 		= date('m/d/Y',strtotime($value->date));
					$orig_stage_desc = $value->orig_stage_desc;
					$high_stage_desc = $value->high_stage_desc;
					$stage_desc = $value->stage_desc;
					$healedLabel = "";
				}				

				if($type)
					$title = $value->wound_type_desc.' - '.$value->site_desc.' - '.$value->infection_desc.$healedLabel;
				else 
					$title = '('.$value->name.')'.$value->wound_type_desc.' - '.$value->site_desc.' - '.$value->infection_desc.$healedLabel;

				$data['data'][] 	= array(
					'id' 			=> $value->id,
					'resident_id'	=> $value->resident_id,
					'wound_id' 		=> $value->wound_id,
					'wound_no_text' => $wound_no,
					'wound_no'		=> $value->wound_no,					
					'title' 		=> $title,
					'room' 			=> $room,
					'healed'		=> $healed,
					'ohealed'		=> $ohealed,
					'healed_date'	=> $healed_date,
					'orig_stage_desc' => $orig_stage_desc,
					'high_stage_desc' => $high_stage_desc,
					'stage_desc' 	=> $stage_desc,
					'date' 			=> $date);
			}

			$data['count'] = count($query_result);

			die(json_encode($data));

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function woundcrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id				= $this->input->post('id');
			$wound_no  		= $this->input->post('wound_no');
			$wound_id  		= $this->input->post('wound_id');
			$resident_id	= $this->input->post('resident_id');
			$room_id		= $this->input->post('rooms');
			$wound_type_id	= $this->input->post('wound_types');
			$infection_id	= $this->input->post('infections');
			$dietary_supplements = $this->input->post('dietary_supplements');
			$risk_factors	= $this->input->post('risk_factors');
			$site_id		= $this->input->post('sites');
			$stage_id		= $this->input->post('stages');
			$exudate_id		= $this->input->post('exudates');
			$exudate_amount_id = $this->input->post('exudates_amount');
			$odor_id		= $this->input->post('odors');
			$wound_bed_id	= $this->input->post('wound_beds');
			$peri_wound_id	= $this->input->post('peri_wounds');
			$temperature_id = $this->input->post('temperatures');
			$treatment		= strip_tags(trim($this->input->post('treatment')));
			$date 			= $this->input->post('date');
			$slength 		= $this->input->post('slength');
			$swidth 		= $this->input->post('swidth');
			$sdepth			= $this->input->post('sdepth');
			$pain			= $this->input->post('pain');
			$tunneling_direction = $this->input->post('tunneling_direction');
			$tunneling_length = $this->input->post('tunneling_length');
			$undermining_direction = $this->input->post('undermining_direction');
			$undermining_length = $this->input->post('undermining_length');
			$remarks		= strip_tags(trim($this->input->post('remarks')));
			$surface_area	= $this->input->post('surface_area');
			$nurse_id		= $this->input->post('nurse');
			$physician_id	= $this->input->post('physicians');
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "UPDATE wound_details set active = 0 where id = $id";
				$result = $this->db->query($commandText);

				$commandText = "SELECT * FROM wound_details WHERE wound_id = (SELECT wound_id FROM wound_details WHERE id = 17) AND active = 1";
				$result = $this->db->query($commandText);
				$activeWound = $result->result(); 

				if(count($activeWound) == 0)
				{
					$commandText = "UPDATE wound_header set active = 0 where id = (SELECT wound_id FROM wound_details where id = $id)";
					$result = $this->db->query($commandText);
				}
					
				$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('".$this->modulename('Label')."', $id, 'wound_details', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
				$result = $this->db->query($commandText);
			}
			else
			{				
				$this->load->model('Wound_Details');
				if ($type == "Add") 
				{
					$commandText = "SELECT COUNT(*) AS count, id FROM wound_header WHERE resident_id = $resident_id AND infection_id = $infection_id AND wound_type_id = $wound_type_id and site_id = $site_id";
					$result = $this->db->query($commandText);
					$wound_header_result = $result->result(); 
					
					if($wound_header_result[0]->count == 0)
					{
						$this->load->model('Wound_Header');
						
						$this->Wound_Header->resident_id 	= $resident_id;					
						$this->Wound_Header->wound_type_id 	= $wound_type_id;
						$this->Wound_Header->infection_id 	= $infection_id;
						$this->Wound_Header->site_id 		= $site_id;							
						$this->Wound_Header->active 		= 1;							
						$this->Wound_Header->save(0);

						$commandText = "select id from wound_header order by id desc limit 1";
						$result = $this->db->query($commandText);
						$query_result = $result->result(); 
						$wound_id = $query_result[0]->id;	
					}
					else 
					{
						$wound_id = $wound_header_result[0]->id;
						$commandText = "UPDATE wound_header set active = 1 where id = $wound_id";
						$result = $this->db->query($commandText);						
					}

					#risk_factors
					$commandText = "delete from wound_risk_factors where wound_id = $wound_id";
					$result = $this->db->query($commandText);

					$this->load->model('Wound_Risk_Factors');
					$risk_factors=explode(',',$risk_factors);
					foreach($risk_factors as $key => $value) 
					{
						$this->Wound_Risk_Factors->wound_id 		= $wound_id;
						$this->Wound_Risk_Factors->risk_factor_id 	= $value;
						$this->Wound_Risk_Factors->save(0);	
					}				

					#dietary_supplements
					$commandText = "delete from wound_diet_supplements where wound_id = $wound_id";
					$result = $this->db->query($commandText);

					$this->load->model('wound_diet_supplements');
					$dietary_supplements=explode(',',$dietary_supplements);
					foreach($dietary_supplements as $key => $value) 
					{
						$this->wound_diet_supplements->wound_id 		= $wound_id;
						$this->wound_diet_supplements->supplement_id 	= $value;
						$this->wound_diet_supplements->save(0);	
					}
					$id = 0;
				}
				if ($type == "Edit") 				
					$this->Wound_Details->id = $id;

				if (!is_numeric($sdepth))
					$sdepth = null;
				if (!is_numeric($tunneling_length))
					$tunneling_length = null;
				if (!is_numeric($undermining_length))
					$undermining_length = null;
				if (!($tunneling_direction))
					$tunneling_direction = null;
				if (!($undermining_direction))
					$undermining_direction = null;
				if (!is_numeric($surface_area))
					$surface_area = null;

				$this->Wound_Details->wound_no 		= $wound_no;
				$this->Wound_Details->wound_id 		= $wound_id;
				$this->Wound_Details->exudate_id 	= $exudate_id;
				$this->Wound_Details->exudate_amount_id = $exudate_amount_id;
				$this->Wound_Details->odor_id 		= $odor_id;
				$this->Wound_Details->wound_bed_id 	= $wound_bed_id;
				$this->Wound_Details->peri_wound_id = $peri_wound_id;
				$this->Wound_Details->temperature_id = $temperature_id;
				$this->Wound_Details->stage_id 		= $stage_id;
				$this->Wound_Details->orig_stage_id = $stage_id;
				$this->Wound_Details->high_stage_id = $stage_id;
				$this->Wound_Details->room_id 		= $room_id;
				$this->Wound_Details->nurse_id 		= $nurse_id;
				$this->Wound_Details->physician_id 	= $physician_id;
				$this->Wound_Details->treatment 	= $treatment;
				$this->Wound_Details->date 			= date('Y-m-d',strtotime($date));
				$this->Wound_Details->slength 		= $slength;
				$this->Wound_Details->swidth 		= $swidth;				
				$this->Wound_Details->sdepth 		= $sdepth;		
				$this->Wound_Details->pain 			= $pain;				
				$this->Wound_Details->tunneling_direction = $tunneling_direction;
				$this->Wound_Details->tunneling_length = $tunneling_length;
				$this->Wound_Details->undermining_direction	= $undermining_direction;
				$this->Wound_Details->undermining_length = $undermining_length;
				$this->Wound_Details->remarks 		= $remarks;
				$this->Wound_Details->surface_area  = $surface_area;
				$this->Wound_Details->active  		= 1;
				$this->Wound_Details->save($id);
			}

			$commandText = "SELECT * FROM wound_details WHERE wound_id = $wound_id and wound_no = $wound_no";
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			if(count($query_result) > 0)
			{				
				#update original stage
				$commandText = "SELECT stage_id FROM wound_details WHERE wound_id = $wound_id and wound_no = $wound_no ORDER BY DATE ASC, id ASC LIMIT 1";
				$result = $this->db->query($commandText);
				$stage_id_result = $result->result(); 
				$orig_stage_id = $stage_id_result[0]->stage_id;

				$commandText = "UPDATE wound_details SET orig_stage_id = $orig_stage_id WHERE wound_id = $wound_id and wound_no = $wound_no";
				$result = $this->db->query($commandText);

				#update highest stage
				$commandText = "SELECT 
									stage_id
								FROM wound_details a 									
									LEFT JOIN stages b ON a.stage_id = b.id
								WHERE a.wound_id = $wound_id and a.wound_no = $wound_no
								ORDER BY b.order DESC
								LIMIT 1";
				$result = $this->db->query($commandText);
				$stage_id_result = $result->result(); 
				$high_stage_id = $stage_id_result[0]->stage_id;

				$commandText = "UPDATE wound_details SET high_stage_id = $high_stage_id WHERE wound_id = $wound_id and wound_no = $wound_no";
				$result = $this->db->query($commandText);

				$this->load->model('Logs'); $this->Logs->audit_logs($id, 'wound_details', $type, $this->modulename('Label'));
			}	
			
			$arr = array();  
			$arr['success'] = true;
			$arr['wound_id'] = $wound_id;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function headercrud() 
	{
		try 
		{ 
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$id  			= $this->input->post('wound_id');
			$resident_id	= $this->input->post('resident_id');
			$wound_type_id	= $this->input->post('wound_types');
			$infection_id	= $this->input->post('infections');
			$site_id		= $this->input->post('sites');
			$dietary_supplements = $this->input->post('dietary_supplements');
			$risk_factors	= $this->input->post('risk_factors');
			$healed			= $this->input->post('healed');
			$healed_date	= date('Y-m-d',strtotime($this->input->post('healed_date')));
			$wound_no		= $this->input->post('wound_no');
			$type			= $this->input->post('type');
			
			$this->load->model('Access'); $this->Access->rights($this->modulename('link'), $type, null);
			if ($type == "Delete")
			{
				$commandText = "UPDATE wound_header set active = 0 where id = $id";
				$result = $this->db->query($commandText);

				$commandText = "UPDATE wound_details set active = 0 where wound_id = $id";
				$result = $this->db->query($commandText);

				$commandText = "SELECT id FROM wound_details WHERE wound_id = $id";
				$result = $this->db->query($commandText);
				$query_result = $result->result();  

				foreach($query_result as $key => $value) 
				{						
					$commandText = "insert into audit_logs (transaction_type, transaction_id, entity, query_type, created_by, date_created, time_created) values ('".$this->modulename('Label')."', ".$value->id.", 'wound_details', 'Delete', ".$this->session->userdata('id').", '".date('Y-m-d')."', '".date('H:i:s')."')";
					$result = $this->db->query($commandText);
				}				
			}
			else
			{				
				$this->load->model('Wound_Header');
				$this->Wound_Header->id 			= $id;
				$this->Wound_Header->resident_id 	= $resident_id;
				$this->Wound_Header->wound_type_id  = $wound_type_id;
				$this->Wound_Header->infection_id  	= $infection_id;
				$this->Wound_Header->site_id  		= $site_id;
				$this->Wound_Header->active  		= 1;
				$this->Wound_Header->save($id);	

				if($healed)
				{					
					$commandText = "UPDATE wound_details set healed = $healed, healed_date ='$healed_date' WHERE wound_id = $id and wound_no = $wound_no";
					$result = $this->db->query($commandText);
				}
				else
				{
					$commandText = "UPDATE wound_details set healed = null, healed_date = null WHERE wound_id = $id and wound_no = $wound_no";
					$result = $this->db->query($commandText);
				}

				#risk_factors
				$commandText = "delete from wound_risk_factors where wound_id = $id";
				$result = $this->db->query($commandText);

				$this->load->model('Wound_Risk_Factors');
				$risk_factors=explode(',',$risk_factors);
				foreach($risk_factors as $key => $value) 
				{
					$this->Wound_Risk_Factors->wound_id 		= $id;
					$this->Wound_Risk_Factors->risk_factor_id 	= $value;
					$this->Wound_Risk_Factors->save(0);	
				}				

				#dietary_supplements
				$commandText = "delete from wound_diet_supplements where wound_id = $id";
				$result = $this->db->query($commandText);

				$this->load->model('wound_diet_supplements');
				$dietary_supplements=explode(',',$dietary_supplements);
				foreach($dietary_supplements as $key => $value) 
				{
					$this->wound_diet_supplements->wound_id 		= $id;
					$this->wound_diet_supplements->supplement_id 	= $value;
					$this->wound_diet_supplements->save(0);	
				}
			}

			$arr = array();  
			$arr['success'] = true;
			if ($type == "Add") 
				$arr['data'] = "Successfully Created";
			if ($type == "Edit")
				$arr['data'] = "Successfully Updated";
			if ($type == "Delete")
				$arr['data'] = "Successfully Deleted";
			die(json_encode($arr));
		}
		catch(Exception $e) 
		{
			$data = array("success"=> false, "data"=>$e->getMessage());
			die(json_encode($data));
		}
	}	

	public function headerview()
	{ 
		try 
		{
			$id = $this->input->post('id');
			$resident_id = $this->input->post('resident_id');
			$type = $this->input->post('type');

			die(json_encode($this->generateheaderview($id, $resident_id, $type)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateheaderview($id, $resident_id, $type)
	{		
		#update session
			$this->load->model('Session');$this->Session->Validate();

		$record = array();
		if($type == 'Edit')
		{			
			$commandText = "SELECT 	
								a.*,		
								CONCAT (b.lname, ', ', b.fname) AS name,									
								d.description AS wound_type_desc,
								e.description AS infection_desc,
								h.description AS site_desc,
								i.risk_factors_id,
								i.risk_factors_desc,
								j.dietary_supplements_id,
								j.dietary_supplements_desc
							FROM wound_header a
								LEFT JOIN residents b ON a.resident_id = b.id									
								LEFT JOIN wound_types d ON a.wound_type_id = d.id
								LEFT JOIN infections e ON a.infection_id = e.id
								LEFT JOIN sites h ON a.site_id = h.id
								LEFT JOIN (SELECT 
											a.wound_id,
											GROUP_CONCAT(risk_factor_id) risk_factors_id,
											GROUP_CONCAT(b.description) risk_factors_desc	
									   FROM wound_risk_factors a
										JOIN risk_factors b ON a.risk_factor_id = b.id
									   WHERE a.wound_id = $id) i ON a.id = i.wound_id
								LEFT JOIN (SELECT 
											a.wound_id,
											GROUP_CONCAT(supplement_id) dietary_supplements_id,
											GROUP_CONCAT(b.description) dietary_supplements_desc	
									   FROM wound_diet_supplements a
										JOIN dietary_supplements b ON a.supplement_id = b.id
									   WHERE a.wound_id = $id) j ON a.id = j.wound_id
							WHERE a.id = $id";		
			$result = $this->db->query($commandText);
			$query_result = $result->result(); 

			foreach($query_result as $key => $value) 
			{	
				$record['id'] 				= $value->id;					
				$record['name'] 			= $value->name;
				$record['wound_type_id'] 	= $value->wound_type_id;
				$record['wound_type_desc'] 	= $value->wound_type_desc;
				$record['infection_id']		= $value->infection_id;
				$record['infection_desc'] 	= $value->infection_desc;
				$record['site_id']			= $value->site_id;
				$record['site_desc']		= $value->site_desc;
				$record['risk_factors_id'] 	= $value->risk_factors_id;
				$record['risk_factors_desc'] = $value->risk_factors_desc;							
				$record['dietary_supplements_id'] = $value->dietary_supplements_id;
				$record['dietary_supplements_desc'] = $value->dietary_supplements_desc;							
			}			
		}

		#diagnosis
		$commandText = "SELECT 
							COUNT(a.diagnose_id) AS diagnose_count,
							b.description,
							GROUP_CONCAT(b.description SEPARATOR ' and ') as tooltip
						FROM residents_profile_diagnosis a
							LEFT JOIN diagnosis b ON a.diagnose_id = b.id
						WHERE a.profile_id = (SELECT a.id FROM residents_profile a
												LEFT JOIN residents_medical_records b ON a.resident_id = b.resident_id AND a.record_no = b.record_no
												WHERE b.status = 1 AND b.active = 1 AND b.resident_id = $resident_id)";
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		foreach($query_result as $key => $value) 
		{
			$data['diagnose_desc'] = $value->description;
			$data['diagnose_count'] = $value->diagnose_count;
			$data['diagnose_tip'] = $value->tooltip;
		}	

		$data['data'] = $record;
		$data['success'] = true;
		return $data;
	}

	public function detailview()
	{ 
		try 
		{
			$id = $this->input->post('id');
			$resident_id = $this->input->post('resident_id');
			$type = $this->input->post('type');

			die(json_encode($this->generatedetailview($id, $resident_id, $type)));
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generatedetailview($id, $resident_id, $type)
	{
		#update session
			$this->load->model('Session');$this->Session->Validate();
			
		if($type == 'Edit')
		$commandText = "SELECT 	
							a1.*,			
							i.description AS stage_desc,
							r.description AS orig_stage_desc,
							s.description AS high_stage_desc,
							j.description AS wound_bed_desc,
							l.description AS exudate_desc,
							m.description AS exudate_amount_desc,
							n.description AS odor_desc,	
							o.description AS peri_wound_desc,
							p.description AS temperature_desc,
							q.description AS room_desc,
							CONCAT(t.lname,', ',t.fname) AS nurse_desc,
							u.description AS physician_desc
						FROM wound_details a1 LEFT JOIN wound_header a ON a1.wound_id = a.id
							LEFT JOIN stages i ON a1.stage_id = i.id
							LEFT JOIN wound_beds j ON a1.wound_bed_id = j.id
							LEFT JOIN exudates l ON a1.exudate_id = l.id
							LEFT JOIN exudates_amount m ON a1.exudate_amount_id = m.id
							LEFT JOIN odors n ON a1.odor_id = n.id
							LEFT JOIN peri_wounds o ON a1.peri_wound_id = o.id
							LEFT JOIN temperatures p ON a1.temperature_id = p.id						
							LEFT JOIN (SELECT a.id, CONCAT(alias,'-',b.description,'-',a.description) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) q ON a1.room_id = q.id
							LEFT JOIN stages r ON a1.orig_stage_id = r.id
							LEFT JOIN stages s ON a1.high_stage_id = s.id
							LEFT JOIN staff t ON a1.nurse_id = t.id
							LEFT JOIN physicians u ON a1.physician_id = u.id
						WHERE a1.id = $id";
		else 
		$commandText = "SELECT 
								a.room_id,
								b.description room_desc
							FROM residents_profile a 
								LEFT JOIN (SELECT a.id, CONCAT(alias,'-',b.description,'-',a.description) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) b ON a.room_id = b.id
							WHERE a.resident_id = $resident_id";	
		$result = $this->db->query($commandText);
		$query_result = $result->result(); 

		$record = array();

		foreach($query_result as $key => $value) 
		{	

			if($type == 'Edit')
			{
				$record['id'] 			= $value->id;					
				$record['wound_no']		= $value->wound_no;	
				$record['treatment']	= $value->treatment;	
				$record['date']			= $value->date;	
				$record['slength']		= $value->slength;	
				$record['swidth']		= $value->swidth;	
				$record['sdepth']		= $value->sdepth;	
				$record['pain']			= $value->pain;
				$record['tunneling_direction'] = $value->tunneling_direction;
				$record['tunneling_length'] = $value->tunneling_length;
				$record['undermining_direction'] = $value->undermining_direction;
				$record['undermining_length'] = $value->undermining_length;
				$record['remarks']		= $value->remarks;
				$record['surface_area'] = $value->surface_area;
				$record['stage_id']		= $value->stage_id;
				$record['stage_desc']	= $value->stage_desc;
				$record['orig_stage_desc']	= $value->orig_stage_desc;
				$record['high_stage_desc']	= $value->high_stage_desc;
				$record['exudate_id']	= $value->exudate_id;
				$record['exudate_desc']	= $value->exudate_desc;
				$record['exudate_amount_id'] = $value->exudate_amount_id;
				$record['exudate_amount_desc'] = $value->exudate_amount_desc;
				$record['odor_id']		= $value->odor_id;
				$record['odor_desc']	= $value->odor_desc;
				$record['wound_bed_id']	= $value->wound_bed_id;
				$record['wound_bed_desc'] = $value->wound_bed_desc;
				$record['peri_wound_id'] = $value->peri_wound_id;
				$record['peri_wound_desc'] = $value->peri_wound_desc;
				$record['temperature_id'] = $value->temperature_id;
				$record['temperature_desc']	= $value->temperature_desc;
				$record['nurse_id'] 	= $value->nurse_id;
				$record['nurse_desc']	= $value->nurse_desc;
				$record['physician_id'] = $value->physician_id;
				$record['physician_desc']	= $value->physician_desc;
			}

			$record['room_id']		= $value->room_id;	
			$record['room_desc']	= $value->room_desc;
		}

		$data['data'] = $record;
		$data['success'] = true;
		return $data;
	}

	public function sitelist(){ 
		try 
		{
			#update session
			$this->load->model('Session');$this->Session->Validate();

			$wound_id 	= $_GET['wound_id'];
			$wound_no 	= $_GET['wound_no'];

			$commandText = "SELECT 
								a.*,								
								b.description AS stage_desc,
								c.description AS orig_stage_desc,
								d.description AS high_stage_desc,
								e.description AS wound_bed_desc,
								f.description AS room
							FROM wound_details a								
								LEFT JOIN stages b ON a.stage_id = b.id
								LEFT JOIN stages c ON a.orig_stage_id = c.id
								LEFT JOIN stages d ON a.high_stage_id = d.id
								LEFT JOIN wound_beds e ON a.wound_bed_id = e.id
								LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) f ON a.room_id = f.id
							WHERE wound_id = $wound_id AND wound_no = $wound_no AND a.active = 1
							ORDER BY a.date DESC";
			$result = $this->db->query($commandText);
			$query_result = $result->result();  

			$xCount = 1; 

			foreach($query_result as $key => $value) 
			{	
				if(is_null($value->sdepth))
					$sdepth = null;
				else 
					$sdepth = ', <b>D:</b>'.number_format($value->sdepth,1);

				if(is_null($value->tunneling_direction))
					$tunneling = null;	
				else 
					$tunneling = '<b>Direction:</b>'.$value->tunneling_direction.', <b>L:</b>'.number_format($value->tunneling_length,1).'cm';
				
				if(is_null($value->undermining_direction))
					$undermining = null;	
				else 
					$undermining = '<b>Direction:</b>'.$value->undermining_direction.', <b>L:</b>'.number_format($value->undermining_length,1).'cm';
 
 				if ($value->healed)
 					$healed_date = date('m/d/Y',strtotime($value->healed_date));
 				else 
 					$healed_date = null;

				$data['data'][] 	= array(
					'id' 			=> $value->id,
					'room' 			=> $value->room,
					'date' 			=> date('m/d/Y',strtotime($value->date)),					
					'stage_desc' 	=> $value->stage_desc,
					'orig_stage_desc' => $value->orig_stage_desc,
					'high_stage_desc' => $value->high_stage_desc,
					'healed' 		=> $value->healed,
					'healed_date' 	=> $healed_date,
					'size' 			=> '<b>L:</b>'.number_format($value->slength,1).', '.'<b>W:</b>'.number_format($value->swidth,1).$sdepth,
					'wound_bed_desc' => $value->wound_bed_desc,
					'tunneling' 	=> $tunneling,
					'undermining' 	=> $undermining,
					'treatment' 	=> $value->treatment,
					'remarks' 		=> $value->remarks);					
			}

			$data['count'] = count($query_result);

			die(json_encode($data));

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function generateWoundList($query, $date_from, $date_to)
	{
		#update session
		$this->load->model('Session');$this->Session->Validate();

		$commandText = "SELECT  
							a.date,
							g.description AS room,								
							CONCAT (c.lname, ', ', c.fname) AS name,
							d.description AS wound_type_desc,
							e.description AS infection_desc,
							f.description AS site_desc,
							a.wound_no,
							j.description AS stage_desc,
							a.slength,
							a.swidth,
							a.sdepth,
							a.tunneling_direction,
							a.tunneling_length,
							a.undermining_direction,
							a.undermining_length,
							a.surface_area,	
							k.description AS wound_bed_desc,
							a.treatment,
							h.description AS orig_stage_desc,
							i.description AS high_stage_desc,
							a.healed_date,
							a.healed,
							a.remarks
						FROM wound_details a
							LEFT JOIN wound_header b ON a.wound_id = b.id
							LEFT JOIN residents c ON b.resident_id = c.id	
							LEFT JOIN wound_types d ON b.wound_type_id = d.id
							LEFT JOIN infections e ON b.infection_id = e.id
							LEFT JOIN sites f ON b.site_id = f.id
							LEFT JOIN (SELECT a.id, CONCAT(a.description,'-',b.description,'-',alias) AS description FROM rooms a JOIN floors b ON a.cat_id = b.id JOIN buildings c ON b.cat_id = c.id) g ON a.room_id = g.id
							LEFT JOIN stages h ON a.orig_stage_id = h.id
							LEFT JOIN stages i ON a.high_stage_id = i.id
							LEFT JOIN stages j ON a.stage_id = j.id
							LEFT JOIN wound_beds k ON a.wound_bed_id = k.id
						WHERE (a.date between '$date_from' and '$date_to') and 
								(
									d.description like '%$query%' or
									e.description like '%$query%' or
									f.description like '%$query%' or
									g.description like '%$query%' or
									h.description like '%$query%' or
									i.description like '%$query%' or 
									j.description like '%$query%'
								)
								AND a.active = 1 AND b.active = 1
						ORDER BY c.lname ASC, wound_no ASC, a.date DESC";
		$result = $this->db->query($commandText);
		$query_result = $result->result();  

		if(count($query_result) == 0) 
		{
			$data = array("success"=> false, "data"=>'No records found!');
			die(json_encode($data));
		}	

		foreach($query_result as $key => $value) 
		{	
			if(is_null($value->tunneling_direction))
				$tunneling = null;	
			else 
				$tunneling = $value->tunneling_direction.' '.number_format($value->tunneling_length,1).'cm';
			
			if(is_null($value->undermining_direction))
				$undermining = null;	
			else 
				$undermining = $value->undermining_direction.' '.number_format($value->undermining_length,1).'cm';

			if ($value->healed)
				$healed 	= 'YES ('.date('m/d/Y',strtotime($value->healed_date)).')';
			else
				$healed 	= 'NO';

			$data['data'][] 	= array(
				'date' 			=> date('m/d/Y',strtotime($value->date)),					
				'room' 			=> $value->room,
				'name' 			=> strtoupper($value->name),
				'wound_type_desc' => $value->wound_type_desc,
				'infection_desc' => $value->infection_desc,
				'site_desc' 	=> $value->site_desc,
				'wound_no' 		=> $value->wound_no,
				'stage_desc' 	=> $value->stage_desc,
				'slength' 		=> number_format($value->slength,1),
				'swidth' 		=> number_format($value->swidth,1),
				'sdepth' 		=> number_format($value->sdepth,1),
				'tunneling' 	=> $tunneling,
				'undermining' 	=> $undermining,
				'surface_area' 	=> $value->surface_area,
				'wound_bed_desc' => $value->wound_bed_desc,
				'treatment' 	=> $value->treatment,
				'orig_stage_desc' => $value->orig_stage_desc,
				'high_stage_desc' => $value->high_stage_desc,
				'healed' 		=> $healed,
				'remarks' 		=> $value->remarks);					
		}

		$data['count'] = count($query_result);

		return $data;
	}

	public function exportdocument()
	{
		$this->load->model('Session');$this->Session->Validate();

		$report	=  $this->input->post('report');
		$response = array();
        $response['success'] = true;

        if($report == 'WoundList')
        {
			$query 		= addslashes(strip_tags(trim($this->input->post('query'))));
			$type 		=  $this->input->post('type');
        	$date_from 	=  $this->input->post('date_from');
        	$date_to 	=  $this->input->post('date_to');
        	
        	// $this->load->model('Logs'); $this->Logs->audit_logs(0, 'wound_details', 'Report-'.$type, $this->modulename('Label').' (From '.date('m/d/Y',strtotime($date_from)).' To '.date('m/d/Y',strtotime($date_to)).')');
        	$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values (0, '".$this->modulename('Label')."  (From ".date('m/d/Y',strtotime($date_from))." To ".date('m/d/Y',strtotime($date_to)).")', 'wound_details', ".$this->session->userdata('id').", 'Report-".$type."', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);
	        if($type == "PDF")
	        	$response['filename'] = $this->exportpdfWoundList($this->generateWoundList($query, $date_from, $date_to), $query, $date_from, $date_to);
	        else         	
				$response['filename'] = $this->exportexcelWoundList($this->generateWoundList($query, $date_from, $date_to), $query, $date_from, $date_to);
		}
		if($report == 'WoundNoReport')
		{
			$id 		=  $this->input->post('id');
			$wound_id 	=  $this->input->post('wound_id');
			$resident_id =  $this->input->post('resident_id');

        	// $this->load->model('Logs'); $this->Logs->audit_logs($id, 'wound_details', 'Report', $this->modulename('Label'));
        	$commandText = "insert into audit_logs (transaction_id, transaction_type, entity, created_by, query_type, date_created, time_created) values ($id, '".$this->modulename('Label')."', 'wound_details', ".$this->session->userdata('id').", 'Report', '".date('Y-m-d')."', '".date('H:i:s')."')";
			$result = $this->db->query($commandText);

        	$response['filename'] = $this->exportpdfWoundNoReport($this->generateheaderview($wound_id, $resident_id, 'Edit'), $this->generatedetailview($id, $resident_id, 'Edit'));
		}
		die(json_encode($response));
	}

	public function exportexcelWoundList($data, $query, $date_from, $date_to)
	{
		try 
		{
		 	// Starting the PHPExcel library
	        $this->load->library('PHPExcel');
	 
	        $objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(3);

			$fDate = date("Ymd_His");
			$filename = "WoundList".$fDate.".xls";

			$objPHPExcel->getProperties()->setCreator("Engr. Julius P. Esclamado")
					      ->setLastModifiedBy("user")
					      ->setTitle("WoundList")
					      ->setSubject("Report")
					      ->setDescription("Generating WoundList")
					      ->setKeywords("eMedical Data Solutions")
					      ->setCategory("Reports");

			#Dimensions
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(16);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(4);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(4);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(4);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(16);
			$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);

			$objPHPExcel->getActiveSheet()->mergeCells('J9:L9');

			#Font & Alignment
			$objPHPExcel->getActiveSheet()->getStyle('B9:U9')->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()->getStyle('B11:U11')->getAlignment()->setWrapText(true); 

			$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('B9:U10')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('H9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('J9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('M9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('N9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('O9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('R9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('S9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('T9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('J10:L10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle('H11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('M11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('N11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('O11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('R11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('S11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('T11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('J11:L11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle('B9:U9')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('J10:L10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('B11:U11')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('B12:U12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			#Duplicate Cell Styles
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('B12'), 'B12:U'.($data['count']+10));
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('H11'), 'H11:H'.($data['count']+10));
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('M11'), 'M11:O'.($data['count']+10));
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('R11'), 'R11:T'.($data['count']+10));
			$objPHPExcel->getActiveSheet()->duplicateStyle( $objPHPExcel->getActiveSheet()->getStyle('J11'), 'J11:L'.($data['count']+10));

			### Title
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B3", 'Wound List');
			###DATE

			if(!$query) $query = 'NULL';

			$objPHPExcel->setActiveSheetIndex(0)
					      ->setCellValue("B5", "Searched by:")		
					      ->setCellValue("B6", "Date From:")
					      ->setCellValue("B7", "Date To:")
					      ->setCellValue("C5", $query)
					      ->setCellValue("C6", date('m/d/Y',strtotime($date_from)))
					      ->setCellValue("C7", date('m/d/Y',strtotime($date_to)))
					      ->setCellValue("B9", "Date")
					      ->setCellValue("C9", "Room No.")
					      ->setCellValue("D9", "Name")
					      ->setCellValue("E9", "Kind of Wound")
					      ->setCellValue("F9", "Wound Origin")
					      ->setCellValue("G9", "Site")
					      ->setCellValue("H9", "Site No.")
					      ->setCellValue("I9", "Stage/Degree")
					      ->setCellValue("J9", "Size (cm)")
					      ->setCellValue("J10", "L")
					      ->setCellValue("K10", "W")
					      ->setCellValue("L10", "D")
					      ->setCellValue("M9", "Tunneling")
					      ->setCellValue("N9", "Undermining")
					      ->setCellValue("O9", "Burn (%)")
					      ->setCellValue("P9", "Wound Bed")
					      ->setCellValue("Q9", "Treatment")
					      ->setCellValue("R9", "Orignal Stage")
					      ->setCellValue("S9", "Highest Stage")
					      ->setCellValue("T9", "Healed")
					      ->setCellValue("U9", "Remarks");

			for ($i = 0; $i<$data['count'];$i++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
						      ->setCellValue("B".($i+11), $data['data'][$i]['date'])
						      ->setCellValue("C".($i+11), $data['data'][$i]['room'])
						      ->setCellValue("D".($i+11), $data['data'][$i]['name'])
						      ->setCellValue("E".($i+11), $data['data'][$i]['wound_type_desc'])
						      ->setCellValue("F".($i+11), $data['data'][$i]['infection_desc'])
						      ->setCellValue("G".($i+11), $data['data'][$i]['site_desc'])
						      ->setCellValue("H".($i+11), $data['data'][$i]['wound_no'])
						      ->setCellValue("I".($i+11), $data['data'][$i]['stage_desc'])
						      ->setCellValue("J".($i+11), $data['data'][$i]['slength'])
						      ->setCellValue("K".($i+11), $data['data'][$i]['swidth'])
						      ->setCellValue("L".($i+11), $data['data'][$i]['sdepth'])
						      ->setCellValue("M".($i+11), $data['data'][$i]['tunneling'])
						      ->setCellValue("N".($i+11), $data['data'][$i]['undermining'])
						      ->setCellValue("O".($i+11), $data['data'][$i]['surface_area'])
						      ->setCellValue("P".($i+11), $data['data'][$i]['wound_bed_desc'])
						      ->setCellValue("Q".($i+11), $data['data'][$i]['treatment'])
						      ->setCellValue("R".($i+11), $data['data'][$i]['orig_stage_desc'])
						      ->setCellValue("S".($i+11), $data['data'][$i]['high_stage_desc'])
						      ->setCellValue("T".($i+11), $data['data'][$i]['healed'])
						      ->setCellValue("U".($i+11), $data['data'][$i]['remarks']);
	      	}					      
	      	
			$this->load->library('session');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($i+19).':L11'.($i+20))->getFont()->setSize(8); 
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+19), 'Printed by: '.$this->session->userdata('name'));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".($i+20), 'Date Printed: '.date('m/d/Y h:i:sa'));

			$objPHPExcel->setActiveSheetIndex(0);				
			$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
			$path = "documents";
			$objWriter->save("$path/$filename");
			return "$path/$filename";

		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfWoundList($data, $query, $date_from, $date_to)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "WoundList".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('WoundList');
			$pdf->SetSubject('WoundList');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('L', 'LEGAL');
			
			if(!$query) $query = 'NULL';
			
			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Wound Details</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table><br><br>			

					<table>
						<tr style="font-size:24px;">
						  <td width="8%">Searched by</td>
						  <td width="1%" >:</td>
						  <td width="50%">'.$query.'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date From</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_from)).'</td>
						</tr>
						<tr style="font-size:24px;">
						  <td>Date To</td>
						  <td>:</td>
						  <td>'.date('m/d/Y',strtotime($date_to)).'</td>
						</tr>
					</table>

					<br>

					<table border="1" cellpadding="2">						
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td rowspan="2" width="5%"  style="padding: 10px;" align="left">Date</td>
						  <td rowspan="2" width="6%"  style="padding: 10px;" align="left">Room No.</td>
						  <td rowspan="2" width="8%"  style="padding: 10px;" align="left">Name</td>
						  <td rowspan="2" width="7%"  style="padding: 10px;" align="left">Kind of Wound</td>
						  <td rowspan="2" width="8%"  style="padding: 10px;" align="left">Wound Origin</td>
						  <td rowspan="2" width="3%"  style="padding: 10px;" align="left">Site</td>
						  <td rowspan="2" width="3%"  style="padding: 10px;" align="center">Site No.</td>
						  <td rowspan="2" width="6%"  style="padding: 10px;" align="left">Stage/Degree</td>
						  <td colspan="3"width="6%"  style="padding: 10px;" align="center">Size (cm)</td>
						  <td rowspan="2" width="5%"  style="padding: 10px;" align="center">Tunneling</td>
						  <td rowspan="2" width="6%"  style="padding: 10px;" align="center">Undermining</td>
						  <td rowspan="2" width="2%"  style="padding: 10px;" align="center">Burn (%)</td>
						  <td rowspan="2" width="4%"  style="padding: 10px;" align="left">Wound Bed</td>
						  <td rowspan="2" width="8%"  style="padding: 10px;" align="left">Treatment</td>
						  <td rowspan="2" width="4%"  style="padding: 10px;" align="center">Orignal Stage</td>
						  <td rowspan="2" width="4%"  style="padding: 10px;" align="center">Highest Stage</td>
						  <td rowspan="2" width="4%"  style="padding: 10px;" align="center">Healed</td>
						  <td rowspan="2" width="8%"  style="padding: 10px;" align="left">Remarks</td>						
						</tr>
						<tr style="padding: 10px;font-weight:bold;font-size:17px;">
						  <td align="center">L</td>
						  <td align="center">W</td>
						  <td align="center">D</td>
						</tr>';


			for ($i = 0; $i<$data['count'];$i++)
			{
				if($i%2 == 0)
				{
					$html .= '<tr style="background-color:#f7f6f6;font-size:15px;">
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['date'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['room'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['name'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['wound_type_desc'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['infection_desc'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['site_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['wound_no'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['stage_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['slength'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['swidth'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['sdepth'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['tunneling'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['undermining'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['surface_area'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['wound_bed_desc'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['treatment'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['orig_stage_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['high_stage_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['healed'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['remarks'].'</td>
						</tr>';
				}
				else
				{
					$html .= '<tr style="font-size:15px;">
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['date'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['room'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['name'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['wound_type_desc'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['infection_desc'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['site_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['wound_no'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['stage_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['slength'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['swidth'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['sdepth'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['tunneling'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['undermining'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['surface_area'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['wound_bed_desc'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['treatment'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['orig_stage_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['high_stage_desc'].'</td>
						  <td style="padding: 10px;" align="center">'.$data['data'][$i]['healed'].'</td>
						  <td style="padding: 10px;" align="left">'.$data['data'][$i]['remarks'].'</td>
						</tr>';
				}
			}

			$html .= '</table><br><br><br>';


			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}

	public function exportpdfWoundNoReport($dataHeader, $dataDetail)
	{
		try 
		{
			$this->load->library('PHPExcel/Shared/PDF/tcpdf');
			$pdf = new TCPDF();
			$fDate = date("Ymd_His"); 
			$filename = "WoundReport".$fDate.".pdf";
			
			// set document information
			$pdf->SetCreator('esclamadojp@gmail.com');
			$pdf->SetAuthor('eMedical Data Solutions');
			$pdf->SetTitle('WoundReport');
			$pdf->SetSubject('WoundReport');
			$pdf->SetKeywords('eMedical Data Solutions');
			
			//set margins
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			// add a page
			$pdf->AddPage('P', 'LETTER');
		
			$pdf->Image('image/logo.jpg', 10, 8, 40, 20, 'JPG', null, '', true, 300, '', false, false, 0, false, false, false);

			$html  = '
			<table border=1>
				<tr style="font-weight:bold;font-size:45px;">
				  <td width="110"></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >eMedical Data Services, LLC</font></td>
				</tr>
				<tr style="font-weight:bold;font-size:30px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >Wound Report</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >94th StreetEast Elmhurst New York 11369 United States</font></td>
				</tr>
				<tr style="font-size:15px;">
				  <td></td>
				  <td style="background: black; padding: 10px;" align="left"><font face="Arial" >347-543-0482</font></td>
				</tr>
			</table><br><br>

				<table>					

					<tr style="font-size:24px;">
						<td valign="top" align="right" width="18%">Date</td>
						<td valign="top" width="1%">:</td>
						<td valign="top" align="left" width="81%">'.$dataDetail['data']['date'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Name</td>
						<td valign="top">:</td>
						<td valign="top" align="left"><b>'.$dataHeader['data']['name'].'</b></td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Type of Wound</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataHeader['data']['wound_type_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Wound Origin</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataHeader['data']['infection_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Site</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataHeader['data']['site_desc'].' #'.$dataDetail['data']['wound_no'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Actual Stage / Degree</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['stage_desc'].'</td>
					</tr>';

			if(is_null($dataDetail['data']['sdepth']))
				$sdepth = null;
			else 
				$sdepth = 'D: '.number_format($dataDetail['data']['sdepth'],1);

			if(is_null($dataDetail['data']['tunneling_direction']))
				$tunneling = null;	
			else 
				$tunneling = $dataDetail['data']['tunneling_direction'].', '.number_format($dataDetail['data']['tunneling_length'],1).'cm';
			
			if(is_null($dataDetail['data']['undermining_direction']))
				$undermining = null;	
			else 
				$undermining = $dataDetail['data']['undermining_direction'].', '.number_format($dataDetail['data']['undermining_length'],1).'cm';

			$html .= '<tr style="font-size:24px;">
						<td valign="top" align="right">Size</td>
						<td valign="top">:</td>
						<td valign="top" align="left">L: '.number_format($dataDetail['data']['slength'], 1).' W: '.number_format($dataDetail['data']['swidth'], 1).' '.$sdepth.'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Tunneling</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$tunneling.'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Undermining</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$undermining.'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Burn Surface Area</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['surface_area'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Wound Bed</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['wound_bed_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Peri Wound</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['peri_wound_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Exudate</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['exudate_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Amount of Exudate</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['exudate_amount_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Odor</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['odor_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Temperature</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['temperature_desc'].'</td>
					</tr>';

			if ($dataDetail['data']['pain'] == 1) $pain = 'Yes';
			else $pain = 'No';

			$html .= '<tr style="font-size:24px;">
						<td valign="top" align="right">Pain</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$pain.'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Treatment</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['treatment'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Dietary Supplements</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataHeader['data']['dietary_supplements_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Risk Factors</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataHeader['data']['risk_factors_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Diagnosis</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataHeader['diagnose_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Original Stage</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['orig_stage_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Highest Stage</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['high_stage_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Remarks</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['remarks'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Physician</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['physician_desc'].'</td>
					</tr>

					<tr style="font-size:24px;">
						<td valign="top" align="right">Nurse</td>
						<td valign="top">:</td>
						<td valign="top" align="left">'.$dataDetail['data']['nurse_desc'].'</td>
					</tr>

				</table><br><br><br>';

			$this->load->library('session');
			$html .= '<div style="font-size:14px;">Printed by:'.$this->session->userdata('name').'<br>Date Printed: '.date('m/d/Y h:i:sa').'</div>';

			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			$path = "documents";
			$pdf->Output("$path/$filename", 'F');

			return "$path/$filename";
		} 
		catch (Exception $e) 
		{
			print $e->getMessage();
			die();	
		}
	}
}