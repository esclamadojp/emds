<?php date_default_timezone_set('America/New_York');?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>eCNotes v1.0</title>
	<link rel="icon" type="image/x-icon" href="../image/stethoscope.ico">
	<!-- for extjs -->
	<script type="text/javascript" src="<?php echo base_url(); ?>extjs/extjs-build/ext-all.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>extjs/extjs-build/resources/css/ext-all.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>extjs/extjs-build/resources/css/ext-all-neptune.css">

	<!-- for css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/myExt.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/menu.css"/>

	<!-- messages -->
	<script type="text/javascript">	
		
		var sheight = screen.availHeight-195;
		var setLimit = Math.round(screen.availHeight/50);

		function warningFunction(title_, msg_)
		{
			Ext.Msg.show({
				title   : title_,
				width   : '100%',
				msg     : msg_,
				closable: false,
				icon    : Ext.Msg.WARNING,
				buttons : Ext.Msg.OK
			});	
		}

		function infoFunction(title_, msg_)
		{
			Ext.Msg.show({
				title   : title_,
				width   : '100%',
				msg     : msg_,
				closable: false,
				icon    : Ext.Msg.INFO,
				buttons : Ext.Msg.OK
			});	
		}

		function loadFunction(title_, msg_)
		{
			Ext.Msg.show({
				title   : title_,
				width   : '100%',
				msg     : msg_,
				closable: false,
				icon    : Ext.Msg.INFO
			});
		}

		function errorFunction(title_, msg_)
		{
			Ext.Msg.show({
				title   : title_,
				width   : '100%',
				msg     : msg_,
				closable: false,
				icon    : Ext.Msg.ERROR,
				buttons : Ext.Msg.OK
			});	
		}

		function processingFunction(msg_)
		{
			Ext.MessageBox.show({
				msg     : msg_,
				width   : '100%',
				wait    : true,
				waitConfig : {interval:100}
			});	
		}
		
		Ext.tip.QuickTipManager.init();	
		Ext.QuickTips.interceptTitles = true;
		Ext.QuickTips.init();

		if (Ext.isSafari && Ext.safariVersion == 7) {
		    delete Ext.tip.Tip.prototype.minWidth;
		} 
		
		if(Ext.isIE10) { 
	          Ext.supports.Direct2DBug = true;
	      }

      	function addTooltip(value, metadata, record, rowIndex, colIndex, store){
	        metadata.tdAttr = 'data-qtip="' + value + '"';
	        return value;
	    }

	    function columnWrap(val){
	        return '<div style="text-align:justify; white-space:normal !important;">'+ val +'</div>';
	    }

	</script>

	<!-- CRUD -->
	<script type="text/javascript">	

		function deleteFunction(url, params, extgrid)
		{
			processingFunction("Processing data, please wait...");

			Ext.Ajax.request({
			    url: url,
			    method	: 'POST',
			    params: params,
			    timeout: 1800000,
			    success: function(f,a)
			    {
			    	Ext.MessageBox.hide();
					var response = Ext.decode(f.responseText);									
					if (response.success == true)
					{
						Ext.getCmp(extgrid).getStore().reload({params:{start:0 }, timeout: 300000});      
						infoFunction('Status', response.data);
					}
					else
						errorFunction("Error!",response.data);
			    },
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});
		}

		function addeditFunction(url, params, extgrid, extform, extwindow)
		{
			processingFunction("Processing data, please wait...");
			
			extform.submit({
				url: url,
				method: "POST",	
				params: params,
				timeout: 1800000,
			    success: function(f,action)
			    {
					Ext.MessageBox.hide();

					if (action.result.success == true)
					{
						if (extwindow != null) extwindow.close();						
						infoFunction('Status', action.result.data);
						Ext.getCmp(extgrid).getStore().reload({params:{start:0 }, timeout: 1000});
					}
			    },
				failure: function(f,action) { errorFunction("Error!",action.result.data); }
		    }); 
		}

	</script> 

	<!-- Files -->
	<script type="text/javascript">	

		function export_excel(url, params, type)
		{
			processingFunction("Processing data, please wait...");

			Ext.Ajax.request({
			    url: url,
			    method	: 'POST',
			    params: params,
			    timeout: 1800000,
			    success: function(f,a)
			    {
					var response = Ext.decode(f.responseText);
					Ext.MessageBox.hide();

					if (response.success == true)
					{
						if (type == "PDF")
							window.open("<?php echo base_url(); ?>"+response.filename,'PDFWindow','toolbar=0,menubar=0,location=0,di rectories=0,status=0,resizable=0');
						else
							window.location = "<?php echo base_url(); ?>"+response.filename;
					}
					else
						errorFunction("Error!",response.data);
			    },
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});			
		}

		function ExportDocument(url, params, type)
		{          
			Ext.Msg.show({
				title	: 'Confirmation',
				msg		: 'Are you sure you want to download '+type+'?',
				width	: '100%',
				icon	: Ext.Msg.QUESTION,
				buttons	: Ext.Msg.YESNO,
				fn: function(btn){
					if (btn == 'yes')
						export_excel(url, params, type);
				}
			});
		}

	</script>

	<!-- Date Range -->
	<script type="text/javascript">

	    Ext.apply(Ext.form.field.VTypes, {
	        daterange: function(val, field) {
	            var date = field.parseDate(val);

	            if (!date) {
	                return false;
	            }
	            if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
	                var start = field.up('form').down('#' + field.startDateField);
	                start.setMaxValue(date);
	                start.validate();
	                this.dateRangeMax = date;
	            }
	            else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
	                var end = field.up('form').down('#' + field.endDateField);
	                end.setMinValue(date);
	                end.validate();
	                this.dateRangeMin = date;
	            }
	            /*
	             * Always return true since we're only using this vtype to set the
	             * min/max allowed values (these are tested for after the vtype test)
	             */
	            return true;
	        },

	        daterangeText: 'Start date must be less than end date',

	        password: function(val, field) {
	            if (field.initialPassField) {
	                var pwd = field.up('form').down('#' + field.initialPassField);
	                return (val == pwd.getValue());
	            }
	            return true;
	        },

	        passwordText: 'Passwords do not match'
	    });

	</script>

	<script type="text/javascript">
		function UpdateSessionData(){    
		    Ext.Ajax.request({
		        url: 'commonquery/updateSession',
		        method  : 'POST',
		        success: function(f,a)
		        {
		           var response = Ext.decode(f.responseText);                                   		           
		            if (response.success == true)
		            {
		                Ext.Msg.show({
		                    title   : 'Invalid Session',
		                    msg     : 'Session is already expired!. Please login again.',
		                    width   : '100%',
		                    closable: false,
		                    icon    : Ext.Msg.ERROR,
		                    buttons : Ext.Msg.OK,
		                    fn: function(btn){
		                        if (btn == 'ok')
		                        {
		                        	loadFunction('Route', 'Please wait. re-routing to login page.');
		                            Ext.Ajax.request({
								        url: 'logout/terminateSession',
								        method: 'POST',
								        params: {id: response.data},
								        success: function(f,a)
        								{
        									window.location = "<?php echo base_url().'index.php/login'; ?>";		                        	
        								}
								    });  
		                        }
		                    }		                    
		                });
		            }
		            else
		            	setTimeout("UpdateSessionData();", 5000);  
		        },
		        failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		    });    
		}
	</script>

</head>