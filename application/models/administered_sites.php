<?php
require_once('my_model.php');
class Administered_Sites extends My_Model {

	const DB_TABLE = 'administered_sites';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}