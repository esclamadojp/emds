<?php
require_once('my_model.php');
class ai_agencies extends My_Model {

	const DB_TABLE = 'ai_agencies';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}