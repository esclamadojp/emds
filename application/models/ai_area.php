<?php
require_once('my_model.php');
class ai_area extends My_Model {

	const DB_TABLE = 'ai_area';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}