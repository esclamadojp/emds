<?php
require_once('my_model.php');
class ai_conclusions extends My_Model {

	const DB_TABLE = 'ai_conclusions';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}