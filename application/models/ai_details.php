<?php
require_once('my_model.php');
class ai_details extends My_Model {

	const DB_TABLE = 'ai_details';
	const DB_TABLE_PK = 'id';

	public $id;
	public $kind_id;
	public $area_id;
	public $reported_to_id;
	public $description_ai;
	public $date;
	public $time;
	public $injury;
	public $description_injury;
	public $witness;
	public $summary;
	public $date_report_completed;
	public $report_completed_by;
	public $active;
}