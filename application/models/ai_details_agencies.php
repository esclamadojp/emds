<?php
require_once('my_model.php');
class ai_details_agencies extends My_Model {

	const DB_TABLE = 'ai_details_agencies';
	const DB_TABLE_PK = 'id';

	public $id;
	public $detail_id;
	public $agency_id;
}