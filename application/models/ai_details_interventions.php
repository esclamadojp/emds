<?php
require_once('my_model.php');
class ai_details_interventions extends My_Model {

	const DB_TABLE = 'ai_details_interventions';
	const DB_TABLE_PK = 'id';

	public $id;
	public $detail_id;
	public $intervention_id;
}