<?php
require_once('my_model.php');
class ai_details_staff extends My_Model {

	const DB_TABLE = 'ai_details_staff';
	const DB_TABLE_PK = 'id';

	public $id;
	public $detail_id;
	public $staff_id;
}