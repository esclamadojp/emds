<?php
require_once('my_model.php');
class ai_interventions extends My_Model {

	const DB_TABLE = 'ai_interventions';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}