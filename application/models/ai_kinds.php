<?php
require_once('my_model.php');
class ai_kinds extends My_Model {

	const DB_TABLE = 'ai_kinds';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}