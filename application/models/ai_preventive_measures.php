<?php
require_once('my_model.php');
class ai_preventive_measures extends My_Model {

	const DB_TABLE = 'ai_preventive_measures';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}