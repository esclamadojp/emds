<?php
require_once('my_model.php');
class Allergies extends My_Model {

	const DB_TABLE = 'allergies';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}