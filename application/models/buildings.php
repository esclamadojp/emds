<?php
require_once('my_model.php');
class Buildings extends My_Model {

	const DB_TABLE = 'buildings';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
	public $alias;
}