<?php
require_once('my_model.php');
class Conditions extends My_Model {

	const DB_TABLE = 'conditions';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}