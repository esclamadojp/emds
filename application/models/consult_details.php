<?php
require_once('my_model.php');
class Consult_Details extends My_Model {

	const DB_TABLE = 'consult_details';
	const DB_TABLE_PK = 'id';

	public $id;
	public $resident_id;
	public $room_id;
	public $consult_id;
	public $reason_id;
	public $location_id;
	public $status_id;
	public $user_id;	
	public $transportation_status;
	public $date;
	public $time;
	public $others;
	public $date_entry;
	public $accompanied_by;
	public $active;
}