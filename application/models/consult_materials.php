<?php
require_once('my_model.php');
class consult_materials extends My_Model {

	const DB_TABLE = 'consult_materials';
	const DB_TABLE_PK = 'id';

	public $id;
	public $consult_id;
	public $material_id;
}