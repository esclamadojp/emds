<?php
require_once('my_model.php');
class Consults extends My_Model {

	const DB_TABLE = 'consults';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}