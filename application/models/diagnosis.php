<?php
require_once('my_model.php');
class Diagnosis extends My_Model {

	const DB_TABLE = 'diagnosis';
	const DB_TABLE_PK = 'id';

	public $id;
	public $cat_id;
	public $care_plan;
	public $description;
}