<?php
require_once('my_model.php');
class Diagnosis_Category extends My_Model {

	const DB_TABLE = 'diagnosis_category';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
	public $infection_control;
}