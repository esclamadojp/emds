<?php
require_once('my_model.php');
class dietary_supplements extends My_Model {

	const DB_TABLE = 'dietary_supplements';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}