<?php
require_once('my_model.php');
class Dosages extends My_Model {

	const DB_TABLE = 'dosages';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}