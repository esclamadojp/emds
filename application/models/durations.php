<?php
require_once('my_model.php');
class Durations extends My_Model {

	const DB_TABLE = 'durations';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}