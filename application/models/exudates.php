<?php
require_once('my_model.php');
class Exudates extends My_Model {

	const DB_TABLE = 'exudates';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}