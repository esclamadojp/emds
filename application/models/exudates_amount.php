<?php
require_once('my_model.php');
class Exudates_Amount extends My_Model {

	const DB_TABLE = 'exudates_amount';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}