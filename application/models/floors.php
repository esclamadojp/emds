<?php
require_once('my_model.php');
class Floors extends My_Model {

	const DB_TABLE = 'floors';
	const DB_TABLE_PK = 'id';

	public $id;
	public $cat_id;
	public $description;
}