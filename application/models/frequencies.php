<?php
require_once('my_model.php');
class Frequencies extends My_Model {

	const DB_TABLE = 'frequencies';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}