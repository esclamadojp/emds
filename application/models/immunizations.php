<?php
require_once('my_model.php');
class Immunizations extends My_Model {

	const DB_TABLE = 'immunizations';
	const DB_TABLE_PK = 'id';

	public $id;
	public $cat_id;
	public $description;
}