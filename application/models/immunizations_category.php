<?php
require_once('my_model.php');
class Immunizations_Category extends My_Model {

	const DB_TABLE = 'immunizations_category';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}