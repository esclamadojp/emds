<?php
require_once('my_model.php');
class Immunizations_Details extends My_Model {

	const DB_TABLE = 'immunizations_details';
	const DB_TABLE_PK = 'id';

	public $id;
	public $name_id;
	public $room_id;
	public $immunization_id;
	public $induration_id;
	public $administered_site_id;
	public $confirmation_test_id;
	public $reaction_id;
	public $intervention_id;
	public $administered_by;
	public $read_by;
	public $name_type;
	public $date;
	public $administered;
	public $result;
	public $brand;
	public $expiration_date;
	public $confirmation_date;
	public $confirmation_result;
	public $titer_result;
	public $active;
}