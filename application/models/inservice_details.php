<?php
require_once('my_model.php');
class Inservice_Details extends My_Model {

	const DB_TABLE = 'inservice_details';
	const DB_TABLE_PK = 'id';

	public $id;
	public $shift_id;
	public $topic_id;
	public $servicedate;	
	public $duration;
	public $instructor;
	public $active;
}