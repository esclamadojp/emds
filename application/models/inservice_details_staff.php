<?php
require_once('my_model.php');
class Inservice_Details_Staff extends My_Model {

	const DB_TABLE = 'inservice_details_staff';
	const DB_TABLE_PK = 'id';

	public $id;
	public $service_id;
	public $staff_id;
}