<?php
require_once('my_model.php');
class Locations extends My_Model {

	const DB_TABLE = 'locations';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}