<?php
require_once('my_model.php');
class Medications extends My_Model {

	const DB_TABLE = 'medications';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}