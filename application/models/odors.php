<?php
require_once('my_model.php');
class Odors extends My_Model {

	const DB_TABLE = 'odors';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}