<?php
require_once('my_model.php');
class Page extends My_Model {
    /**
    */
    function set_page($page) {
        try 
        {
            $this->load->library('session');
            $user_id = $this->session->userdata('id');

            if(!$user_id){
                header("Location:".base_url().'index.php/login');
                exit();
            }
            
            #module
            $commandText = "SELECT module_name FROM modules WHERE link = '$page'";
            $result = $this->db->query($commandText);
            $query_result = $result->result(); 
            $module_name = $query_result[0]->module_name;

            #access right to Maintenance(...) ADD / EDIT / DELETE
            $commandText = "SELECT * FROM modules_users WHERE module_id = 51 AND user_id = $user_id AND (uadd = 1 OR uedit = 1 OR udelete = 1)";
            $result = $this->db->query($commandText);
            $query_result = $result->result(); 

            #generating menu         
            $this->load->model('Menu');
            $data['menu']       = $this->Menu->set_userid($user_id);
            $data['username']   = $this->session->userdata('name');
            $data['admin']      = $this->session->userdata('admin');
            $data['department'] = $this->session->userdata('department_description'). ' Department';
            $data['module_name'] = $module_name;
            if(count($query_result) > 0) $data['boolMaintenance'] = 1;
            else $data['boolMaintenance'] = 0;

            if($page == 'thumbnailmenu')
            {   
                $date3days  = Date('Y-m-d', strtotime("+3 days"));
                $datenow    = Date('Y-m-d');
                $commandText = "SELECT 
                                    COUNT(*) AS count, 
                                    GROUP_CONCAT(DISTINCT CONCAT(lname, ', ', fname) SEPARATOR '<br>') AS name
                                FROM (
                                    SELECT 
                                        a.resident_id,
                                        (SELECT admission_date FROM residents_medical_records WHERE resident_id = a.resident_id AND STATUS = 1 ORDER BY admission_date ASC LIMIT 1) AS admission_date,
                                        (SELECT IF(discharge_date = NULL, discharge_date, ADDDATE(CURDATE(), 180)) AS discharge_date FROM residents_medical_records WHERE resident_id = a.resident_id AND STATUS = 1 ORDER BY discharge_date ASC LIMIT 1) discharge_date,
                                        a.date
                                    FROM consult_details a 
                                    WHERE a.active = 1 
                                    AND a.date BETWEEN '$datenow' and '$date3days') b JOIN residents c ON b.resident_id = c.id
                                WHERE b.date >= b.admission_date AND b.date <= b.discharge_date";
                $result = $this->db->query($commandText);
                $query_result = $result->result(); 

                $data['consult_count'] = $query_result[0]->count;
                if($query_result[0]->count > 0)
                    $data['consult_name']  = strtoupper($query_result[0]->name).'<br>';
                else 
                    $data['consult_name']  = strtoupper($query_result[0]->name);

                $date21days     = Date('Y-m-d', strtotime("-365 days"));
                $date7days      = Date('Y-m-d', strtotime("-7 days"));

                $commandText = "SELECT 
                                    COUNT(*) AS count, 
                                    GROUP_CONCAT(CONCAT(c.rsName, ' (', c.name_type,')') SEPARATOR '<br>') AS name
                                FROM (
                                    SELECT 
                                        a.name_id,
                                        GROUP_CONCAT(a.immunization_id) AS immunization,
                                        IF(a.name_type = 'Resident', (SELECT CONCAT(fname, ' ', mname, ' ', lname) FROM residents WHERE id = a.name_id), 
                                                         (SELECT CONCAT(fname, ' ', mname, ' ', lname) FROM staff WHERE id = a.name_id)) AS rsName,
                                        a.name_type
                                    FROM ( SELECT 
                                            *
                                        FROM immunizations_details
                                        WHERE immunization_id = 1   
                                            AND DATE BETWEEN '$date21days' AND '$date7days' AND active = 1
                                        UNION   
                                        SELECT 
                                            *
                                        FROM immunizations_details
                                        WHERE immunization_id = 2   
                                            AND DATE BETWEEN '$date21days' AND '$datenow' AND active = 1) a 
                                    GROUP BY name_id ) c    
                                WHERE c.immunization = '1'";
                $result = $this->db->query($commandText);
                $query_result = $result->result();                         

                $data['immunization_count'] = $query_result[0]->count;                
                $data['immunization_name'] = strtoupper($query_result[0]->name);

            }

            $module = array('module' => $page);
            $this->session->set_userdata($module);

            #validating user access to module
            $this->load->model('Module_Validation');
            $this->Module_Validation->module_name($user_id, $page);

            $this->load->helper('common_helper');
            $this->load->view('templates/header', $data);
            $this->load->view($page.'/index');
            $this->load->view('templates/footer');
        }
        catch (Exception $e) 
        {
            print $e->getMessage();
            die();  
        }
    }
}