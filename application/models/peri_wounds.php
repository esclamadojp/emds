<?php
require_once('my_model.php');
class Peri_Wounds extends My_Model {

	const DB_TABLE = 'peri_wounds';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}