<?php
require_once('my_model.php');
class Physicians extends My_Model {

	const DB_TABLE = 'physicians';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}