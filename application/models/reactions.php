<?php
require_once('my_model.php');
class Reactions extends My_Model {

	const DB_TABLE = 'reactions';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}