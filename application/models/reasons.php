<?php
require_once('my_model.php');
class Reasons extends My_Model {

	const DB_TABLE = 'reasons';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}