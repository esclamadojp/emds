<?php
require_once('my_model.php');
class Resident_Case_Details extends My_Model {

	const DB_TABLE = 'resident_case_details';
	const DB_TABLE_PK = 'id';

	public $id;
	public $header_case_id;
	public $condition_id;
	public $physician_id;
	public $precaution_id;
	public $test_id;
	public $consult_id;
	public $date;
	public $lab_report;
	public $microorganism;
	public $case_type;
	public $other;
	public $active;	
}