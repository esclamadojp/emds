<?php
require_once('my_model.php');
class Resident_Case_Diagnosis extends My_Model {

	const DB_TABLE = 'resident_case_diagnosis';
	const DB_TABLE_PK = 'id';

	public $id;
	public $header_case_id;
	public $diagnose_id;
}