<?php
require_once('my_model.php');
class Resident_Case_Header extends My_Model {

	const DB_TABLE = 'resident_case_header';
	const DB_TABLE_PK = 'id';

	public $id;
	public $room_id;
	public $resident_id;
	public $infection_id;
	public $date;
	public $date_resolved;
	public $active;
}