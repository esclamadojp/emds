<?php
require_once('my_model.php');
class Resident_Case_Medications extends My_Model {

	const DB_TABLE = 'resident_case_medications';
	const DB_TABLE_PK = 'id';

	public $id;
	public $details_case_id;
	public $medication_id;
	public $dosage_id;
	public $tablet_id;
	public $route_id;
	public $frequency_id;
	public $duration_id;
}