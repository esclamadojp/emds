<?php
require_once('my_model.php');
class Resident_Case_Sign_Symtoms extends My_Model {

	const DB_TABLE = 'resident_case_sign_symtoms';
	const DB_TABLE_PK = 'id';

	public $id;
	public $details_case_id;
	public $ss_id;
}