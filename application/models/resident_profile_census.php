<?php
require_once('my_model.php');
class resident_profile_census extends My_Model {

	const DB_TABLE = 'resident_profile_census';
	const DB_TABLE_PK = 'id';

	public $id;
	public $profile_id;
	public $census_id;
}