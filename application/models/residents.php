<?php
require_once('my_model.php');
class Residents extends My_Model {

	const DB_TABLE = 'residents';
	const DB_TABLE_PK = 'id';

	public $id;
	public $fname;
	public $mname;
	public $lname;
	public $age;
	public $sex;
	public $status;
	public $active;
}