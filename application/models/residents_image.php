<?php
require_once('my_model.php');
class Residents_Image extends My_Model {

	const DB_TABLE = 'residents_image';
	const DB_TABLE_PK = 'id';

	public $id;
	public $resident_id;
	public $src;
}