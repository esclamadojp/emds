<?php
require_once('my_model.php');
class residents_medical_records extends My_Model {

	const DB_TABLE = 'residents_medical_records';
	const DB_TABLE_PK = 'id';

	public $id;
	public $resident_id;
	public $record_no;
	public $admission_date;
	public $discharge_date;
	public $status;
	public $active;
}