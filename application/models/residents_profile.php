<?php
require_once('my_model.php');
class Residents_Profile extends My_Model {

	const DB_TABLE = 'residents_profile';
	const DB_TABLE_PK = 'id';

	public $id;
	public $resident_id;
	public $record_no;
	public $room_id;
	public $medicare;
	public $medicaid;
	public $original_admission_date;
	public $latest_admission_date;
	public $latest_discharge_date;	
	public $birthdate;	
	public $weight;	
	public $others;		
}