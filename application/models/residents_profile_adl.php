<?php
require_once('my_model.php');
class residents_profile_adl extends My_Model {

	const DB_TABLE = 'residents_profile_adl';
	const DB_TABLE_PK = 'id';

	public $id;
	public $profile_id;
	public $adl_id;
	public $independent;
	public $assist;
	public $dependent;
}