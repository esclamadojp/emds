<?php
require_once('my_model.php');
class residents_profile_care_plan extends My_Model {

	const DB_TABLE = 'residents_profile_care_plan';
	const DB_TABLE_PK = 'id';

	public $id;
	public $profile_id;
	public $diagnose_id;
	public $remarks;
	public $date;
	public $created_by;
	public $date_created;
	public $type;
	public $status;	
}