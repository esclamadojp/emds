<?php
require_once('my_model.php');
class residents_profile_care_plan_defaults extends My_Model {

	const DB_TABLE = 'residents_profile_care_plan_defaults';
	const DB_TABLE_PK = 'id';

	public $id;
	public $diagnosis_id;
	public $remarks;
	public $type;
}