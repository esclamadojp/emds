<?php
require_once('my_model.php');
class Residents_Profile_Diagnosis extends My_Model {

	const DB_TABLE = 'residents_profile_diagnosis';
	const DB_TABLE_PK = 'id';

	public $id;
	public $profile_id;
	public $diagnose_id;
}