<?php
require_once('my_model.php');
class risk_factors extends My_Model {

	const DB_TABLE = 'risk_factors';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}