<?php
require_once('my_model.php');
class Rooms extends My_Model {

	const DB_TABLE = 'rooms';
	const DB_TABLE_PK = 'id';

	public $id;
	public $cat_id;
	public $description;
}