<?php
require_once('my_model.php');
class Routes extends My_Model {

	const DB_TABLE = 'routes';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}