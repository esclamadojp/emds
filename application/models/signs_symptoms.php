<?php
require_once('my_model.php');
class Signs_Symptoms extends My_Model {

	const DB_TABLE = 'signs_symptoms';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}