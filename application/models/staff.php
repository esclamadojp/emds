<?php
require_once('my_model.php');
class staff extends My_Model {

	const DB_TABLE = 'staff';
	const DB_TABLE_PK = 'id';

	public $id;
	public $fname;
	public $mname;
	public $lname;
	public $position_id;
	public $department_id;
	public $active;
}