<?php
require_once('my_model.php');
class Stages extends My_Model {

	const DB_TABLE = 'stages';
	const DB_TABLE_PK = 'id';

	public $id;
	public $order;
	public $description;
}