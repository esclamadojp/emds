<?php
require_once('my_model.php');
class Status extends My_Model {

	const DB_TABLE = 'status';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}