<?php
require_once('my_model.php');
class Topics extends My_Model {

	const DB_TABLE = 'topics';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}