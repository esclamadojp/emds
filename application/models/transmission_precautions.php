<?php
require_once('my_model.php');
class Transmission_Precautions extends My_Model {

	const DB_TABLE = 'transmission_precautions';
	const DB_TABLE_PK = 'id';

	public $id;
	public $cat_id;
	public $description;
}