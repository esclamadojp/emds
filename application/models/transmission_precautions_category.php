<?php
require_once('my_model.php');
class Transmission_Precautions_Category extends My_Model {

	const DB_TABLE = 'transmission_precautions_category';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
	public $infection_control;
}