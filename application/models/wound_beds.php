<?php
require_once('my_model.php');
class Wound_Beds extends My_Model {

	const DB_TABLE = 'wound_beds';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}