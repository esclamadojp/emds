<?php
require_once('my_model.php');
class Wound_Details extends My_Model {

	const DB_TABLE = 'wound_details';
	const DB_TABLE_PK = 'id';

	public $id;
	public $wound_no;
	public $wound_id;	
	public $exudate_id;
	public $exudate_amount_id;
	public $odor_id;
	public $wound_bed_id;
	public $peri_wound_id;
	public $temperature_id;
	public $stage_id;
	public $orig_stage_id;
	public $high_stage_id;
	public $room_id;
	public $nurse_id;
	public $physician_id;
	public $healed;
	public $healed_date;
	public $treatment;
	public $date;
	public $slength;
	public $swidth;
	public $sdepth;
	public $pain;	
	public $tunneling_direction;
	public $tunneling_length;
	public $undermining_direction;
	public $undermining_length;
	public $remarks;
	public $surface_area;
	public $active;
}