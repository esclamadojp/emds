<?php
require_once('my_model.php');
class wound_diet_supplements extends My_Model {

	const DB_TABLE = 'wound_diet_supplements';
	const DB_TABLE_PK = 'id';

	public $id;
	public $wound_id;
	public $supplement_id;
}