<?php
require_once('my_model.php');
class Wound_Header extends My_Model {

	const DB_TABLE = 'wound_header';
	const DB_TABLE_PK = 'id';

	public $id;
	public $resident_id;
	public $wound_type_id;
	public $infection_id;
	public $site_id;
	public $active;
}