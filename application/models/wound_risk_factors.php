<?php
require_once('my_model.php');
class Wound_Risk_Factors extends My_Model {

	const DB_TABLE = 'wound_risk_factors';
	const DB_TABLE_PK = 'id';

	public $id;
	public $wound_id;
	public $risk_factor_id;
}