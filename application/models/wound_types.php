<?php
require_once('my_model.php');
class Wound_Types extends My_Model {

	const DB_TABLE = 'wound_types';
	const DB_TABLE_PK = 'id';

	public $id;
	public $description;
}