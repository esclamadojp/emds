var accident_incidentscrud, accident_incidentscrudID, accident_incidentscrudForm, residentID;
var residentIDCount, staffIDCount;
var fieldsetResidents, fieldsetStaff;
var myResidents = new Array();
var myStaff 	= new Array();

function inArray(value, array) {
    var length = array.length;
    for(var i = 0; i < length; i++) {
        if(array[i] == value) return true;
    }
    return false;
}

function residentsInsert(id, residentID, residentName)
{
	fieldsetResidents.add(
	{
		xtype: 'panel',
		id: 'panelResidents' + id,
		margin : '0 0 0 5',
		layout:'column',
		border: false,
		items:[{
			    xtype   : 'label',
			    html  	: residentName,
			},{
			    xtype   : 'label',
			    autoEl: {
				  tag: 'label',
				  'data-qtip': '<b>REMOVE:</b> '+residentName
				},
			    html  : '<a href="#" onclick="DeleteResident('+id+')"><IMG SRC="../image/delete.gif"</a><br>',
		}]
	});
	fieldsetResidents.doLayout();
	myResidents.splice(id, 0, residentID);
	residentIDCount++;
}

function residentRemove(id)
{
	fieldsetResidents.remove('panelResidents' + id);
	fieldsetResidents.doLayout();
	myResidents.splice(id, 1, null);
}

function DeleteResident(id)
{
	Ext.Msg.show({
		title	: 'Confirmation',
		msg		: 'Are you sure you want to Remove?',
		width	: '100%',
		icon	: Ext.Msg.QUESTION,
		buttons	: Ext.Msg.YESNO,
		fn: function(btn){
			if (btn == 'yes')
				residentRemove(id);
		}
	});	
}

function staffInsert(id, staffID, staffName)
{
	fieldsetStaff.add(
	{
		xtype: 'panel',
		id: 'panelStaff' + id,
		margin : '0 0 0 5',
		layout:'column',
		border: false,
		items:[{
			    xtype   : 'label',
			    html  	: staffName,
			},{
			    xtype   : 'label',
			    autoEl: {
				  tag: 'label',
				  'data-qtip': '<b>REMOVE:</b> '+staffName
				},
			    html  : '<a href="#" onclick="DeleteStaff('+id+')"><IMG SRC="../image/delete.gif"</a><br>',
		}]
	});
	fieldsetStaff.doLayout();
	myStaff.splice(id, 0, staffID);
	staffIDCount++;
}

function staffRemove(id)
{
	fieldsetStaff.remove('panelStaff' + id);
	fieldsetStaff.doLayout();
	myStaff.splice(id, 1, null);
}

function DeleteStaff(id)
{
	Ext.Msg.show({
		title	: 'Confirmation',
		msg		: 'Are you sure you want to Remove?',
		width	: '100%',
		icon	: Ext.Msg.QUESTION,
		buttons	: Ext.Msg.YESNO,
		fn: function(btn){
			if (btn == 'yes')
				staffRemove(id);
		}
	});	
}

function accidentincidentcrud(type)
{
	params = new Object();

	if (type == "Delete")
	{
		params.id	= accident_incidentscrudID;
		params.type	= type;

		deleteFunction('accident_incident/accidentincidentcrud', params, 'accident_incidentGrid');
	}
	else
	{		
		params.id			= accident_incidentscrudID;
		params.residents	= myResidents.toString();
		params.staff		= myStaff.toString();
		params.ai_kinds		= Ext.get('ai_kinds').dom.value;
		params.ai_area		= Ext.get('ai_area').dom.value;
		params.ai_interventions	= Ext.get('ai_interventions').dom.value;
		params.ai_preventive_measures = Ext.get('ai_preventive_measures').dom.value;
		params.ai_agencies	= Ext.get('ai_agencies').dom.value;
		params.ai_conclusions = Ext.get('ai_conclusions').dom.value;
		params.reported_to	= Ext.get('reported_to').dom.value;
		params.report_completed_by = Ext.get('report_completed_by').dom.value;
		params.type			= type;

		addeditFunction('accident_incident/accidentincidentcrud', params, 'accident_incidentGrid', accident_incidentscrudForm, accident_incidentscrud);
	}
}

function AddEditDeleteAccidentIncidents(type)
{          
	myResidents = [];
	myStaff 	= [];
	residentIDCount = 0;
	staffIDCount = 0;

	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp("accident_incidentGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}
		accident_incidentscrudID = sm.selected.items[0].data.id;
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					accidentincidentcrud(type);
			}
		});
	}
	else
	{		
		fieldsetResidents = new Ext.form.FieldSet({
			autoScroll: true,		
			autoHeight:true,
			margin : '5 0 0 0',
			title: '<font size=2><b>Resident/s</b></font>' + '&nbsp<a href="#" onclick="AddResidentStaff(\'resident\')"><button class="btnadd">ADD</button></a>'
		});

		fieldsetStaff = new Ext.form.FieldSet({
			autoScroll: true,		
			autoHeight:true,
			margin : '5 0 10 0',
			title: '<font size=2><b>Staff</b></font>' + '&nbsp<a href="#" onclick="AddResidentStaff(\'staff\')"><button class="btnadd">ADD</button></a>'
		});


		accident_incidentscrudForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px 15px 0 15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					anchor	: '100%'
		        },
				items: [{
					xtype: 'fieldcontainer',
					fieldDefaults: {
                        labelWidth: 135,    
                        afterLabelTextTpl: required,               
                        allowBlank: false
                    },
					layout: 'hbox',
                    items: [
                    {
                    	xtype: 'panel',
	                    border: false,	                    
	                    width: '60%',
	                    items:[{
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',		
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
								xtype	: 'datefield',
								flex	: 1,
								id		: 'date',
								name	: 'date',
								value	: new Date(),
								fieldLabel: 'Date'		
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
								xtype	: 'timefield',
								flex	: 1,
								id		: 'time',
								name	: 'time',
								fieldLabel: 'Time',
						        minValue: '12:00 AM',
						        maxValue: '11:00 PM',
						        increment: 30		
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'ai_kinds',
					            fieldLabel	: 'Kind of A/I',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'ai_kinds', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('ai_kinds').dom.value = record[0].data.id;
					                }
					            }
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Kind of A/I',
					        	handler: function (){ viewMaintenance('ai_kinds', null); }
					        }]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
								xtype	: 'textareafield',
								flex	: 1,
								afterLabelTextTpl: null,               
                        		allowBlank: true,
								margin	: '0 0 3',
								id		: 'description_ai',
								name	: 'description_ai',	
								height	: 38,					
								fieldLabel: 'Description of A/I'
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'ai_area',
					            fieldLabel	: 'Area',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'ai_area', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('ai_area').dom.value = record[0].data.id;
					                }
					            }  
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Area',
					        	handler: function (){ viewMaintenance('ai_area', null); }
					        }]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex		:1,
					            id			: 'injury',
					            name		: 'injury',
					            fieldLabel	: 'Injury',
					            valueField	: 'description',
					            displayField: 'description',
					            allowBlank	: false,
					            editable	: false,
					            mode    	: 'local',
					            triggerAction : 'all',
					            store   : new Ext.data.ArrayStore({
					                fields: ['id', 'description'],
					                data: [[1, 'No Injury'], [2, 'Minor Injury'], [3, 'Major Injury'], [4, 'N/A']]
					            })
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
								xtype	: 'textareafield',
								flex	: 1,
								afterLabelTextTpl: null,               
                        		allowBlank: true,
								margin	: '0 0 3',
								id		: 'description_injury',
								name	: 'description_injury',	
								height	: 38,					
								fieldLabel: 'Description of Injury',
								afterLabelTextTpl: null,
								allowBlank: true
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
								xtype	: 'textfield',
								flex	: 1,
								id		: 'witness',
								name	: 'witness',
								fieldLabel: 'Witnessed by'
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
		                        xtype       : 'combo',
		                        flex: 1,
		                        id          : 'ai_interventions',
		                        fieldLabel  : 'Intervention/s',                        
		                        allowBlank: false,
		                        multiSelect : true,
		                        valueField  : 'id',
		                        displayField: 'description',
		                        afterLabelTextTpl: required,
		                        triggerAction: 'all',
		                        minChars    : 3,
		                        enableKeyEvents: true,
		                        readOnly    : false,
		                        ddReorder: true,
		                        forceSelection: true,
		                        store: new Ext.data.JsonStore({
		                            proxy: {
		                                type: 'ajax',
		                                url: 'commonquery/caseentrylist',
		                                extraParams: {query:null, type: 'ai_interventions', category:null},
		                                reader: {
		                                    type: 'json',
		                                    root: 'data',
		                                    idProperty: 'id'
		                                }
		                            },
		                            params: {start: 0, limit: 10},
		                            fields: [{name: 'id', type: 'int'}, 'description']
		                        }),
		                        listeners: 
		                        {
		                            select: function (combo, record, index)
		                            {   
		                                var interventionValue = Ext.getCmp("ai_interventions").getValue();
		                                Ext.get('ai_interventions').dom.value = interventionValue.toString();
		                            }
		                        }
		                    },
		                    {
		                        xtype: 'button',
		                        hidden: crudMaintenance,
		                        margins     : '0 0 0 5',
		                        text: '...',
		                        tooltip: 'Add/Edit/Delete Intervention/s',
		                        handler: function (){ viewMaintenance('ai_interventions', null); }
		                    }]
		                }, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
		                    border:false,                            
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
		                        xtype       : 'combo',
		                        flex: 1,
		                        id          : 'ai_preventive_measures',
		                        fieldLabel  : 'Preventive Measures',                        
		                        allowBlank: false,
		                        multiSelect : true,
		                        valueField  : 'id',
		                        displayField: 'description',
		                        afterLabelTextTpl: required,
		                        triggerAction: 'all',
		                        minChars    : 3,
		                        enableKeyEvents: true,
		                        readOnly    : false,
		                        ddReorder: true,
		                        forceSelection: true,
		                        store: new Ext.data.JsonStore({
		                            proxy: {
		                                type: 'ajax',
		                                url: 'commonquery/caseentrylist',
		                                extraParams: {query:null, type: 'ai_preventive_measures', category:null},
		                                reader: {
		                                    type: 'json',
		                                    root: 'data',
		                                    idProperty: 'id'
		                                }
		                            },
		                            params: {start: 0, limit: 10},
		                            fields: [{name: 'id', type: 'int'}, 'description']
		                        }),
		                        listeners: 
		                        {
		                            select: function (combo, record, index)
		                            {   
		                                var preventiveValue = Ext.getCmp("ai_preventive_measures").getValue();
		                                Ext.get('ai_preventive_measures').dom.value = preventiveValue.toString();
		                            }
		                        }
		                    },
		                    {
		                        xtype: 'button',
		                        hidden: crudMaintenance,
		                        margins     : '0 0 0 5',
		                        text: '...',
		                        tooltip: 'Add/Edit/Delete Preventive Measures',
		                        handler: function (){ viewMaintenance('ai_preventive_measures', null); }
		                    }]
		                }, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
		                    border:false,                            
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
		                        xtype       : 'combo',
		                        flex: 1,
		                        id          : 'ai_agencies',
		                        fieldLabel  : 'Agency Involved',                        
		                        allowBlank: false,
		                        multiSelect : true,
		                        valueField  : 'id',
		                        displayField: 'description',
		                        afterLabelTextTpl: required,
		                        triggerAction: 'all',
		                        minChars    : 3,
		                        enableKeyEvents: true,
		                        readOnly    : false,
		                        ddReorder: true,
		                        forceSelection: true,
		                        store: new Ext.data.JsonStore({
		                            proxy: {
		                                type: 'ajax',
		                                url: 'commonquery/caseentrylist',
		                                extraParams: {query:null, type: 'ai_agencies', category:null},
		                                reader: {
		                                    type: 'json',
		                                    root: 'data',
		                                    idProperty: 'id'
		                                }
		                            },
		                            params: {start: 0, limit: 10},
		                            fields: [{name: 'id', type: 'int'}, 'description']
		                        }),
		                        listeners: 
		                        {
		                            select: function (combo, record, index)
		                            {   
		                                var agencyValue = Ext.getCmp("ai_agencies").getValue();
		                                Ext.get('ai_agencies').dom.value = agencyValue.toString();
		                            }
		                        }
		                    },
		                    {
		                        xtype: 'button',
		                        hidden: crudMaintenance,
		                        margins     : '0 0 0 5',
		                        text: '...',
		                        tooltip: 'Add/Edit/Delete Agency Involved',
		                        handler: function (){ viewMaintenance('ai_agencies', null); }
		                    }]
		                }, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
                            height	: 50,
		                    items: [
		                    {
								xtype	: 'textareafield',
								flex	: 1,
								margin	: '0 0 3',
								id		: 'summary',
								name	: 'summary',	
								height	: 38,				
								fieldLabel: 'Summary',
								afterLabelTextTpl: null,
								allowBlank: true			
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'ai_conclusions',
					            fieldLabel	: 'Conclusion',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'ai_conclusions', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('ai_conclusions').dom.value = record[0].data.id;
					                }
					            }  
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Conclusion',
					        	handler: function (){ viewMaintenance('ai_conclusions', null); }
					        }]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'reported_to',
					            fieldLabel	: 'Reported To',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'ai_agencies', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('reported_to').dom.value = record[0].data.id;
					                }
					            }  
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Agency',
					        	handler: function (){ viewMaintenance('ai_agencies', null); }
					        }]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
								xtype	: 'datefield',
								flex	: 1,
								id		: 'date_report_completed',
								name	: 'date_report_completed',
								value	: new Date(),
								fieldLabel: 'A/I Report Completed'		
							}]
						}, {
		                    xtype: 'panel',
		                    bodyStyle: 'padding:0 5px 5px;',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            id			: 'report_completed_by',     
					            fieldLabel	: 'Report Completed by',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            flex		: 1,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/staffname',
							            extraParams: {query:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('report_completed_by').dom.value = record[0].data.id;
					                }
					            }  
				        	}]
						}]
                    },
                    {
                    	xtype: 'fieldset',
                        fixed: true,
                        margins : '0 5 0 5',
                        width: '40%',
                        height: '100%',
                        items:[fieldsetResidents, fieldsetStaff]
                    }]
				}]
			});

			accident_incidentscrud = Ext.create('Ext.window.Window', {
			title		: type + ' Accident / Incident',
			closable	: true,
			modal		: true,
			width		: 800,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',			
			header: {titleAlign: 'center'},			
			items: [accident_incidentscrudForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!accident_incidentscrudForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								accidentincidentcrud(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	accident_incidentscrud.close();
			    }
			}],
		});

		if(type == 'Edit'){
			accident_incidentscrudForm.getForm().load({
				url: 'accident_incident/view',
				timeout: 30000,
				waitMsg:'Loading data, please wait...',
				params: { id: this.accident_incidentscrudID },		
				success: function(form, action) {
					accident_incidentscrud.show();
					var data = action.result.data;

					Ext.getCmp("ai_kinds").setRawValue(data.kind_desc);
					Ext.getCmp("ai_area").setRawValue(data.area_desc);
					Ext.getCmp("ai_interventions").setRawValue(data.intervention_desc);
					Ext.getCmp("ai_preventive_measures").setRawValue(data.preventive_measure_desc);
					Ext.getCmp("ai_agencies").setRawValue(data.agency_desc);
					Ext.getCmp("ai_conclusions").setRawValue(data.conclusion_desc);
					Ext.getCmp("reported_to").setRawValue(data.reported_desc);
					Ext.getCmp("report_completed_by").setRawValue(data.report_completed_desc);

					Ext.get('ai_kinds').dom.value = data.kind_id;
					Ext.get('ai_area').dom.value = data.area_id;
					Ext.get('ai_interventions').dom.value = data.intervention_id;
					Ext.get('ai_preventive_measures').dom.value = data.preventive_measure_id;
					Ext.get('ai_agencies').dom.value = data.agency_id;
					Ext.get('ai_conclusions').dom.value = data.conclusion_id;
					Ext.get('reported_to').dom.value = data.reported_to_id;
					Ext.get('report_completed_by').dom.value = data.report_completed_id;

	       			for (var i = 0; i < action.result.resident_count; i++) 
	       				residentsInsert(residentIDCount, action.result.resident_id[i], action.result.resident_name[i])

	       			for (var i = 0; i < action.result.staff_count; i++) 
	       				staffInsert(staffIDCount, action.result.staff_id[i], action.result.staff_name[i])

				},		
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});
		}
		else
			accident_incidentscrud.show();

		Ext.getCmp("date").focus();
	}
}