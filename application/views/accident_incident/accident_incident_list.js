setTimeout("UpdateSessionData();", 0);

var query = null;

function ExportDocs(type) {

    params = new Object();
    params.query    = query;  
    params.date_from = Ext.getCmp("dateFrom").getValue();
    params.date_to  = Ext.getCmp("dateTo").getValue();
    params.reporttype = 'List';
    params.filetype = type;
    ExportDocument('accident_incident/exportdocument', params, type);
}

Ext.onReady(function(){
 
    var store = new Ext.data.JsonStore({
        pageSize: setLimit,
        storeId: 'myStore',
        proxy: {
            type: 'ajax',
            url: 'accident_incident/ailist',
            extraParams: {query:query, date_from: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), date_to: Ext.Date.add(new Date(), Ext.Date.DAY, 180)},
            remoteSort: false,
            params: {start: 0, limit: setLimit},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },        
        fields: [{name: 'id', type: 'int'}, 'date', 'time', 'description', 'area', 'injury', 'agencies', 'residents', 'staff']
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("accident_incidentGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    function gridTooltip(value, metadata, record, rowIndex, colIndex, store){
        metadata.tdAttr = 'data-qtip="' + value + '"';
        return value;
    }

    var grid = Ext.create('Ext.grid.Panel', {
        id      : 'accident_incidentGrid',
        region  : 'center',
        store:store,        
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Date', dataIndex: 'date', width: '8%'},
            { text: 'Time', dataIndex: 'time', width: '8%'},
            { text: 'Kind of A/I', dataIndex: 'description', width: '12%', renderer:gridTooltip},
            { text: 'Area', dataIndex: 'area', width: '12%'},
            { text: 'Injury', dataIndex: 'injury', width: '10%'},
            { text: 'Agencies Involved', dataIndex: 'agencies', width: '17%', renderer:gridTooltip},
            { text: 'Residents', dataIndex: 'residents', width: '16%', renderer:gridTooltip},
            { text: 'Staff', dataIndex: 'staff', width: '16%', renderer:gridTooltip},
        ],
        columnLines: true,
        width: '100%',
        margin: '0 0 10 0',
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    View();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        },
        bbar: Ext.create('Ext.PagingToolbar', {
            store: store,
            pageSize: setLimit,
            displayInfo: true,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No record/s to display"
        })
    });
    RefreshGridStore(); 

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'View Details',
            icon: '../image/lists.png',
            handler: function (){ View();}
        },
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteAccidentIncidents('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteAccidentIncidents('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteAccidentIncidents('Delete');}
        }]
    });


    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [grid],
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchId',
            emptyText: 'Search here...',
            width   : '30%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("accident_incidentGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchId").getValue();
                        query = Ext.getCmp("searchId").getValue();
                        RefreshGridStore();
                    }
                }
            }
        }, 
        {
            xtype: 'label',
            html: '<font size=2 color=><b>From:</b></font>'
        },
        { xtype: 'datefield', id:'dateFrom', emptyText: 'From',  value: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), width: 100},
        {
            xtype: 'label',
            html: '<font size=2 color=><b>To:</b></font>'
        },
        { xtype: 'datefield', id:'dateTo', emptyText: 'To',  value: Ext.Date.add(new Date(), Ext.Date.DAY, 180), width: 100},
        { xtype: 'button', text: 'RELOAD', icon: '../image/load.png', tooltip: 'Reload grid based on date range', 
            handler: function (){ 
                Ext.getCmp("accident_incidentGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                Ext.getCmp("accident_incidentGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                RefreshGridStore();
            }
        },        
        { xtype: 'tbfill'},
        { xtype: 'button', text: 'ADD', icon: '../image/add.png', tooltip: 'Add Accident / Incident Record', handler: function (){ AddEditDeleteAccidentIncidents('Add');}},
        { xtype: 'button', text: 'EDIT', icon: '../image/edit.png', tooltip: 'Edit Accident / Incident Record', handler: function (){ AddEditDeleteAccidentIncidents('Edit');}},
        { xtype: 'button', text: 'DELETE', icon: '../image/delete.png', tooltip: 'Delete Accident / Incident Record', handler: function (){ AddEditDeleteAccidentIncidents('Delete');}},
        '-',
        {
            text: 'View Details',
            tooltip: 'View accident / incident details',
            icon: '../image/lists.png',
            handler: function (){ View();}
        },
        '-',
        {
            text: 'Download',
            tooltip: 'Extract Data to PDF or EXCEL File Format',
            icon: '../image/download.png',
            menu: 
            {
                items: 
                [
                    {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            ExportDocs('PDF');
                        }
                    }, 
                    {
                        text    : 'Export Excel Format',
                        icon: '../image/excel.png',
                        handler: function ()
                        {
                            ExportDocs('Excel');
                        }
                    }
                ]
            }
        }]
    });
});
