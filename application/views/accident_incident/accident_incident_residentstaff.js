var staff_residentCRUDWindow, staff_residentCRUDForm;

function AddResidentStaff(type)
{          	
	staff_residentCRUDForm = Ext.create('Ext.form.Panel', {
		border			: false,
		bodyStyle		: 'padding:15px;',	
		items: [{
            xtype   	: 'combo',
            anchor		: '100%',
            id          : 'resident_staffID',
            emptyText  	: 'Select '+type,   
            valueField  : 'id',
            displayField: 'description',
            triggerAction: 'all',
            minChars    : 3,
            enableKeyEvents: true,
            readOnly    : false,
            ddReorder	: true,
            forceSelection: true,
            store: new Ext.data.JsonStore({
		        proxy: {
		            type: 'ajax',
		            url: 'commonquery/'+type+'name',
		            extraParams: {query:null},
		            reader: {
		                type: 'json',
		                root: 'data',
		                idProperty: 'id'
		            }
		        },
		        params: {start: 0, limit: 10},
		        fields: [{name: 'id', type: 'int'}, 'description']
            }),
            listeners: 
            {
                select: function (combo, record, index)
                {		 
                	if(type == 'resident')
                	{
                		if (inArray(record[0].data.id, myResidents))
                			errorFunction('Error!', record[0].data.description + ' has already been added.');
                		else
                		{
							var id 	= residentIDCount;
							residentsInsert(id, record[0].data.id, record[0].data.description);
						}
                	}
                	else 
                	{
                		if (inArray(record[0].data.id, myStaff))
                			errorFunction('Error!', record[0].data.description + ' has already been added.');
                		else
                		{
							var id 	= staffIDCount;
							staffInsert(id, record[0].data.id, record[0].data.description);
						}
                	}					
                }
            }  
    }]
	});

	staff_residentCRUDWindow = Ext.create('Ext.window.Window', {
			title		: 'Add '+type,
			closable	: true,
			modal		: true,
			width		: 250,
			autoHeight	: true,
			resizable	: false,
			header: {titleAlign: 'center'},
			items: [staff_residentCRUDForm]
		}).show();

	Ext.getCmp("resident_staffID").focus();
}