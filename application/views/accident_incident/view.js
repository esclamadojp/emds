function ExportRecord(type) {

    params = new Object();
    params.id    = accident_incidentscrudID;  
    params.reporttype  = 'Record';
    params.filetype = type;

    ExportDocument('accident_incident/exportdocument', params, type);
}

function View()
{          
      var sm = Ext.getCmp("accident_incidentGrid").getSelectionModel();
      if (!sm.hasSelection())
      {
        warningFunction("Warning!","Please select record.");
        return;
      }
      accident_incidentscrudID = sm.selected.items[0].data.id;

      Ext.MessageBox.wait('Loading...');
      Ext.Ajax.request(
      {
            url     :"accident_incident/view",
            method  : 'POST',
            params: { id: this.accident_incidentscrudID },  
            success: function(f,a)
            {
                  var response = Ext.decode(f.responseText);                     
                  var htmlLeft = new Ext.XTemplate(
                        '<table width="100%" style="padding: 10px 0 10px 10px;">',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px; width:160px" ><font color=black size=2><b>Date</b></td>',
                        '<td valign="top" align="left" style="padding: 2px; width:5px" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.date+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Time</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.time+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Kind of A/I</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.kind_desc+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Description of A/I</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px 10px 0 0; text-align: justify" ><font color=black size=2>'+response.data.description_ai+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Area</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.area_desc+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Injury</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.injury+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Description of Injury</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px 10px 0 0; text-align: justify" ><font color=black size=2>'+response.data.description_injury+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Witnessed by</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.witness+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Intervention/s</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.intervention_desc+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Preventive Measures</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.preventive_measure_desc+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Agencies Involved</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.agency_desc+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Summary</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px 10px 0 0; text-align: justify" ><font color=black size=2>'+response.data.summary+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Conclusion</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.conclusion_desc+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Reported To</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.reported_desc+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>A/I Report Completed</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.date_report_completed+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>Report Completed by</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2><b>:</b></td>',
                        '<td valign="top" align="left" style="padding: 2px;" ><font color=black size=2>'+response.data.report_completed_desc+'</td>',
                        '</tr>',
                        '</table>'
                );                  
                  
                  var htmlData = 
                         '<table width="100%" style="padding: 10px 0 10px 10px;">';                        

                       

                htmlData += '<tr >' +
                          '<td valign="top" align="left" style="padding: 2px;" width="100%"><font color=black size=2><b>Residents</b></td>' +
                        '</tr>';

                  for (var i = 0; i < response.resident_count; i++) {
                        htmlData += '<tr><td valign="top" align="left" style="padding: 2px;" width="100%"><font color=black size=2>'+(i+1)+'.) '+response.resident_name[i]+'</td></tr>';                     
                  };

                   htmlData += '<tr style="">' + 
                        '<td style="padding: 2px;" width="100%"><font color=black size=2></font></td>' +
                        '</tr>';

                   htmlData += '<tr >' +
                          '<td valign="top" align="left" style="padding: 2px;" width="100%"><font color=black size=2><b>Staff</b></td>' +
                        '</tr>';

                  for (var i = 0; i < response.staff_count; i++) {
                        htmlData += '<tr><td valign="top" align="left" style="padding: 2px;" width="100%"><font color=black size=2>'+(i+1)+'.) '+response.staff_name[i]+'</td></tr>';                     
                  };

                  htmlData +='</table>';

                  var htmlRight = new Ext.XTemplate(htmlData);
                        
                  var leftPanel = Ext.create('Ext.panel.Panel', {
                    split   : true,
                    region  : 'west',
                    width     : '70%',
                    border: false,
                    autoScroll      : true,        
                    html: htmlLeft.applyTemplate(null),
                    minWidth: 300
                });

                  var rightPanel = Ext.create('Ext.panel.Panel', {                    
                    region  : 'center',                    
                    border: false,
                    width     : '30%',
                    autoScroll      : true,
                    html: htmlRight.applyTemplate(null),
                    minWidth: 200
                });

                mainWindow = Ext.create('Ext.window.Window', {
                        title       : 'Accident / Incident View',
                      closable      : true,
                      modal         : true,
                      width         : 750,
                      height        : 480,
                        resizable   : false,        
                        maximizable : true,
                        layout: 'fit',
                        plain: true,  
                        buttonAlign : 'center',
                        header            : {titleAlign: 'center'},
                        layout            : 'border',
                      items         : [leftPanel, rightPanel],
                      buttons: [
                      {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            ExportRecord('PDF');
                        }
                    },
                      {
                          text: 'Edit',
                          icon: '../image/edit.png',
                          handler: function ()
                          {
                              AddEditDeleteAccidentIncidents('Edit');
                              mainWindow.close();
                          }
                      },{
                          text      : 'Close',
                          icon: '../image/close.png',   
                          handler: function ()
                          {
                              mainWindow.close();
                          }
                      }],
                  }).show();
                Ext.MessageBox.hide();  
            }
      });
}