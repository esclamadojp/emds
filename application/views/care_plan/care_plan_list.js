setTimeout("UpdateSessionData();", 0);

Ext.onReady(function(){
 
    var store = new Ext.data.JsonStore({
        pageSize: setLimit,
        storeId: 'myStore',
        proxy: {
            type: 'ajax',
            url: 'care_plan/careplanList',
            extraParams: {query:null,},
            remoteSort: false,
            params: {start: 0, limit: setLimit},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },        
        fields: [{name: 'id', type: 'int'}, 'category', 'diagnosis']
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("care_planGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    var grid1 = Ext.create('Ext.grid.Panel', {
        id      : 'care_planGrid',
        region  : 'center',
        store:store,        
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Category', dataIndex: 'category', width: '25%', renderer:addTooltip},
            { text: 'Diagnosis', dataIndex: 'diagnosis', width: '50%', renderer:addTooltip}
        ],
        columnLines: true,
        width: '100%',
        margin: '0 0 10 0',
        viewConfig: {
            listeners: {                
                itemdblclick: function() {
                    CarePlan();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        },
        bbar: Ext.create('Ext.PagingToolbar', {
            store: store,
            pageSize: setLimit,
            displayInfo: true,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No record/s to display"
        })
    });
    RefreshGridStore(); 

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'View Defaults',
            icon: '../image/view.png',
            handler: function (){ CarePlan();}
        }]
    });
    
    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [grid1],
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchId',
            emptyText: 'Search here...',
            width   : '30%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("care_planGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchId").getValue();
                        query = Ext.getCmp("searchId").getValue();
                        RefreshGridStore();
                    }
                }
            }
        },
        { xtype: 'tbfill'},
        { xtype: 'button', text: 'VIEW Defaults', icon: '../image/view.png', tooltip: 'View default GOAL/S, INTERVENTION/S.', handler: function (){ CarePlan();}
        }]
    });
});
