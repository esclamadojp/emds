var diagnosisID;

function CarePlan()
{          
    var sm = Ext.getCmp('care_planGrid').getSelectionModel();
    if (!sm.hasSelection())
    {
        errorFunction("Status","Please select a record!");
        return;
    }

    diagnosisID = sm.selected.items[0].data.id;
    diagnosisDesc = sm.selected.items[0].data.diagnosis;    

    var form = Ext.create('Ext.form.Panel', {
        border      : false,
        bodyStyle   : 'padding:15px 15px 0 15px;',      
        items:[
        {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            anchor  : '80%',
            items: [
            {
                xtype: 'button',
                margins     : '0 0 0 5',
                text: 'Add',
                handler: function (){ AddEditDeleteCP('Add', 'GOAL'); }
            }]
        }]
    });

    var goalStore = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'care_plan/gicList',
            extraParams: {start: 0, limit: 20, id:diagnosisID, type:'GOAL'},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'description']
    });

    var goalPanel = Ext.create('Ext.grid.Panel', {
        border: false,
        id      : 'goalGrid',
        hideHeaders: true,
        autoHeight: true,
        // autoScroll: true,
        store: goalStore,
        columns: [
            { dataIndex: 'id', hidden: true},
            { dataIndex: 'description', width: '96%', renderer: columnWrap},
            {
                xtype: 'actioncolumn',
                width: '3%',    
                align: 'center',
                items: [{
                    icon   : '../image/delete.gif',
                    tooltip: 'Delete Record',
                    handler: function(grid, rowIndex, colIndex) {
                        goalPanel.getSelectionModel().select(rowIndex);
                        AddEditDeleteCP('Delete', 'GOAL');
                    }
                }]
            }
        ],        
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteCP('Edit', 'GOAL');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    goalMenu.showAt(e.getXY());
                }
            }
        }
    });
    Ext.getCmp("goalGrid").getStore().reload({params:{reset:1 }, timeout: 300000});

    var goalMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteCP('Edit', 'GOAL');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteCP('Delete', 'GOAL');}
        }]
    });

    var interventionStore = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'care_plan/gicList',
            extraParams: {start: 0, limit: 20, id:diagnosisID, type:'INTERVENTION'},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'description']
    });

    var interventionPanel = Ext.create('Ext.grid.Panel', {
        border: false,
        id      : 'interventionGrid',
        hideHeaders: true,
        autoHeight: true,
        // autoScroll: true,
        store: interventionStore,
        columns: [
            { dataIndex: 'id', hidden: true},
            { dataIndex: 'description', width: '96%', renderer:columnWrap},
            {
                xtype: 'actioncolumn',
                width: '3%',    
                align: 'center',
                items: [{
                    icon   : '../image/delete.gif',
                    tooltip: 'Delete Record',
                    handler: function(grid, rowIndex, colIndex) {
                        interventionPanel.getSelectionModel().select(rowIndex);
                        AddEditDeleteCP('Delete', 'INTERVENTION');
                    }
                }]
            }
        ],        
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteCP('Edit', 'INTERVENTION');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    interventionMenu.showAt(e.getXY());
                }
            }
        }
    });
    Ext.getCmp("interventionGrid").getStore().reload({params:{reset:1 }, timeout: 300000});


    var interventionMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteCP('Edit', 'INTERVENTION');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteCP('Delete', 'INTERVENTION');}
        }]
    });

    fieldsetGoal = new Ext.form.FieldSet({
        title: '<font color=green>GOAL/s</font>',        
        items: [goalPanel]
    }); 

    fieldsetIntervention = new Ext.form.FieldSet({
        title: '<font color=blue>INTERVENTION/s</font>',
        items: [interventionPanel]
    }); 

    var fieldsetForm = Ext.create('Ext.form.Panel', {
        border      : false,                
        bodyStyle   : 'padding:0 15px 5px 15px;',  
        items: [fieldsetGoal, fieldsetIntervention]   
    });

    var mainForm = Ext.create('Ext.panel.Panel', {
      border   : false,
      region   : 'center',
      width    : '100%',        
      items    : [form, fieldsetForm]
   });

    AcademicRecordWindow = Ext.create('Ext.window.Window', {
        title       : diagnosisDesc + ' - Defaults',
        header      : {titleAlign: 'center'},
        closable    : true,
        modal       : true,
        width       : 850,
        autoHeight  : true,
        resizable   : false,      
        maximizable : true,
        layout      : 'fit',
        plain       : true, 
        items       : [mainForm],
    }).show();
}