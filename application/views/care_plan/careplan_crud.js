var cpWindow, cpID, cpForm, ttype; 

function cpCRUD(type)
{
	params = new Object();
	params.id			= cpID;
	params.diagnosis_id	= diagnosisID;
	params.ttype		= ttype;
	params.type			= type;

	if (ttype == 'GOAL')
		var grid = 'goalGrid';
	if (ttype == 'INTERVENTION')
		var grid = 'interventionGrid';

	if (type == "Delete")
		deleteFunction('care_plan/giccrud', params, grid);
	else
		addeditFunction('care_plan/giccrud', params, grid, cpForm, cpWindow);
}

function AddEditDeleteCP(type, transactType)
{          	
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
	ttype = transactType;
	tType = "";
	if(type == 'Edit' || type == 'Delete')	
	{
		tType = ttype;
		var sm1 = Ext.getCmp("goalGrid").getSelectionModel();
		var sm2 = Ext.getCmp("interventionGrid").getSelectionModel();
		if (!(sm1.hasSelection() || sm2.hasSelection() || sm3.hasSelection()))
		{
			warningFunction("Warning!","Please select record.");
			return;
		}

		if (ttype == 'GOAL')
			cpID = sm1.selected.items[0].data.id;
		if (ttype == 'INTERVENTION')
			cpID = sm2.selected.items[0].data.id;
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					cpCRUD(type);
			}
		});
	}
	else
	{		
        var booltype = true;
        if(type == 'Add')
        	booltype = false;

		cpForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:10px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 70,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [
				{
		            xtype: 'radiogroup',
		            id: 'gic_type',
		            fieldLabel: 'Type',
		            disabled: booltype,
		            hidden: booltype,
		            items: [
		                {boxLabel: 'Goal', name: 'gic_type', inputValue: 1, checked: true},
		                {boxLabel: 'Intervention', name: 'gic_type',inputValue: 2 }
		            ],
		            listeners:
		            {
		                change : function(newValue, oldValue, eOpts ) {
		                	if(Ext.getCmp("gic_type").getValue().gic_type == 1)
		                		ttype = 'GOAL';
		                	if(Ext.getCmp("gic_type").getValue().gic_type == 2)
		                		ttype = 'INTERVENTION';
		                }
		            }
		        }, {
					xtype	: 'textarea',	
					name	: 'remarks',				
					height	: 70,
					fieldLabel: 'Remarks'
				}]
			});

			cpWindow = Ext.create('Ext.window.Window', {
			title		: type + ' ' + tType,
			closable	: true,
			modal		: true,
			width		: 500,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [cpForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!cpForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								cpCRUD(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	cpWindow.close();
			    }
			}],
		});

		if(type == 'Edit')
		{
			cpForm.getForm().load({
				url: 'care_plan/gicView',
				timeout: 30000,
				waitMsg:'Loading data...',
				params: { id: this.cpID },		
				success: function(form, action) {
					cpWindow.show();
				},		
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});
		}
		else
			cpWindow.show();
	}
}