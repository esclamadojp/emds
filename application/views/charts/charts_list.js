setTimeout("UpdateSessionData();", 0);

var chart, buildingID = 0, infectionID = 0, gridTitle = 'All Buildings, All Origin of Infection';

function ExportGridList(type) {

    params = new Object();
    params.type         = type;
    params.date_from    = Ext.getCmp("dateFrom").getValue();
    params.date_to      = Ext.getCmp("dateTo").getValue();
    params.building_id  = buildingID;  
    params.infection_id = infectionID;
    params.title        = gridTitle;

    ExportDocument('charts/exportdocument', params, type);
}

Ext.onReady(function(){
 
    var storeChart = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'charts/chartdata',
            extraParams: {building_id:0,floor_id:0,infection_id:0, date_from: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), date_to: Ext.Date.add(new Date(), Ext.Date.DAY, 180)},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'data', type: 'int'}, 'cat', 'name']
    });
    
    var RefreshChartStore = function () {
        Ext.getCmp("infectionControlChart").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };

    chart = Ext.create('Ext.chart.Chart', {
        id: 'infectionControlChart',
        flex:1,
        height: 400,
        animate: true,
        border: false,
        legend: {
            position: 'bottom'
        },        
        store: storeChart,
        axes: [
            {
                type: 'Numeric',
                position: 'left',
                fields: ['data'],
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                },
                title: 'Number of Residents',
                grid: true,
                minimum: 0
            },
            {
                type: 'Category',
                position: 'bottom',
                fields: [ 'cat' ],
                label: {
                    rotate: { degrees: 270 }
                },
                title: 'Diagnosis Category'
            }
        ],
        series: [
            {
                type: 'column',
                axis: 'left',
                highlight: true,
                xField: 'cat',
                yField: [ 'data'],
                title: [ 'All Buildings, All Floors, All Origin of Infection'],
                renderer: function(sprite, record, attr, index, store) {
                    var fieldValue = Math.random() * 20 + 10,
                        value = (record.get('data') >> 0) % 5,
                        color = [
                                'rgb(213, 70, 121)', 
                                 'rgb(232, 12, 122)', 
                                 'rgb(255, 31, 12)', 
                                 'rgb(255, 83, 13)', 
                                 'rgb(44, 153, 201)', 
                                 'rgb(191, 3, 255)',
                                 ][value];
                    return Ext.apply(attr, {
                        fill: color
                    });
                },
                tips: {
                    // trackMouse: true,
                    renderer: function(storeItem, item) {
                        this.setTitle(storeItem.get('name'));
                    }
                },
                label: {
                    display: 'insideEnd',
                    'text-anchor': 'middle',
                    field: 'data',
                    renderer: Ext.util.Format.numberRenderer('0'),
                    orientation: 'vertical',
                    color: 'White'
                },
            },
            {
                type: 'line',
                axis: 'left',
                highlight: true,
                xField: 'cat',
                yField: [ 'data'],
                showInLegend: false,
                style: {
                    fill: '#38B8BF',
                    stroke: '#38B8BF',
                    'stroke-width': 3
                }
            }
        ]
    });
    RefreshChartStore();

    var chartPanel = Ext.create('Ext.panel.Panel', {
        width: '100%',
        bodyPadding: 20,
        border: false,
        layout: 'hbox',
        items: [chart],
        tbar: [
        {
            xtype: 'label',
            html: '<font size=2 color=><b>From:</b></font>'
        },
        { xtype: 'datefield', id:'dateFrom', emptyText: 'From',  allowBlank : false, value: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), width: 100},
        {
            xtype: 'label',
            html: '<font size=2 color=><b>To:</b></font>'
        },
        { xtype: 'datefield', id:'dateTo', emptyText: 'To',  allowBlank : false, value: Ext.Date.add(new Date(), Ext.Date.DAY, 180), width: 100},
        '-',
        {
            xtype   : 'checkbox',
            id      : 'ckBuilding',
            name    : 'ckBuilding',
            fieldLabel: 'Building',
            checked : true,
            labelWidth: 40,
            inputValue: 1,
            boxLabel: 'All',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('ckBuilding').checked == true)
                    {
                        Ext.getCmp('cbBuilding').setVisible(false);
                        Ext.getCmp("cbBuilding").setDisabled(true);
                        Ext.getCmp("cbFloor").reset();                            
                        Ext.getCmp("cbFloor").getStore().proxy.extraParams["cat_id"] = 0;
                        Ext.getCmp("cbFloor").getStore().proxy.extraParams["query"] = '';
                        Ext.getCmp("cbFloor").getStore().reload();

                    }
                    else{
                        Ext.getCmp('cbBuilding').setVisible(true);
                        Ext.getCmp("cbBuilding").setDisabled(false);
                    }
                }
            }
        },
        {
            xtype       : 'combo',
            id          : 'cbBuilding',
            allowBlank  : false,
            hideLabel   : true,
            hidden      : true,
            disabled    : true,
            allowBlank  : false,
            editable    : false,
            valueField  : 'id',
            displayField: 'description',
            triggerAction: 'all',
            width       : 120,
            minChars    : 3,
            matchFieldWidth: false,
            forceSelection: true,
            enableKeyEvents: true,
            readOnly    : false,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    url: 'commonquery/caseentrylist',
                    extraParams: {query:null, type: 'buildings', category:null},
                    reader: {
                        type: 'json',
                        root: 'data',
                        idProperty: 'id'
                    }
                },
                params: {start: 0, limit: 10},
                fields: [{name: 'id', type: 'int'}, 'description']
            }),
            listeners: 
            {
                select: function (combo, record, index)
                {                    
                    Ext.getCmp("cbFloor").reset();                            
                    Ext.getCmp("cbFloor").getStore().proxy.extraParams["cat_id"] = record[0].data.id;
                    Ext.getCmp("cbFloor").getStore().proxy.extraParams["query"] = '';
                    Ext.getCmp("cbFloor").getStore().reload();
                }
            }
        },
        '-',
        {
            xtype   : 'checkbox',
            id      : 'ckFloor',
            name    : 'ckFloor',
            fieldLabel: 'Floor',
            checked : true,
            labelWidth: 28,
            inputValue: 1,
            boxLabel: 'All',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('ckFloor').checked == true)
                    {
                        Ext.getCmp('cbFloor').setVisible(false);
                        Ext.getCmp("cbFloor").setDisabled(true);
                    }
                    else{
                        Ext.getCmp('cbFloor').setVisible(true);
                        Ext.getCmp("cbFloor").setDisabled(false);
                    }
                }
            }
        },
        {
            xtype       : 'combo',
            id          : 'cbFloor',
            allowBlank  : false,
            hideLabel   : true,
            hidden      : true,
            disabled    : true,
            allowBlank  : false,
            editable    : false,
            valueField  : 'id',
            displayField: 'description',
            triggerAction: 'all',
            width       : 120,
            minChars    : 3,
            matchFieldWidth: false,
            forceSelection: true,
            enableKeyEvents: true,
            readOnly    : false,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    url: 'charts/caseentrylist',
                    extraParams: {cat_id: 0, query:null},
                    reader: {
                        type: 'json',
                        root: 'data',
                        idProperty: 'id'
                    }
                },
                params: {start: 0, limit: 10},
                fields: [{name: 'id', type: 'int'}, 'description']
            })
        },
        '-',
        {
            xtype   : 'checkbox',
            id      : 'ckInfection',
            name    : 'ckInfection',
            fieldLabel: 'Origin of Infection',
            checked : true,
            labelWidth: 95,
            inputValue: 1,
            boxLabel: 'All',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('ckInfection').checked == true)
                    {
                        Ext.getCmp('cbInfection').setVisible(false);
                        Ext.getCmp("cbInfection").setDisabled(true);
                    }
                    else{
                        Ext.getCmp('cbInfection').setVisible(true);
                        Ext.getCmp("cbInfection").setDisabled(false);
                    }
                }
            }
        },
        {
            xtype       : 'combo',
            id          : 'cbInfection',
            allowBlank  : false,
            hideLabel   : true,
            hidden      : true,
            disabled    : true,
            allowBlank  : false,
            editable    : false,
            valueField  : 'id',
            displayField: 'description',
            triggerAction: 'all',
            width       : 120,
            minChars    : 3,
            matchFieldWidth: false,
            forceSelection: true,
            enableKeyEvents: true,
            readOnly    : false,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    url: 'commonquery/caseentrylist',
                    extraParams: {query:null, type: 'infections', category:null},
                    reader: {
                        type: 'json',
                        root: 'data',
                        idProperty: 'id'
                    }
                },
                params: {start: 0, limit: 10},
                fields: [{name: 'id', type: 'int'}, 'description']
            })
        },
        '->',
        { xtype: 'button', text: 'RELOAD', icon: '../image/load.png', tooltip: 'Reload Data', 
            handler: function (){ 
                if (!Ext.getCmp("cbBuilding").isValid() || !Ext.getCmp("cbFloor").isValid() || !Ext.getCmp("cbInfection").isValid()){
                    Ext.Msg.show({
                        title: 'Error!',
                        msg: "Please fill-in the required fields (Marked red).",
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });
                    return;
                }

                Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                Ext.getCmp("chartGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                Ext.getCmp("chartGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                
                var c = chart.series.items[0];                

                var titleFilter="";
                var titleFilter2="";

                if(Ext.getCmp('ckBuilding').checked == true) 
                {
                    Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["building_id"] = 0;
                    Ext.getCmp("chartGrid").getStore().proxy.extraParams["building_id"] = 0;
                    buildingID = 0;
                    titleFilter += "All Buildings";
                    titleFilter2 += "All Buildings";
                }
                else
                {
                    Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["building_id"] = Ext.getCmp("cbBuilding").getValue();
                    Ext.getCmp("chartGrid").getStore().proxy.extraParams["building_id"] = Ext.getCmp("cbBuilding").getValue();
                    buildingID = Ext.getCmp("cbBuilding").getValue();
                    titleFilter += Ext.getCmp("cbBuilding").getRawValue();
                    titleFilter2 += Ext.getCmp("cbBuilding").getRawValue();
                }
                if(Ext.getCmp('ckFloor').checked == true) 
                {
                    Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["floor_id"] = 0;
                    titleFilter += ", All Floors";
                }
                else
                {
                    Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["floor_id"] = Ext.getCmp("cbFloor").getValue();
                    titleFilter += ", " + Ext.getCmp("cbFloor").getRawValue();
                }
                if(Ext.getCmp('ckInfection').checked == true) 
                {
                    Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["infection_id"] = 0;
                    Ext.getCmp("chartGrid").getStore().proxy.extraParams["infection_id"] = 0;
                    infectionID = 0;
                    titleFilter += ", All Origin of Infection";
                    titleFilter2 += ", All Origin of Infection";
                }
                else
                {
                    Ext.getCmp("infectionControlChart").getStore().proxy.extraParams["infection_id"] = Ext.getCmp("cbInfection").getValue();
                    Ext.getCmp("chartGrid").getStore().proxy.extraParams["infection_id"] = Ext.getCmp("cbInfection").getValue();
                    infectionID = Ext.getCmp("cbInfection").getValue();
                    titleFilter += ", " + Ext.getCmp("cbInfection").getRawValue();
                    titleFilter2 += ", " + Ext.getCmp("cbInfection").getRawValue();
                }

                Ext.getCmp("gridLabel").setText(titleFilter2);
                gridTitle = titleFilter2;
                c.setTitle(titleFilter);

                RefreshChartStore();
                RefreshGridStore();
            }
        },   
        {
            text: 'Download Chart',
            margins: '0 15 0 0',
            tooltip: 'Download an image file for the chart.',
            icon: '../image/chart.png',
            handler: function() {
                Ext.MessageBox.confirm('Confirm Download', 'Would you like to download the chart as an image?', function(choice){
                    if(choice == 'yes'){
                        chart.save({
                            type: 'image/png'
                        });
                    }
                });
            }
        }]        
    });

    var storeGrid = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'charts/griddata',
            extraParams: {start: 0, limit: 20, building_id:0, infection_id:0, type: 'List', date_from: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), date_to: Ext.Date.add(new Date(), Ext.Date.DAY, 180)},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [],
        listeners: {
            metachange: function(store, meta) {
                Ext.getCmp("chartGrid").reconfigure(null, meta.columns);
            }
        }
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("chartGrid").getStore().reload({params:{reset:1 }, timeout: 300000});           
    }; 

    var gridPanel = Ext.create('Ext.panel.Panel', {
        width: '100%',
        border: false,
        bodyPadding: 20,
        layout: 'hbox',        
        items: [
            {
                xtype   : 'grid',
                id      : 'chartGrid',
                flex    : 1,
                margins : '9 0 0 0',
                store: storeGrid,
                columns: [{
                    text     : '',
                    locked   : true,
                    width    : 145
                }],
                columnLines: true,
                width: '50%',
                height: 400,
                loadMask: true,
                viewConfig: {
                    stripeRows: true
                },
                tbar: [
                {
                    text: 'Download',
                    tooltip: 'Extract Data to EXCEL File Format',
                    icon: '../image/excel.png',
                    handler: function ()
                    {
                        ExportGridList('Excel');
                    }
                },
                {
                    xtype: 'label',
                    id : 'gridLabel',
                    html: 'All Buildings, All Origin of Infection'
                }]
            }
        ]
    });
    RefreshGridStore();

    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        border: false,        
        items: [{
            border: false,
            items: [chartPanel]            
        }, {
            border: false,
            items: [gridPanel]  
        }],
        renderTo: "innerdiv"
    });

});
