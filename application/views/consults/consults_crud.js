var consultscrudWindow, consultscrudID, consultscrudForm, room_id;

function fConsultCRUD(type)
{
	params = new Object();

	if (type == "Delete")
	{
		params.id	= consultscrudID;
		params.type	= type;

		deleteFunction('consults/consultscrud', params, 'consultsGrid')
	}
	else
	{		
		params.id		= consultscrudID;
		params.resident_id = residentID;
		params.rooms	= room_id;
		params.consults	= Ext.get('consults').dom.value;
		params.reasons	= Ext.get('reasons').dom.value;
		params.locations = Ext.get('locations').dom.value;
		params.status	= Ext.get('status').dom.value;
		params.materials = Ext.get('materials').dom.value;
		params.type		= type;

		addeditFunction('consults/consultscrud', params, 'consultsGrid', consultscrudForm, consultscrudWindow);
	}
}

function AddEditDeleteConsults(type)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp("consultsGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}
		consultscrudID = sm.selected.items[0].data.id;
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					fConsultCRUD(type);
			}
		});
	}
	else
	{		
		var smStaff = Ext.getCmp("staffTree").getSelectionModel();
		if (!smStaff.hasSelection() || Ext.getCmp('radio2').checked == true)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}
		residentID = smStaff.selected.items[0].data.id;
		if (!residentID)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}
		
		consultscrudForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 110,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [{
					xtype: 'fieldset',
                    title: 'Insurance',
                    items: [{
	                    xtype: 'panel',	                    
	                    border:false,                            
	                    labelStyle: 'font-weight:bold;padding:0;',
	                    layout: 'hbox',
	                    items: [
	                    {
	                        xtype: 'label',
	                        html: 'Medicare'
	                    }, {
	                        xtype: 'label',
	                        margin: '0 2', 
	                        flex: 1,
	                        id: 'lblPrimary',
	                        html: ': None'
	                    }]
	                }, {
	                    xtype: 'panel',	      
	                    margin: '0 0 5',              
	                    border:false,                            
	                    labelStyle: 'font-weight:bold;padding:0;',
	                    layout: 'hbox',
	                    items: [
	                    {
	                        xtype: 'label',
	                        html: 'Medicaid'
	                    }, {
	                        xtype: 'label',
	                        margin: '0 2', 
	                        flex: 1,
	                        id: 'lblSecondary',
	                        html: ': None'
	                    }]
	                }]
				}, {
                    xtype: 'panel',
                    margin: '0 0 5 67',
                    border:false,                            
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
                        xtype: 'label',
                        html: 'Room'
                    }, {
                        xtype: 'label',
                        flex: 1,
                        id: 'lblRooms',
                        html: ': None'
                    }]
                }, {
					xtype	: 'datefield',
					id		: 'date',
					name	: 'date',
					value	: new Date(),
					fieldLabel: 'Date'		
				}, {
			        xtype: 'timefield',
			        id		: 'time',
			        name	: 'time',
			        fieldLabel: 'Time',
			        minValue: '12:00 AM',
			        maxValue: '11:00 PM',
			        increment: 30
			   }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'consults',
			            fieldLabel	: 'Consult',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'consults', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('consults').dom.value = record[0].data.id;
			                }
			            }
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Consult',
			        	handler: function (){ viewMaintenance('consults', null); }
			        }]
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'reasons',
			            fieldLabel	: 'Reason',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'reasons', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('reasons').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Reason(s)',
			        	handler: function (){ viewMaintenance('reasons', null); }
			        }]
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'locations',
			            fieldLabel	: 'Location',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'locations', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('locations').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Location',
			        	handler: function (){ viewMaintenance('locations', null); }
			        }]
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'status',
			            fieldLabel	: 'Status',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'status', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('status').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Status',
			        	handler: function (){ viewMaintenance('status', null); }
			        }]
				}, {
		            xtype   	: 'combo',
		            id			: 'transportation_status',
		            name		: 'transportation_status',
		            fieldLabel	: 'Transportation Status',
		            valueField	: 'description',
		            displayField: 'description',
		            editable	: false,
		            mode    	: 'local',
		            triggerAction : 'all',
		            store   : new Ext.data.ArrayStore({
		                fields: ['id', 'description'],
		                data: [[1, 'Pending'], [2, 'Arranged'], [3, 'Arranged by Family']]
		            }),
		            listeners: 
		            {
		                select: function (combo, record, index)
		                {		                	
		                }
		            }
				}, {
					xtype	: 'textareafield',
					id		: 'others',
					name	: 'others',	
					height	: 40,					
					fieldLabel: 'Others',
					afterLabelTextTpl: null,
					allowBlank: true			
				}, {
					xtype	: 'textfield',
					id		: 'accompanied_by',
					name	: 'accompanied_by',
					fieldLabel: 'Accompanied by',
					afterLabelTextTpl: null,
					allowBlank: true	
				}, {
                    xtype: 'panel',
                    margin: '0 0 5',
                    border:false,                            
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
                        xtype       : 'combo',
                        flex: 1,
                        id          : 'materials',
                        fieldLabel  : 'Materials',                        
                        afterLabelTextTpl: null,
                        allowBlank	: true,
                        multiSelect : true,
                        valueField  : 'id',
                        displayField: 'description',                        
                        triggerAction: 'all',
                        minChars    : 3,
                        enableKeyEvents: true,
                        readOnly    : false,
                        ddReorder: true,
                        forceSelection: true,
                        store: new Ext.data.JsonStore({
                            proxy: {
                                type: 'ajax',
                                url: 'commonquery/caseentrylist',
                                extraParams: {query:null, type: 'materials', category:null},
                                reader: {
                                    type: 'json',
                                    root: 'data',
                                    idProperty: 'id'
                                }
                            },
                            params: {start: 0, limit: 10},
                            fields: [{name: 'id', type: 'int'}, 'description']
                        }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {   
                                var materialsValue = Ext.getCmp("materials").getValue();
                                Ext.get('materials').dom.value = materialsValue.toString();
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        hidden: crudMaintenance,
                        margins     : '0 0 0 5',
                        text: '...',
                        tooltip: 'Add/Edit/Delete Materials',
                        handler: function (){ viewMaintenance('materials', null); }
                    }]
                }]
			});

			consultscrudWindow = Ext.create('Ext.window.Window', {
			title		: type + ' Consultation for ' + rName,
			closable	: true,
			modal		: true,
			width		: 400,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [consultscrudForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!consultscrudForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								fConsultCRUD(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	consultscrudWindow.close();
			    }
			}],
		});

		consultscrudForm.getForm().load({
			url: 'consults/serviceview',
			timeout: 30000,
			waitMsg:'Loading data...',
			params: {
				id: this.consultscrudID, 
				resident_id:residentID,
				type: type
			},		
			success: function(form, action) {
				consultscrudWindow.show();
				var data = action.result.data;

				Ext.getCmp("lblRooms").setText(': ' + data.room_desc, false);
				room_id = data.room_id;	

				if(data.medicare != null)
					Ext.getCmp("lblPrimary").setText(': ' + data.medicare, false);
				if(data.medicaid  != null)
					Ext.getCmp("lblSecondary").setText(': ' + data.medicaid, false);

				if(type == 'Edit'){
					Ext.getCmp("consults").setRawValue(data.consult_desc);
					Ext.getCmp("reasons").setRawValue(data.reason_desc);
					Ext.getCmp("locations").setRawValue(data.location_desc);
					Ext.getCmp("status").setRawValue(data.status_desc);
					Ext.getCmp("materials").setRawValue(data.material_desc);

					Ext.get('consults').dom.value = data.consult_id;
					Ext.get('reasons').dom.value = data.reason_id;
					Ext.get('locations').dom.value = data.location_id;
					Ext.get('status').dom.value = data.status_id;		
					Ext.get('materials').dom.value = data.material_id;		
				}
				else
				{	
					if(data.room_id == null)									
					{
						errorFunction("Load failed", 'Please identify the room of the resident at the <img src="../image/profile.png"> PROFILE RESIDENT form.');
						consultscrudWindow.close();
					}	
				}
			},		
			failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		});

		Ext.getCmp("date").focus();
	}
}