setTimeout("UpdateSessionData();", 0);

Ext.onReady(function(){

    var usersStore = new Ext.data.JsonStore({
        pageSize: setLimit,
        storeId: 'usersStore',
        proxy: {
            type: 'ajax',
            url: 'exam_courses_resources/userslist',
            extraParams: {query:null},
            remoteSort: false,
            params: {start: 0, limit: setLimit},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'username', 'name']
    });

    var moduleStore = new Ext.data.JsonStore({
        storeId: 'moduleStore',
        proxy: {
            type: 'ajax',
            url: 'exam_courses_resources/modulelist',
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'sno', 'module_name']
    });

    var submoduleStore = new Ext.data.JsonStore({
        storeId: 'submoduleStore',
        proxy: {
            type: 'ajax',
            url: 'exam_courses_resources/submodulelist',
            extraParams: {start: 0, limit: 20, module_id:0},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'sno', 'parent_id', 'module_name', 'link', 'thumbnail', 'menu', 'icon']
    });

    var RefreshusersGridStore = function () {
        Ext.getCmp("usersGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    var RefreshmoduleGridStore = function () {
        Ext.getCmp("moduleGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    var RefreshsubmoduleGridStore = function () {
        Ext.getCmp("submoduleGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    function addTooltip(value, metadata, record, rowIndex, colIndex, store){
        metadata.tdAttr = 'data-qtip="' + value + '"';
        return value;
    }

    var grid1 = Ext.create('Ext.grid.Panel', {
        title: 'Courses',
        id: 'usersGrid',
        store:usersStore,
    	split	: true,
        collapsible: true,
        region	: 'west',
        columns: [
            Ext.create('Ext.grid.RowNumberer'),
            { dataIndex: 'id', hidden: true},
            { text: 'Course', dataIndex: 'name', width: '99%', renderer:addTooltip}
        ],
        columnLines: true,
        width: '30%',
        height: 400,        
        margin: '0 0 10 0',
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchId',
            emptyText: 'Search here...',
            width   : '60%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("usersGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchId").getValue();
                        RefreshusersGridStore();
                    }
                }
            }
        },
        { xtype: 'tbfill'},
        { xtype: 'button', tooltip: 'Add Course', icon: '../image/add.png', handler: function (){ AddEditDeleteUser('Add');}},
        { xtype: 'button', tooltip: 'Edit Course', icon: '../image/edit.png', handler: function (){ AddEditDeleteUser('Edit');}},
        { xtype: 'button', tooltip: 'Delete Course', icon: '../image/delete.png', handler: function (){ AddEditDeleteUser('Delete');}}
        ],
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteUser('Edit');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    usersMenu.showAt(e.getXY());
                }
            }
        }
    });
    RefreshusersGridStore();

    var usersMenu = Ext.create('Ext.menu.Menu', {
        items: [{
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteUser('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteUser('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteUser('Delete');}
        }]
    });

    var grid2 = Ext.create('Ext.grid.Panel', {
        title: 'Items',
        id: 'moduleGrid',
        store:moduleStore,
        split   : true,
        region  : 'west',
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Items', dataIndex: 'sno', width: '99%'}
        ],
        columnLines: true,
        width: '30%',
        height: 400,        
        margin: '0 0 10 0',
        tbar: [
        { xtype: 'tbfill'},
        { xtype: 'button', tooltip: 'Add Item', icon: '../image/add.png', handler: function (){ AddEditDeleteModule('Add');}},
        { xtype: 'button', tooltip: 'Edit Item', icon: '../image/edit.png', handler: function (){ AddEditDeleteModule('Edit');}},
        { xtype: 'button', tooltip: 'Delete Item', icon: '../image/delete.png', handler: function (){ AddEditDeleteModule('Delete');}}
        ],
        viewConfig: {
            listeners: {
                itemclick: function(view,rec,item,index,eventObj) {                    
                    Ext.getCmp("submoduleGrid").getStore().proxy.extraParams["module_id"] = rec.get('id');
                    RefreshsubmoduleGridStore();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    moduleMenu.showAt(e.getXY());
                }
            }
        }
    });
    RefreshmoduleGridStore();

    var moduleMenu = Ext.create('Ext.menu.Menu', {
        items: [{
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteModule('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteModule('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteModule('Delete');}
        }]
    });

	var grid3 = Ext.create('Ext.grid.Panel', {
        title: 'Resources',
        id: 'submoduleGrid',
        store:submoduleStore,
		region	: 'center',
        columns: [
            { dataIndex: 'id', hidden: true},
            { dataIndex: 'parent_id', hidden: true},
            { text: 'Description', dataIndex: 'module_name', width: '70%', renderer:addTooltip},
            { text: 'Type', dataIndex: 'link', width: '29%', renderer:addTooltip}
        ],
        columnLines: true,
        width: '70%',
        height: 400,        
        loadMask: true,
        margin: '0 0 10 0',
        tbar: [
        { xtype: 'tbfill'},
        { xtype: 'button', text: 'ADD', icon: '../image/add.png', handler: function (){ AddEditDeleteSubModule('Add');}},
        { xtype: 'button', text: 'EDIT', icon: '../image/edit.png', handler: function (){ AddEditDeleteSubModule('Edit');}},
        { xtype: 'button', text: 'DELETE', icon: '../image/delete.png', handler: function (){ AddEditDeleteSubModule('Delete');}}
        ],
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteSubModule('Edit');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    submoduleMenu.showAt(e.getXY());
                }
            }
        }
    });
    RefreshsubmoduleGridStore();

    var submoduleMenu = Ext.create('Ext.menu.Menu', {
        items: [{
            text: 'Users',
            icon: '../image/users.png',
            handler: function (){ AddEditDeleteModuleUsers('Sub');}
        }, {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteSubModule('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteSubModule('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteSubModule('Delete');}
        }]
    });

    var panel1 = Ext.create('Ext.panel.Panel', {
        region  : 'center',
        border  : false,
        width: '70%',
        height: '100%',
        layout: 'border',
        items   : [grid2, grid3]
    });

	Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
	    width: '100%',
	    height: sheight,
	    renderTo: "innerdiv",
	    layout: 'border',
        border: false,
	    items	: [grid1, panel1]
	});
});
