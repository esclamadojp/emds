setTimeout("UpdateSessionData();", 0);

var sName, staffID, serviceType = 'Name';

Ext.onReady(function(){
          
    var treeStore = Ext.create('Ext.data.TreeStore', {
        proxy: {
            type: 'ajax',
            reader: 'json',
            extraParams: {query:null},
            url: 'exam_enrolment/courselist'
        }     
    });

    var RefreshTreeStore = function () {
        Ext.getCmp("courseTree").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };

    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Course',
        split   : true,
        region  : 'west',
        collapsible: true,
        id : 'courseTree',
        store: treeStore,
        rowLines: true,
        width: '30%',
        minWidth: 200,
        margin: '0 0 10 0',
        height: 500,        
        rootVisible: false,
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchName',
            emptyText: 'Search here...',
            width   : '50%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("courseTree").getStore().proxy.extraParams["query"] = Ext.getCmp("searchName").getValue();
                        RefreshTreeStore();
                    }
                }
            }
        }
        ],
        viewConfig: {
            listeners: {
                itemclick: function(view,rec,item,index,eventObj) {
                    Ext.getCmp("exam_enrolmentGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                    Ext.getCmp("exam_enrolmentGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                    Ext.getCmp("exam_enrolmentGrid").getStore().proxy.extraParams["staffID"] = rec.get('id');
                    staffID = rec.get('id');
                    sName = rec.get('text');
                    if (sName == 'Names')
                        Ext.getCmp("exam_enrolmentGrid").setTitle('Details');
                    else
                        Ext.getCmp("exam_enrolmentGrid").setTitle('In-Service details attended by <font size=2><b>'+sName+'</b></font>');
                    RefreshGridStore();
                }
            }
        }
    });

    var storeServiceList = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'exam_enrolment/servicelist',
            extraParams: {start: 0, limit: 20, servicequery:null, staffID: 0, searchServiceType: 'Name', date_from: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), date_to: Ext.Date.add(new Date(), Ext.Date.DAY, 180)},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, {name: 'staff_id', type: 'int'}, {name: 'topic_id', type: 'int'}, 'servicedate', 'duration', 'instructor', 'shift', 'topic']
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("exam_enrolmentGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };
   
    var equivalent = null, remarks = null, comboflag=false;

    var grid = Ext.create('Ext.grid.Panel', {
        id      : 'exam_enrolmentGrid',
        region  : 'center',
        store: storeServiceList,
        columns: [  
            Ext.create('Ext.grid.RowNumberer'),
            {dataIndex: 'id', hidden: true},
            {text: "Staff", dataIndex: 'servicedate', width: '40%'},
            {text: "Added by", dataIndex: 'instructor', width: '50%'}
        ],
        columnLines: true,
        width: '80%',
        minWidth: 700,
        height: 400,
        title: 'Enrolled Staff',
        loadMask: true,
        margin: '0 0 10 0',
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    LoadParticipants();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        },
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchTopic',
            emptyText: 'Search here...',
            width   : '25%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("exam_enrolmentGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("exam_enrolmentGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("exam_enrolmentGrid").getStore().proxy.extraParams["servicequery"] = Ext.getCmp("searchTopic").getValue();
                        RefreshGridStore();
                    }
                }
            }
        },
        { xtype: 'tbfill'},
        { xtype: 'button', text: 'Add', icon: '../image/add.png', tooltip: 'Add Staff', handler: function (){ AddEditDeleteService('Add');}},
        { xtype: 'button', text: 'Delete', icon: '../image/delete.png', tooltip: 'Delete Staff', handler: function (){ AddEditDeleteService('Delete');}},
        {
            text: 'Download',
            tooltip: 'Extract Data to PDF or EXCEL File Format',
            icon: '../image/download.png',
            menu: 
            {
                items: 
                [
                    {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            ExportDocs('PDF');
                        }
                    }, 
                    {
                        text    : 'Export Excel Format',
                        icon: '../image/excel.png',
                        handler: function ()
                        {
                            ExportDocs('Excel');
                        }
                    }
                ]
            }
        }]
    });

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteService('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteService('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteService('Delete');}
        }]
    });

    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [tree,grid]
    });
});
