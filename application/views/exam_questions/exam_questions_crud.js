var inservicecrud, inservicecrudID, inservicecrudForm;
var arrayStaffID = new Array();
var arrayStaffName = new Array();

function serviceCRUD(type)
{
	params = new Object();

	if (type == "Delete")
	{
		params.id	= inservicecrudID;
		params.type	= type;

		deleteFunction('inservice/inservicecrud', params, 'inserviceGrid');
	}
	else
	{	
		if(!arrayStaffID)
	        var staff_list = [];
	    else{
	        var staff_list = arrayStaffID.toString();
	    }

		params.id		= inservicecrudID;
		params.staff_id	= staffID;
		params.topics	= Ext.get('topics').dom.value;
		params.shifts	= Ext.get('shifts').dom.value;
		params.staff	= staff_list;
		params.type		= type;

		addeditFunction('inservice/inservicecrud', params, 'inserviceGrid', inservicecrudForm, inservicecrud);
	}
}

function AddEditDeleteQuestions(type)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp("exam_enrolmentGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}
		inservicecrudID = sm.selected.items[0].data.id;
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'This will affect to all staff. Are you sure you want to ' + type + ' In-Service record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					serviceCRUD(type);
			}
		});
	}
	else
	{
		// var smStaff = Ext.getCmp("staffTree").getSelectionModel();
		// if (!smStaff.hasSelection() || Ext.getCmp('radio2').checked == true)
		// {
		// 	warningFunction("Warning!","Please select a name.");
		// 	return;
		// }
		// staffID = smStaff.selected.items[0].data.id;
		// if (!staffID)
		// {
		// 	warningFunction("Warning!","Please select a name.");
		// 	return;
		// }
		
		inservicecrudForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 122,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [{
					xtype	: 'textarea',
					heigth  : 100,
					fieldLabel: 'Question'		
				}, {
					xtype	: 'textfield',
					fieldLabel: 'Mark'		
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'textfield',
			            flex: 1,
			            fieldLabel	: 'Option 1'
			        },
			        {
			        	xtype: 'checkbox',
			        	margins		: '0 0 0 5', 
			        	boxLabel: 'Answer'
			        }]
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'textfield',
			            flex: 1,
			            fieldLabel	: 'Option 2'
			        },
			        {
			        	xtype: 'checkbox',
			        	margins		: '0 0 0 5', 
			        	boxLabel: 'Answer'
			        }]
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'textfield',
			            flex: 1,
			            fieldLabel	: 'Option 3'
			        },
			        {
			        	xtype: 'checkbox',
			        	margins		: '0 0 0 5', 
			        	boxLabel: 'Answer'
			        }]
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'textfield',
			            flex: 1,
			            fieldLabel	: 'Option 4'
			        },
			        {
			        	xtype: 'checkbox',
			        	margins		: '0 0 0 5', 
			        	boxLabel: 'Answer'
			        }]
				}, {
					xtype: 'button',
					margin: '0 0 0 125',
					text: 'Add Option'
				}]
			});

			inservicecrud = Ext.create('Ext.window.Window', {
			title		: type + ' QUESTION',
			closable	: true,
			modal		: true,
			width		: 800,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [inservicecrudForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!inservicecrudForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								serviceCRUD(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	inservicecrud.close();
			    }
			}],
		});

		if(type == 'Edit')
		{
			inservicecrudForm.getForm().load({
				url: 'inservice/inserviceview',
				timeout: 30000,
				waitMsg:'Loading data...',
				params: {
					id: this.inservicecrudID, staff_id: staffID, type: type
				},		
				success: function(form, action) {
					inservicecrud.show();
					var data = action.result.data;
					var staff_id = action.result.staff_id;
    				var staff_name = action.result.staff_name;

					Ext.getCmp("shifts").setRawValue(data.shift_desc);
					Ext.getCmp("topics").setRawValue(data.topic_desc);
					Ext.get('shifts').dom.value = data.shift_id;
					Ext.get('topics').dom.value = data.topic_id;		

		            arrayStaffID = staff_id;
		            arrayStaffName = staff_name;

		            if(arrayStaffID.length == 0)
		                Ext.getCmp("lblParticipants").setText('None', false);
		            else if(arrayStaffID.length == 1)
		                Ext.getCmp("lblParticipants").setText('<a href="#" href="#" onclick="commonGrid()" style="text-decoration:none;font: 9px;">'+arrayStaffName[0]+'</a>', false);
		            else 
		                Ext.getCmp("lblParticipants").setText('<a href="#" href="#" onclick="commonGrid()" style="text-decoration:none;font: 9px;">'+arrayStaffName[0]+' and ('+(arrayStaffID.length-1)+') more...</a>', false);
					
					if(arrayStaffID.length != 0)		            
					    document.getElementById("lblParticipants").setAttribute("data-qtip", "<b>Click</b> to <font color=green>ADD</font> / <font color=red>DELETE</font> Participant");
					else
						document.getElementById("lblParticipants").setAttribute("data-qtip", "");
				},		
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});
		}
		else
			inservicecrud.show();

		Ext.getCmp("servicedate").focus();
	}
}