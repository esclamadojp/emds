
function takeView()
{           
    var sm = Ext.getCmp("examsGrid").getSelectionModel();
    if (!(sm.hasSelection()))
    {
        warningFunction("Warning!","Please select record.");
        return;
    }
    if(sm.selected.items[0].data.status == "Scheduled")
        errorFunction("Active Date!","This exam is not yet active.");
    else if(sm.selected.items[0].data.status == "Active")
        takeExam();
    else 
        viewExam();

}

function viewExam()
{
    var sm = Ext.getCmp("examsGrid").getSelectionModel();
    var htmlDetails = 
                    '<table width="100%" style="padding: 2px;background: #ff6666;border: solid 1px white;">' +
                    '<tr style="background: #ff6666;">' +
                    '<td style="padding: 2px;" width="15%"><font color=white size=2><b>Exam</b></font></td>' +
                    '<td style="padding: 2px;" width="85%"><font color=white size=2>'+sm.selected.items[0].data.course + ' - ' + sm.selected.items[0].data.items+'</font></td>' +
                    '</tr>' +
                    '<tr style="background: #ff6666;">' +
                    '<td style="padding: 2px;" ><font color=white size=2><b>Score</b></font></td>' +
                    '<td style="padding: 2px;" ><font color=white size=2>'+sm.selected.items[0].data.score+'</font></td>' +
                    '</tr>' +
                    '<tr style="background: #ff6666;">' +
                    '<td style="padding: 2px;" ><font color=white size=2><b>Status</b></font></td>' +
                    '<td style="padding: 2px;" ><font color=white size=2>'+sm.selected.items[0].data.mystatus+'</font></td>' +
                    '</tr>' +
                    '</table>';

    viewForm = Ext.create('Ext.form.Panel', {
            border      : false,
            bodyStyle   : 'padding:10px;',      
            items: [
            {
                xtype: 'panel',
                border: false,
                html: htmlDetails
            },
            {
                xtype: 'panel',
                border: false,
                margin: '10',
                items: [
                    {
                        xtype: 'label',
                        html: 'Question - 1 (2 Marks)<br>This is just a sample question.<br> Your answer: Option 2 (<font color=red>Wrong</font>)' +
                              '<br><br>Question - 2 (2 Marks)<br>This is just a sample question.<br> Your answer: Option 1 (<font color=red>Wrong</font>)' +
                              '<br><br>Question - 3 (2 Marks)<br>This is just a sample question.<br> Your answer: Option 3 (<font color=green>Correct</font>)'
                    }
                ]
            }]
        });

    viewWindow = Ext.create('Ext.window.Window', {
        title       : 'Review',
        closable    : true,
        modal       : true,
        width       : 900,
        autoHeight  : true,
        resizable   : false,
        header: {titleAlign: 'center'},
        items: [viewForm]
    }).show();
}

function takeExam()
{
    var sm = Ext.getCmp("examsGrid").getSelectionModel();
    var htmlDetails = 
                    '<table width="100%" style="padding: 2px;background: #5aa865;border: solid 1px white;">' +
                    '<tr style="background: #5aa865;">' +
                    '<td style="padding: 2px;" width="15%"><font color=white size=2><b>Exam</b></font></td>' +
                    '<td style="padding: 2px;" width="85%"><font color=white size=2>'+sm.selected.items[0].data.course + ' - ' + sm.selected.items[0].data.items+'</font></td>' +
                    '</tr>' +
                    '<tr style="background: #5aa865;">' +
                    '<td style="padding: 2px;" ><font color=white size=2><b>Duration</b></font></td>' +
                    '<td style="padding: 2px;" ><font color=white size=2>30 Minutes</font></td>' +
                    '</tr>' +
                    '<tr style="background: #5aa865;">' +
                    '<td style="padding: 2px;" ><font color=white size=2><b>Mark</b></font></td>' +
                    '<td style="padding: 2px;" ><font color=white size=2>50</font></td>' +
                    '</tr>' +
                    '<tr style="background: #5aa865;">' +
                    '<td style="padding: 2px;" ><font color=white size=2><b>Total Question</b></font></td>' +
                    '<td style="padding: 2px;" ><font color=white size=2>25</font></td>' +
                    '</tr>' +
                    '</table>';

    takeForm = Ext.create('Ext.form.Panel', {
            border      : false,
            bodyStyle   : 'padding:10px;',      
            items: [
            {
                xtype: 'panel',
                border: false,
                html: htmlDetails
            },
            {
                xtype: 'panel',
                border: false,
                margin: '10',
                items: [
                    {
                        xtype: 'label',
                        html: 'Question - 1 (2 Marks)<br>This is just a sample question.<br><br>'
                    },
                    {
                        xtype: 'radio',
                        name      : 'option',
                        boxLabel: 'Option 1'
                    },
                    {
                        xtype: 'radio',
                        name      : 'option',
                        boxLabel: 'Option 2'
                    },
                    {
                        xtype: 'radio',
                        name      : 'option',
                        boxLabel: 'Option 3'
                    },
                    {
                        xtype: 'radio',
                        name      : 'option',
                        boxLabel: 'Option 4'
                    }
                ]
            }]
        });

    takeWindow = Ext.create('Ext.window.Window', {
        title       : 'Exam',
        closable    : true,
        modal       : true,
        width       : 900,
        autoHeight  : true,
        resizable   : false,
        buttonAlign : 'left',
        header: {titleAlign: 'center'},
        items: [takeForm],
        buttons: [
        {
            text    : 'Next',
            icon    : '../image/next.png',
            handler: function ()
            {
                infoFunction("Test!","Next question.");
            }
        }],
    }).show();
}