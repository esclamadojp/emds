var moduleusersWindow, moduleID;

function Resources(type)
{          
    var sm = Ext.getCmp("examsGrid").getSelectionModel();

    if (!sm.hasSelection())
    {
        warningFunction("Warning!","Please select a record.");
        return;
    }
    moduleID = sm.selected.items[0].data.id;        

    var moduleusersStore = new Ext.data.JsonStore({
        pageSize: 10,
        proxy: {
            type: 'ajax',
            url: 'usermanagement/moduleuserslist',
            extraParams: {module_id: moduleID, query:null},
            remoteSort: false,
            params: {start: 0, limit: 10},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'username', 'name', 'add', 'edit', 'delete']
    });

    var RefreshModuleUsersGridStore = function () {
        Ext.getCmp("moduleusersGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    var ModuleUsersGrid = Ext.create('Ext.grid.Panel', {
        id: 'moduleusersGrid',
        store:moduleusersStore,
        border:false,
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Description', dataIndex: 'name', width: '79%'},
            { text: 'Type', dataIndex: 'username', width: '20%'}       
        ],
        columnLines: true,
        width: '100%',
        height: 360,    
        bbar: Ext.create('Ext.PagingToolbar', {
            store: moduleusersStore,
            pageSize: 10,
            displayInfo: true,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No record/s to display"
        })
    });
    RefreshModuleUsersGridStore();

        moduleusersWindow = Ext.create('Ext.window.Window', {
        title       : sm.selected.items[0].data.course + ' - ' + sm.selected.items[0].data.items + ' Resources',
        closable    : true,
        modal       : true,
        width       : 600,
        autoHeight  : true,
        resizable   : false,
        header: {titleAlign: 'center'},
        items: [ModuleUsersGrid]
    }).show();
}
