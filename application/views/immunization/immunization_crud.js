var immunizationWindow, immunizationID, immunizationCRUDForm, nameID, immunizationCat;

function clearValue(object, type)
{
	if(type == 1)
	{
		Ext.getCmp(object).clearValue(); 
		Ext.get(object).dom.value = null;
	}
	else if(type == 2)
	{
		Ext.getCmp(object).clearValue(); 
		Ext.getCmp(object).setValue(null);
	}
	else
		Ext.getCmp(object).setValue(null);
}

function immunizationFunction(cat_id)
{
	if(cat_id == 1)
	{									
		Ext.getCmp("administered").setVisible(true);Ext.getCmp("administered").setDisabled(false);
		Ext.getCmp("administered_by").setVisible(true);Ext.getCmp("administered_by").setDisabled(false);
		Ext.getCmp("read_by").setVisible(true);Ext.getCmp("read_by").setDisabled(false);
		Ext.getCmp("result").setVisible(true);Ext.getCmp("result").setDisabled(false);
		Ext.getCmp("indurations_container").setVisible(true);Ext.getCmp("indurations_container").setDisabled(false);
		Ext.getCmp("confirmation_tests_container").setVisible(true);Ext.getCmp("confirmation_tests_container").setDisabled(false);
		Ext.getCmp("confirmation_date").setVisible(true);Ext.getCmp("confirmation_date").setDisabled(false);
		Ext.getCmp("confirmation_result").setVisible(true);Ext.getCmp("confirmation_result").setDisabled(false);
		Ext.getCmp("interventions_container").setVisible(true);Ext.getCmp("interventions_container").setDisabled(false);
		Ext.getCmp("administered_sites_container").setVisible(false);Ext.getCmp("administered_sites_container").setDisabled(true);
		Ext.getCmp("reactions_container").setVisible(false);Ext.getCmp("reactions_container").setDisabled(true);
		Ext.getCmp("titer_result").setVisible(false);Ext.getCmp("titer_result").setDisabled(true);
		clearValue("administered_sites", 1);
		clearValue("reactions", 1);
		clearValue("titer_result", 3);
	}
	else if(cat_id == 2)
	{
		Ext.getCmp("administered").setVisible(true);Ext.getCmp("administered").setDisabled(false);
		Ext.getCmp("administered_by").setVisible(true);Ext.getCmp("administered_by").setDisabled(false);
		Ext.getCmp("read_by").setVisible(false);Ext.getCmp("read_by").setDisabled(true);
		Ext.getCmp("result").setVisible(false);Ext.getCmp("result").setDisabled(true);
		Ext.getCmp("indurations_container").setVisible(false);Ext.getCmp("indurations_container").setDisabled(true);
		Ext.getCmp("confirmation_tests_container").setVisible(false);Ext.getCmp("confirmation_tests_container").setDisabled(true);
		Ext.getCmp("confirmation_date").setVisible(false);Ext.getCmp("confirmation_date").setDisabled(true);
		Ext.getCmp("confirmation_result").setVisible(false);Ext.getCmp("confirmation_result").setDisabled(true);
		Ext.getCmp("interventions_container").setVisible(true);Ext.getCmp("interventions_container").setDisabled(false);
		Ext.getCmp("administered_sites_container").setVisible(true);Ext.getCmp("administered_sites_container").setDisabled(false);
		Ext.getCmp("reactions_container").setVisible(true);Ext.getCmp("reactions_container").setDisabled(false);
		Ext.getCmp("titer_result").setVisible(false);Ext.getCmp("titer_result").setDisabled(true);
		clearValue("read_by", 1);
		clearValue("result", 2);
		clearValue("indurations", 1);
		clearValue("confirmation_tests", 2);
		clearValue("confirmation_date", 3);
		clearValue("confirmation_result", 2);
		clearValue("titer_result", 3);
		
	}
	else
	{
		Ext.getCmp("administered").setVisible(true);Ext.getCmp("administered").setDisabled(false);
		Ext.getCmp("administered_by").setVisible(true);Ext.getCmp("administered_by").setDisabled(false);
		Ext.getCmp("read_by").setVisible(false);Ext.getCmp("read_by").setDisabled(true);
		Ext.getCmp("result").setVisible(false);Ext.getCmp("result").setDisabled(true);
		Ext.getCmp("indurations_container").setVisible(false);Ext.getCmp("indurations_container").setDisabled(true);
		Ext.getCmp("confirmation_tests_container").setVisible(false);Ext.getCmp("confirmation_tests_container").setDisabled(true);
		Ext.getCmp("confirmation_date").setVisible(false);Ext.getCmp("confirmation_date").setDisabled(true);
		Ext.getCmp("confirmation_result").setVisible(false);Ext.getCmp("confirmation_result").setDisabled(true);
		Ext.getCmp("interventions_container").setVisible(true);Ext.getCmp("interventions_container").setDisabled(false);
		Ext.getCmp("administered_sites_container").setVisible(true);Ext.getCmp("administered_sites_container").setDisabled(false);
		Ext.getCmp("reactions_container").setVisible(true);Ext.getCmp("reactions_container").setDisabled(false);
		Ext.getCmp("titer_result").setVisible(true);Ext.getCmp("titer_result").setDisabled(false);
		clearValue("read_by", 1);
		clearValue("result", 2);
		clearValue("indurations", 1);
		clearValue("confirmation_tests", 1);
		clearValue("confirmation_date", 3);
		clearValue("confirmation_result", 2);
	}

	Ext.getCmp("brand").setVisible(true);Ext.getCmp("brand").setDisabled(false);
	Ext.getCmp("expiration_date").setVisible(true);Ext.getCmp("expiration_date").setDisabled(false);
	immunizationWindow.center();
}

function administeredFunction(description)
{
	if(description == "Inhouse")
	{
		if (immunizationCat == 1)
		{
			Ext.getCmp("result").setVisible(true);Ext.getCmp("result").setDisabled(false);
			Ext.getCmp("indurations_container").setVisible(true);Ext.getCmp("indurations_container").setDisabled(false);
			Ext.getCmp("confirmation_tests_container").setVisible(true);Ext.getCmp("confirmation_tests_container").setDisabled(false);
			Ext.getCmp("confirmation_date").setVisible(true);Ext.getCmp("confirmation_date").setDisabled(false);
			Ext.getCmp("confirmation_result").setVisible(true);Ext.getCmp("confirmation_result").setDisabled(false);
			Ext.getCmp("read_by").setVisible(true);Ext.getCmp("read_by").setDisabled(false);
		}
		else
		{
			if (immunizationCat == 3){
				Ext.getCmp("titer_result").setVisible(true);Ext.getCmp("titer_result").setDisabled(false);
			}
			Ext.getCmp("indurations_container").setVisible(false);Ext.getCmp("indurations_container").setDisabled(true);
			Ext.getCmp("brand").setVisible(true);Ext.getCmp("brand").setDisabled(false);
			Ext.getCmp("expiration_date").setVisible(true);Ext.getCmp("expiration_date").setDisabled(false);
			Ext.getCmp("reactions_container").setVisible(true);Ext.getCmp("reactions_container").setDisabled(false);
			Ext.getCmp("administered_sites_container").setVisible(true);Ext.getCmp("administered_sites_container").setDisabled(false);
			clearValue("indurations", 1);
		}
		Ext.getCmp("interventions_container").setVisible(true);Ext.getCmp("interventions_container").setDisabled(false);
		Ext.getCmp("administered_by").setVisible(true);Ext.getCmp("administered_by").setDisabled(false);		
		Ext.getCmp("brand").setVisible(true);Ext.getCmp("brand").setDisabled(false);
		Ext.getCmp("expiration_date").setVisible(true);Ext.getCmp("expiration_date").setDisabled(false);
	}
	else
	{
		if (immunizationCat == 1)
		{
			Ext.getCmp("result").setVisible(false);Ext.getCmp("result").setDisabled(true);
			Ext.getCmp("confirmation_tests_container").setVisible(false);Ext.getCmp("confirmation_tests_container").setDisabled(true);
			Ext.getCmp("confirmation_date").setVisible(false);Ext.getCmp("confirmation_date").setDisabled(true);
			Ext.getCmp("confirmation_result").setVisible(false);Ext.getCmp("confirmation_result").setDisabled(true);
			clearValue("result", 2);
			clearValue("confirmation_tests", 1);
			clearValue("confirmation_date", 3);
			clearValue("confirmation_result", 2);
		}
		else
		{
			Ext.getCmp("brand").setVisible(false);Ext.getCmp("brand").setDisabled(true);
			Ext.getCmp("expiration_date").setVisible(false);Ext.getCmp("expiration_date").setDisabled(true);
			Ext.getCmp("reactions_container").setVisible(false);Ext.getCmp("reactions_container").setDisabled(true);
			Ext.getCmp("administered_sites_container").setVisible(false);Ext.getCmp("administered_sites_container").setDisabled(true);
			clearValue("brand", 3);
			clearValue("expiration_date", 3);
			clearValue("reactions", 1);
			clearValue("administered_sites", 1);
		}
		Ext.getCmp("interventions_container").setVisible(false);Ext.getCmp("interventions_container").setDisabled(true);
		Ext.getCmp("indurations_container").setVisible(false);Ext.getCmp("indurations_container").setDisabled(true);
		Ext.getCmp("administered_by").setVisible(false);Ext.getCmp("administered_by").setDisabled(true);
		Ext.getCmp("read_by").setVisible(false);Ext.getCmp("read_by").setDisabled(true);
		Ext.getCmp("brand").setVisible(false);Ext.getCmp("brand").setDisabled(true);
		Ext.getCmp("expiration_date").setVisible(false);Ext.getCmp("expiration_date").setDisabled(true);
		clearValue("interventions", 1);
		clearValue("indurations", 1);
		clearValue("administered_by", 1);
		clearValue("read_by", 1);
		clearValue("brand", 3);
		clearValue("expiration_date", 3);		
	}
	immunizationWindow.center();
}

function resultFunction(description)
{
	if(description == "Positive (+)")
	{
		Ext.getCmp("confirmation_tests_container").setVisible(true);Ext.getCmp("confirmation_tests_container").setDisabled(false);
		Ext.getCmp("confirmation_date").setVisible(true);Ext.getCmp("confirmation_date").setDisabled(false);
		Ext.getCmp("confirmation_result").setVisible(true);Ext.getCmp("confirmation_result").setDisabled(false);
		Ext.getCmp("interventions_container").setVisible(true);Ext.getCmp("interventions_container").setDisabled(false);
		Ext.getCmp("indurations_container").setVisible(true);Ext.getCmp("indurations_container").setDisabled(false);
		Ext.getCmp("read_by").setVisible(true);Ext.getCmp("read_by").setDisabled(false);
	}
	else if(description == "Negative (-)")
	{
		Ext.getCmp("confirmation_tests_container").setVisible(false);Ext.getCmp("confirmation_tests_container").setDisabled(true);
		Ext.getCmp("confirmation_date").setVisible(false);Ext.getCmp("confirmation_date").setDisabled(true);
		Ext.getCmp("confirmation_result").setVisible(false);Ext.getCmp("confirmation_result").setDisabled(true);
		Ext.getCmp("interventions_container").setVisible(false);Ext.getCmp("interventions_container").setDisabled(true);		
		Ext.getCmp("indurations_container").setVisible(true);Ext.getCmp("indurations_container").setDisabled(false);
		Ext.getCmp("read_by").setVisible(true);Ext.getCmp("read_by").setDisabled(false);
		clearValue("confirmation_tests", 1);		
		clearValue("confirmation_date", 3);		
		clearValue("confirmation_result", 2);		
		clearValue("interventions", 1);		
	}	
	else if(description == "Pending")
	{
		Ext.getCmp("confirmation_tests_container").setVisible(false);Ext.getCmp("confirmation_tests_container").setDisabled(true);
		Ext.getCmp("confirmation_date").setVisible(false);Ext.getCmp("confirmation_date").setDisabled(true);
		Ext.getCmp("confirmation_result").setVisible(false);Ext.getCmp("confirmation_result").setDisabled(true);
		Ext.getCmp("interventions_container").setVisible(false);Ext.getCmp("interventions_container").setDisabled(true);		
		Ext.getCmp("indurations_container").setVisible(false);Ext.getCmp("indurations_container").setDisabled(true);
		Ext.getCmp("read_by").setVisible(false);Ext.getCmp("read_by").setDisabled(true);
		clearValue("confirmation_tests", 1);	
		clearValue("confirmation_date", 3);	
		clearValue("confirmation_result", 2);	
		clearValue("interventions", 1);	
		clearValue("indurations", 1);	
		clearValue("read_by", 1);	
	}	
	immunizationWindow.center();
}

function immunizationCRUD(type)
{
	params = new Object();

	if (type == "Delete")
	{
		params.id			= immunizationID;
		params.name_type	= nameType;
		params.type			= type;

		deleteFunction('immunization/immunizationcrud', params, 'immunizationGrid');
	}
	else
	{		
		params.id			= immunizationID;
		params.immunization_cat	= immunizationCat;
		params.name_id		= nameID;
		params.name_type	= nameType;
		params.rooms		= Ext.get('rooms').dom.value;
		params.immunizations = Ext.get('immunizations').dom.value;
		params.indurations	= Ext.get('indurations').dom.value;
		params.administered_sites = Ext.get('administered_sites').dom.value;
		params.confirmation_tests = Ext.get('confirmation_tests').dom.value;
		params.reactions	= Ext.get('reactions').dom.value;
		params.interventions = Ext.get('interventions').dom.value;
		params.administered_by = Ext.get('administered_by').dom.value;
		params.read_by 		= Ext.get('read_by').dom.value;
		params.type			= type;

		addeditFunction('immunization/immunizationcrud', params, 'immunizationGrid', immunizationCRUDForm, immunizationWindow);
	}
}

function AddEditDeleteImmunization(type)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp("immunizationGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}
		immunizationID = sm.selected.items[0].data.id;
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					immunizationCRUD(type);
			}
		});
	}
	else
	{
		var smStaff = Ext.getCmp("staffTree").getSelectionModel();
		if (!smStaff.hasSelection() || Ext.getCmp('radio2').checked == true)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}
		if (!nameID || nameID == 0)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}		

		immunizationCRUDForm = Ext.create('Ext.form.Panel', {
                border      : false,
                bodyStyle   : 'padding:10px;',  
                fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 130,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },                    
                items: [
                {
		            xtype   	: 'textfield',
		            id			: 'department',
		            name		: 'department',
		            fieldLabel	: 'Department',
		            readOnly	: true,
		            afterLabelTextTpl: null,
		            allowBlank: true
		        }, {
					xtype	: 'datefield',
					id		: 'date',
					name	: 'date',
					value	: new Date(),
					fieldLabel: 'Date'		
			   	}, {
		            xtype   	: 'combo',
		            id			: 'rooms',
		            fieldLabel	: 'Room',
		            readOnly	: true,
		            afterLabelTextTpl: null,
		            allowBlank: true
		        }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'immunizations',
			            fieldLabel	: 'Immunization',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,			            
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'immunizations', category:'immunizations_category'},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, {name: 'cat_id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('immunizations').dom.value = record[0].data.id;
			                	immunizationCat = record[0].data.cat_id;
			                	Ext.getCmp("administered").setValue(null);Ext.getCmp("administered").setRawValue(null);
			                	immunizationFunction(record[0].data.cat_id);
			                }
			            }
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Immunization Category',
			        	handler: function (){ viewMaintenance('immunizations_category', null); }
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Immunization',
			        	handler: function (){ viewMaintenance('immunizations', 'immunizations_category'); }
			        }]
				}, {
		            xtype   	: 'combo',
		            id			: 'administered',
		            name		: 'administered',
		            fieldLabel	: 'Administered',
		            valueField	: 'description',
		            displayField: 'description',
		            editable	: false,
		            hidden		: true,
                    disabled 	: true,
		            mode    	: 'local',
		            triggerAction : 'all',
		            store   : new Ext.data.ArrayStore({
		                fields: ['id', 'description'],
		                data: [[1, 'Inhouse'], [2, 'Outside Facility']]
		            }),
		            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	administeredFunction(record[0].data.description);
			                }
			            }
				}, {
			            xtype   	: 'combo',
			            id			: 'administered_by',     
			            fieldLabel	: 'Administered by',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            hidden		: true,
                    	disabled 	: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/staffname',
					            extraParams: {query:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('administered_by').dom.value = record[0].data.id;
			                }
			            }  
		        }, {
					xtype	: 'textareafield',
					height	: 40,
					id		: 'brand',
					name	: 'brand',
					hidden		: true,
					disabled 	: true,
					fieldLabel: 'Brand'			        			        	
				}, {	
					xtype	: 'datefield',
					id		: 'expiration_date',
					name	: 'expiration_date',
					value	: new Date(),
					hidden		: true,
					disabled 	: true,
					fieldLabel: 'Expiration Date'
				}, {
		            xtype   	: 'combo',
		            id			: 'result',
		            name		: 'result',
		            fieldLabel	: '48-72hrs. Result',
		            valueField	: 'description',
		            displayField: 'description',
		            hidden		: true,
		            disabled 	: true,
		            editable	: false,
		            mode    	: 'local',
		            triggerAction : 'all',
		            store   : new Ext.data.ArrayStore({
		                fields: ['id', 'description'],
		                data: [[1, 'Positive (+)'], [2, 'Negative (-)'], [3, 'Pending']]
		            }),
		            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	resultFunction(record[0].data.description);
			                }
			            }
				}, {
                    xtype: 'fieldcontainer',                    
                    id: 'indurations_container',
                    hidden		: true,
                    disabled 	: true,
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex		: 1,			       
			            id			: 'indurations',     
			            fieldLabel	: 'Induration',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'indurations', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('indurations').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Induration',
			        	handler: function (){ viewMaintenance('indurations', null); }
			        }]
				}, {
                    xtype: 'fieldcontainer',                    
                    id: 'administered_sites_container',
                    hidden		: true,
                    disabled 	: true,
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex		: 1,			       
			            id			: 'administered_sites',     
			            fieldLabel	: 'Site Administered',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'administered_sites', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('administered_sites').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Sites Administered',
			        	handler: function (){ viewMaintenance('administered_sites', null); }
			        }]
				}, {
			            xtype   	: 'combo',
			            id			: 'read_by',     
			            fieldLabel	: 'Read by',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            hidden		: true,
                    	disabled 	: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/staffname',
					            extraParams: {query:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('read_by').dom.value = record[0].data.id;
			                }
			            }  
		        }, {
                    xtype: 'fieldcontainer',                    
                    id: 'confirmation_tests_container',
                    hidden		: true,
                    disabled 	: true,
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex		: 1,			       
			            id			: 'confirmation_tests',     
			            fieldLabel	: 'Confirmation Test',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'confirmation_tests', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('confirmation_tests').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Confirmation Tests',
			        	handler: function (){ viewMaintenance('confirmation_tests', null); }
			        }]
				}, {	
					xtype	: 'datefield',
					id		: 'confirmation_date',
					name	: 'confirmation_date',
					value	: new Date(),
					hidden		: true,
					disabled 	: true,
					fieldLabel: 'Confirmation Date'
				}, {
		            xtype   	: 'combo',
		            id			: 'confirmation_result',
		            name		: 'confirmation_result',
		            fieldLabel	: 'Confirmation Result',
		            valueField	: 'description',
		            displayField: 'description',
		            hidden		: true,
		            disabled 	: true,
		            editable	: false,
		            mode    	: 'local',
		            triggerAction : 'all',
		            store   : new Ext.data.ArrayStore({
		                fields: ['id', 'description'],
		                data: [[1, 'Positive (+)'], [2, 'Negative (-)'], [3, 'Pending']]
		            })
				}, {
                    xtype: 'fieldcontainer',                    
                    id: 'reactions_container',
                    hidden		: true,
                    disabled 	: true,
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex		: 1,			       
			            id			: 'reactions',     
			            fieldLabel	: 'Reactions',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'reactions', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('reactions').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Reactions',
			        	handler: function (){ viewMaintenance('reactions', null); }
			        }]
				},{
                    xtype: 'fieldcontainer',                    
                    id: 'interventions_container',
                    hidden		: true,
                    disabled 	: true,
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex		: 1,			       
			            id			: 'interventions',     
			            fieldLabel	: 'Interventions',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'interventions', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('interventions').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Interventions',
			        	handler: function (){ viewMaintenance('interventions', null); }
			        }]
				},{
					xtype	: 'textfield',
					id		: 'titer_result',
					name	: 'titer_result',
					hidden		: true,
					disabled 	: true,
					fieldLabel: 'Titer Result'			        			        	
				}]
            });

			immunizationWindow = Ext.create('Ext.window.Window', {
			title		: type + ' Entry of <b>' + rsName + '</b>',
			closable	: true,
			modal		: true,
			width		: 450,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [immunizationCRUDForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!immunizationCRUDForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								immunizationCRUD(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	immunizationWindow.close();
			    }
			}],
		});

		immunizationCRUDForm.getForm().load({
			url: 'immunization/immunizationview',
			timeout: 30000,
			waitMsg:'Loading data...',
			params: {
				id: this.immunizationID, 
				name_id : nameID,    
	            name_type : nameType, 
				type: type
			},		
			success: function(form, action) {
				immunizationWindow.show();
				var data = action.result.data;
				Ext.getCmp("rooms").setRawValue(data.room_desc);
				Ext.get('rooms').dom.value = data.room_id;				

				if(type == 'Edit'){
					Ext.getCmp("immunizations").setRawValue(data.immunization_desc);
					Ext.getCmp("indurations").setRawValue(data.induration_desc);
					Ext.getCmp("administered_sites").setRawValue(data.administered_site_desc);
					Ext.getCmp("confirmation_tests").setRawValue(data.confirmation_test_desc);
					Ext.getCmp("reactions").setRawValue(data.reaction_desc);
					Ext.getCmp("interventions").setRawValue(data.intervention_desc);
					Ext.getCmp("administered_by").setRawValue(data.administered_name);
					Ext.getCmp("read_by").setRawValue(data.read_name);

					Ext.get('immunizations').dom.value = data.immunization_id;
					Ext.get('indurations').dom.value = data.induration_id;
					Ext.get('administered_sites').dom.value = data.administered_site_id;
					Ext.get('confirmation_tests').dom.value = data.confirmation_test_id;
					Ext.get('reactions').dom.value = data.reaction_id;
					Ext.get('interventions').dom.value = data.intervention_id;
					Ext.get('administered_by').dom.value = data.administered_by_id;
					Ext.get('read_by').dom.value = data.read_by_id;

					immunizationCat = data.cat_id;
					immunizationFunction(data.cat_id);
					administeredFunction(data.administered);
					resultFunction(data.result);
				}
				else
				{	
					if(data.room_id == null & nameType == 'Resident')	
					{
						errorFunction("Load Failed!", 'Please identify the room of the resident at the <img src="../image/profile.png"> PROFILE RESIDENT form.');
						immunizationWindow.close();
					}	
				}
			},		
			failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		});
		if (nameType == 'Resident') Ext.getCmp("department").setVisible(false);
		else Ext.getCmp("rooms").setVisible(false);

		Ext.getCmp("administered").focus();
	}
}