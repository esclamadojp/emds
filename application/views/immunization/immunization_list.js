setTimeout("UpdateSessionData();", 0);

var rsName, nameType = 'Resident', gridType = 'Name';

function ExportDocs(type) { 

    params = new Object();
    params.query        = Ext.getCmp("searchImmunization").getValue();  
    params.name_id      = nameID;
    params.rsname       = rsName;
    params.date_from    = Ext.getCmp("dateFrom").getValue();
    params.date_to      = Ext.getCmp("dateTo").getValue();
    params.type         = gridType;
    params.name_type    = nameType;
    params.filetype     = type;

    ExportDocument('immunization/exportdocument', params, type);
}

Ext.onReady(function(){
      
    var treeStore = Ext.create('Ext.data.TreeStore', {
        proxy: {
            type: 'ajax',
            reader: 'json',
            extraParams: {query:null, type:'Resident'},
            url: 'immunization/namelist'
        }     
    }); 

    var RefreshTreeStore = function () {
        Ext.getCmp("staffTree").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };

    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Names',
        split   : true,
        region  : 'west',
        collapsible: true,
        id : 'staffTree',
        store: treeStore,
        rowLines: true,
        width: '30%',
        minWidth: 200,
        margin: '0 0 10 0',
        height: 500,        
        rootVisible: false,
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchName',
            emptyText: 'Search here...',
            width   : '40%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("staffTree").getStore().proxy.extraParams["query"] = Ext.getCmp("searchName").getValue();
                        RefreshTreeStore();
                    }
                }
            }
        },
        { xtype: 'tbfill'},        
        {
            xtype   : 'radio',
            boxLabel  : 'Resident',
            inputValue: '1',
            checked   : true,
            id        : 'rbResident',
            name      : 'nameType',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('rbResident').checked == true)
                    {
                        Ext.getCmp("staffTree").getStore().proxy.extraParams["type"] = 'Resident';
                        Ext.getCmp("staffTree").getStore().proxy.extraParams["query"] = null;
                        Ext.getCmp('searchName').setValue(null);
                        nameType = 'Resident';
                        nameID = 0;
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["name_id"] = 0; 
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["name_type"] = nameType; 
                        Ext.getCmp("immunizationGrid").setTitle('Details');
                        RefreshTreeStore();  
                        RefreshGridStore();
                    }
                } 
            }
        }, {
            xtype   : 'radio',
            boxLabel  : 'Staff',
            inputValue: '2',            
            id        : 'rbStaff',
            name      : 'nameType',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('rbStaff').checked == true)
                    {
                        Ext.getCmp("staffTree").getStore().proxy.extraParams["type"] = 'Staff';
                        Ext.getCmp("staffTree").getStore().proxy.extraParams["query"] = null;
                        Ext.getCmp('searchName').setValue(null);
                        nameType = 'Staff';
                        nameID = 0;
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["name_id"] = 0;   
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["name_type"] = nameType; 
                        Ext.getCmp("immunizationGrid").setTitle('Details');
                        RefreshTreeStore();
                        RefreshGridStore();
                    }
                }
            }
        },
        { xtype: 'button', icon: '../image/add.png', tooltip: 'Add Name', 
            handler: function ()
                    { 
                        if (nameType == 'Resident')
                            AddEditDeleteResident('Add', 'staffTree', null);
                        else 
                            AddEditDeleteStaff('Add', 'staffTree', null);
                    }
        },
        { xtype: 'button', icon: '../image/edit.png', tooltip: 'Edit Name', 
            handler: function ()
                    {   
                        if (nameType == 'Resident')
                            AddEditDeleteResident('Edit', 'staffTree', null);
                        else 
                            AddEditDeleteStaff('Edit', 'staffTree', null);
                    }
        },
        { xtype: 'button', icon: '../image/delete.png', tooltip: 'Delete Name', 
            handler: function ()
                    { 
                        if (nameType == 'Resident')
                            AddEditDeleteResident('Delete', 'staffTree', 'immunizationGrid');
                        else 
                            AddEditDeleteStaff('Delete', 'staffTree', 'immunizationGrid');
                    }
        }
        ],
        viewConfig: {
            listeners: {
                itemclick: function(view,rec,item,index,eventObj) {
                    Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                    Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                    Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["name_id"] = rec.get('id');  
                    nameID = rec.get('id');  
                    rsName = rec.get('text');
                    if (rsName == 'Names')
                        Ext.getCmp("immunizationGrid").setTitle('Details');
                    else
                        Ext.getCmp("immunizationGrid").setTitle('Immunization details of <font size=2><b>'+rsName+'</b></font>');
                    RefreshGridStore();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    nameMenu.showAt(e.getXY());
                }
            }
        },
        listeners:
        {
            collapse : function() {
                Ext.getCmp('radio1').setValue(false);
                Ext.getCmp('radio2').setValue(true);
            },
            expand : function() {
                Ext.getCmp('radio1').setValue(true);
                Ext.getCmp('radio2').setValue(false);
            }
        }
    });

    var nameMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function ()
            { 
                if (nameType == 'Resident')
                    AddEditDeleteResident('Add', 'staffTree', null);
                else 
                    AddEditDeleteStaff('Add', 'staffTree', null);
            }
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function ()
            { 
                if (nameType == 'Resident')
                    AddEditDeleteResident('Edit', 'staffTree', null);
                else 
                    AddEditDeleteStaff('Edit', 'staffTree', null);
            }
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function ()
            { 
                if (nameType == 'Resident')
                    AddEditDeleteResident('Delete', 'staffTree', 'immunizationGrid');
                else 
                    AddEditDeleteStaff('Delete', 'staffTree', 'immunizationGrid');
            }
        }]
    });

    var storeServiceList = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'immunization/immunizationlist',
            extraParams: {start: 0, limit: 20, query:null, type: 'Name', name_id: 0, name_type: nameType, date_from: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), date_to: Ext.Date.add(new Date(), Ext.Date.DAY, 180)},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'date', 'immunization_desc', 'administered', 'html']
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("immunizationGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };     

    var grid = Ext.create('Ext.grid.Panel', {
        id      : 'immunizationGrid',
        region  : 'center',
        store: storeServiceList,
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Date',  dataIndex: 'date', width: '10%'},
            { text: 'Immunization', dataIndex: 'immunization_desc', width: '20%'},
            { text: 'Administered', dataIndex: 'administered', width: '15%'}
        ],
        plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate('{html}')
        }],
        columnLines: true,
        width: '80%',
        minWidth: 700,
        height: 400,
        title: 'Details',
        loadMask: true,
        margin: '0 0 10 0',
        viewConfig: {
            listeners: {
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        },
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchImmunization',
            emptyText: 'Search here...',
            width   : '15%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchImmunization").getValue();
                        RefreshGridStore();
                    }
                }
            }
        },
        {
            xtype   : 'radio',
            boxLabel  : 'Name',
            name      : 'searchtype',            
            inputValue: '1',
            checked   : true,
            id        : 'radio1',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio1').checked == true)
                    {
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["type"] = 'Name';
                        gridType = 'Name';
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["query"] = null;
                        Ext.getCmp('searchImmunization').setValue(null);
                        RefreshGridStore();
                        Ext.getCmp("immunizationGrid").setTitle('Immunization details of <font size=2><b>'+rsName+'b></font>');
                        Ext.getCmp('staffTree').expand();
                    }
                }
            }
        }, {
            xtype   : 'radio',
            boxLabel  : 'All',
            name      : 'searchtype',            
            inputValue: '2',            
            id        : 'radio2',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio2').checked == true){
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["type"] = 'All';
                        gridType = 'All';
                        Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["query"] = null;
                        Ext.getCmp('searchImmunization').setValue(null);
                        RefreshGridStore();
                        Ext.getCmp("immunizationGrid").setTitle('Details');
                        Ext.getCmp("staffTree").collapse();
                    }
                }
            }
        }, 
        {
            xtype: 'label',
            html: '<font size=2 color=><b>From:</b></font>'
        },
        { xtype: 'datefield', id:'dateFrom', emptyText: 'From',  value: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), width: 100},
        {
            xtype: 'label',
            html: '<font size=2 color=><b>To:</b></font>'
        },
        { xtype: 'datefield', id:'dateTo', emptyText: 'To',  value: Ext.Date.add(new Date(), Ext.Date.DAY, 180), width: 100},
        { xtype: 'button', text: 'RELOAD', icon: '../image/load.png', tooltip: 'Reload grid based on date range', 
            handler: function (){ 
                Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                Ext.getCmp("immunizationGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                RefreshGridStore();
            }
        },        
        { xtype: 'tbfill'},
        { xtype: 'button', icon: '../image/add.png', tooltip: 'Add Immunization', handler: function (){ AddEditDeleteImmunization('Add');}},
        { xtype: 'button', icon: '../image/edit.png', tooltip: 'Edit Immunization', handler: function (){ AddEditDeleteImmunization('Edit');}},
        { xtype: 'button', icon: '../image/delete.png', tooltip: 'Delete Immunization', handler: function (){ AddEditDeleteImmunization('Delete');}},
        {
            text: 'Download',
            tooltip: 'Extract Data to PDF or EXCEL File Format',
            icon: '../image/download.png',
            menu: 
            {
                items: 
                [
                    {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            ExportDocs('PDF');
                        }
                    }, 
                    {
                        text    : 'Export Excel Format',
                        icon: '../image/excel.png',
                        handler: function ()
                        {
                            ExportDocs('Excel');
                        }
                    }
                ]
            }
        }]
    });
    RefreshGridStore(); 

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteImmunization('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteImmunization('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteImmunization('Delete');}
        }]
    });

    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [tree,grid]
    });
});
