var caseEntryWindow, caseEntryForm, medicationIDCount;
var myMedication= new Array();
var myDosage 	= new Array();
var myTablet 	= new Array();
var myRoute 	= new Array();
var myFrequency = new Array();
var myDuration 	= new Array();
var fieldsetMedication;

function medicationInsert(id, medicationID, dosageID, tabletID, routeID, frequencyID, durationID, medication, dosage, tablet, route, frequency, duration)
{
	fieldsetMedication.add(
	{
		xtype: 'panel',
		id: 'panelMedication' + id,
		margin : '0 0 0 20',
		layout:'column',
		border: false,
		items:[{
			    xtype   : 'label',
			    autoEl: {
				  tag: 'label',
				  'data-qtip': '<b>'+medication+'</b> <font color=deeppink>'+dosage+'</font> <font color=orangered>'+tablet+'</font> <font color=darkred>'+route+'</font>, <font color=darkgreen>'+frequency+'</font> <font color= darkblue>'+duration+'</font>'
				},
			    html  	: '<a href="#" style="text-decoration:none;">'+medication+' '+dosage+' '+tablet+' ...</a>',
			},{
			    xtype   : 'label',
			    autoEl: {
				  tag: 'label',
				  'data-qtip': '<b>REMOVE:</b> '+medication+' '+dosage+' '+tablet+' '+route+'...'
				},
			    html  : '<a href="#" onclick="DeleteMedication('+id+')"><IMG SRC="../image/delete.gif"</a><br>',
		}]
	});
	fieldsetMedication.doLayout();
	
	caseEntryWindow.center();
	myMedication.splice(id, 0, medicationID);
	myDosage.splice(id, 0, dosageID);
	myTablet.splice(id, 0, tabletID);
	myRoute.splice(id, 0, routeID);
	myFrequency.splice(id, 0, frequencyID);
	myDuration.splice(id, 0, durationID);
	medicationIDCount++;
}

function functionHideDisabled(option) 
{
	if(option == 1)
	{
    	Ext.getCmp("case_type_id").setValue(null);Ext.getCmp("case_type_id").setRawValue(null);
    	Ext.get('case_type_id').dom.value = null;
        Ext.getCmp("case_type_id").setVisible(false);Ext.getCmp("case_type_id").setDisabled(true);
        Ext.getCmp("diagnosis_container").setVisible(true);Ext.getCmp("diagnosis_container").setDisabled(false);
        Ext.getCmp("infections_container").setVisible(true);Ext.getCmp("infections_container").setDisabled(false);
        Ext.getCmp("date_resolved").setVisible(true);Ext.getCmp("date_resolved").setDisabled(false);
	}
	else
	{
        Ext.getCmp("case_type_id").setVisible(true);Ext.getCmp("case_type_id").setDisabled(false);
        Ext.getCmp("diagnosis_container").setVisible(false);Ext.getCmp("diagnosis_container").setDisabled(true);
        Ext.getCmp("infections_container").setVisible(false);Ext.getCmp("infections_container").setDisabled(true);
        Ext.getCmp("date_resolved").setVisible(false);Ext.getCmp("date_resolved").setDisabled(true);
	}


}

function residentCRUD(type)
{ 
	params = new Object();

	if (type == "Delete")
	{
		params.id	= caseEntryID;
		params.type	= type;

		deleteFunction('infection_control/crud', params, 'ResidentGrid');
	}
	else
	{
		params.id			= caseEntryID;
		params.resident_id	= residentID;
		params.room_no		= Ext.get('rooms').dom.value;
		params.diagnosis	= Ext.get('diagnosis').dom.value;
		params.case_type_id	= Ext.get('case_type_id').dom.value;
		params.signs_symptoms = Ext.get('signs_symptoms').dom.value;
		params.infections	= Ext.get('infections').dom.value;
		params.tests		= Ext.get('tests').dom.value;
		params.conditions	= Ext.get('conditions').dom.value;
		params.transmission_precautions	= Ext.get('transmission_precautions').dom.value;
		params.consults		= Ext.get('consults').dom.value;
		params.physicians	= Ext.get('physicians').dom.value;
		params.medications	= myMedication.toString();
		params.dosages		= myDosage.toString();
		params.tablets		= myTablet.toString();
		params.routes		= myRoute.toString();
		params.frequencies	= myFrequency.toString();
		params.durations	= myDuration.toString();
		params.type			= type;

		addeditFunction('infection_control/crud', params, 'ResidentGrid', caseEntryForm, caseEntryWindow);		
	}
}
 
function AddEditDeleteResidentCase(type)
{          
	myMedication = [];
	myDosage 	 = [];
	myTablet 	 = [];
	myRoute 	 = [];
	myFrequency  = [];
	myDuration 	 = [];
	medicationIDCount = 0;

	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp("ResidentGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select a record.");
			return;
		}
		caseEntryID = sm.selected.items[0].data.id;
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					residentCRUD(type);
			}
		});
	}
	else 
	{
		if(type == 'Edit') var caseHide = true; else var caseHide = false;

		fieldsetMedication = new Ext.form.FieldSet({
			autoScroll: true,		
			autoHeight:true,
			margin : '0 0 10 10',
			title: '<font size=2><b>Medication/s</b></font>' + '&nbsp<a href="#" onclick="AddMedication()"><button class="btnadd">ADD</button></a>'
		});

		caseEntryForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',				
				fieldDefaults: {
		        	labelWidth: 120,
					labelAlign	: 'right',
					anchor:'100%'					
		        },
				items: [	
				{
					xtype: 'label',
					id: 'lblNewCase',
					hidden: true,
					html: '<font align="center" size=3 color=red><b>NEW CASE</b></font>'
				}, {
					xtype: 'label',
					id: 'lblCurrentCase',
					hidden: true,
					html: '<font size=3><b>CURRENT / SAME CASE</b></font>'
				}, {
		            xtype: 'radiogroup',	
		            hidden: caseHide,	            
		            fieldLabel: 'Case Type',
		            items: [
	                {
	                	boxLabel: '<font color=red>NEW CASE</font>', 
	                	inputValue: 1, 
	                	name: 'case_type', 
	                	checked: true,
	                	id: 'ckNewCase',
		                listeners:
			            {
			                change : function() {
			                	if(Ext.getCmp('ckNewCase').checked == true)
				                	functionHideDisabled(1);
			                }
			            }
		            },
	                {
	                	boxLabel: 'Current Case', 
	                	inputValue: 2, 
	                	name: 'case_type',
	                	id: 'ckCurrentCase',
		                listeners:
			            {
			                change : function() {
			                	if(Ext.getCmp('ckCurrentCase').checked == true)
			                		functionHideDisabled(2);
			                }
			            }
		            }]
		        }, {
		            xtype   	: 'combo',
		            id			: 'rooms',
		            fieldLabel	: 'Room',
		            disabled	: true
		        }, 
		        {    
                    xtype   : 'datefield',
                    id      : 'date',
                    name    : 'date',
                    value	: new Date(),
                    afterLabelTextTpl: required,
		            allowBlank: false,	
                    fieldLabel: 'Case Date'       
                }, {
                    xtype: 'fieldcontainer',                    
                    id: 'diagnosis_container',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
                        xtype       : 'combo',
                        flex: 1,
                        id          : 'diagnosis',
                        fieldLabel  : 'Infectious Disease',                        
                        allowBlank: false,
                        multiSelect : true,
                        valueField  : 'id',
                        displayField: 'description',
                        afterLabelTextTpl: required,
			            allowBlank: false,
                        triggerAction: 'all',
                        minChars    : 3,
                        matchFieldWidth: false,
                        enableKeyEvents: true,
                        readOnly    : false,
                        ddReorder: true,
                        forceSelection: true,
                        store: new Ext.data.JsonStore({
                            proxy: {
                                type: 'ajax',
                                url: 'commonquery/caseentrylist',
                                extraParams: {query:null, type: 'diagnosis', category:null},
                                reader: {
                                    type: 'json',
                                    root: 'data',
                                    idProperty: 'id'
                                }
                            },
                            params: {start: 0, limit: 10},
                            fields: [{name: 'id', type: 'int'}, 'description']
                        }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {   
                                var diagnosisValue = Ext.getCmp("diagnosis").getValue();
                                Ext.get('diagnosis').dom.value = diagnosisValue.toString();
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        hidden: crudMaintenance,
                        margins     : '0 0 0 5',
                        text: '...',
                        tooltip: 'Add/Edit/Delete Diagnosis Category',
                        handler: function (){ viewMaintenance('diagnosis_category', null); }
                    },
                    {
                        xtype: 'button',
                        hidden: crudMaintenance,
                        margins     : '0 0 0 5',
                        text: '...',
                        tooltip: 'Add/Edit/Delete Diagnose',
                        handler: function (){ viewMaintenance('diagnosis','diagnosis_category'); }
                    }]
                }, {
                    xtype: 'fieldcontainer',
                    id: 'infections_container',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'infections',
			            fieldLabel	: 'Origin of Infection',
			            afterLabelTextTpl: required,
			            allowBlank: false,
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,			            
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'infections', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {        
                                Ext.get('infections').dom.value = record[0].data.id;
                            }
                        }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Infection',
			        	handler: function (){ viewMaintenance('infections', null); }
			        }]
				}, {
		            xtype   	: 'combo',
		            flex: 1,
		            id			: 'case_type_id',
		            fieldLabel	: 'Case Type',
		            afterLabelTextTpl: required,
		            allowBlank: false,
					valueField	: 'id',
					displayField: 'description',
					triggerAction: 'all',
					minChars    : 3,			            
		            disabled 	: true,
		            matchFieldWidth: false,
		            enableKeyEvents: true,
		            hidden		: true,
		            readOnly    : false,
		            forceSelection: true,
		            store: new Ext.data.JsonStore({
				        proxy: {
				            type: 'ajax',
				            url: 'infection_control/casetypelist',
				            extraParams: {query:null, resident_id: residentID},
				            reader: {
				                type: 'json',
				                root: 'data',
				                idProperty: 'id'
				            }
				        },
				        params: {start: 0, limit: 10},
				        fields: [{name: 'id', type: 'int'}, 'description']
		            }),
                    listeners: 
                    {
                        select: function (combo, record, index)
                        {        
                            Ext.get('case_type_id').dom.value = record[0].data.id;
                        }
                    } 		        
		        }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'signs_symptoms',
			            fieldLabel	: 'Sign & Symptoms',
			            multiSelect	: true,
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'signs_symptoms', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {   
                                var signs_symptomsValue = Ext.getCmp("signs_symptoms").getValue();
                                Ext.get('signs_symptoms').dom.value = signs_symptomsValue.toString();
                            }
                        }
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Sign and Symptom',
			        	handler: function (){ viewMaintenance('signs_symptoms', null); }
			        }]
			    }, fieldsetMedication, {	
					xtype	: 'datefield',
					id		: 'lab_report',
					name	: 'lab_report',
					allowBlank : true,
					fieldLabel: 'Lab Report Date'		
				},{
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'tests',
			            fieldLabel	: 'Test',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            ddReorder: true,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'tests', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {        
                                Ext.get('tests').dom.value = record[0].data.id;
                            }
                        }
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Test',
			        	handler: function (){ viewMaintenance('tests', null); }
			        }]            
				}, {
					xtype	: 'textareafield',
					id		: 'microorganism',
					name	: 'microorganism',	
					height	: 40,					
					fieldLabel: 'Microorganisms'				
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'conditions',
			            fieldLabel	: 'Condition',
			            afterLabelTextTpl: required,
			            allowBlank: false,
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'conditions', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {        
                                Ext.get('conditions').dom.value = record[0].data.id;
                            }
                        }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Condition',
			        	handler: function (){ viewMaintenance('conditions', null); }
			        }]		
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'transmission_precautions',
			            fieldLabel	: 'Trans. Precaution',
			            afterLabelTextTpl: required,
			            allowBlank: false,
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'transmission_precautions', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {        
                                Ext.get('transmission_precautions').dom.value = record[0].data.id;
                            }
                        }    
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Transmission Precaution Category',
			        	handler: function (){ viewMaintenance('transmission_precautions_category', null); }
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Transmission Precaution',
			        	handler: function (){ viewMaintenance('transmission_precautions', 'transmission_precautions_category'); }
			        }]		
				}, {
                    xtype: 'fieldcontainer',
                    id: 'consults_container',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'consults',
			            fieldLabel	: 'Consult',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            ddReorder: true,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'consults', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {        
                                Ext.get('consults').dom.value = record[0].data.id;
                            }
                        }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Consult',
			        	handler: function (){ viewMaintenance('consults', null); }
			        }]
                }, {
                    xtype: 'fieldcontainer',                    
                    id: 'physicians_container',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex		: 1,			       
			            id			: 'physicians',     
			            fieldLabel	: 'Physician',
			            afterLabelTextTpl: required,
			            allowBlank: false,
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'physicians', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
                        listeners: 
                        {
                            select: function (combo, record, index)
                            {        
                                Ext.get('physicians').dom.value = record[0].data.id;
                            }
                        }    
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Physician',
			        	handler: function (){ viewMaintenance('physicians', null); }
			        }]
				}, {
					xtype	: 'textareafield',
					id		: 'other',
					name	: 'other',				
					height	: 40,	
					fieldLabel: 'Other'                				
			    },{    
                    xtype   : 'datefield',
                    id      : 'date_resolved',
                    name    : 'date_resolved',
                    fieldLabel: 'Date Resolved'       
                }]
			});

			caseEntryWindow = Ext.create('Ext.window.Window', {
			title		: type + ' Case Entry',
			closable	: true,
			modal		: true,
			width		: 400,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [caseEntryForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!caseEntryForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								residentCRUD(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	caseEntryWindow.close();
			    }
			}],
		});

		caseEntryForm.getForm().load({
			url: 'infection_control/view',
			timeout: 30000,
			waitMsg:'Loading data...',
			params: {
				id: caseEntryID, 
				resident_id:residentID,
				type: type
			},		
			success: function(form, action) {
				caseEntryWindow.show();
				var data = action.result.data;
				Ext.getCmp("rooms").setRawValue(data.room_desc);
				Ext.get('rooms').dom.value = data.room_id;				
	
				if(type == 'Edit')
				{
					Ext.getCmp("diagnosis").setRawValue(data.diagnose_desc);
					Ext.getCmp("infections").setRawValue(data.infection_desc);
					Ext.getCmp("signs_symptoms").setRawValue(data.ss_desc);
					Ext.getCmp("tests").setRawValue(data.test_desc);
					Ext.getCmp("conditions").setRawValue(data.condition_desc);
					Ext.getCmp("transmission_precautions").setRawValue(data.tp_desc);
					Ext.getCmp("consults").setRawValue(data.consult_desc);
					Ext.getCmp("physicians").setRawValue(data.physician_desc);
					Ext.getCmp("case_type_id").setRawValue(data.case_type_desc);

					Ext.get('diagnosis').dom.value = data.diagnose_id;
					Ext.get('infections').dom.value = data.infection_id;
					Ext.get('signs_symptoms').dom.value = data.ss_id;				
					Ext.get('tests').dom.value = data.test_id;
					Ext.get('conditions').dom.value = data.condition_id;
					Ext.get('transmission_precautions').dom.value = data.tp_id;
					Ext.get('consults').dom.value = data.consult_id;
					Ext.get('physicians').dom.value = data.physician_id;
					Ext.get('case_type_id').dom.value = data.case_type_id;

					functionHideDisabled(data.case_type);
           			for (var i = 0; i < action.result.totalCount; i++) 
						medicationInsert(medicationIDCount, action.result.medicationID[i], action.result.dosageID[i], action.result.tabletID[i], action.result.routeID[i], action.result.frequencyID[i], action.result.durationID[i], action.result.medication[i], action.result.dosage[i], action.result.tablet[i], action.result.route[i], action.result.frequency[i], action.result.duration[i]);
					
					if (data.case_type == 1) Ext.getCmp("lblNewCase").setVisible(true);
					else Ext.getCmp("lblCurrentCase").setVisible(true);

					var admin = '<?php echo $admin;?>';
					if(!admin && (data.date_resolved)) {
						errorFunction('Error! Edit is not allowed.', 'Case resolved dated: '+data.resolved_date);
						caseEntryWindow.close();
					}

					Ext.getCmp("case_type").setValue(data.case_type);					
				}
			},		
			failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		});
	}		
}