setTimeout("UpdateSessionData();", 0);

var status = 2, query = null;

function ExportResidentsList(type) {
    params = new Object();
    params.type     = type;
    params.query    = query;
    params.status   = status;
    params.report   = 'ResidentsList';

    ExportDocument('infection_control/exportdocument', params, type);
}

Ext.onReady(function(){
 
    var store = new Ext.data.JsonStore({
        pageSize: setLimit,
        storeId: 'myStore',
        proxy: {
            type: 'ajax',
            url: 'infection_control/residentcaseList',
            extraParams: {query:query, status: status},
            remoteSort: false,
            params: {start: 0, limit: setLimit},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },        
        fields: [{name: 'id', type: 'int'}, 'name', 'age', 'room', 'details', 'sex']
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("resident_caseGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    function addTooltip(value, metadata, record, rowIndex, colIndex, store){
        metadata.tdAttr = 'data-qtip="' + record.get('details') + '"';
        return value;
    }
    var grid1 = Ext.create('Ext.grid.Panel', {
        id      : 'resident_caseGrid',
        region  : 'center',
        store:store,
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Name', dataIndex: 'name', width: '25%'},
            { text: 'Room', dataIndex: 'room', width: '12%', renderer:addTooltip},
            { text: 'Age', dataIndex: 'age', width: '6%', align:'center'},
            { text: 'Sex', dataIndex: 'sex', width: '6%'}
        ],
        columnLines: true,
        width: '100%',
        margin: '0 0 10 0',
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    LoadResidentCases('grid', null, null, null, null);
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        },
        bbar: Ext.create('Ext.PagingToolbar', {
            store: store,
            pageSize: setLimit,
            displayInfo: true,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No record/s to display"
        })
    });
    RefreshGridStore(); 

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'View Profile Details',
            icon: '../image/profile.png',
            handler: function (){ LoadResidentProfile("resident_caseGrid");}
        },
        {
            text: 'View Case Details',
            icon: '../image/view.png',
            handler: function (){ LoadResidentCases('grid', null, null, null, null);}
        },
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteResident('Add', 'resident_caseGrid', null);}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteResident('Edit', 'resident_caseGrid', null);}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteResident('Delete', 'resident_caseGrid', null);}
        }]
    });


    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [grid1],
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchId',
            emptyText: 'Search here...',
            width   : '30%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("resident_caseGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchId").getValue();
                        query = Ext.getCmp("searchId").getValue();
                        RefreshGridStore();
                    }
                }
            }
        },
        { xtype: 'button', icon: '../image/gear.png', tooltip: 'Advanced Search', handler: function (){ AdvancedSearch();}},
        '-',
        {
            xtype   : 'radio',
            boxLabel  : 'All',
            name      : 'Status',            
            inputValue: '1',
            id        : 'radio1',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio1').checked == true)
                    {
                        Ext.getCmp("resident_caseGrid").getStore().proxy.extraParams["status"] = 1;
                        status = 1;
                        RefreshGridStore();
                    }
                }
            }
        }, {
            xtype   : 'radio',
            boxLabel  : 'Admitted',
            name      : 'Status',
            checked   : true,
            inputValue: '2',            
            id        : 'radio2',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio2').checked == true)
                    {
                        Ext.getCmp("resident_caseGrid").getStore().proxy.extraParams["status"] = 2;
                        status = 2;
                        RefreshGridStore();
                    }
                }
            }
        }, {
            xtype   : 'radio',
            boxLabel  : 'Discharged',
            name      : 'Status',
            inputValue: '3',
            id        : 'radio3',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio3').checked == true)
                    {
                        Ext.getCmp("resident_caseGrid").getStore().proxy.extraParams["status"] = 3;
                        status = 3;
                        RefreshGridStore();
                    }
                }
            }
        },        
        { xtype: 'tbfill'},
        { xtype: 'button', text: 'ADD', icon: '../image/add.png', tooltip: 'Add Resident Record', handler: function (){ AddEditDeleteResident('Add', 'resident_caseGrid', null);}},
        { xtype: 'button', text: 'EDIT', icon: '../image/edit.png', tooltip: 'Edit Resident Record', handler: function (){ AddEditDeleteResident('Edit', 'resident_caseGrid', null);}},
        { xtype: 'button', text: 'DELETE', icon: '../image/delete.png', tooltip: 'Delete Resident Record', handler: function (){ AddEditDeleteResident('Delete', 'resident_caseGrid', null);}},
        '-',
        {
            text: 'View Profile Details',
            tooltip: 'View resident profile details',
            icon: '../image/profile.png',
            handler: function (){ LoadResidentProfile("resident_caseGrid");}
        },
        '-',
        {
            text: 'View Case Details',
            tooltip: 'View resident case details',
            icon: '../image/view.png',
            handler: function (){ LoadResidentCases('grid', null, null, null, null);}
        },
        '-',
        {
            text: 'Download',
            tooltip: 'Extract Data to PDF or EXCEL File Format',
            icon: '../image/download.png',
            menu: 
            {
                items: 
                [
                    {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            ExportResidentsList('PDF');
                        }
                    }, 
                    {
                        text    : 'Export Excel Format',
                        icon: '../image/excel.png',
                        handler: function ()
                        {
                            ExportResidentsList('Excel');
                        }
                    }
                ]
            }
        }]
    });
});
