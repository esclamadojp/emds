var medicationCRUDWindow, medicationCRUDForm;

function medicationRemove(id)
{
	fieldsetMedication.remove('panelMedication' + id);
	fieldsetMedication.doLayout();
	myMedication.splice(id, 1, null);
	myDosage.splice(id, 1, null);
	myTablet.splice(id, 1, null);
	myRoute.splice(id, 1, null);
	myFrequency.splice(id, 1, null);
	myDuration.splice(id, 1, null);

	caseEntryWindow.center();
}

function DeleteMedication(id)
{
	Ext.Msg.show({
		title	: 'Confirmation',
		msg		: 'Are you sure you want to Remove?',
		width	: '100%',
		icon	: Ext.Msg.QUESTION,
		buttons	: Ext.Msg.YESNO,
		fn: function(btn){
			if (btn == 'yes')
				medicationRemove(id);
		}
	});	
}

function AddMedication()
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

		medicationCRUDForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 120,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [{
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'medications',
			            name		: 'medications',
			            fieldLabel	: 'Medication',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'medications', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            })  
			        },
			        {
			        	xtype: 'button',
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Medication',
			        	handler: function (){ viewMaintenance('medications', null); }
			        }]    			    			           
                }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'dosages',
			            name		: 'dosages',
			            fieldLabel	: 'Dosage',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'dosages', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            })  
			        },
			        {
			        	xtype: 'button',
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Dosage',
			        	handler: function (){ viewMaintenance('dosages', null); }
			        }]    			    			           
                }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'tablets',
			            name		: 'tablets',
			            fieldLabel	: 'Medication Form',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'tablets', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            })  
			        },
			        {
			        	xtype: 'button',
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Tablet',
			        	handler: function (){ viewMaintenance('tablets', null); }
			        }]    			    			           
                }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'routes',
			            name		: 'routes',
			            fieldLabel	: 'Route',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'routes', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            })  
			        },
			        {
			        	xtype: 'button',
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Route',
			        	handler: function (){ viewMaintenance('routes', null); }
			        }]    			    			           
                }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'frequencies',
			            name		: 'frequencies',
			            fieldLabel	: 'Frequency',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'frequencies', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            })  
			        },
			        {
			        	xtype: 'button',
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Frequency',
			        	handler: function (){ viewMaintenance('frequencies', null); }
			        }]    			    			           
                }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'durations',
			            name		: 'durations',
			            fieldLabel	: 'Duration',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'durations', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            })  
			        },
			        {
			        	xtype: 'button',
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Duration',
			        	handler: function (){ viewMaintenance('durations', null); }
			        }]    			    			           
                }]
			});

			medicationCRUDWindow = Ext.create('Ext.window.Window', {
			title		: 'Add Medication',
			closable	: true,
			modal		: true,
			width		: 380,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [medicationCRUDForm],
			buttons: [
			{
			    text	: 'Add',
			    icon	: '../image/add.png',
			    handler: function ()
			    {
					if (!medicationCRUDForm.form.isValid()){
						Ext.Msg.show({
							title: 'Error!',
							msg: "Please fill-in the required fields (Marked red).",
							icon: Ext.Msg.ERROR,
							buttons: Ext.Msg.OK
						});
					    return;
			        }

			        var id 			= medicationIDCount;
					var medication 	= Ext.getCmp('medications').getRawValue();
					var dosage 		= Ext.getCmp('dosages').getRawValue();
					var tablet 		= Ext.getCmp('tablets').getRawValue();
					var route 		= Ext.getCmp('routes').getRawValue();
					var frequency 	= Ext.getCmp('frequencies').getRawValue();
					var duration 	= Ext.getCmp('durations').getRawValue();

					var medicationID= Ext.getCmp('medications').getValue();
					var dosageID 	= Ext.getCmp('dosages').getValue();
					var tabletID 	= Ext.getCmp('tablets').getValue();
					var routeID 	= Ext.getCmp('routes').getValue();
					var frequencyID = Ext.getCmp('frequencies').getValue();
					var durationID 	= Ext.getCmp('durations').getValue();

					medicationInsert(id, medicationID, dosageID, tabletID, routeID, frequencyID, durationID, medication, dosage, tablet, route, frequency, duration);
					medicationCRUDWindow.close();
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {			    	
			    	medicationCRUDWindow.close();
			    }
			}],
		}).show();
		Ext.getCmp("medications").focus();
}