var commonWindow;

function setLabel()
{
    var storeCommon = Ext.getCmp("commonGrid").getSelectionModel();

        arrayStaffID = [];
        arrayStaffName = [];

    for (var i = 0; i < storeCommon.store.data.length; i++) {
            arrayStaffID.push(storeCommon.store.data.items[i].data.id);
            arrayStaffName.push(storeCommon.store.data.items[i].data.description);
    };

    if(storeCommon.store.data.length == 0)
        Ext.getCmp("lblParticipants").setText('None', false);
    else if(storeCommon.store.data.length == 1)
        Ext.getCmp("lblParticipants").setText('<a href="#" href="#" onclick="commonGrid()" style="text-decoration:none;font: 9px;">'+storeCommon.store.data.items[0].data.description+'</a>', false);
    else 
        Ext.getCmp("lblParticipants").setText('<a href="#" href="#" onclick="commonGrid()" style="text-decoration:none;font: 9px;">'+storeCommon.store.data.items[0].data.description+' and ('+(storeCommon.store.data.length-1)+') more...</a>', false);

    if(arrayStaffID.length != 0)                    
        document.getElementById("lblParticipants").setAttribute("data-qtip", "<b>Click</b> to <font color=green>ADD</font> / <font color=red>DELETE</font> Participant");
    else
        document.getElementById("lblParticipants").setAttribute("data-qtip", "");
}

function loadcommonGrid()
{
    for (var i = 0; i < arrayStaffID.length; i++)
        Ext.getCmp("commonGrid").getStore().add({id: arrayStaffID[i], description: arrayStaffName[i]}); 
}

function commonGrid()
{      
    Ext.define('commonModel', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'id', type: 'int'},
            {name: 'description'}
         ]
    });

    var participantsStore = new Ext.create('Ext.data.ArrayStore', {
        model: 'commonModel'
    });
    
	var grid = Ext.create('Ext.grid.Panel', {
        id      : 'commonGrid',
        store: participantsStore,
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Description', dataIndex: 'description', width: '90%'},
            {
                xtype: 'actioncolumn',
                width: '8%',
                align: 'center',
                items: [{
                    icon   : '../image/delete.gif',
                    tooltip: 'Remove',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = participantsStore.getAt(rowIndex);
                        if (!rec) {
                            return false;
                        }
                        grid.store.remove(rec);
                        setLabel();
                    }
                }]
            }],
        autoHeight: true,
        border: false,
        columnLines: true,
        width: '100%',
        height: 400
    });


	commonWindow = Ext.create('Ext.window.Window', {
    	title		: 'Participants',
    	closable	: true,
    	modal		: true,
    	width		: 300,
    	autoHeight	: true,
    	resizable	: false,
        border      : false,
    	buttonAlign	: 'center',
    	header: {titleAlign: 'center'},
    	items: [grid],
    	tbar: [
        {
            xtype       : 'combo',
            flex: 1,
            id          : 'participants',
            emptyText  : 'Select Participant',   
            valueField  : 'id',
            displayField: 'description',
            triggerAction: 'all',
            minChars    : 3,
            enableKeyEvents: true,
            readOnly    : false,
            ddReorder: true,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    url: 'inservice/staffname',
                    extraParams: {query:null, staff_id:staffID},
                    reader: {
                        type: 'json',
                        root: 'data',
                        idProperty: 'id'
                    }
                },
                params: {start: 0, limit: 10},
                fields: [{name: 'id', type: 'int'}, 'description']
            }),
            listeners: 
            {
                select: function (combo, record, index)
                {   
                    var validate = true;
                    var storeCommon = Ext.getCmp("commonGrid").getSelectionModel();
                    for (var i = 0; i < storeCommon.store.data.length; i++) {
                        if (storeCommon.store.data.items[i].data.id == record[0].data.id)
                        {
                            validate = false;
                            errorFunction('Error!', 'Participant already exist.');
                        }
                    };

                    if(validate)
                    {
                        Ext.getCmp("commonGrid").getStore().add({id: record[0].data.id, description: record[0].data.description});
                        setLabel();
                    }                    
                }
            }
        }]
    }).show();
    loadcommonGrid();
}