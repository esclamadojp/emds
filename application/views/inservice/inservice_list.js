setTimeout("UpdateSessionData();", 0);

var sName, staffID, serviceType = 'Name';

function ExportDocs(type) {

    params = new Object();
    params.servicequery = Ext.getCmp("searchTopic").getValue();  
    params.staffID      = staffID;
    params.sName        = sName;
    params.date_from    = Ext.getCmp("dateFrom").getValue();
    params.date_to      = Ext.getCmp("dateTo").getValue();
    params.searchServiceType  = serviceType;
    params.filetype     = type;

    ExportDocument('inservice/exportdocument', params, type);
}

Ext.onReady(function(){
          
    var treeStore = Ext.create('Ext.data.TreeStore', {
        proxy: {
            type: 'ajax',
            reader: 'json',
            extraParams: {query:null},
            url: 'inservice/namelist'
        }     
    });

    var RefreshTreeStore = function () {
        Ext.getCmp("staffTree").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };

    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Names',
        split   : true,
        region  : 'west',
        collapsible: true,
        id : 'staffTree',
        store: treeStore,
        rowLines: true,
        width: '30%',
        minWidth: 200,
        margin: '0 0 10 0',
        height: 500,        
        rootVisible: false,
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchName',
            emptyText: 'Search here...',
            width   : '50%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("staffTree").getStore().proxy.extraParams["query"] = Ext.getCmp("searchName").getValue();
                        RefreshTreeStore();
                    }
                }
            }
        },
        { xtype: 'tbfill'},
        { xtype: 'button', icon: '../image/add.png', tooltip: 'Add Name', handler: function (){ AddEditDeleteStaff('Add', 'staffTree', null);}},
        { xtype: 'button', icon: '../image/edit.png', tooltip: 'Edit Name', handler: function (){ AddEditDeleteStaff('Edit', 'staffTree', null);}},
        { xtype: 'button', icon: '../image/delete.png', tooltip: 'Delete Name', handler: function (){ AddEditDeleteStaff('Delete', 'staffTree', 'inserviceGrid');}}
        ],
        viewConfig: {
            listeners: {
                itemclick: function(view,rec,item,index,eventObj) {
                    Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                    Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                    Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["staffID"] = rec.get('id');
                    staffID = rec.get('id');
                    sName = rec.get('text');
                    if (sName == 'Names')
                        Ext.getCmp("inserviceGrid").setTitle('Details');
                    else
                        Ext.getCmp("inserviceGrid").setTitle('In-Service details attended by <font size=2><b>'+sName+'</b></font>');
                    RefreshGridStore();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    nameMenu.showAt(e.getXY());
                }
            }
        },
        listeners:
        {
            collapse : function() {
                Ext.getCmp('radio1').setValue(false);
                Ext.getCmp('radio2').setValue(true);
            },
            expand : function() {
                Ext.getCmp('radio1').setValue(true);
                Ext.getCmp('radio2').setValue(false);
            }
        }
    });

    var nameMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteStaff('Add', 'staffTree', null);}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteStaff('Edit', 'staffTree', null);}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteStaff('Delete', 'staffTree', 'inserviceGrid');}
        }]
    });

    var storeServiceList = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'inservice/servicelist',
            extraParams: {start: 0, limit: 20, servicequery:null, staffID: 0, searchServiceType: 'Name', date_from: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), date_to: Ext.Date.add(new Date(), Ext.Date.DAY, 180)},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, {name: 'staff_id', type: 'int'}, {name: 'topic_id', type: 'int'}, 'servicedate', 'duration', 'instructor', 'shift', 'topic']
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("inserviceGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };
   
    var equivalent = null, remarks = null, comboflag=false;

    var grid = Ext.create('Ext.grid.Panel', {
        id      : 'inserviceGrid',
        region  : 'center',
        store: storeServiceList,
        columns: [  
            Ext.create('Ext.grid.RowNumberer'),
            {dataIndex: 'id', hidden: true},
            {dataIndex: 'staff_id', hidden: true},
            {dataIndex: 'topic_id', hidden: true},
            {text: "Date", dataIndex: 'servicedate', width: '10%'},
            {text: "Topic", dataIndex: 'topic', width: '30%'},
            {text: "Shift", dataIndex: 'shift', width: '20%'},
            {text: "Duration", dataIndex: 'duration', width: '15%'},
            {text: "Instructor / By", dataIndex: 'instructor', width: '20%'}
        ],
        columnLines: true,
        width: '80%',
        minWidth: 700,
        height: 400,
        title: 'Details',
        loadMask: true,
        margin: '0 0 10 0',
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    LoadParticipants();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        },
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchTopic',
            emptyText: 'Search here...',
            width   : '15%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["servicequery"] = Ext.getCmp("searchTopic").getValue();
                        RefreshGridStore();
                    }
                }
            }
        },
        {
            xtype   : 'radio',
            boxLabel  : 'Name',
            name      : 'searchtype',            
            inputValue: '1',
            checked   : true,
            id        : 'radio1',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio1').checked == true)
                    {
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["searchServiceType"] = 'Name';
                        serviceType = 'Name';
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["servicequery"] = null;
                        Ext.getCmp('searchTopic').setValue(null);
                        RefreshGridStore();
                        Ext.getCmp("inserviceGrid").setTitle('In-Service details attended by <font size=2><b>'+sName+'</b></font>');
                        Ext.getCmp('staffTree').expand();
                    }
                }
            }
        }, {
            xtype   : 'radio',
            boxLabel  : 'Topic',
            name      : 'searchtype',            
            inputValue: '2',            
            id        : 'radio2',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio2').checked == true)
                    {
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["searchServiceType"] = 'Topic';
                        serviceType = 'Topic';
                        Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["servicequery"] = null;
                        Ext.getCmp('searchTopic').setValue(null);
                        RefreshGridStore();
                        Ext.getCmp("inserviceGrid").setTitle('Details');
                        Ext.getCmp("staffTree").collapse();
                    }
                }
            }
        }, 
        {
            xtype: 'label',
            html: '<font size=2 color=><b>From:</b></font>'
        },
        { xtype: 'datefield', id:'dateFrom', emptyText: 'From',  value: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), width: 100},
        {
            xtype: 'label',
            html: '<font size=2 color=><b>To:</b></font>'
        },
        { xtype: 'datefield', id:'dateTo', emptyText: 'To',  value: Ext.Date.add(new Date(), Ext.Date.DAY, 180), width: 100},
        { xtype: 'button', text: 'RELOAD', icon: '../image/load.png', tooltip: 'Reload grid based on date range', 
            handler: function (){ 
                Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                Ext.getCmp("inserviceGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                RefreshGridStore();
            }
        },        
        { xtype: 'tbfill'},
        { xtype: 'button', icon: '../image/add.png', tooltip: 'Add Topic', handler: function (){ AddEditDeleteService('Add');}},
        { xtype: 'button', icon: '../image/edit.png', tooltip: 'Edit Topic', handler: function (){ AddEditDeleteService('Edit');}},
        { xtype: 'button', icon: '../image/delete.png', tooltip: 'Delete Topic', handler: function (){ AddEditDeleteService('Delete');}},
        { xtype: 'button', icon: '../image/lists.png', tooltip: 'List of Participants', handler: function (){ LoadParticipants();}},
        {
            text: 'Download',
            tooltip: 'Extract Data to PDF or EXCEL File Format',
            icon: '../image/download.png',
            menu: 
            {
                items: 
                [
                    {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            ExportDocs('PDF');
                        }
                    }, 
                    {
                        text    : 'Export Excel Format',
                        icon: '../image/excel.png',
                        handler: function ()
                        {
                            ExportDocs('Excel');
                        }
                    }
                ]
            }
        }],
        bbar: [{
            xtype: 'label',
            id: 'lblSgpa',
            margin: '0 0 0 460',
            html: 'Total Duration'
        },'-',{
            xtype: 'label',
            html: '0.00'
        }]
    });

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'List of Participants',
            icon: '../image/lists.png',
            handler: function (){ LoadParticipants();}
        },
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteService('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteService('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteService('Delete');}
        }]
    });

    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [tree,grid]
    });
});
