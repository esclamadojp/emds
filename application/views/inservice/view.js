function ExportListDocs(type) {

    params = new Object();
    params.id    = inservicecrudID;  
    params.searchServiceType  = 'List';
    params.filetype = type;

    ExportDocument('inservice/exportdocument', params, type);
}

function LoadParticipants()
{      
      var sm = Ext.getCmp("inserviceGrid").getSelectionModel();
      if (!sm.hasSelection())
      {
        warningFunction("Warning!","Please select record.");
        return;
      }
      inservicecrudID = sm.selected.items[0].data.id;

      Ext.MessageBox.wait('Loading...');
      Ext.Ajax.request(
      {
            url     :"inservice/topicsview",
            method  : 'POST',
            params: {id: inservicecrudID},
            success: function(f,a)
            {
                  var response = Ext.decode(f.responseText);                 
                  var htmlLeft = new Ext.XTemplate(
                        '<table width="100%" style="background: #ff6666;border: solid 1px white;">',
                        '<tr style="background: #ff6666;">',
                        '<td colspan="2" style="padding: 2px;" width="100%"><font color=white size=2></font></td>',
                        '</tr>',
                        '<tr >',
                        '<td align="right" style="background: #ffbec2; padding: 2px;" width="40%"><font color=black size=2>Date</td>',
                        '<td align="left" style="background: #ffd9d9; padding: 2px;" width="60%"><font color=black size=2>'+response.header.servicedate+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td align="right" style="background: #ffbec2; padding: 2px;" width="40%"><font color=black size=2>Shift</td>',
                        '<td align="left" style="background: #ffd9d9; padding: 2px;" width="60%"><font color=black size=2>'+response.header.shift+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td align="right" style="background: #ffbec2; padding: 2px;" width="40%"><font color=black size=2>Topic</td>',
                        '<td align="left" style="background: #ffd9d9; padding: 2px;" width="60%"><font color=black size=2>'+response.header.topic+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td align="right" style="background: #ffbec2; padding: 2px;" width="40%"><font color=black size=2>Duration</td>',
                        '<td align="left" style="background: #ffd9d9; padding: 2px;" width="60%"><font color=black size=2>'+response.header.duration+'</td>',
                        '</tr>',
                        '<tr >',
                        '<td align="right" style="background: #ffbec2; padding: 2px;" width="40%"><font color=black size=2>Instructor / By</td>',
                        '<td align="left" style="background: #ffd9d9; padding: 2px;" width="60%"><font color=black size=2>'+response.header.instructor+'</td>',
                        '</tr>',
                        '</table>'
                );                  
                  
                  var htmlData = 
                         '<table width="100%" style="background: #5aa865;border: solid 1px white;">' +
                        '<tr style="background: #5aa865;">' + 
                        '<td colspan="2" style="padding: 2px;" width="100%"><font color=white size=2></font></td>' +
                        '</tr>';

                       

                htmlData += '<tr >' +
                          '<td align="left" style="background: #c1e1c6; padding: 2px;" width="40%"><font color=black size=2>Name</td>' +
                          '<td align="left" style="background: #c1e1c6; padding: 2px;" width="30%"><font color=black size=2>Department</td>' +
                          '<td align="left" style="background: #c1e1c6; padding: 2px;" width="30%"><font color=black size=2>Position</td>' +
                        '</tr>';

                  for (var i = 0; i < response.count; i++) {
                        htmlData += '<tr>' + 
                                    '<td align="left" style="background: #d8f1dc; padding: 2px;" width="40%"><font color=black size=2>'+response.participants[i].name+'</td>' +
                                    '<td align="left" style="background: #d8f1dc; padding: 2px;" width="30%"><font color=black size=2>'+response.participants[i].department+'</td>' +
                                    '<td align="left" style="background: #d8f1dc; padding: 2px;" width="30%"><font color=black size=2>'+response.participants[i].positions+'</td>' +
                                    '</tr>';                     
                       
                  };

                  htmlData +='</table>';

                  var htmlRight = new Ext.XTemplate(htmlData);
                        
                  var leftPanel = Ext.create('Ext.panel.Panel', {
                    title: 'Details',
                    id: 'detialsPanel',
                    split   : true,
                    collapsible: true,
                    region  : 'west',
                    width     : '35%',        
                    html: htmlLeft.applyTemplate(null),
                    minWidth: 200
                });

                  var rightPanel = Ext.create('Ext.panel.Panel', {
                    title: 'Participants',
                    region  : 'center',
                    width     : '75%',
                    autoScroll      : true,
                    html: htmlRight.applyTemplate(null),
                    minWidth: 400
                });

                mainWindow = Ext.create('Ext.window.Window', {
                        title       : 'In-Service Participants',
                      closable      : true,
                      modal         : true,
                      width         : 800,
                      height        : 500,
                        resizable   : false,        
                        maximizable : true,
                        layout: 'fit',
                        plain: true,  
                        buttonAlign : 'center',
                        header            : {titleAlign: 'center'},
                        layout            : 'border',
                      items         : [leftPanel, rightPanel],
                      buttons: [
                      {
                          text      : 'Export PDF',
                          icon: '../image/pdf.png',   
                          handler: function ()
                          {
                              ExportListDocs('PDF');
                          }
                      },
                      {
                          text      : 'Export EXCEL',
                          icon: '../image/excel.png',   
                          handler: function ()
                          {
                              ExportListDocs('Excel');
                          }
                      },{
                          text      : 'Close',
                          icon: '../image/close.png',   
                          handler: function ()
                          {
                              mainWindow.close();
                          }
                      }],
                  }).show();
                Ext.MessageBox.hide();  
                Ext.getCmp("detialsPanel").collapse();                
            }
      });
}