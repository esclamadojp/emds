var advancedSearchWindow, htmlData, rightPanel, filterForm;

function ExportListDocs(type) {

   params = new Object();
   params.diagnosis   = Ext.get('diagnosis').dom.value;
   params.allergies   = Ext.get('allergies').dom.value;
   params.census      = Ext.get('census').dom.value;
   params.building    = Ext.getCmp("cbBuilding").getValue();
   params.floor       = Ext.getCmp("cbFloor").getValue();
   params.start_date  = Ext.getCmp("startdt").getValue();
   params.end_date    = Ext.getCmp("enddt").getValue();
   params.medicare    = Ext.getCmp("medicare").getValue();
   params.medicaid    = Ext.getCmp("medicaid").getValue();
   params.filetype    = type;

   ExportDocument('residents_profile/exportdocument', params, type);
}

function loadData()
{
   filterForm.getForm().load({
         url: 'residents_profile/residentslist',
         timeout: 30000,
         waitMsg:'Loading data...',
         params: {
            diagnosis   : Ext.get('diagnosis').dom.value,
            allergies   : Ext.get('allergies').dom.value,
            census      : Ext.get('census').dom.value,
            building    : Ext.getCmp("cbBuilding").getValue(),
            floor       : Ext.getCmp("cbFloor").getValue(),
            start_date  : Ext.getCmp("startdt").getValue(),
            end_date    : Ext.getCmp("enddt").getValue(),
            medicare    : Ext.getCmp("medicare").getValue(),
            medicaid    : Ext.getCmp("medicaid").getValue()
         },    
         success: function(form, action) {
            var data = action.result.data;
            htmlData = 
                   '<table width="100%" style="background: #5aa865;border: solid 1px white;">' +
                  '<tr style="background: #5aa865;">' + 
                  '<td colspan="2" style="padding: 2px;" width="100%"><font color=white size=2><b>Residents</b></font></td>' +
                  '</tr>';

            htmlData += '<tr >' +
                    '<td align="left" style="background: #c1e1c6; padding: 2px;" width="60%"><font color=black size=2><b>Name</b></td>' +
                    '<td align="center" style="background: #c1e1c6; padding: 2px;" width="20%"><font color=black size=2><b>Age</b></td>' +
                    '<td align="left" style="background: #c1e1c6; padding: 2px;" width="20%"><font color=black size=2><b>Sex</b></td>' +
                  '</tr>';

            for (var i = 0; i < action.result.count; i++) {
                  htmlData += '<tr>' + 
                              '<td align="left" style="background: #d8f1dc; padding: 2px;"><font color=black size=2>'+data[i].resident_name+'</td>' +
                              '<td align="center" style="background: #d8f1dc; padding: 2px;"><font color=black size=2>'+data[i].age+'</td>' +
                              '<td align="left" style="background: #d8f1dc; padding: 2px;"><font color=black size=2>'+data[i].sex+'</td>' +
                              '</tr>';                     
            };

            htmlData +='</table>';

            var htmlRight = new Ext.XTemplate(htmlData);
            rightPanel.body.update(htmlRight.applyTemplate(null));
         },    
         failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
      });   
}

function AdvancedSearch()
{      
   filterForm = Ext.create('Ext.form.Panel', {
         border      : false,
         bodyStyle   : 'padding:15px;',      
         fieldDefaults: {
            labelAlign  : 'right',
            labelWidth: 140,
            anchor   : '100%'
           },
         items: [
         {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype       : 'combo',
               id          : 'diagnosis',
               fieldLabel  : 'Diagnosis',                        
               multiSelect : true,
               valueField  : 'id',
               flex        : 1,
               displayField: 'description',
               triggerAction: 'all',
               minChars    : 3,
               enableKeyEvents: true,
               readOnly    : false,
               ddReorder   : true,
               matchFieldWidth: false,
               store: new Ext.data.JsonStore({
                  proxy: {
                     type: 'ajax',
                     url: 'commonquery/caseentrylist',
                     extraParams: {query:null, type: 'diagnosis', category:null},
                     reader: {
                        type: 'json',
                        root: 'data',
                        idProperty: 'id'
                     }
                  },
                  params: {start: 0, limit: 10},
                  fields: [{name: 'id', type: 'int'}, 'description']
               }),
               listeners: 
               {
                  select: function (combo, record, index)
                  {   
                     var diagnosisValue = Ext.getCmp("diagnosis").getValue();
                     Ext.get('diagnosis').dom.value = diagnosisValue.toString();
                  }
               }
            },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Diagnosis',
               handler: function (){ 
                  Ext.getCmp("diagnosis").clearValue(); 
                  Ext.get('diagnosis').dom.value = null;
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype       : 'combo',
               id          : 'allergies',
               fieldLabel  : 'Allergies',                        
               multiSelect : true,
               valueField  : 'id',
               flex        : 1,
               displayField: 'description',
               triggerAction: 'all',
               minChars    : 3,
               enableKeyEvents: true,
               readOnly    : false,
               ddReorder: true,
               matchFieldWidth: false,
               store: new Ext.data.JsonStore({
                   proxy: {
                       type: 'ajax',
                       url: 'commonquery/caseentrylist',
                       extraParams: {query:null, type: 'allergies', category:null},
                       reader: {
                           type: 'json',
                           root: 'data',
                           idProperty: 'id'
                       }
                   },
                   params: {start: 0, limit: 10},
                   fields: [{name: 'id', type: 'int'}, 'description']
               }),
               listeners: 
               {
                   select: function (combo, record, index)
                   {   
                       var allergiesValue = Ext.getCmp("allergies").getValue();
                       Ext.get('allergies').dom.value = allergiesValue.toString();
                   }
               }
           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Allergies',
               handler: function (){ 
                  Ext.getCmp("allergies").clearValue(); 
                  Ext.get('allergies').dom.value = null;
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype       : 'combo',
               id          : 'census',
               fieldLabel  : 'Census',                        
               valueField  : 'id',
               flex        : 1,
               displayField: 'description',
               triggerAction: 'all',
               minChars    : 3,
               enableKeyEvents: true,
               readOnly    : false,
               ddReorder: true,
               matchFieldWidth: false,
               store: new Ext.data.JsonStore({
                   proxy: {
                       type: 'ajax',
                       url: 'commonquery/caseentrylist',
                       extraParams: {query:null, type: 'resident_census', category:null},
                       reader: {
                           type: 'json',
                           root: 'data',
                           idProperty: 'id'
                       }
                   },
                   params: {start: 0, limit: 10},
                   fields: [{name: 'id', type: 'int'}, 'description']
               }),
               listeners: 
               {
                   select: function (combo, record, index)
                   {   
                       var censusValue = Ext.getCmp("census").getValue();
                       Ext.get('census').dom.value = censusValue.toString();
                   }
               }
           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Census',
               handler: function (){ 
                  Ext.getCmp("census").clearValue(); 
                  Ext.get('census').dom.value = null;
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype       : 'combo',
               id          : 'cbBuilding',
               fieldLabel  : 'Building',  
               valueField  : 'id',
               flex        : 1,
               displayField: 'description',
               triggerAction: 'all',
               minChars    : 3,
               enableKeyEvents: true,
               readOnly    : false,
               ddReorder: true,
               matchFieldWidth: false,
               store: new Ext.data.JsonStore({
                   proxy: {
                       type: 'ajax',
                       url: 'commonquery/caseentrylist',
                       extraParams: {query:null, type: 'buildings', category:null},
                       reader: {
                           type: 'json',
                           root: 'data',
                           idProperty: 'id'
                       }
                   },
                   params: {start: 0, limit: 10},
                   fields: [{name: 'id', type: 'int'}, 'description']
               }),
               listeners: 
               {
                   select: function (combo, record, index)
                   {                    
                       Ext.getCmp("cbFloor").reset();                            
                       Ext.getCmp("cbFloor").getStore().proxy.extraParams["cat_id"] = record[0].data.id;
                       Ext.getCmp("cbFloor").getStore().proxy.extraParams["query"] = '';
                       Ext.getCmp("cbFloor").getStore().reload();
                   }
               }
           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Building',
               handler: function (){ 
                  Ext.getCmp("cbBuilding").clearValue(); 
                  Ext.getCmp("cbFloor").clearValue(); 
                  Ext.getCmp("cbBuilding").setValue(null);
                  Ext.getCmp("cbFloor").setValue(null);
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype       : 'combo',
               id          : 'cbFloor',
               fieldLabel  : 'Floor',  
               valueField  : 'id',
               flex        : 1,
               displayField: 'description',
               triggerAction: 'all',
               minChars    : 3,
               enableKeyEvents: true,
               readOnly    : false,
               ddReorder: true,
               matchFieldWidth: false,
               store: new Ext.data.JsonStore({
                   proxy: {
                       type: 'ajax',
                       url: 'charts/caseentrylist',
                       extraParams: {cat_id: 0, query:null},
                       reader: {
                           type: 'json',
                           root: 'data',
                           idProperty: 'id'
                       }
                   },
                   params: {start: 0, limit: 10},
                   fields: [{name: 'id', type: 'int'}, 'description']
               })
           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Floor',
               handler: function (){ 
                  Ext.getCmp("cbFloor").clearValue(); 
                  Ext.getCmp("cbFloor").setValue(null);
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype: 'datefield',
               flex:1,
               fieldLabel: 'Admisssion (From)',
               name: 'startdt',
               id: 'startdt',
               vtype: 'daterange',
               // emptyText : 'Date From',
               endDateField: 'enddt' // id of the end date field

           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Admisssion (From)',
               handler: function (){ 
                  // Ext.getCmp("startdt").clearValue(); 
                  Ext.getCmp("startdt").setValue(null);
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype: 'datefield',
               flex: 1,
               fieldLabel: 'Admisssion (To)',
               name: 'enddt',
               id: 'enddt',
               vtype: 'daterange',
               startDateField: 'startdt' // id of the start date field
           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Admission (To)',
               handler: function (){ 
                  // Ext.getCmp("enddt").clearValue(); 
                  Ext.getCmp("enddt").setValue(null);
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype: 'textfield',
               flex:1,
               fieldLabel: 'Insurance 1(Medicare)',
               id: 'medicare'

           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Insurance',
               handler: function (){ 
                  Ext.getCmp("medicare").setValue(null);
                  loadData();
               }
            }]
         }, {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            items: [
            {
               xtype: 'textfield',
               flex:1,
               fieldLabel: 'Insurance 2(Medicaid)',
               id: 'medicaid'

           },
            {
               xtype: 'button',
               margins     : '0 0 0 5',
               icon: '../image/clear.png',   
               tooltip: 'Clear Insurance',
               handler: function (){ 
                  Ext.getCmp("medicaid").setValue(null);
                  loadData();
               }
            }]
         }]
      });

   var leftPanel = Ext.create('Ext.panel.Panel', {
      title    : 'Search Filters',
      id       : 'filterPanel',
      split    : true,
      collapsible: true,
      region   : 'west',
      width    : '45%',        
      items    : filterForm,
      minWidth : 200,
      buttonAlign : 'left',
      buttons: [
      {
         text      : 'Search',
         icon: '../image/search.png',   
         handler: function ()
         {
            loadData();
         }
      }]
   });

   rightPanel = Ext.create('Ext.panel.Panel', {
      region   : 'center',
      width    : '55%',
      autoScroll : true,      
      minWidth : 400
   });

   advancedSearchWindow = Ext.create('Ext.window.Window', {
      title       : 'Advanced Search for <?php echo addslashes($module_name);?>',
      closable    : true,
      modal       : true,
      width       : 800,
      height      : 500,
      resizable   : false,        
      maximizable : true,
      layout      : 'fit',
      plain       : true,  
      buttonAlign : 'right',
      header      : {titleAlign: 'center'},
      layout      : 'border',
      items       : [leftPanel, rightPanel],
      buttons: [
      {
         text      : 'Export PDF',
         icon: '../image/pdf.png',   
         handler: function ()
         {
            ExportListDocs('PDF');
         }
      }, 
      {
         text      : 'Export EXCEL',
         icon: '../image/excel.png',   
         handler: function ()
         {
            ExportListDocs('Excel');
         }
      }, 
      {
         text      : 'Close',
         icon: '../image/close.png',   
         handler: function ()
         {
            advancedSearchWindow.close();
         }
      }]
   }).show();   
   loadData();
}