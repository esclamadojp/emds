var rdiagnosisID;

function ExportCarePlan(type) {

    if (!Ext.getCmp("problem").isValid()){
        errorFunction("Error!",'Please fill-in the required fields (Marked red).');
        return;
    }

    params = new Object();
    params.id   = rdiagnosisID;
    params.resident_id   = residentID;
    params.type = type;

    ExportDocument('residents_profile/careplandocument', params, type);
}

function CarePlan(grid)
{          
    var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

    var sm = Ext.getCmp(grid).getSelectionModel();
    if (!sm.hasSelection())
    {
        errorFunction("Status","Please select a record!");
        return;
    }
    residentID = sm.selected.items[0].data.id;
    patientName = sm.selected.items[0].data.name;    

    var form = Ext.create('Ext.form.Panel', {
        border      : false,
        bodyStyle   : 'padding:15px 15px 0 15px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            labelWidth: 70,
            afterLabelTextTpl: required,            
            allowBlank: false
        },
        items:[
        {
            xtype: 'fieldcontainer',
            labelStyle: 'font-weight:bold;padding:0;',
            layout: 'hbox',
            anchor  : '80%',
            items: [
            {
                xtype       : 'combo',
                flex: 1,
                id          : 'problem',
                fieldLabel  : 'Problem',
                valueField  : 'id',
                displayField: 'description',
                triggerAction: 'all',
                minChars    : 3,
                enableKeyEvents: true,
                matchFieldWidth: false,
                readOnly    : false,
                forceSelection: true,
                store: new Ext.data.JsonStore({
                    proxy: {
                        type: 'ajax',
                        url: 'residents_profile/diagnosis_care_plan',
                        extraParams: {query:null, resident_id: residentID},
                        reader: {
                            type: 'json',
                            root: 'data',
                            idProperty: 'id'
                        }
                    },
                    params: {start: 0, limit: 10},
                    fields: [{name: 'id', type: 'int'}, 'description']
                }),
                listeners: 
                {
                    select: function (combo, record, index)
                    {        
                        rdiagnosisID = record[0].data.id;
                        Ext.getCmp("goalGrid").getStore().proxy.extraParams["id"] = record[0].data.id;
                        Ext.getCmp("goalGrid").getStore().reload({params:{reset:1 }, timeout: 300000});
                        
                        Ext.getCmp("interventionGrid").getStore().proxy.extraParams["id"] = record[0].data.id;
                        Ext.getCmp("interventionGrid").getStore().reload({params:{reset:1 }, timeout: 300000});
                        
                        Ext.getCmp("commentGrid").getStore().proxy.extraParams["id"] = record[0].data.id;
                        Ext.getCmp("commentGrid").getStore().reload({params:{reset:1 }, timeout: 300000});
                        
                    }
                }
            },
            {
                xtype: 'button',
                margins     : '0 0 0 5',
                text: 'Add',
                handler: function (){ AddEditDeleteCP('Add', 'GOAL'); }
            }, 
            {
                xtype: 'button',
                margins     : '0 0 0 5',
                text: 'Print',
                handler: function (){ ExportCarePlan('PDF'); }
            },
            {
                xtype   : 'radio',
                boxLabel  : 'All',
                inputValue: '1',
                margins     : '0 0 0 5',
                checked   : true,
                id        : 'rbAll',
                name      : 'rbStatus',
                listeners:
                {
                    change : function() {
                        if(Ext.getCmp('rbAll').checked == true)
                        {
                            Ext.getCmp("goalGrid").getStore().proxy.extraParams["status"] = 1;
                            Ext.getCmp("interventionGrid").getStore().proxy.extraParams["status"] = 1;
                            Ext.getCmp("commentGrid").getStore().proxy.extraParams["status"] = 1;

                            Ext.getCmp("goalGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                            Ext.getCmp("interventionGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                            Ext.getCmp("commentGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                        }
                    } 
                }
            }, 
            {
                xtype   : 'radio',
                boxLabel  : 'Active',
                inputValue: '2',
                margins     : '0 0 0 5',
                id        : 'rbActive',
                name      : 'rbStatus',
                listeners:
                {
                    change : function() {
                        if(Ext.getCmp('rbActive').checked == true)
                        {
                            Ext.getCmp("goalGrid").getStore().proxy.extraParams["status"] = 2;
                            Ext.getCmp("interventionGrid").getStore().proxy.extraParams["status"] = 2;
                            Ext.getCmp("commentGrid").getStore().proxy.extraParams["status"] = 2;

                            Ext.getCmp("goalGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                            Ext.getCmp("interventionGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                            Ext.getCmp("commentGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                        }
                    } 
                }
            },
            {
                xtype   : 'radio',
                boxLabel  : 'Discontinued',
                inputValue: '3',
                margins     : '0 0 0 5',
                id        : 'rbDiscontinued',
                name      : 'rbStatus',
                listeners:
                {
                    change : function() {
                        if(Ext.getCmp('rbDiscontinued').checked == true)
                        {
                            Ext.getCmp("goalGrid").getStore().proxy.extraParams["status"] = 3;
                            Ext.getCmp("interventionGrid").getStore().proxy.extraParams["status"] = 3;
                            Ext.getCmp("commentGrid").getStore().proxy.extraParams["status"] = 3;

                            Ext.getCmp("goalGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                            Ext.getCmp("interventionGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                            Ext.getCmp("commentGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
                        }
                    } 
                }
            }]
        }]
    });

    var goalStore = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'residents_profile/gicList',
            extraParams: {start: 0, limit: 20, id:0, status: 1, type:'GOAL', resident_id: residentID},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'date', 'description', 'status']
    });

    var goalPanel = Ext.create('Ext.grid.Panel', {
        border: false,
        id      : 'goalGrid',
        hideHeaders: true,
        autoHeight: true,
        // autoScroll: true,
        store: goalStore,
        columns: [
            { dataIndex: 'id', hidden: true},
            { dataIndex: 'date', width: '14%'},
            { dataIndex: 'description', width: '68%', renderer: columnWrap},
            { dataIndex: 'status', width: '14%', renderer:addTooltip},
            {
                xtype: 'actioncolumn',
                width: '3%',    
                align: 'center',
                items: [{
                    icon   : '../image/delete.gif',
                    tooltip: 'Delete Record',
                    handler: function(grid, rowIndex, colIndex) {
                        goalPanel.getSelectionModel().select(rowIndex);
                        AddEditDeleteCP('Delete', 'GOAL');
                    }
                }]
            }
        ],        
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteCP('Edit', 'GOAL');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    goalMenu.showAt(e.getXY());
                }
            }
        }
    });

    var goalMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteCP('Edit', 'GOAL');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteCP('Delete', 'GOAL');}
        }]
    });

    var interventionStore = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'residents_profile/gicList',
            extraParams: {start: 0, limit: 20, id:0, status: 1, type:'INTERVENTION', resident_id: residentID},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'date', 'description', 'created_by', 'date_created', 'status']
    });

    var interventionPanel = Ext.create('Ext.grid.Panel', {
        border: false,
        id      : 'interventionGrid',
        hideHeaders: true,
        autoHeight: true,
        // autoScroll: true,
        store: interventionStore,
        columns: [
            { dataIndex: 'id', hidden: true},
            { dataIndex: 'date', width: '14%'},
            { dataIndex: 'description', width: '42%', renderer:columnWrap},
            { dataIndex: 'created_by', width: '14%', renderer:addTooltip},
            { dataIndex: 'date_created', width: '12%'},
            { dataIndex: 'status', width: '14%', renderer:addTooltip},
            {
                xtype: 'actioncolumn',
                width: '3%',    
                align: 'center',
                items: [{
                    icon   : '../image/delete.gif',
                    tooltip: 'Delete Record',
                    handler: function(grid, rowIndex, colIndex) {
                        interventionPanel.getSelectionModel().select(rowIndex);
                        AddEditDeleteCP('Delete', 'INTERVENTION');
                    }
                }]
            }
        ],        
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteCP('Edit', 'INTERVENTION');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    interventionMenu.showAt(e.getXY());
                }
            }
        }
    });

    var interventionMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteCP('Edit', 'INTERVENTION');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteCP('Delete', 'INTERVENTION');}
        }]
    });

    var commentStore = new Ext.data.JsonStore({
         proxy: {
            type: 'ajax',
            url: 'residents_profile/gicList',
            extraParams: {start: 0, limit: 20, id:0, status: 1, type:'COMMENT', resident_id: residentID},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'date', 'description', 'created_by', 'date_created', 'status']
    });

    var commentPanel = Ext.create('Ext.grid.Panel', {
        border: false,
        id      : 'commentGrid',
        hideHeaders: true,
        autoHeight: true,
        // autoScroll: true,
        store: commentStore,
        columns: [
            { dataIndex: 'id', hidden: true},
            { dataIndex: 'date', width: '14%'},
            { dataIndex: 'description', width: '42%', renderer:columnWrap},
            { dataIndex: 'created_by', width: '14%', renderer:addTooltip},
            { dataIndex: 'date_created', width: '12%'},
            { dataIndex: 'status', width: '14%', renderer:addTooltip},
            {
                xtype: 'actioncolumn',
                width: '3%',    
                align: 'center',
                items: [{
                    icon   : '../image/delete.gif',
                    tooltip: 'Delete Record',
                    handler: function(grid, rowIndex, colIndex) {
                        commentPanel.getSelectionModel().select(rowIndex);
                        AddEditDeleteCP('Delete', 'COMMENT');
                    }
                }]
            }
        ],        
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteCP('Edit', 'COMMENT');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    commentMenu.showAt(e.getXY());
                }
            }
        }
    });

    var commentMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteCP('Edit', 'COMMENT');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteCP('Delete', 'COMMENT');}
        }]
    });

    fieldsetGoal = new Ext.form.FieldSet({
        title: '<font color=green>GOAL/s</font>',        
        items: [goalPanel]
    }); 

    fieldsetIntervention = new Ext.form.FieldSet({
        title: '<font color=blue>INTERVENTION/s</font>',
        items: [interventionPanel]
    }); 

    fieldsetComment = new Ext.form.FieldSet({
        title: '<font color=red>COMMENT/s</font>',
        items: [commentPanel]
    }); 

    var fieldsetForm = Ext.create('Ext.form.Panel', {
        border      : false,                
        bodyStyle   : 'padding:0 15px 5px 15px;',  
        items: [fieldsetGoal, fieldsetIntervention, fieldsetComment]   
    });

    var mainForm = Ext.create('Ext.panel.Panel', {
      border   : false,
      region   : 'center',
      width    : '100%',        
      items    : [form, fieldsetForm]
   });

    careplanWindow = Ext.create('Ext.window.Window', {
        title       : patientName + ' - Care Plan',
        header      : {titleAlign: 'center'},
        closable    : true,
        modal       : true,
        width       : 850,
        autoHeight  : true,
        resizable   : false,      
        maximizable : true,
        layout      : 'fit',
        plain       : true, 
        items       : [mainForm],
    }).show();
}