<div id="innerdiv">
</div>

<script type="text/javascript">		
	
	<?php include_once("advanced_search.js") ?>
	<?php include_once("careplan.js") ?>
	<?php include_once("careplan_crud.js") ?>
	<?php include_once("medicalrecords.js") ?>
	<?php include_once("medicalrecords_crud.js") ?>
	<?php include_once("residents_profile_list.js") ?>

</script>

<script type="text/javascript" src="<?php echo base_url(); ?>commonjs/maintenancecrud.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>commonjs/maintenancegrid.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>commonjs/resident_crud.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>commonjs/residents_profile.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>commonjs/residents_profile_form.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>commonjs/image_upload.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/common.css" />
