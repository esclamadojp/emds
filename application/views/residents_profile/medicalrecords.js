var medrecordsWindow, residentID;

function MedicalRecords()
{          
	var sm = Ext.getCmp("resident_caseGrid").getSelectionModel();
	if (!sm.hasSelection())
    {
        warningFunction("Warning!","Please select record.");
        return;
    }    
    residentID = sm.selected.items[0].data.id;
    patientName = sm.selected.items[0].data.name;    
    patientAge = sm.selected.items[0].data.age;
    patientSex = sm.selected.items[0].data.sex;	

    var medicalrecordsStore = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'residents_profile/medicalrecords',
            extraParams: {start: 0, limit: 20, resident_id: residentID},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'record_no', 'admission_date', 'discharge_date', 'status']
    });

    var RefreshMedicalRecordsGridStore = function () {
        Ext.getCmp("medicalrecordsGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };

    var medicalrecordsGrid = Ext.create('Ext.grid.Panel', {
        id: 'medicalrecordsGrid',
        store:medicalrecordsStore,
        border:false,
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Record No.', dataIndex: 'record_no', width: '22%', align:'center'},
            { text: 'Admission Date', dataIndex: 'admission_date', width: '28%'},
            { text: 'Discharge Date', dataIndex: 'discharge_date', width: '28%'},
            { text: 'Status', dataIndex: 'status', width: '18%'}
        ],
        columnLines: true,
        width: '100%',
        height: 300,        
        margin: '0 0 10 0',
        tbar: [
        { xtype: 'tbfill'},
        { xtype: 'button', text: 'ADD', icon: '../image/add.png', handler: function (){ AddEditDeleteMedicalRecord('Add');}},
        '-',
        { xtype: 'button', text: 'EDIT', icon: '../image/edit.png', handler: function (){ AddEditDeleteMedicalRecord('Edit');}},
        '-',
        { xtype: 'button', text: 'DELETE', icon: '../image/delete.png', handler: function (){ AddEditDeleteMedicalRecord('Delete');}}
        ],        
        viewConfig: {
            listeners: {
                itemdblclick: function(view,rec,item,index,eventObj) {                    
                    AddEditDeleteMedicalRecord('Edit');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    moduleusersMenu.showAt(e.getXY());
                }
            }
        }
    });
    RefreshMedicalRecordsGridStore();

    var moduleusersMenu = Ext.create('Ext.menu.Menu', {
        items: [{
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteMedicalRecord('Add'); }
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteMedicalRecord('Edit'); }
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteMedicalRecord('Delete'); }
        }]
    });

		medrecordsWindow = Ext.create('Ext.window.Window', {
		title		: patientName + ' Medical Record/s',
		closable	: true,
		modal		: true,
		width		: 450,
		autoHeight	: true,
		resizable	: false,
		buttonAlign	: 'center',
		header: {titleAlign: 'center'},
		items: [medicalrecordsGrid],
		buttons: [
		{
		    text	: 'Close',
		    icon	: '../image/close.png',
		    handler: function ()
		    {
		    	medrecordsWindow.close();
		    }
		}],
	}).show();
}
