var mrCrudWindow, recordID, medicalrecordForm;

function medicalrecords_crud(type)
{
	params = new Object();

	if (type == "Delete")
	{
		params.id		= recordID;
		params.type		= type;

		deleteFunction('residents_profile/crud', params, 'medicalrecordsGrid');
	}
	else
	{
		params.id	= recordID;
		params.resident_id = residentID;
		params.type	= type;

		addeditFunction('residents_profile/crud', params, 'medicalrecordsGrid', medicalrecordForm, mrCrudWindow);
	}
}

function AddEditDeleteMedicalRecord(type)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp("medicalrecordsGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select a record.");
			return;
		}
		this.recordID = sm.selected.items[0].data.id;		
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					medicalrecords_crud(type);
			}
		});
	}
	else
	{
		medicalrecordForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 120,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [{
					xtype	: 'numberfield',	
					id		: 'record_no',
					name	: 'record_no',
					minValue: 1,
					maxValue: 100,
					fieldLabel: 'Order'
				}, {
	               xtype: 'datefield',
	               flex:1,
	               fieldLabel: 'Admisssion Date',
	               name: 'admission_date',
	               id: 'admission_date',
	               vtype: 'daterange',
	               endDateField: 'discharge_date'

	           }, {
	               xtype: 'datefield',
	               flex: 1,
	               fieldLabel: 'Discharge Date',
	               name: 'discharge_date',
	               id: 'discharge_date',
	               vtype: 'daterange',
	               afterLabelTextTpl: null,
	               allowBlank: true,
	               startDateField: 'admission_date' 
	           },{
		            xtype: 'radiogroup',	
		            fieldLabel: 'Status',
		            items: [
	                {
	                	boxLabel: '<font color=green>Active</font>', 
	                	inputValue: 1, 
	                	name: 'status', 
	                	checked: true
		            },
	                {
	                	boxLabel: '<font color=gray>Closed</font>', 
	                	inputValue: 2, 
	                	name: 'status'
		            }]
		        }]
			});

			mrCrudWindow = Ext.create('Ext.window.Window', {
			title		: type + ' Medical Record',
			closable	: true,
			modal		: true,
			width		: 350,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [medicalrecordForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!medicalrecordForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								medicalrecords_crud(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	mrCrudWindow.close();
			    }
			}],
		});

		if(type == 'Edit')
		{
			medicalrecordForm.getForm().load({
				url: 'residents_profile/view',
				timeout: 30000,
				waitMsg:'Loading data...',
				params: {
					id: this.recordID
				},	
				success: function(form, action) { mrCrudWindow.show(); },				
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});
		}
		else
			mrCrudWindow.show();

		Ext.getCmp("record_no").focus();
	}
}