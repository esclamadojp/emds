<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome-4.2.0/css/font-awesome.min.css" />

<style type="text/css">
	.permit-icon {
			float: left;
			text-align: center;
			font-weight: bold;
			background-color: #f7f7f7;
			padding: 10px;
			border-radius: 10px;
			margin: 5px;
	} 
	.permit-icon:hover {
			cursor: pointer;
			background-color: #eaeaea;
	}

	#navigation {
		height: 50px !important;
		padding-top: 16px;
	}

	#realtime {
		top:13px;
	    left: 5px;
	    margin:0px 0px 0px 0px;    
	    padding: 0px 0px 0px 0px;
	}

	#user {
		top:3px;
	    left: 5px;
	    margin:0px 0px 0px 0px;    
	    padding: 0px 0px 0px 0px;
	}

	#menu {
	    top:58px;
	}

</style>	

<div id="innerdiv">
</div>

<script type="text/javascript">	

	<?php include_once("window.js") ?>

</script>

<style type="text/css">
	.div-notifications{
		margin: 0px 0 0 70px;
		width: 600px;
	}

	/*#left_side {
    	float: left;
    	margin: 50px 0 0 70px;
    	width: 500px;
	}

	#right_side {
	    float: right;
	    margin: 50px 280px 0 0px;
	    width: 500px;
	}*/
</style>

<div class="div-notifications">
    <div class="alert alert-danger" id="left_side">
        <strong>(<?php echo $consult_count;?>) consultation schedule/s within 3 days!</strong><br> <a href="<?php echo site_url('consults'); ?>" style="text-decoration:none"><?php echo $consult_name;?></a>
    </div>
    <div class="alert alert-danger" id="left_side">
        <strong>(<?php echo $immunization_count;?>) PPD Boosters needed after 7-21 days of PPD Initial!</strong><br> <a href="<?php echo site_url('immunization'); ?>" style="text-decoration:none"><?php echo $immunization_name;?>
    </div>
</div>