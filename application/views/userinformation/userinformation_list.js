setTimeout("UpdateSessionData();", 0);

var query = null;

function ExportStaffList(type) {
    params = new Object();
    params.type     = type;
    params.query    = query;

    ExportDocument('userinformation/exportdocument', params, type);
}

Ext.onReady(function(){
 
    var store = new Ext.data.JsonStore({
        pageSize: setLimit,
        storeId: 'myStore',
        proxy: {
            type: 'ajax',
            url: 'userinformation/stafflist',
            extraParams: {query:query},
            remoteSort: false,
            params: {start: 0, limit: setLimit},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount'
            }
        },        
        fields: [{name: 'id', type: 'int'}, 'name', 'position_desc', 'department_desc', 'query_type', 'created_by']
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("userinformationGrid").getStore().reload({params:{start:0 }, timeout: 300000});      
    };

    var grid = Ext.create('Ext.grid.Panel', {
        id      : 'userinformationGrid',
        region  : 'center',
        store:store,        
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Name', dataIndex: 'name', width: '20%'},
            { text: 'Department', dataIndex: 'department_desc', width: '20%'},
            { text: 'Position', dataIndex: 'position_desc', width: '20%'}
        ],
        columnLines: true,
        width: '100%',
        margin: '0 0 10 0',
        viewConfig: {
            listeners: {
                itemdblclick: function(view,rec,item,index,eventObj) {
                    AddEditDeleteStaff('Edit', 'userinformationGrid', null);
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    nameMenu.showAt(e.getXY());
                }
            }
        },
        bbar: Ext.create('Ext.PagingToolbar', {
            store: store,
            pageSize: setLimit,
            displayInfo: true,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No record/s to display"
        })
    });
    RefreshGridStore(); 

    var nameMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteStaff('Add', 'userinformationGrid', null);}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteStaff('Edit', 'userinformationGrid', null);}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteStaff('Delete', 'userinformationGrid', null);}
        }]
    });

    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [grid],
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchId',
            emptyText: 'Search here...',
            width   : '30%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("userinformationGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchId").getValue();
                        query = Ext.getCmp("searchId").getValue();
                        RefreshGridStore();
                    }
                }
            }
        }, 
        { xtype: 'tbfill'},
        { xtype: 'button', text: 'ADD', icon: '../image/add.png', tooltip: 'Add Staff Record', handler: function (){ AddEditDeleteStaff('Add', 'userinformationGrid', null);}},
        { xtype: 'button', text: 'EDIT', icon: '../image/edit.png', tooltip: 'Edit Staff Record', handler: function (){ AddEditDeleteStaff('Edit', 'userinformationGrid', null);}},
        { xtype: 'button', text: 'DELETE', icon: '../image/delete.png', tooltip: 'Delete Staff Record', handler: function (){ AddEditDeleteStaff('Delete', 'userinformationGrid', null);}},
        {
            text: 'Download',
            tooltip: 'Extract Data to PDF or EXCEL File Format',
            icon: '../image/download.png',
            menu: 
            {
                items: 
                [
                    {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            ExportStaffList('PDF');
                        }
                    }, 
                    {
                        text    : 'Export Excel Format',
                        icon: '../image/excel.png',
                        handler: function ()
                        {
                            ExportStaffList('Excel');
                        }
                    }
                ]
            }
        }]
    });
});
