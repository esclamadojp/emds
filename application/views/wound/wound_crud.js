var woundWindow, woundHeaderID,  woundForm, residentID, woundNo;

function woundCRUD(type) 
{
	params = new Object();

	if (type == "Delete")
	{
		params.wound_id		= woundHeaderID;
		params.type			= type;

		deleteFunction('wound/headercrud', params, 'woundGrid');
	}
	else
	{		
		params.wound_id		= woundHeaderID;
		params.resident_id 	= residentID;
		params.wound_types	= Ext.get('wound_types').dom.value;
		params.infections	= Ext.get('infections').dom.value;
		params.sites		= Ext.get('sites').dom.value;
		params.dietary_supplements	= Ext.get('dietary_supplements').dom.value;
		params.risk_factors	= Ext.get('risk_factors').dom.value;	
		params.type			= type;

		addeditFunction('wound/headercrud', params, 'woundGrid', woundForm, woundWindow);
	}
}

function AddEditDeleteWound(type)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	var sm = Ext.getCmp("woundGrid").getSelectionModel();
	if(type == 'Edit' || type == 'Delete')	
	{
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}		
		woundHeaderID = sm.selected.items[0].data.wound_id;
		woundNo = sm.selected.items[0].data.wound_no;
		title = sm.selected.items[0].data.title;
	}

	if (type == "Delete")
	{
		if (sm.selected.items[0].data.ohealed == 'Yes')
		{
			errorFunction("Error!","Cannot delete healed record.");
			return;
		} 
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'This will delete all records for '+title+'. Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					woundCRUD(type);
			}
		});
	}
	else
	{
		var smStaff = Ext.getCmp("staffTree").getSelectionModel();
		if (!smStaff.hasSelection() || Ext.getCmp('radio2').checked == true)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}
		residentID = smStaff.selected.items[0].data.id;
		if (!residentID)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}		

		formHeader = Ext.create('Ext.form.Panel', {
                border      : false,
                bodyStyle   : 'padding:10px 10px 0 10px;',                      
                items: [                
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items: [
                    {
                        xtype: 'panel',                                            
                        width: '50%',
                        border:false,
                        items:[ 
                        {
                            xtype: 'fieldset',
                            border: false,
                            fieldDefaults: {
									labelAlign	: 'right',
									labelWidth: 70,
									afterLabelTextTpl: required,
									anchor	: '100%',
									allowBlank: false
						        }, 
                            items: [
                            {
                                xtype: 'panel',
                                border: false,                                                                
                                layout: 'anchor',
                                items: [
                                {
				                    xtype: 'fieldcontainer',
				                    labelStyle: 'font-weight:bold;padding:0;',
				                    layout: 'hbox',
				                    items: [
				                    {
							            xtype   	: 'combo',
							            flex: 1,
							            id			: 'wound_types',
							            fieldLabel	: 'Type',
							            valueField	: 'id',
							            displayField: 'description',
							            triggerAction: 'all',
							            minChars    : 3,
							            enableKeyEvents: true,
							            readOnly    : false,
							            forceSelection: true,
							            store: new Ext.data.JsonStore({
									        proxy: {
									            type: 'ajax',
									            url: 'commonquery/caseentrylist',
									            extraParams: {query:null, type: 'wound_types', category:null},
									            reader: {
									                type: 'json',
									                root: 'data',
									                idProperty: 'id'
									            }
									        },
									        params: {start: 0, limit: 10},
									        fields: [{name: 'id', type: 'int'}, 'description']
							            }),
							            listeners: 
							            {
							                select: function (combo, record, index)
							                {		 
							                	Ext.get('wound_types').dom.value = record[0].data.id;
							                }
							            }  
							        },
							        {
							        	xtype: 'button',
							        	hidden: crudMaintenance,
							        	margins		: '0 0 0 5',
							        	text: '...',
							        	tooltip: 'Add/Edit/Delete Type',
							        	handler: function (){ viewMaintenance('wound_types', null); }
							        }]
								}, {
				                    xtype: 'fieldcontainer',
				                    labelStyle: 'font-weight:bold;padding:0;',
				                    layout: 'hbox',
				                    items: [
				                    {
							            xtype   	: 'combo',
							            flex: 1,
							            id			: 'infections',
							            fieldLabel	: 'Origin',
							            valueField	: 'id',
							            displayField: 'description',
							            triggerAction: 'all',
							            minChars    : 3,
							            enableKeyEvents: true,
							            readOnly    : false,
							            forceSelection: true,
							            store: new Ext.data.JsonStore({
									        proxy: {
									            type: 'ajax',
									            url: 'commonquery/caseentrylist',
									            extraParams: {query:null, type: 'infections', category:null},
									            reader: {
									                type: 'json',
									                root: 'data',
									                idProperty: 'id'
									            }
									        },
									        params: {start: 0, limit: 10},
									        fields: [{name: 'id', type: 'int'}, 'description']
							            }),
							            listeners: 
							            {
							                select: function (combo, record, index)
							                {		 
							                	Ext.get('infections').dom.value = record[0].data.id;
							                }
							            }
							        },
							        {
							        	xtype: 'button',
							        	hidden: crudMaintenance,
							        	margins		: '0 0 0 5',
							        	text: '...',
							        	tooltip: 'Add/Edit/Delete Origin',
							        	handler: function (){ viewMaintenance('infections', null); }
							        }]
								}, {
				                    xtype: 'fieldcontainer',
				                    labelStyle: 'font-weight:bold;padding:0;',
				                    layout: 'hbox',
				                    items: [
				                    {
							            xtype   	: 'combo',
							            flex: 1,
							            id			: 'sites',
							            fieldLabel	: 'Site',
							            valueField	: 'id',
							            displayField: 'description',
							            triggerAction: 'all',
							            minChars    : 3,
							            enableKeyEvents: true,
							            readOnly    : false,
							            forceSelection: true,
							            store: new Ext.data.JsonStore({
									        proxy: {
									            type: 'ajax',
									            url: 'commonquery/caseentrylist',
									            extraParams: {query:null, type: 'sites', category:null},
									            reader: {
									                type: 'json',
									                root: 'data',
									                idProperty: 'id'
									            }
									        },
									        params: {start: 0, limit: 10},
									        fields: [{name: 'id', type: 'int'}, 'description']
							            }),
							            listeners: 
							            {
							                select: function (combo, record, index)
							                {		 
							                	Ext.get('sites').dom.value = record[0].data.id;
							                }
							            }  
							        },
							        {
							        	xtype: 'button',
							        	hidden: crudMaintenance,
							        	margins		: '0 0 0 5',
							        	text: '...',
							        	tooltip: 'Add/Edit/Delete Site',
							        	handler: function (){ viewMaintenance('sites', null); }
							        }]
								}]
                            }]
                        }]
                    },
                    {
                        xtype: 'panel',                       
                        width: '50%',
                        border:false,
                        items:[ 
                        {
                            xtype: 'fieldset',
                            border: false,
                            fieldDefaults: {
									labelAlign	: 'right',
									labelWidth: 130,
									afterLabelTextTpl: required,
									anchor	: '100%',
									allowBlank: false
						        }, 
                            items: [
                            {
                                xtype: 'panel',
                                border: false,                                
                                layout: 'anchor',
                                items: [
                                {
				                    xtype: 'panel',
				                    margin: '0 0 5',
		                            border:false,                            
		                            labelStyle: 'font-weight:bold;padding:0;',
				                    layout: 'hbox',
				                    items: [
				                    {
				                        xtype       : 'combo',
				                        flex: 1,
				                        id          : 'dietary_supplements',
				                        fieldLabel  : 'Dietary Supplements',                        
				                        multiSelect : true,
				                        valueField  : 'id',
				                        displayField: 'description',
				                        triggerAction: 'all',
				                        minChars    : 3,
				                        enableKeyEvents: true,
				                        readOnly    : false,
				                        ddReorder: true,
				                        forceSelection: true,
				                        store: new Ext.data.JsonStore({
				                            proxy: {
				                                type: 'ajax',
				                                url: 'commonquery/caseentrylist',
				                                extraParams: {query:null, type: 'dietary_supplements', category:null},
				                                reader: {
				                                    type: 'json',
				                                    root: 'data',
				                                    idProperty: 'id'
				                                }
				                            },
				                            params: {start: 0, limit: 10},
				                            fields: [{name: 'id', type: 'int'}, 'description']
				                        }),
				                        listeners: 
				                        {
				                            select: function (combo, record, index)
				                            {   
				                                var dietary_supplementsValue = Ext.getCmp("dietary_supplements").getValue();
				                                Ext.get('dietary_supplements').dom.value = dietary_supplementsValue.toString();
				                            }
				                        }
				                    },
				                    {
				                        xtype: 'button',
				                        hidden: crudMaintenance,
				                        margins     : '0 0 0 5',
				                        text: '...',
				                        tooltip: 'Add/Edit/Delete Dietary Supplements',
				                        handler: function (){ viewMaintenance('dietary_supplements', null); }
				                    }]
				                }, {
				                    xtype: 'panel',
				                    margin: '0 0 5',
		                            border:false,                            
		                            labelStyle: 'font-weight:bold;padding:0;',
				                    layout: 'hbox',
				                    items: [
				                    {
				                        xtype       : 'combo',
				                        flex: 1,
				                        id          : 'risk_factors',
				                        fieldLabel  : 'Risk Factor',                        
				                        allowBlank: false,
				                        multiSelect : true,
				                        valueField  : 'id',
				                        displayField: 'description',
				                        afterLabelTextTpl: required,
				                        triggerAction: 'all',
				                        minChars    : 3,
				                        enableKeyEvents: true,
				                        readOnly    : false,
				                        ddReorder: true,
				                        forceSelection: true,
				                        store: new Ext.data.JsonStore({
				                            proxy: {
				                                type: 'ajax',
				                                url: 'commonquery/caseentrylist',
				                                extraParams: {query:null, type: 'risk_factors', category:null},
				                                reader: {
				                                    type: 'json',
				                                    root: 'data',
				                                    idProperty: 'id'
				                                }
				                            },
				                            params: {start: 0, limit: 10},
				                            fields: [{name: 'id', type: 'int'}, 'description']
				                        }),
				                        listeners: 
				                        {
				                            select: function (combo, record, index)
				                            {   
				                                var risk_factorsValue = Ext.getCmp("risk_factors").getValue();
				                                Ext.get('risk_factors').dom.value = risk_factorsValue.toString();
				                            }
				                        }
				                    },
				                    {
				                        xtype: 'button',
				                        hidden: crudMaintenance,
				                        margins     : '0 0 0 5',
				                        text: '...',
				                        tooltip: 'Add/Edit/Delete Risk Factor',
				                        handler: function (){ viewMaintenance('risk_factors', null); }
				                    }]
				                }]
                            }]
                        }]
                    }]
                }]
            });

	var storeSiteGrid = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'wound/sitelist',
            extraParams: {wound_id: 0, wound_no: 1},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        listeners: {
            load: function(store, records, successful, eOpts) {
            	if(records[0])
            	{
            		Ext.getCmp("healed").setValue(records[0].data.healed);
            		Ext.getCmp("healed_date").setValue(records[0].data.healed_date);
            		Ext.getCmp("healed_date").setRawValue(records[0].data.healed_date);
	            	Ext.getCmp("lblOriginalStage").setText('<font size=2 color=blue>'+records[0].data.orig_stage_desc+'</font>', false);
	            	Ext.getCmp("lblHighestStage").setText('<font size=2 color=red>'+records[0].data.high_stage_desc+'</font>', false);	            	

            		var admin = '<?php echo $admin;?>';
					if(!admin)
					{
						if(records[0].data.healed)
						{
							Ext.getCmp("healed").setDisabled(true);
							Ext.getCmp("healed_date").setDisabled(true);
						}
						else
							Ext.getCmp("healed").setDisabled(false);
					}					
	            }
	            else{
	            	Ext.getCmp("healed").setDisabled(false);	            	
	            	Ext.getCmp("healed").setValue(0);
	            }
            }
        },
        fields: [{name: 'id', type: 'int'}, 'room', 'date', 'stage_desc', 'orig_stage_desc', 'high_stage_desc', 'healed', 'healed_date', 'size', 'wound_bed_desc', 'tunneling', 'undermining', 'treatment', 'remarks'],
        groupField: 'title'
    });
    
    var RefreshSiteGridStore = function () {
        Ext.getCmp("siteGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };     

    function gridTooltip(value, metadata, record, rowIndex, colIndex, store){
        metadata.tdAttr = 'data-qtip="' + value + '"';
        return value;
    }

	gridPanel = Ext.create('Ext.grid.Panel', {
        id      : 'siteGrid',
        region  : 'center',
        store: storeSiteGrid,
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Rm #', dataIndex: 'room', width: 150},
            { text: 'Date', dataIndex: 'date', width: 90},
            { text: 'Stage / Degree', dataIndex: 'stage_desc', width: 150, renderer:gridTooltip},
            { text: 'Size (cm)', dataIndex: 'size', width: 150, renderer:gridTooltip},
            { text: 'Wound Bed', dataIndex: 'wound_bed_desc', width: 150, renderer:gridTooltip},
            { text: 'Tunneling', dataIndex: 'tunneling', width: 178, renderer:gridTooltip},
            { text: 'Undermining', dataIndex: 'undermining', width: 150, renderer:gridTooltip},
            { text: 'Treatment', dataIndex: 'treatment', width: 150, renderer:gridTooltip},
            { text: 'Remarks', dataIndex: 'remarks', width: 150, renderer:gridTooltip}
        ],
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        autoHeight: true,
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteWoundNo('Edit');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    siteMenu.showAt(e.getXY());
                }
            }
        },
        tbar: [
        {
            xtype   	: 'combo',
            id			: 'wound_no',
            name		: 'wound_no',
            fieldLabel	: 'Wound No.',
            valueField	: 'description',
            displayField: 'description',
            allowBlank	: false,
            labelWidth	: 60,
            width		: '20%',
            editable	: false,
            value		: 1,
            mode    	: 'local',
            triggerAction : 'all',
            store   : new Ext.data.ArrayStore({
                fields: ['id', 'description'],
                data: [[1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6], [7, 7], [8, 8], [9, 9], [10, 10]]
            }),
            listeners: 
            {
                select: function (combo, record, index)
                {
                	woundNo = record[0].data.id;
                	Ext.getCmp("siteGrid").getStore().proxy.extraParams["wound_no"] = record[0].data.id;
                	RefreshSiteGridStore();
                }
            }
		}, 
		{
            xtype: 'checkbox',
            id  : 'healed',
            name: 'healed',                                    
            inputValue: 1,   
            boxLabel: '<font color=green><b>Healed</b></font>',
            listeners: 
            {
                change : function() 
                {
                	if(Ext.getCmp('healed').checked == true){
                    	Ext.getCmp("healed_date").setVisible(true);Ext.getCmp("healed_date").setDisabled(false);
                	}
                    else{
                    	Ext.getCmp("healed_date").setVisible(false);Ext.getCmp("healed_date").setDisabled(true);
                    }
                }
            }
        },
        {
			xtype	: 'datefield',
			id		: 'healed_date',
			name	: 'healed_date',
			width	: 110,
			hidden	: true,
			disabled: true,
			allowBlank: false,
			emptyText : 'Date Healed',
			value	: new Date()
		},
		'-',
		{
            xtype	: 'label',
            html	: '<font size=2>Original Stage: </font>'
        },
        {
            xtype	: 'label',
            id		: 'lblOriginalStage',
            html	: '<font size=2 color=blue>N/A</font>'
        },
        '-',
		{
            xtype	: 'label',
            html	: '<font size=2>Highest Stage: </font>'
        },
        {
            xtype	: 'label',
            id		: 'lblHighestStage', 
            html	: '<font size=2 color=red>N/A</font>'
        },
        { xtype: 'tbfill'},
        { xtype: 'button', icon: '../image/add.png', tooltip: 'Add', handler: function (){ AddEditDeleteWoundNo('Add');}},
        { xtype: 'button', icon: '../image/edit.png', tooltip: 'Edit', handler: function (){ AddEditDeleteWoundNo('Edit');}},
        { xtype: 'button', icon: '../image/delete.png', tooltip: 'Delete', handler: function (){ AddEditDeleteWoundNo('Delete');}},
        { xtype: 'button', icon: '../image/pdf.png', tooltip: 'Extract Data to PDF File Format', 
	        handler: function (){ 

	        	var sm = Ext.getCmp("siteGrid").getSelectionModel();
				if (!sm.hasSelection())
				{
					warningFunction("Warning!","Please select record.");
					return;
				}
	        	ExportWoundNo(sm.selected.items[0].data.id, residentID, 'PDF');

	    	}
        }]
    });
	RefreshSiteGridStore();

	var siteMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteWoundNo('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteWoundNo('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteWoundNo('Delete');}
        }, {
            text: 'Extract Data to PDF File Format',
            icon: '../image/pdf.png',
            handler: function (){ 
            	var sm = Ext.getCmp("siteGrid").getSelectionModel();
				if (!sm.hasSelection())
				{
					warningFunction("Warning!","Please select record.");
					return;
				}
	        	ExportWoundNo(sm.selected.items[0].data.id, residentID, 'PDF');
            }
        }]
    });

		woundForm = Ext.create('Ext.form.Panel', {
			border		: false,
			anchor: '90%',
			items: [
			{
                xtype: 'panel',
                border:false,                            
                bodyStyle   : 'padding:10px 5px 0 10px;',
                anchor		: '100%',
                items: [
                {
                    xtype: 'label',
                    flex: 1,
                    id: 'lblDiagnosis',
                    autoEl: {
					  tag: 'label',
					  'data-qtip': ''
					},
                    html: '<b>GENERAL DIAGNOSIS:</b> <a href="#" href="#" style="text-decoration:none;font: 9px;">Shingles</a>'
                }]
            }, 
            formHeader, 
            {
                	xtype: 'fieldset',
                	title: 'Details',
                	anchor: '100%',
                    fixed: true,
                    margin : '0 5 5 5',
                    items:[gridPanel]
            }]
		});
		
		if(type == 'Add')
			btnHidden = true;
		else 
			btnHidden = false;

		var mainForm = Ext.create('Ext.panel.Panel', {
			  border   : false,
		      region   : 'center',
		      width    : '100%',        
		      items    : woundForm
		   });

		woundWindow = Ext.create('Ext.window.Window', {
			title		: type + ' Wound Care Entry of <b>' + rName + '</b>',
			closable	: true,
			modal		: true,
			width		: 900,
			autoHeight	: true,
			resizable	: false,
			maximizable : true,
	      	layout      : 'fit',
	      	plain       : true,  
			buttonAlign	: 'center',
			header		: {titleAlign: 'center'},
			items		: mainForm,
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    hidden	: btnHidden,
			    handler: function ()
			    {
					if (!woundForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								woundCRUD(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	woundWindow.close();
			    }
			}],
		});

		woundForm.getForm().load({
			url: 'wound/headerview',
			timeout: 30000,
			waitMsg:'Loading data...',
			params: {
				id: woundHeaderID, 
				resident_id:residentID,
				type: type
			},		
			success: function(form, action) {
				woundWindow.show();
				var data = action.result.data;

        		var diagnose_desc = action.result.diagnose_desc;
        		var diagnose_count = action.result.diagnose_count;
        		var diagnose_tip = action.result.diagnose_tip;
	             
	            if(action.result.diagnose_count == 0)
	                Ext.getCmp("lblDiagnosis").setText('<b>GENERAL DIAGNOSIS:</b> None', false);
	            else if(action.result.diagnose_count == 1)

	                Ext.getCmp("lblDiagnosis").setText('<b>GENERAL DIAGNOSIS:</b> <a href="#" href="#" style="text-decoration:none;font: 9px;">'+diagnose_desc+'</a>', false);
	            else 
	                Ext.getCmp("lblDiagnosis").setText('<b>GENERAL DIAGNOSIS:</b> <a href="#" href="#" style="text-decoration:none;font: 9px;">'+diagnose_desc+' and ('+(action.result.diagnose_count-1)+') more...</a>', false);
	            
	            if(action.result.diagnose_count != 0)                    
	                document.getElementById("lblDiagnosis").setAttribute("data-qtip", diagnose_tip);            
	            else 
	                document.getElementById("lblDiagnosis").setAttribute("data-qtip", "");            

				if(type == 'Edit')
				{
					Ext.getCmp("wound_types").setRawValue(data.wound_type_desc);
					Ext.getCmp("infections").setRawValue(data.infection_desc);
					Ext.getCmp("sites").setRawValue(data.site_desc);
					Ext.getCmp("risk_factors").setRawValue(data.risk_factors_desc);
					Ext.getCmp("dietary_supplements").setRawValue(data.dietary_supplements_desc);

					Ext.get('wound_types').dom.value = data.wound_type_id;				
					Ext.get('infections').dom.value = data.infection_id;				
					Ext.get('sites').dom.value = data.site_id;				
					Ext.get('risk_factors').dom.value = data.risk_factors_id;				
					Ext.get('dietary_supplements').dom.value = data.dietary_supplements_id;				

					Ext.getCmp("siteGrid").getStore().proxy.extraParams["wound_id"] = woundHeaderID;
					Ext.getCmp("siteGrid").getStore().proxy.extraParams["wound_no"] = woundNo;
					Ext.getCmp("wound_no").setValue(woundNo);
                	RefreshSiteGridStore();
				}				
			},		
			failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		});
		Ext.getCmp("wound_types").focus();
	}
}