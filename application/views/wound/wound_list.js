setTimeout("UpdateSessionData();", 0);

var rName;

function ExportWoundNo(woundnoDetailID, residentID, type) {

    var sm = Ext.getCmp("woundGrid").getSelectionModel();
    if (!sm.hasSelection())
    {
        warningFunction("Warning!","Please select record.");
        return;
    }

    params = new Object();
    params.id           = woundnoDetailID;
    params.wound_id     = sm.selected.items[0].data.wound_id;
    params.resident_id  = residentID;
    params.report       = 'WoundNoReport';

    ExportDocument('wound/exportdocument', params, type);
}

function ExportGridList(type) {

    params = new Object();
    params.type     = type;
    params.date_from = Ext.getCmp("dateFrom").getValue();
    params.date_to  = Ext.getCmp("dateTo").getValue();
    params.query    = Ext.getCmp("searchWound").getValue();  
    params.report   = 'WoundList';

    ExportDocument('wound/exportdocument', params, type);
}

Ext.onReady(function(){
      
    var treeStore = Ext.create('Ext.data.TreeStore', {
        proxy: {
            type: 'ajax',
            reader: 'json',
            extraParams: {query:null, status:2},
            url: 'consults/namelist'
        }     
    }); 

    var RefreshTreeStore = function () {
        Ext.getCmp("staffTree").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };

    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Names',
        split   : true,
        region  : 'west',
        collapsible: true,
        id : 'staffTree',
        store: treeStore,
        rowLines: true,
        width: '30%',
        minWidth: 200,
        margin: '0 0 10 0',
        height: 500,        
        rootVisible: false,
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchName',
            emptyText: 'Search here...',
            width   : '40%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("staffTree").getStore().proxy.extraParams["query"] = Ext.getCmp("searchName").getValue();
                        RefreshTreeStore();
                    }
                }
            }
        },
        { xtype: 'tbfill'},        
        {
            xtype       : 'combo',
            valueField  : 'id',
            width       : 90,
            displayField: 'Status',
            emptyText: 'Admitted',
            allowBlank  : false,
            editable    : false,
            mode        : 'local',
            triggerAction : 'all',
            store   : new Ext.data.ArrayStore({
                fields: ['id', 'Status'],
                data: [[2, 'Admitted'], [3, 'Discharged'], [1, 'All']]
            }),
            listeners: 
            {
                select: function (combo, record, index)
                {      
                    Ext.getCmp("staffTree").getStore().proxy.extraParams["query"] = Ext.getCmp("searchName").getValue();   
                    Ext.getCmp("staffTree").getStore().proxy.extraParams["status"] = record[0].data.id;
                    RefreshTreeStore();                 
                }
            }
        },
        {
            tooltip: 'View resident profile details',
            icon: '../image/profile.png',
            handler: function (){ LoadResidentProfile("staffTree");}
        },
        { xtype: 'button', icon: '../image/add.png', tooltip: 'Add Name', handler: function (){ AddEditDeleteResident('Add', 'staffTree', null);}},
        { xtype: 'button', icon: '../image/edit.png', tooltip: 'Edit Name', handler: function (){ AddEditDeleteResident('Edit', 'staffTree', null);}},
        { xtype: 'button', icon: '../image/delete.png', tooltip: 'Delete Name', handler: function (){ AddEditDeleteResident('Delete', 'staffTree', 'woundGrid');}}
        ],
        viewConfig: {
            listeners: {
                itemclick: function(view,rec,item,index,eventObj) {
                    Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                    Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                    Ext.getCmp("woundGrid").getStore().proxy.extraParams["resident_id"] = rec.get('id');                                                            
                    rName = rec.get('text');
                    if (rName == 'Names')
                        Ext.getCmp("woundGrid").setTitle('Details');
                    else
                        Ext.getCmp("woundGrid").setTitle('Wound details of <font size=2><b>'+rName+'</b></font>');
                    RefreshGridStore();
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    nameMenu.showAt(e.getXY());
                }
            }
        },
        listeners:
        {
            collapse : function() {
                Ext.getCmp('radio1').setValue(false);
                Ext.getCmp('radio2').setValue(true);
            },
            expand : function() {
                Ext.getCmp('radio1').setValue(true);
                Ext.getCmp('radio2').setValue(false);
            }
        }
    });

    var nameMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'View Profile Details',
            icon: '../image/profile.png',
            handler: function (){ LoadResidentProfile("staffTree");}
        }, {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteResident('Add', 'staffTree', null);}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteResident('Edit', 'staffTree'. null);}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteResident('Delete', 'staffTree', 'woundGrid');}
        }]
    });

    var storeWoundList = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'wound/woundlist',
            extraParams: {start: 0, limit: 20, query:null, type: 'Name', resident_id: 0, date_from: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), date_to: Ext.Date.add(new Date(), Ext.Date.DAY, 180)},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, {name: 'wound_id', type: 'int'}, {name: 'resident_id', type: 'int'}, 'wound_no', 'wound_no_text', 'title', 'room', 'date', 'healed', 'ohealed', 'healed_date', 'stage_desc', 'orig_stage_desc', 'high_stage_desc'],
        groupField: 'title'
    });
    
    var RefreshGridStore = function () {
        Ext.getCmp("woundGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };     

    var grid = Ext.create('Ext.grid.Panel', {
        id      : 'woundGrid',
        region  : 'center',
        store: storeWoundList,
        columns: [
            { dataIndex: 'id', hidden: true},
            { dataIndex: 'resident_id', hidden: true},
            { dataIndex: 'ohealed', hidden: true},            
            { text: 'Rm #', dataIndex: 'room', width: '14%'},
            { text: 'Date', dataIndex: 'date', width: '12%'},
            { text: 'Wound No', dataIndex: 'wound_no_text', width: '10%', align:'center'},
            { text: 'Healed', dataIndex: 'healed', width: '8%', align:'center'},
            { text: 'Date Healed', dataIndex: 'healed_date', width: '12%'},
            { text: 'Stage/Degree', dataIndex: 'stage_desc', width: '14%'},
            { text: 'Original Stage', dataIndex: 'orig_stage_desc', width: '14%'},
            { text: 'Highest Stage', dataIndex: 'high_stage_desc', width: '14%'}
        ],
        features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: '{name}'
        }],
        columnLines: true,
        width: '80%',
        minWidth: 700,
        height: 400,
        title: 'Details',
        loadMask: true,
        margin: '0 0 10 0',
        viewConfig: {
            stripeRows: false, 
            getRowClass: function(record) { 
                return record.get('ohealed') < 'Yes' ? '' : 'healed-row'; 
            }, 
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteWound('Edit');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        },
        tbar: [
        {
            xtype   : 'textfield',
            id      : 'searchWound',
            emptyText: 'Search here...',
            width   : '15%',
            listeners:
            {
                specialKey : function(field, e) {
                    if(e.getKey() == e.ENTER) {
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchWound").getValue();
                        RefreshGridStore();
                    }
                }
            }
        },
        {
            xtype   : 'radio',
            boxLabel  : 'Name',
            name      : 'searchtype',            
            inputValue: '1',
            checked   : true,
            id        : 'radio1',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio1').checked == true)
                    {
                        Ext.getCmp("exportExcel").setVisible(false);
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["type"] = 'Name';
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["query"] = null;
                        Ext.getCmp('searchWound').setValue(null);
                        RefreshGridStore();
                        Ext.getCmp("woundGrid").setTitle('Wound details of <font size=2><b>'+rName+'</b></font>');
                        Ext.getCmp('staffTree').expand();
                    }
                }
            }
        }, {
            xtype   : 'radio',
            boxLabel  : 'All',
            name      : 'searchtype',            
            inputValue: '2',            
            id        : 'radio2',
            listeners:
            {
                change : function() {
                    if(Ext.getCmp('radio2').checked == true)
                    {
                        Ext.getCmp("exportExcel").setVisible(true);
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["type"] = 'All';
                        Ext.getCmp("woundGrid").getStore().proxy.extraParams["query"] = null;
                        Ext.getCmp('searchWound').setValue(null);
                        RefreshGridStore();
                        Ext.getCmp("woundGrid").setTitle('Details');
                        Ext.getCmp("staffTree").collapse();
                    }
                }
            }
        }, 
        {
            xtype: 'label',
            html: '<font size=2 color=><b>From:</b></font>'
        },
        { xtype: 'datefield', id:'dateFrom', emptyText: 'From',  value: Ext.Date.subtract(new Date(), Ext.Date.DAY, 90), width: 100},
        {
            xtype: 'label',
            html: '<font size=2 color=><b>To:</b></font>'
        },
        { xtype: 'datefield', id:'dateTo', emptyText: 'To',  value: Ext.Date.add(new Date(), Ext.Date.DAY, 180), width: 100},
        { xtype: 'button', text: 'RELOAD', icon: '../image/load.png', tooltip: 'Reload grid based on date range', 
            handler: function (){ 
                Ext.getCmp("woundGrid").getStore().proxy.extraParams["query"] = Ext.getCmp("searchWound").getValue();
                Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_from"] = Ext.getCmp("dateFrom").getValue();
                Ext.getCmp("woundGrid").getStore().proxy.extraParams["date_to"] = Ext.getCmp("dateTo").getValue();
                RefreshGridStore();
            }
        },        
        { xtype: 'tbfill'},
        { xtype: 'button', icon: '../image/add.png', tooltip: 'Add Wound', handler: function (){ AddEditDeleteWound('Add');}},
        { xtype: 'button', icon: '../image/edit.png', tooltip: 'Edit Wound', handler: function (){ AddEditDeleteWound('Edit');}},
        { xtype: 'button', icon: '../image/delete.png', tooltip: 'Delete Wound', handler: function (){ AddEditDeleteWound('Delete');}},
        {
            text: 'Download',
            tooltip: 'Extract Data to PDF or EXCEL File Format',
            icon: '../image/download.png',
            menu: 
            {
                items: 
                [
                    {
                        text    : 'Export PDF Format',
                        icon: '../image/pdf.png',
                        handler: function ()
                        {
                            if(Ext.getCmp('radio1').checked == true){

                                var sm = Ext.getCmp("woundGrid").getSelectionModel();
                                if (!sm.hasSelection())
                                {
                                    warningFunction("Warning!","Please select record.");
                                    return;
                                }
                                ExportWoundNo(sm.selected.items[0].data.id, sm.selected.items[0].data.resident_id, 'PDF');                                
                            }
                            else
                                ExportGridList('PDF');
                        }
                    }, 
                    {
                        text    : 'Export Excel Format',
                        id: 'exportExcel',
                        hidden: true,
                        icon: '../image/excel.png',
                        handler: function ()
                        {
                            ExportGridList('Excel');
                        }
                    }
                ]
            }
        }]
    });
    RefreshGridStore(); 
    var view = grid.getView();
    view.getFeature('group').toggleSummaryRow(false);
    view.refresh();

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [
        {
            text: 'Add',
            icon: '../image/add.png',
            handler: function (){ AddEditDeleteWound('Add');}
        }, {
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteWound('Edit');}
        }, {
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteWound('Delete');}
        }]
    });

    Ext.create('Ext.panel.Panel', {
        title: '<?php echo addslashes($module_name);?>',
        width: '100%',
        height: sheight,
        renderTo: "innerdiv",
        layout: 'border',
        border: false,
        items   : [tree,grid]
    });
});
