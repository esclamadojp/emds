var woundnoWindow, woundnoDetailID,  woundnoForm, residentID;
var room_id, diagnosis_id;

function woundnoCRUD(type) 
{
	processingFunction("Processing data, please wait...");
	params = new Object();

	if (type == "Delete")
	{
		params.id			= woundnoDetailID;
		params.wound_id		= woundHeaderID;
		params.type			= type;
		params.wound_no		= Ext.getCmp("wound_no").getValue();

		Ext.Ajax.request({
		    url: 'wound/woundcrud',
		    method	: 'POST',
		    params: params,
		    success: function(f,a)
		    {
		    	Ext.MessageBox.hide();
				var response = Ext.decode(f.responseText);									
				if (response.success == true)
				{
					Ext.getCmp('siteGrid').getStore().reload({params:{reset:1 }, timeout: 300000});      
					Ext.getCmp('woundGrid').getStore().reload({params:{reset:1 }, timeout: 1000});
					infoFunction('Status', response.data);
				}
				else
					errorFunction("Error!",response.data);
		    },
			failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		});
	}
	else
	{		
		params.id			= woundnoDetailID;
		params.wound_id		= woundHeaderID;
		params.wound_no		= Ext.getCmp("wound_no").getValue();
		params.resident_id 	= residentID;
		params.rooms		= room_id;
		params.wound_types	= Ext.get('wound_types').dom.value;
		params.infections	= Ext.get('infections').dom.value;		
		params.sites		= Ext.get('sites').dom.value;
		params.stages		= Ext.get('stages').dom.value;
		params.exudates		= Ext.get('exudates').dom.value;
		params.exudates_amount	= Ext.get('exudates_amount').dom.value;
		params.odors		= Ext.get('odors').dom.value;
		params.wound_beds	= Ext.get('wound_beds').dom.value;
		params.peri_wounds	= Ext.get('peri_wounds').dom.value;
		params.temperatures	= Ext.get('temperatures').dom.value;
		params.risk_factors	= Ext.get('risk_factors').dom.value;
		params.dietary_supplements	= Ext.get('dietary_supplements').dom.value;
		params.nurse		= Ext.get('nurse').dom.value;
		params.physicians	= Ext.get('physicians').dom.value;		
		params.type			= type;

		woundnoForm.submit({
			url: 'wound/woundcrud',
			method: "POST",	
			params: params,
		    success: function(f,action)
		    {
				Ext.MessageBox.hide();

				if (action.result.success == true)
				{
					woundnoWindow.close();						
					infoFunction('Status', action.result.data);
					Ext.getCmp("siteGrid").getStore().proxy.extraParams["wound_id"] = action.result.wound_id;
					Ext.getCmp('siteGrid').getStore().reload({params:{reset:1 }, timeout: 1000});
					Ext.getCmp('woundGrid').getStore().reload({params:{reset:1 }, timeout: 1000});
				}
		    },
			failure: function(f,action) { errorFunction("Error!",action.result.data); }
	    }); 
	}
}

function AddEditDeleteWoundNo(type)
{          	
	if (!woundForm.form.isValid()){
		errorFunction("Error!",'Please fill-in the required fields (Marked red).');
	    return;
    }

    if (Ext.getCmp("healed").getValue())
    {
    	errorFunction("Error!",type + ' is not allowed for healed wound record.');
	    return;
    }

	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp("siteGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}
		woundnoDetailID = sm.selected.items[0].data.id;
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					woundnoCRUD(type);
			}
		});
	}
	else
	{
		var smStaff = Ext.getCmp("staffTree").getSelectionModel();
		if (!smStaff.hasSelection() || Ext.getCmp('radio2').checked == true)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}
		residentID = smStaff.selected.items[0].data.id;
		if (!residentID)
		{
			warningFunction("Warning!","Please select a name.");
			return;
		}		
		
		woundnoForm = Ext.create('Ext.form.Panel', {
			bodyStyle   : 'padding:10px 5px 0 10px;',
            border      : false,
            items: [
            {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [
                {
                    xtype: 'panel',                                            
                    width: '55%',
                    border:false,
                    items:[ 
                    {
                        xtype: 'fieldset',
                        border: false,
                        fieldDefaults: {
								labelAlign	: 'right',
								labelWidth: 110,
								afterLabelTextTpl: required,
								anchor	: '100%',
								allowBlank: false
					        }, 
                        items: [
                        {
		                    xtype: 'panel',
		                    margin: '5 0 10 62',
		                    border:false,                            
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
		                        xtype: 'label',
		                        html: 'ROOM'
		                    }, {
		                        xtype: 'label',
		                        flex: 1,
		                        id: 'lblRooms',
		                        html: ': None'
		                    }]
		                }, {
	                        xtype: 'panel',
	                        border: false,                                                                
	                        layout: 'anchor',
	                        items: [{
								xtype	: 'datefield',
								id		: 'date',
								name	: 'date',
								value	: new Date(),
								fieldLabel: 'Date'		
							}]
	                    }, {
		                    xtype: 'textareafield',
		                    id		: 'treatment',
							name	: 'treatment',
							height	: 40,	
							fieldLabel: 'Treatment'
						}, {
		                    xtype: 'fieldcontainer',
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'stages',
					            fieldLabel	: 'Stage / Degree',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            matchFieldWidth: false,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'stages', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('stages').dom.value = record[0].data.id;
					                }
					            }  
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Stage / Degree',
					        	handler: function (){ viewMaintenance('stages', null); }
					        }]
						}, {
		                    xtype: 'fieldcontainer',
		                    labelStyle: 'font-weight:bold;padding:0;',		                    
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'numberfield',
					            width		: 193,
					            id			: 'slength',
					            name		: 'slength',
					            emptyText	: 'Length',			            
					            minValue	: 0.0,
					            fieldLabel	: 'Size',
					            afterLabelTextTpl: null,
								allowBlank: true,
								listeners: {
									focus: function(field) {
										field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
									},
									blur: function(field) {
										field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
									}
								}
					        },
					        {
					            xtype   	: 'numberfield',
					            width		: 85,
					            id			: 'swidth',
					            name		: 'swidth',
					            emptyText	: 'Width',	
					            minValue	: 0,
					            hideLabel 	: true,
					            afterLabelTextTpl: null,
								allowBlank: true,
								listeners: {
									focus: function(field) {
										field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
									},
									blur: function(field) {
										field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
									}
								}
					        },{
					            xtype   	: 'numberfield',
					            width		: 85,
					            id			: 'sdepth',
					            name		: 'sdepth',
					            emptyText	: 'Depth',	
					            minValue	: 0,
					            hideLabel 	: true,
					            afterLabelTextTpl: null,
								allowBlank: true,
								listeners: {
									focus: function(field) {
										field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
									},
									blur: function(field) {
										field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
									}
								}		           
					        },{
					        	xtype		: 'label',
					        	margins		: '5 0 0 5',
					        	html		: '<font color=green>(<b>cm</b>)<font>'
					        }]
						}, {
		                    xtype: 'fieldcontainer',
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'numberfield',
					            width		: 193,
					            id			: 'surface_area',
					            name		: 'surface_area',
					            minValue	: 1,
					            maxValue	: 100,
					            afterLabelTextTpl: null,
								allowBlank: true,
					            fieldLabel	: 'Burn Surface Area',
					        },{
					        	xtype		: 'label',
					        	margins		: '5 0 0 5',
					        	html		: '<font color=green>(<b>%</b>)<font>'
					        }]
						}, {
		                    xtype: 'fieldcontainer',
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'exudates',
					            fieldLabel	: 'Exudates',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'exudates', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('exudates').dom.value = record[0].data.id;
					                }
					            }  
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Exudates',
					        	handler: function (){ viewMaintenance('exudates', null); }
					        }]
						}, {
		                    xtype: 'fieldcontainer',
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'exudates_amount',
					            fieldLabel	: 'Exu. Amount',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'exudates_amount', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('exudates_amount').dom.value = record[0].data.id;
					                }
					            }  
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Exudates Amount',
					        	handler: function (){ viewMaintenance('exudates_amount', null); }
					        }]
						}, {
		                    xtype: 'fieldcontainer',
		                    labelStyle: 'font-weight:bold;padding:0;',
		                    layout: 'hbox',
		                    items: [
		                    {
					            xtype   	: 'combo',
					            flex: 1,
					            id			: 'odors',
					            fieldLabel	: 'Odor',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/caseentrylist',
							            extraParams: {query:null, type: 'odors', category:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('odors').dom.value = record[0].data.id;
					                }
					            }  
					        },
					        {
					        	xtype: 'button',
					        	hidden: crudMaintenance,
					        	margins		: '0 0 0 5',
					        	text: '...',
					        	tooltip: 'Add/Edit/Delete Odor',
					        	handler: function (){ viewMaintenance('odors', null); }
					        }]
		                }]
                    }]
                },
                {
                    xtype: 'panel',                       
                    width: '45%',
                    border:false,
                    items:[ 
                    {
                        xtype: 'fieldset',
                        border: false,
                        fieldDefaults: {
								labelAlign	: 'right',
								labelWidth: 120,
								afterLabelTextTpl: required,
								anchor	: '100%',
								allowBlank: false
					        }, 
                        items: [
                        {
                            xtype: 'panel',
                            border: false,                                
                            layout: 'anchor',
                            items: [
                            {
			                    xtype: 'fieldcontainer',
			                    labelStyle: 'font-weight:bold;padding:0;',
			                    layout: 'hbox',
			                    items: [
			                    {
						            xtype   	: 'combo',
						            flex: 1,
						            id			: 'wound_beds',
						            fieldLabel	: 'Wound Bed',
						            valueField	: 'id',
						            displayField: 'description',
						            triggerAction: 'all',
						            minChars    : 3,
						            enableKeyEvents: true,
						            readOnly    : false,
						            forceSelection: true,
						            store: new Ext.data.JsonStore({
								        proxy: {
								            type: 'ajax',
								            url: 'commonquery/caseentrylist',
								            extraParams: {query:null, type: 'wound_beds', category:null},
								            reader: {
								                type: 'json',
								                root: 'data',
								                idProperty: 'id'
								            }
								        },
								        params: {start: 0, limit: 10},
								        fields: [{name: 'id', type: 'int'}, 'description']
						            }),
						            listeners: 
						            {
						                select: function (combo, record, index)
						                {		 
						                	Ext.get('wound_beds').dom.value = record[0].data.id;
						                }
						            }  
						        },
						        {
						        	xtype: 'button',
						        	hidden: crudMaintenance,
						        	margins		: '0 0 0 5',
						        	text: '...',
						        	tooltip: 'Add/Edit/Delete Wound Bed',
						        	handler: function (){ viewMaintenance('wound_beds', null); }
						        }]
							}, {
			                    xtype: 'fieldcontainer',
			                    labelStyle: 'font-weight:bold;padding:0;',
			                    layout: 'hbox',
			                    items: [
			                    {
						            xtype   	: 'combo',
						            flex: 1,
						            id			: 'peri_wounds',
						            fieldLabel	: 'Peri Wound',
						            valueField	: 'id',
						            displayField: 'description',
						            triggerAction: 'all',
						            minChars    : 3,
						            enableKeyEvents: true,
						            readOnly    : false,
						            forceSelection: true,
						            store: new Ext.data.JsonStore({
								        proxy: {
								            type: 'ajax',
								            url: 'commonquery/caseentrylist',
								            extraParams: {query:null, type: 'peri_wounds', category:null},
								            reader: {
								                type: 'json',
								                root: 'data',
								                idProperty: 'id'
								            }
								        },
								        params: {start: 0, limit: 10},
								        fields: [{name: 'id', type: 'int'}, 'description']
						            }),
						            listeners: 
						            {
						                select: function (combo, record, index)
						                {		 
						                	Ext.get('peri_wounds').dom.value = record[0].data.id;
						                }
						            }  
						        },
						        {
						        	xtype: 'button',
						        	hidden: crudMaintenance,
						        	margins		: '0 0 0 5',
						        	text: '...',
						        	tooltip: 'Add/Edit/Delete Peri Wound',
						        	handler: function (){ viewMaintenance('peri_wounds', null); }
						        }]
							}, {
			                    xtype: 'fieldcontainer',
			                    labelStyle: 'font-weight:bold;padding:0;',
			                    layout: 'hbox',
			                    items: [
			                    {
						            xtype   	: 'combo',
						            flex: 1,
						            id			: 'temperatures',
						            fieldLabel	: 'Temperature',
						            valueField	: 'id',
						            displayField: 'description',
						            triggerAction: 'all',
						            minChars    : 3,
						            enableKeyEvents: true,
						            readOnly    : false,
						            forceSelection: true,
						            store: new Ext.data.JsonStore({
								        proxy: {
								            type: 'ajax',
								            url: 'commonquery/caseentrylist',
								            extraParams: {query:null, type: 'temperatures', category:null},
								            reader: {
								                type: 'json',
								                root: 'data',
								                idProperty: 'id'
								            }
								        },
								        params: {start: 0, limit: 10},
								        fields: [{name: 'id', type: 'int'}, 'description']
						            }),
						            listeners: 
						            {
						                select: function (combo, record, index)
						                {		 
						                	Ext.get('temperatures').dom.value = record[0].data.id;
						                }
						            }  
						        },
						        {
						        	xtype: 'button',
						        	hidden: crudMaintenance,
						        	margins		: '0 0 0 5',
						        	text: '...',
						        	tooltip: 'Add/Edit/Delete Temperature',
						        	handler: function (){ viewMaintenance('temperatures', null); }
						        }]
							}, {
					            xtype: 'radiogroup',
					            fieldLabel: 'Pain',
					            items: [
					                {boxLabel: 'Yes', name: 'pain', inputValue: 1, checked: true},
					                {boxLabel: 'No', name: 'pain',inputValue: 2, }
					            ]
							}, {
			                    xtype: 'fieldcontainer',
			                    labelStyle: 'font-weight:bold;padding:0;',
			                    layout: 'hbox',
			                    items: [
						        {
							        xtype		: 'timefield',
							        id			: 'tunneling_direction',
							        name		: 'tunneling_direction',
							        emptyText	: 'Direction',
							        flex		: 1,
							        fieldLabel	: 'Tunneling',
							        minValue	: '12:00 AM',
							        maxValue	: '11:00 PM',
							        increment	: 60,
							        afterLabelTextTpl: null,
									allowBlank: true										 
							    },{
						            xtype   	: 'numberfield',							            
						            id			: 'tunneling_length',
						            name		: 'tunneling_length',
						            emptyText	: 'Length',	
						            width		: 75,
						            minValue	: 0,
						            hideLabel 	: true,
						            afterLabelTextTpl: null,
									allowBlank: true,
									listeners: {
										focus: function(field) {
											field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
										},
										blur: function(field) {
											field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
										}
									}		 
						        },{
						        	xtype		: 'label',
						        	margins		: '5 0 0 5',
						        	html		: '<font color=green>(<b>cm</b>)<font>'
						        }]
							}, {
			                    xtype: 'fieldcontainer',
			                    labelStyle: 'font-weight:bold;padding:0;',
			                    layout: 'hbox',
			                    items: [
						        {
							        xtype		: 'timefield',
							        id			: 'undermining_direction',
							        name		: 'undermining_direction',
							        emptyText	: 'Direction',
							        flex		: 1,
							        fieldLabel	: 'Undermining',
							        minValue	: '12:00 AM',
							        maxValue	: '11:00 PM',
							        increment	: 60,
							        afterLabelTextTpl: null,
									allowBlank: true
							    },{
						            xtype   	: 'numberfield',							            
						            id			: 'undermining_length',
						            name		: 'undermining_length',
						            emptyText	: 'Length',	
						            width		: 75,
						            minValue	: 0,
						            hideLabel 	: true,
						            afterLabelTextTpl: null,
									allowBlank: true,
									listeners: {
										focus: function(field) {
											field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
										},
										blur: function(field) {
											field.setRawValue(Ext.util.Format.number(field.getValue(), '0.0'));
										}
									}		 
						        },{
						        	xtype		: 'label',
						        	margins		: '5 0 0 5',
						        	html		: '<font color=green>(<b>cm</b>)<font>'
						        }]
							}, {
								xtype	: 'textareafield',
								id		: 'remarks',
								name	: 'remarks',	
								height	: 40,					
								fieldLabel: 'Remarks',
								afterLabelTextTpl: null,
								allowBlank: true			
							}, {
			                    xtype: 'fieldcontainer',                    
			                    id: 'physicians_container',
			                    labelStyle: 'font-weight:bold;padding:0;',
			                    layout: 'hbox',
			                    items: [
			                    {
						            xtype   	: 'combo',
						            flex		: 1,			       
						            id			: 'physicians',     
						            fieldLabel	: 'Physician',
						            valueField	: 'id',
						            displayField: 'description',
						            triggerAction: 'all',
						            minChars    : 3,
						            enableKeyEvents: true,
						            readOnly    : false,
						            forceSelection: true,
						            store: new Ext.data.JsonStore({
								        proxy: {
								            type: 'ajax',
								            url: 'commonquery/caseentrylist',
								            extraParams: {query:null, type: 'physicians', category:null},
								            reader: {
								                type: 'json',
								                root: 'data',
								                idProperty: 'id'
								            }
								        },
								        params: {start: 0, limit: 10},
								        fields: [{name: 'id', type: 'int'}, 'description']
						            }),
			                        listeners: 
			                        {
			                            select: function (combo, record, index)
			                            {        
			                                Ext.get('physicians').dom.value = record[0].data.id;
			                            }
			                        }    
						        },
						        {
						        	xtype: 'button',
						        	hidden: crudMaintenance,
						        	margins		: '0 0 0 5',
						        	text: '...',
						        	tooltip: 'Add/Edit/Delete Physician',
						        	handler: function (){ viewMaintenance('physicians', null); }
						        }]
							}, {
					            xtype   	: 'combo',
					            id			: 'nurse',     
					            fieldLabel	: 'Nurse',
					            valueField	: 'id',
					            displayField: 'description',
					            triggerAction: 'all',
					            minChars    : 3,
					            enableKeyEvents: true,
					            readOnly    : false,
					            forceSelection: true,
					            store: new Ext.data.JsonStore({
							        proxy: {
							            type: 'ajax',
							            url: 'commonquery/staffname',
							            extraParams: {query:null},
							            reader: {
							                type: 'json',
							                root: 'data',
							                idProperty: 'id'
							            }
							        },
							        params: {start: 0, limit: 10},
							        fields: [{name: 'id', type: 'int'}, 'description']
					            }),
					            listeners: 
					            {
					                select: function (combo, record, index)
					                {		 
					                	Ext.get('nurse').dom.value = record[0].data.id;
					                }
					            }  
				        	}]
                        }]
                    }]
                }]
            }]
        });
		
		woundnoWindow = Ext.create('Ext.window.Window', {
			title		: type + ' Details for Site: ' + Ext.getCmp("sites").getRawValue() + ',  Wound No.: ' + Ext.getCmp("wound_no").getRawValue() + '</b>',
			closable	: true,
			modal		: true,
			width		: 800,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [woundnoForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!woundnoForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								woundnoCRUD(type);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	woundnoWindow.close();
			    }
			}],
		});

		woundnoForm.getForm().load({
			url: 'wound/detailview',
			timeout: 30000,
			waitMsg:'Loading data...',
			params: { 
				id: woundnoDetailID, 
				resident_id:residentID,
				type: type
			},
			success: function(form, action) 
			{
				woundnoWindow.show();
				var data = action.result.data;

				Ext.getCmp("lblRooms").setText(': ' + data.room_desc, false);
				room_id = data.room_id;				

				if(type == 'Edit')
				{
					Ext.getCmp("stages").setRawValue(data.stage_desc);
					Ext.getCmp("exudates").setRawValue(data.exudate_desc);
					Ext.getCmp("exudates_amount").setRawValue(data.exudate_amount_desc);
					Ext.getCmp("odors").setRawValue(data.odor_desc);
					Ext.getCmp("wound_beds").setRawValue(data.wound_bed_desc);
					Ext.getCmp("peri_wounds").setRawValue(data.peri_wound_desc);
					Ext.getCmp("temperatures").setRawValue(data.temperature_desc);
					Ext.getCmp("physicians").setRawValue(data.physician_desc);
					Ext.getCmp("nurse").setRawValue(data.nurse_desc);

					Ext.get('stages').dom.value = data.stage_id;				
					Ext.get('exudates').dom.value = data.exudate_id;				
					Ext.get('exudates_amount').dom.value = data.exudate_amount_id;				
					Ext.get('odors').dom.value = data.odor_id;				
					Ext.get('wound_beds').dom.value = data.wound_bed_id;				
					Ext.get('peri_wounds').dom.value = data.peri_wound_id;				
					Ext.get('temperatures').dom.value = data.temperature_id;				
					Ext.get('physicians').dom.value = data.physician_id;				
					Ext.get('nurse').dom.value = data.nurse_id;				
				}
				else
				{	
					if(data.room_id == null)									
					{
						errorFunction("Load failed", 'Please identify the room of the resident at the <img src="../image/profile.png"> PROFILE RESIDENT form.');
						woundnoWindow.close();
					}	
				}
				Ext.getCmp("slength").focus();
				Ext.getCmp("swidth").focus();
				Ext.getCmp("sdepth").focus();
				Ext.getCmp("tunneling_length").focus();
				Ext.getCmp("undermining_length").focus();
				Ext.getCmp("date").focus();
			},		
			failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		});		
		Ext.getCmp("date").focus();
	}
}