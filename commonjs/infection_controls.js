var caseWindow, caseForm, residentID, caseEntryID;

function ExportCaseList(type) {
    params = new Object();
    params.id       = residentID;
    params.name     = residentName;
    params.age      = residentAge;
    params.sex      = residentSex;
    params.type     = type;
    params.report   = 'CaseList';

    ExportDocument('infection_control/exportdocument', params, type);
}

function LoadResidentCases(type, id, name, age, sex)
{      
    var buttonVisible = false;
    if(type == 'grid')
    {
    	var sm = Ext.getCmp("resident_caseGrid").getSelectionModel();
    	if (!sm.hasSelection())
    	{
    		warningFunction("Warning!","Please select record.");
    		return;
    	}
    	residentID = sm.selected.items[0].data.id;
    	residentName = sm.selected.items[0].data.name;
    	residentAge = sm.selected.items[0].data.age;
    	residentSex = sm.selected.items[0].data.sex;        
    }
    else        
    {
        residentID = id;
        residentName = name;
        residentAge = age;
        residentSex = sex;
        buttonVisible = true;
    }

	var storeResidentCases = new Ext.data.JsonStore({
        proxy: {
            type: 'ajax',
            url: 'infection_control/residentcasegrid',
            extraParams: {start: 0, limit: 20, id:residentID},
            reader: {
                type: 'json',
                root: 'data',
                idProperty: 'id'
            }
        },
        fields: [{name: 'id', type: 'int'}, 'title', 'diagnosis', 'date', 'case_type', 'html', 'test'],
        groupField: 'title'
    });
    
    var RefreshGridResidentStore = function () {
        Ext.getCmp("ResidentGrid").getStore().reload({params:{reset:1 }, timeout: 300000});      
    };

    var ResidentGridPanel = Ext.create('Ext.grid.Panel', {
        id      : 'ResidentGrid',
        region  : 'center',        
        store:storeResidentCases,
        columnLines: true,
        width: '100%',
        features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: '{name}'
        }],
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Date',  dataIndex: 'date', width: '15%'},
            { text: 'Case Type', dataIndex: 'case_type', width: '76%'},
            {
                xtype: 'actioncolumn',
                width: '4%',
                align: 'center',
                hidden: buttonVisible,
                items: [{
                	icon   : '../image/edit.png',
                    dataIndex: 'id',
                    tooltip: 'Edit Case',
                    handler: function(grid, rowIndex, colIndex) {
                        caseEntryID = grid.dataSource.data.keys[rowIndex];
                        AddEditDeleteResidentCase('Edit');
                    }
                }]
             }
        ],
        plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate('{html}')
        }],
        viewConfig: {
            listeners: {
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    rowMenu.showAt(e.getXY());
                }
            }
        }
    });
    RefreshGridResidentStore(); 

    var view = ResidentGridPanel.getView();
    view.getFeature('group').toggleSummaryRow(false);
    view.refresh();

    var rowMenu = Ext.create('Ext.menu.Menu', {
        items: [{
            text: 'Edit',
            icon: '../image/edit.png',
            handler: function (){ AddEditDeleteResidentCase('Edit');}
		},{        	
            text: 'Delete',
            icon: '../image/delete.png',
            handler: function (){ AddEditDeleteResidentCase('Delete');}
        }]
    });

	caseForm = Ext.create('Ext.form.Panel', {
			border		: false,
			height		: 422,
			layout: 'border',
            anchor: '100%',
			items: [ResidentGridPanel],
			tbar: [
			{
			    xtype	: 'label',
                html: '<font size=4><b>'+residentName+'</b></font>'
			},	
			'-',
			{
			    xtype	: 'label',
			    text	: residentAge + 'yrs',
			},	
			'-',
			{
			    xtype	: 'label',
			    text	: residentSex,
			},
	        { xtype: 'tbfill'},
            { xtype: 'button', hidden: buttonVisible, text: 'ADD', icon: '../image/add.png', tooltip: 'Add NEW CASE / Current Case Record', handler: function (){ AddEditDeleteResidentCase('Add'); }},
			{ xtype: 'button', hidden: buttonVisible, text: 'EDIT', icon: '../image/edit.png', tooltip: 'Edit Case Record', handler: function (){ AddEditDeleteResidentCase('Edit'); }},
			{ xtype: 'button', hidden: buttonVisible, text: 'DELETE', icon: '../image/delete.png', tooltip: 'Delete Case Record', handler: function (){ AddEditDeleteResidentCase('Delete'); }},
			'-',
            {
                text: 'Download',
                tooltip: 'Extract Data to PDF File Format',
                icon: '../image/pdf.png',
                handler: function ()
                {
                    ExportCaseList('PDF');
                }   
            }]
		});

		caseWindow = Ext.create('Ext.window.Window', {
		title		: residentName + ' Case/s',
		closable	: true,
        maximizable : true,
		modal		: true,
        layout: 'fit',
        plain: true,
		width		: 750,
    	height		: 500,
		autoHeight	: true,
		resizable	: false,
		header: {titleAlign: 'center'},
		items: [caseForm]
	}).show();
}