var maintenanceCrudWindow, maintenanceCrudID, maintenanceCrudForm;
 
function maintenanceFormCRUD(crudtype, type, category)
{ 
	params = new Object();

	if (crudtype == "Delete")
	{
		params.id		= maintenanceCrudID;
		params.crudtype	= crudtype;
		params.type		= type;

		deleteFunction('commonquery/maintenancecrud', params, 'maintenanceGrid');
	}
	else
	{
		if (category) var 
			cat_id = Ext.get(category).dom.value;
		else 
			var cat_id = null;

		params.id		= maintenanceCrudID;
		params.crudtype = crudtype;
		params.cat_id	= cat_id;
		params.type		= type;

		addeditFunction('commonquery/maintenancecrud', params, 'maintenanceGrid', maintenanceCrudForm, maintenanceCrudWindow);
	}
}

function functionCRUD(crudtype, type, category)
{
	if (!maintenanceCrudForm.form.isValid()){
		errorFunction("Error!",'Please fill-in the required fields (Marked red).');
	    return;
    }
	Ext.Msg.show({
		title	: 'Confirmation',
		msg		: 'Are you sure you want to Save?',
		width	: '100%',
		icon	: Ext.Msg.QUESTION,
		buttons	: Ext.Msg.YESNO,
		fn: function(btn){
			if (btn == 'yes')
				maintenanceFormCRUD(crudtype,type, category);
		}
	});
}

function AddEditDeleteMaintenanceCrud(crudtype,type, category)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(crudtype == 'Edit' || crudtype == 'Delete')	
	{
		var sm = Ext.getCmp("maintenanceGrid").getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select a record.");
			return;
		}
		maintenanceCrudID = sm.selected.items[0].data.id;
	}

	if (crudtype == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + crudtype + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					maintenanceFormCRUD(crudtype,type, category);
			}
		});
	}
	else
	{
		if (category)
		{
			var cat_bool = false;
			var infection_control_bool = true;			
			var alias_bool = true;
		}
		else 
		{
			var cat_bool = true;
			if(type == 'diagnosis_category' || type == 'transmission_precautions_category')
				var infection_control_bool = false;
			else 
				var infection_control_bool = true;

			if(type == 'buildings')
				var alias_bool = false;
			else 
				var alias_bool = true;		

			if(type == 'diagnosis')
				var care_plan_bool = false;
			else 
				var care_plan_bool = true;		
		}

		maintenanceCrudForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 80,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [
				{
		            xtype   	: 'combo',
		            flex: 1,
		            id			: category,
		            fieldLabel	: 'Category',
		            valueField	: 'id',
		            displayField: 'description',
		            triggerAction: 'all',
		            minChars    : 3,
		            enableKeyEvents: true,
		            readOnly    : false,
		            disabled	: cat_bool,
		            hidden		: cat_bool,
		            store: new Ext.data.JsonStore({
				        proxy: {
				            type: 'ajax',
				            url: 'commonquery/caseentrylist',
				            extraParams: {query:null, type: category, category:null},
				            reader: {
				                type: 'json',
				                root: 'data',
				                idProperty: 'id'
				            }
				        },
				        params: {start: 0, limit: 10},
				        fields: [{name: 'id', type: 'int'}, 'description']
		            }),
		            listeners: 
		            {
		                select: function (combo, record, index)
		                {		 
		                	Ext.get(category).dom.value = record[0].data.id;
		                }
		            }
		        }, {
					xtype	: 'textfield',
					id		: 'alias',
					name	: 'alias',
					fieldLabel: 'Alias',
					maxLength: 10,
					disabled	: alias_bool,
		            hidden		: alias_bool,
					listeners:
		            {
		                specialKey : function(field, e) {
		                    if(e.getKey() == e.ENTER) {
		                        Ext.getCmp("description").focus();
		                    }
		                }
		            }
				}, {
					xtype	: 'textfield',
					id		: 'description',
					name	: 'description',
					fieldLabel: 'Description',
					listeners:
		            {
		                specialKey : function(field, e) {
		                    if(e.getKey() == e.ENTER) {
		                        functionCRUD(crudtype, type, category);
		                    }
		                }
		            }
				}, {
                    xtype: 'checkbox',
                    id  : 'infection_control',
                    name: 'infection_control',                                    
                    inputValue: 1,   
                    disabled	: infection_control_bool,
		            hidden		: infection_control_bool,
                    margin: '0 0 0 83',
                    boxLabel: '<font color=red>(<b><i>for Infection Control</i></b>)</font>'
                }, {
                    xtype: 'checkbox',
                    id  : 'care_plan',
                    name: 'care_plan',                                    
                    inputValue: 1,   
                    disabled	: care_plan_bool,
		            hidden		: care_plan_bool,
                    margin: '0 0 0 83',
                    boxLabel: '<font color=red><b><i>Care Plan?</i></b></font>'
                }]
			});

			maintenanceCrudWindow = Ext.create('Ext.window.Window', {
			title		: crudtype + ' ' + type,
			closable	: true,
			modal		: true,
			width		: 350,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [maintenanceCrudForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					functionCRUD(crudtype, type, category);
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	maintenanceCrudWindow.close();
			    }
			}],
		});

		if(crudtype == 'Edit')
		{
			maintenanceCrudForm.getForm().load({
				url: 'commonquery/maintenanceview',
				timeout: 30000,
				waitMsg:'Loading data...',
				params: {
					id: this.maintenanceCrudID, 
					type: type, 
					category: category
				},	
				success: function(form, action) {
					maintenanceCrudWindow.show();
					var data = action.result.data;
					Ext.getCmp(category).setRawValue(data.category);
					Ext.get(category).dom.value = data.cat_id;				
				},			
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});
		}
		else
			maintenanceCrudWindow.show();

		Ext.getCmp("description").focus();
	}
}