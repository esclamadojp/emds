var residentcrud, residentCRUDID, residentCRUDForm; 

function residentMaintenanceCRUD(type, form, grid)
{
	if (type == "Delete")
	{
		Ext.Ajax.request({
		    url: 'commonquery/residentcrud',
		    method	: 'POST',
		    params: {id: residentCRUDID, type: type},
		    success: function(f,a)
		    {
		    	Ext.MessageBox.hide();
		    	if (type == "Close") residentcrud.close();
		    	else
		    	{
					var response = Ext.decode(f.responseText);					
					if (response.success == true)
					{						
						Ext.getCmp(form).getStore().reload({params:{reset:1 }, timeout: 300000});      
						infoFunction('Status', response.data);
						if (grid != null)Ext.getCmp(grid).setTitle('Details');
					}
					else
						errorFunction("Error!",response.data);
				}
		    },
			failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
		});
		processingFunction("Processing data, please wait...");
	}
	else
	{
		params 		= new Object();
		params.id	= residentCRUDID;
		params.type	= type;

		addeditFunction('commonquery/residentcrud', params, form, residentCRUDForm, residentcrud);
	}

	
}

function AddEditDeleteResident(type, form, grid)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp(form).getSelectionModel();		
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}
		residentCRUDID = sm.selected.items[0].data.id;
		if (!residentCRUDID)
		{
			warningFunction("Warning!","Please select record.");
			return;
		}
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					residentMaintenanceCRUD(type, form, grid);
			}
		});
	}
	else
	{
		residentCRUDForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 90,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [{
					xtype	: 'textfield',
					id		: 'fname',
					name	: 'fname',
					fieldLabel: 'First Name'
				}, {
					xtype	: 'textfield',
					id		: 'mname',
					name	: 'mname',
					allowBlank: true,
					afterLabelTextTpl: null,
					fieldLabel: 'Middle Name'
				}, {
					xtype	: 'textfield',
					id		: 'lname',
					name	: 'lname',
					fieldLabel: 'Last Name'
				}, {
					xtype	: 'numberfield',
					id		: 'age',
					name	: 'age',
					value	: 0,
					maxValue: 200,
        			minValue: 0,
					fieldLabel: 'Age'
				}, {
		            xtype: 'radiogroup',
		            fieldLabel: 'Sex',
		            items: [
		                {boxLabel: 'Male', name: 'sex', inputValue: 1, checked: true},
		                {boxLabel: 'Female', name: 'sex',inputValue: 2, }
		            ]
				}, {
					xtype	: 'checkbox',
					id		: 'status',
					name	: 'status',
					fieldLabel: 'Status',
					checked	: true,
					inputValue: 1,
				    boxLabel: 'Admitted'

				}]
			});

			residentcrud = Ext.create('Ext.window.Window', {
			title		: type + ' Resident',
			closable	: true,
			modal		: true,
			width		: 350,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [residentCRUDForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!residentCRUDForm.form.isValid()){
						errorFunction("Error!",'Please fill-in the required fields (Marked red).');
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								residentMaintenanceCRUD(type, form, grid);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	residentcrud.close();
			    }
			}],
		});

		if(type == 'Edit')
		{
			residentCRUDForm.getForm().load({
				url: 'commonquery/residentview',
				timeout: 30000,
				waitMsg:'Loading data...',
				params: {
					id: this.residentCRUDID, type: type
				},	
				success: function(form, action) { residentcrud.show(); },		
				failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
			});
		}
		else
			residentcrud.show();

		Ext.getCmp("fname").focus();
	}
}