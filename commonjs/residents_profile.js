var profileWindow, residentForm, residentID, fallHistoryIDCount;
var arrayInjury = new Array();
var arrayDate = new Array();
var arrayDiagnosisID = new Array();
var arrayDiagnosisDescription = new Array();
var arrayAllergiesID = new Array();
var arrayAllergiesDescription = new Array();
var independent_array = new Array();
var assist_array = new Array();
var dependent_array = new Array();
var census_description = new Array();
var census_type = new Array();
var census = new Array();
var census_count;
var fieldset2Expanded = true;
var fieldset3Expanded = true;
var fieldset4Expanded = true;
var fieldset5Expanded = true;
var fieldset6Expanded = true;
var fieldset7Expanded = true;
var fieldset8Expanded = true;

function ExportProfileDocs(type) {

    params = new Object();
    params.id   = residentID;
    params.name = patientName;
    
    ExportDocument('commonquery/exportdocument', params, type);
}

function profileCRUD(grid)
{
    if(!arrayDiagnosisID){
        errorFunction("Error!",'Please add at least one (1) diagnose.');
        return;
    }

    var meds = Ext.getCmp("fieldSet1Grid1").getSelectionModel();    
    var adls = Ext.getCmp("fieldSet1Grid2").getSelectionModel();
    
    independent_array = [];
    assist_array = [];
    dependent_array = [];

    for (var i = 0; i < adls.store.data.length; i++) 
    {
        if(adls.store.data.items[i].data.independent == 1)
            independent_array.push(1);
        else 
            independent_array.push(0);
        if(adls.store.data.items[i].data.assist == 1)
            assist_array.push(1);
        else 
            assist_array.push(0);
        if(adls.store.data.items[i].data.dependent == 1)
            dependent_array.push(1);
        else 
            dependent_array.push(0);
    }
    
    active_checkbox = [];
    active_checkbox.push(0);
    for (var i = 0; i < census_count; i++) 
        if(Ext.getCmp("panelCheckBox"+(i+1)))
            active_checkbox.push(i+1);

    params = new Object();
    params.resident_id      = residentID;
    params.diagnosis_id     = arrayDiagnosisID.toString();
    params.allergies_id     = arrayAllergiesID;
    params.rooms            = Ext.get('rooms').dom.value;
    params.medicare         = meds.store.data.items[0].data.medicare;
    params.medicaid         = meds.store.data.items[0].data.medicaid;
    params.others           = meds.store.data.items[0].data.others;
    params.independent      = independent_array.toString();
    params.assist           = assist_array.toString();
    params.dependent        = dependent_array.toString();
    params.active_checkbox  = active_checkbox.toString();

    addeditFunction('commonquery/resident_profilecrud', params, grid, residentForm, null);

}

function loadcensus(type, fieldSets)
{    
    for (var i = 0; i < census_count; i++) 
    {
        var boolCensus = true;
        if(census[i] == 0)
            boolCensus = false;                        
        if(census_type[i] == type) {
            var checkboxf = Ext.create('Ext.form.field.Checkbox', {
                id: 'panelCheckBox' + (i+1),
                name: 'panelCheckBox' + (i+1),
                margin : '0 0 0 20',
                checked: boolCensus,
                boxLabel  : census_description[i]
            });
            fieldSets.add(checkboxf);fieldSets.doLayout();
        }
    }
    Ext.MessageBox.hide();  
}

function LoadResidentProfile(grid)
{       
    arrayInjury = [];
    arrayDate = [];
    arrayDiagnosisID = [];
    arrayDiagnosisDescription = [];
    arrayAllergiesID = [];
    arrayAllergiesDescription = [];
    fallHistoryIDCount = 0;
    independent_array = [];
    assist_array = [];
    dependent_array = [];

    fieldset2Expanded = true;
    fieldset3Expanded = true;
    fieldset4Expanded = true;
    fieldset5Expanded = true;
    fieldset6Expanded = true;
    fieldset7Expanded = true;
    fieldset8Expanded = true;

    var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

    var sm = Ext.getCmp(grid).getSelectionModel();
    if (!sm.hasSelection())
    {
        warningFunction("Warning!","Please select record.");
        return;
    }    
    residentID = sm.selected.items[0].data.id;
    patientName = sm.selected.items[0].data.name;    
    patientAge = sm.selected.items[0].data.age;
    patientSex = sm.selected.items[0].data.sex;    

    if(typeof patientName == 'undefined') patientName = sm.selected.items[0].data.text;

    fieldSet1 = Ext.create('Ext.form.Panel', {
                border      : false,
                bodyStyle   : 'padding:10px;',      
                fieldDefaults: {
                    labelAlign  : 'right',
                    anchor  : '100%'
                },
                items: [
                {
                    xtype: 'fieldcontainer',
                    fieldDefaults: {
                        labelWidth: 170                        
                    },
                    layout: 'hbox',
                    items: [
                    {
                        xtype: 'panel',
                        border: false,
                        fixed: true,
                        margins : '0 1 0 15',
                        width: '20%',
                        items:[ {
                            xtype: 'label',
                            id: 'lblImage',
                            autoEl: {
                                  tag: 'label', 
                                  'data-qtip': 'Click to upload / change profile picture.',
                                },
                            html: '<a href="#" onclick="ImageUpload()"><IMG style="border:1px dashed DarkSlateBlue;" SRC="../profile_pic/pic1.png" height="160" width="160"></a><br>'
                        }]
                    }, {
                       xtype: 'panel',                       
                       width: '80%',
                       margins : '0 5 0 0',
                       border:false,
                       items:[
                           {
                            xtype: 'panel',
                            margin: '0 0 5',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
                            items: [
                            {
                                xtype: 'label',
                                margin: '0 0 0 45',
                                html: '<font color=red><b>General Diagnosis:</b></font>'
                            }, {
                                xtype: 'label',
                                margin: '0 0 0 10',
                                flex: 1,
                                id: 'lblDiagnosis',
                                autoEl: {
                                  tag: 'label', 
                                  'data-qtip': 'None',
                                },
                                html: 'None'
                            }, {
                                xtype: 'button',
                                hidden: crudMaintenance,
                                icon: '../image/plus.png',
                                margins     : '0 0 0 5',
                                tooltip: 'Add/Delete Diagnosis',
                                handler: function (){ commonGrid('Diagnosis'); }
                            }]
                        }, {
                            xtype: 'panel',
                            margin: '0 0 5',
                            border:false,                            
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
                            items: [
                            {
                                xtype: 'label',
                                margin: '0 0 0 106',
                                html: '<font color=red><b>Allergies:</b></font>'
                            }, {
                                xtype: 'label',
                                margin: '0 0 0 10',
                                flex: 1,
                                id: 'lblAllergy',
                                autoEl: {
                                  tag: 'label', 
                                  'data-qtip': 'None',
                                },
                                html: 'None'
                            }, {
                                xtype: 'button',
                                hidden: crudMaintenance,
                                icon: '../image/plus.png',
                                margins     : '0 0 0 5',
                                tooltip: 'Add/Delete Allergy',
                                handler: function (){ commonGrid('Allergy'); }
                            }]
                        }, {
                            xtype: 'panel',
                            margin: '0 0 5',
                            border:false,
                            labelStyle: 'font-weight:bold;padding:0;',
                            layout: 'hbox',
                            items: [
                            {
                                xtype       : 'combo',
                                flex: 1,
                                id          : 'rooms',
                                fieldLabel  : 'Room',                        
                                allowBlank: false,
                                valueField  : 'id',
                                displayField: 'description',
                                afterLabelTextTpl: required,
                                triggerAction: 'all',
                                minChars    : 3,
                                enableKeyEvents: true,
                                readOnly    : false,
                                ddReorder: true,
                                forceSelection: true,
                                store: new Ext.data.JsonStore({
                                    proxy: {
                                        type: 'ajax',
                                        url: 'commonquery/caseentrylist',
                                        extraParams: {query:null, type: 'rooms', category:null},
                                        reader: {
                                            type: 'json',
                                            root: 'data',
                                            idProperty: 'id'
                                        }
                                    },
                                    params: {start: 0, limit: 10},
                                    fields: [{name: 'id', type: 'int'}, 'description']
                                }),
                                listeners: 
                                {
                                    select: function (combo, record, index)
                                    {        
                                        Ext.get('rooms').dom.value = record[0].data.id;
                                    }
                                }
                            },
                            {
                                xtype: 'button',
                                hidden: crudMaintenance,
                                margins     : '0 0 0 5',
                                text: '...',
                                tooltip: 'Add/Edit/Delete Building',
                                handler: function (){ viewMaintenance('buildings', null); }
                            },
                            {
                                xtype: 'button',
                                hidden: crudMaintenance,
                                margins     : '0 0 0 5',
                                text: '...',
                                tooltip: 'Add/Edit/Delete Floor',
                                handler: function (){ viewMaintenance('floors', 'buildings'); }
                            },
                            {
                                xtype: 'button',
                                hidden: crudMaintenance,
                                margins     : '0 0 0 5',
                                text: '...',
                                tooltip: 'Add/Edit/Delete Room',
                                handler: function (){ viewMaintenance('rooms','floors'); }
                            }]
                        }, {
                            xtype: 'panel',
                            layout: 'hbox',
                            border: false,
                            items:[ 
                            {
                                xtype: 'panel',                       
                                width: '50%',
                                border: false,
                                items:[{    
                                    xtype   : 'datefield',
                                    id      : 'original_admission_date',
                                    name    : 'original_admission_date',
                                    labelWidth: 170,
                                    width   : 300,
                                    allowBlank : false,
                                    afterLabelTextTpl: required,
                                    readOnly : true,
                                    fieldLabel: 'Original Admission Date'       
                                }, { 
                                    xtype   : 'datefield',
                                    labelWidth: 170,
                                    id      : 'latest_admission_date',
                                    name    : 'latest_admission_date',
                                    width   : 300,
                                    allowBlank : false,
                                    afterLabelTextTpl: required,  
                                    readOnly : true,               
                                    fieldLabel: 'Latest Admission Date'       
                                }, { 
                                    xtype   : 'datefield',
                                    labelWidth: 170,
                                    id      : 'latest_discharge_date',
                                    name    : 'latest_discharge_date',
                                    width   : 300,
                                    readOnly : true,
                                    fieldLabel: 'Latest Discharge Date'       
                                }]
                            },
                            {
                                xtype: 'panel',                       
                                width: '50%',    
                                border: false,                           
                                items:[{ 
                                    xtype       : 'datefield',
                                    id          : 'birthdate',
                                    name        : 'birthdate',
                                    value       : new Date(),
                                    labelWidth  : 90,
                                    width       : 220,
                                    allowBlank : false,
                                    afterLabelTextTpl: required,      
                                    fieldLabel  : 'Birthdate'       
                                },{
                                    xtype       : 'numberfield',
                                    id          : 'weight',
                                    name        : 'weight',
                                    labelWidth  : 90,
                                    width       : 220,
                                    minValue    : 0,
                                    allowBlank : false,
                                    afterLabelTextTpl: required,      
                                    fieldLabel  : 'Weight (lbs.)'                                    
                                }]
                            }]
                        }]
                    }]
                }]
            });
    
    var fieldSet1storeGrid1 = new Ext.data.JsonStore({
        fields: [{name: 'id', type: 'int'}, 'medicare', 'medicaid', 'others']
    });

    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    }); 

    gridPanel1 = Ext.create('Ext.grid.Panel', {
        id      : 'fieldSet1Grid1',
        region  : 'center',
        store: fieldSet1storeGrid1,
        plugins: [cellEditing],
        columns: [
            { text: 'Provider No.', dataIndex: 'room', width: '19%'},
            { text: 'Medicare', dataIndex: 'medicare', width: '20%', renderer:addTooltip,
                editor: 
                {
                    xtype   : 'textfield', 
                    maxLength: 50,
                    id      : 'medicare_field'
                }   
            },
            { text: 'Medicaid', dataIndex: 'medicaid', width: '20%', renderer:addTooltip,
                editor: 
                {
                    xtype   : 'textfield', 
                    maxLength: 50,
                    id      : 'medicaid_field'
                }
            },
            { text: 'Other', dataIndex: 'others', width: '20%', renderer:addTooltip,
                editor: 
                {
                    xtype   : 'textfield', 
                    maxLength: 255,
                    id      : 'other_field'
                }
            },
            { text: 'Total Residents', dataIndex: 'tunneling', width: '20%'}
        ],
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteWoundNo('Edit');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    siteMenu.showAt(e.getXY());
                }
            }
        }
    });

    var fieldSet1storeGrid2 = new Ext.data.JsonStore({
        fields: [{name: 'id', type: 'int'}, 'description', 'independent', 'assist', 'dependent']
    });

    gridPanel2 = Ext.create('Ext.grid.Panel', {
        id      : 'fieldSet1Grid2',
        region  : 'center',
        store: fieldSet1storeGrid2,
        columns: [
            { text: 'ADL', dataIndex: 'description', width: '19%', renderer:addTooltip},
            { xtype: 'checkcolumn', text: 'Independent', dataIndex: 'independent', width: '27%'},
            { xtype: 'checkcolumn', text: 'Assist of One or Two Staff', dataIndex: 'assist', width: '27%'},
            { xtype: 'checkcolumn', text: 'Dependent', dataIndex: 'dependent', width: '26%'}
        ],
        columnLines: true,
        width: '100%',
        margin : '0 0 5',
        loadMask: true,
        viewConfig: {
            listeners: {
                itemdblclick: function() {
                    AddEditDeleteWoundNo('Edit');
                },
                itemcontextmenu: function(view, record, item, index, e){
                    e.stopEvent();
                    siteMenu.showAt(e.getXY());
                }
            }
        }
    });

    fieldSet2 = Ext.create('Ext.form.Panel', {
        border      : false,
        bodyStyle   : 'padding:10px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            anchor  : '100%'
        }
    });

    fieldSet3 = Ext.create('Ext.form.Panel', {
        border      : false,
        bodyStyle   : 'padding:10px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            anchor  : '100%'
        }
    });

    fieldSet4 = Ext.create('Ext.form.Panel', {
        title       : 'F108-114 - indicate the number of residents with:',
        border      : false,
        bodyStyle   : 'padding:10px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            anchor  : '100%'
        }
    });

    fieldSet5 = Ext.create('Ext.form.Panel', {    
        title       : 'F115-118 - indicate the number of residents with:',    
        border      : false,
        bodyStyle   : 'padding:10px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            anchor  : '100%'
        }
    });

    fieldSet6 = Ext.create('Ext.form.Panel', {
        title       : 'F119-132 - indicate the number of residents receiving:',
        border      : false,
        bodyStyle   : 'padding:10px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            anchor  : '100%'
        }
    });

    fieldSet7 = Ext.create('Ext.form.Panel', {
        title       : 'F133-139 - indicate the number of residents receiving:',
        border      : false,
        bodyStyle   : 'padding:10px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            anchor  : '100%'
        }
    });

    fieldSet8 = Ext.create('Ext.form.Panel', {
        border      : false,
        bodyStyle   : 'padding:10px;',      
        fieldDefaults: {
            labelAlign  : 'right',
            anchor  : '100%'
        }
    });


    residentForm = Ext.create('Ext.form.Panel', {
        width: '100%',        
        height: 575,
        autoScroll  : true,
        border : false,        
        items: [{
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsible: true,
            title: 'Personal Information',
            items:[fieldSet1, gridPanel1, gridPanel2]            
        }, {
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsed: true,
            collapsible: true,
            title: 'A. Bowel/Bladder Status',
            items:[fieldSet2],
            listeners: 
            {
                expand: function (combo, record, index)
                {                            
                    if(fieldset2Expanded)
                    {
                        Ext.MessageBox.wait('Loading census data...');
                        setTimeout("loadcensus('A', fieldSet2);", 500);                                    
                    }
                    fieldset2Expanded = false;
                }
            }
        }, {
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsed: true,
            collapsible: true,
            title: 'B. Mobility',
            items:[fieldSet3],
            listeners: 
            {
                expand: function (combo, record, index)
                {                            
                    if(fieldset3Expanded){
                        Ext.MessageBox.wait('Loading census data...');
                        setTimeout("loadcensus('B', fieldSet3);", 500);    
                    }
                    fieldset3Expanded = false;
                }
            }
        }, {
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsed: true,
            collapsible: true,
            title: 'C. Mental Status',
            items:[fieldSet4],
            listeners: 
            {
                expand: function (combo, record, index)
                {                            
                    if(fieldset4Expanded){
                        Ext.MessageBox.wait('Loading census data...');
                        setTimeout("loadcensus('C', fieldSet4);", 500);    
                    }
                    fieldset4Expanded = false;
                }
            }
        }, {
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsed: true,
            collapsible: true,
            title: 'D. Skin Integrity',
            items:[fieldSet5],
            listeners: 
            {
                expand: function (combo, record, index)
                {                            
                    if(fieldset5Expanded){
                        Ext.MessageBox.wait('Loading census data...');
                        setTimeout("loadcensus('D', fieldSet5);", 500);    
                    } 
                    fieldset5Expanded = false;
                }
            }
        }, {
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsed: true,
            collapsible: true,
            title: 'E. Special Care',
            items:[fieldSet6],
            listeners: 
            {
                expand: function (combo, record, index)
                {                            
                    if(fieldset6Expanded){
                        Ext.MessageBox.wait('Loading census data...');
                        setTimeout("loadcensus('E', fieldSet6);", 500);    
                    }
                    fieldset6Expanded = false;
                }
            }
        }, {
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsed: true,
            collapsible: true,
            title: 'F. Medications',
            items:[fieldSet7],
            listeners: 
            {
                expand: function (combo, record, index)
                {                            
                    if(fieldset7Expanded){
                        Ext.MessageBox.wait('Loading census data...');
                        setTimeout("loadcensus('F', fieldSet7);", 500);    
                    }
                    fieldset7Expanded = false;
                }
            }
        }, {
            xtype: 'fieldset',
            margin : '10 10 0 10',
            collapsed: true,
            collapsible: true,
            title: 'G. Other',
            items:[fieldSet8],
            listeners: 
            {
                expand: function (combo, record, index)
                {                            
                    if(fieldset8Expanded){
                        Ext.MessageBox.wait('Loading census data...');
                        setTimeout("loadcensus('G', fieldSet8);", 500);    
                    }
                    fieldset8Expanded = false;
                }
            }
        }]
    });

    profileWindow = Ext.create('Ext.window.Window', {
        title       : 'Resident Census and Conditions of Residents - ' + '<b><font size=3>' + patientName +', </b>' + patientAge +'yrs., ' + patientSex +'</font>',
        closable    : true,
        modal       : true,
        width       : 920,
        height      : 660,
        resizable   : false,
        buttonAlign : 'center',
        header: {titleAlign: 'center'},
        items: [residentForm],
        buttons: [
        {
            text    : 'Export PDF Format',
            icon: '../image/pdf.png',
            handler: function ()
            {
                ExportProfileDocs('PDF');
            }
        },
        {
            text    : 'Save',
            icon    : '../image/save.png',
            handler: function ()
            {
                if (!residentForm.form.isValid()){
                    errorFunction("Error!",'Please fill-in the required fields (Marked red).');
                    return;
                }
                Ext.Msg.show({
                    title   : 'Confirmation',
                    msg     : 'Are you sure you want to Save?',
                    width   : '100%',
                    icon    : Ext.Msg.QUESTION,
                    buttons : Ext.Msg.YESNO,
                    fn: function(btn){
                        if (btn == 'yes')
                            profileCRUD(grid);
                    }
                });
            }
        },
        {
            text    : 'Close',
            icon    : '../image/close.png',
            handler: function ()
            {
                profileWindow.close();
            }
        }],
    });

    residentForm.getForm().load({
        url: 'commonquery/resident_profileview',
        timeout: 30000,
        waitMsg:'Loading data...',
        params: {
            resident_id: residentID
        },         
        success: function(form, action) {
            var data = action.result.data;
            
            if(data == 'NO RECORD')
                errorFunction('Medical Record Not Found','Please choose a medical record.');
            else 
            {
                profileWindow.show();
                var diagnose_id = action.result.diagnose_id;
                var diagnose_desc = action.result.diagnose_desc;
                var allergy_id = action.result.allergy_id;
                var allergy_desc = action.result.allergy_desc;

                Ext.getCmp("rooms").setRawValue(data.room_desc);
                Ext.get('rooms').dom.value = data.room_id;

                arrayDiagnosisID = diagnose_id;
                arrayDiagnosisDescription = diagnose_desc;
                 
                if(action.result.diagnose_count == 0)
                    Ext.getCmp("lblDiagnosis").setText('None', false);
                else if(action.result.diagnose_count == 1)
                    Ext.getCmp("lblDiagnosis").setText('<a href="#" href="#" onclick="commonGrid(\'Diagnosis\')" style="text-decoration:none;font: 9px;">'+arrayDiagnosisDescription[0]+'</a>', false);
                else 
                    Ext.getCmp("lblDiagnosis").setText('<a href="#" href="#" onclick="commonGrid(\'Diagnosis\')" style="text-decoration:none;font: 9px;">'+arrayDiagnosisDescription[0]+' and ('+(arrayDiagnosisID.length-1)+') more...</a>', false);
                
                if(action.result.diagnose_count != 0)                    
                    document.getElementById("lblDiagnosis").setAttribute("data-qtip", arrayDiagnosisDescription.toString());            
                else 
                    document.getElementById("lblDiagnosis").setAttribute("data-qtip", "");            

                arrayAllergiesID = allergy_id;
                arrayAllergiesDescription = allergy_desc;

                if(action.result.allergy_count == 0)
                    Ext.getCmp("lblAllergy").setText('None', false);
                else if(action.result.allergy_count == 1)
                    Ext.getCmp("lblAllergy").setText('<a href="#" href="#" onclick="commonGrid(\'Allergy\')" style="text-decoration:none;font: 9px;">'+arrayAllergiesDescription[0]+'</a>', false);
                else 
                    Ext.getCmp("lblAllergy").setText('<a href="#" href="#" onclick="commonGrid(\'Allergy\')" style="text-decoration:none;font: 9px;">'+arrayAllergiesDescription[0]+' and ('+(arrayAllergiesID.length-1)+') more...</a>', false);
                
                if(action.result.allergy_count != 0)
                    document.getElementById("lblAllergy").setAttribute("data-qtip", arrayAllergiesDescription.toString());
                else
                    document.getElementById("lblAllergy").setAttribute("data-qtip", "");
                
                if(data.image)                    
                    Ext.getCmp("lblImage").setText('<a href="#" onclick="ImageUpload()"><IMG style="border:1px dashed DarkSlateBlue;" SRC="../profile_pic/'+data.image+'" height="160" width="160"></a>', false);            

                Ext.getCmp("fieldSet1Grid1").getStore().add({medicare: data.medicare, medicaid: data.medicaid, others: data.others});

                var description = action.result.description;
                var independent = action.result.independent;
                var assist = action.result.assist;
                var dependent = action.result.dependent;

                for (var i = 0; i < action.result.adl_count; i++) 
                    Ext.getCmp("fieldSet1Grid2").getStore().add({description: description[i], independent: independent[i], assist: assist[i], dependent: dependent[i]});

                census_description = action.result.census_description;
                census_type = action.result.census_type;
                census = action.result.census;
                census_count = action.result.census_count;
            }


        },       
        failure: function(f,action) { errorFunction("Error!",'Please contact system administrator.'); }
    });
}