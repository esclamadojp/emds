var commonWindow;

function setLabel(type)
{
    var storeCommon = Ext.getCmp("commonGrid").getSelectionModel();

    if(type == 'Diagnosis')
    {
        arrayDiagnosisID = [];
        arrayDiagnosisDescription = [];
    }
    else
    {
        arrayAllergiesID = [];
        arrayAllergiesDescription = [];
    }

    for (var i = 0; i < storeCommon.store.data.length; i++) {
        if(type == 'Diagnosis'){
            arrayDiagnosisID.push(storeCommon.store.data.items[i].data.id);
            arrayDiagnosisDescription.push(storeCommon.store.data.items[i].data.description);
        }
        else
        {
            arrayAllergiesID.push(storeCommon.store.data.items[i].data.id);
            arrayAllergiesDescription.push(storeCommon.store.data.items[i].data.description);
        }
    };

    if(type == 'Diagnosis')
    {
        if(storeCommon.store.data.length == 0)
            Ext.getCmp("lblDiagnosis").setText('None', false);
        else if(storeCommon.store.data.length == 1)
            Ext.getCmp("lblDiagnosis").setText('<a href="#" href="#" onclick="commonGrid(\'Diagnosis\')" style="text-decoration:none;font: 9px;">'+storeCommon.store.data.items[0].data.description+'</a>', false);
        else 
            Ext.getCmp("lblDiagnosis").setText('<a href="#" href="#" onclick="commonGrid(\'Diagnosis\')" style="text-decoration:none;font: 9px;">'+storeCommon.store.data.items[0].data.description+' and ('+(storeCommon.store.data.length-1)+') more...</a>', false);
        
        if(arrayDiagnosisID.length != 0)
            document.getElementById("lblDiagnosis").setAttribute("data-qtip", arrayDiagnosisDescription.toString());
        else
            document.getElementById("lblDiagnosis").setAttribute("data-qtip", "");
    }
    else
    {
        if(storeCommon.store.data.length == 0)
            Ext.getCmp("lblAllergy").setText('None', false);
        else if(storeCommon.store.data.length == 1)
            Ext.getCmp("lblAllergy").setText('<a href="#" href="#" onclick="commonGrid(\'Allergy\')" style="text-decoration:none;font: 9px;">'+storeCommon.store.data.items[0].data.description+'</a>', false);
        else 
            Ext.getCmp("lblAllergy").setText('<a href="#" href="#" onclick="commonGrid(\'Allergy\')" style="text-decoration:none;font: 9px;">'+storeCommon.store.data.items[0].data.description+' and ('+(storeCommon.store.data.length-1)+') more...</a>', false);

        if(arrayDiagnosisID.length != 0)
            document.getElementById("lblAllergy").setAttribute("data-qtip", arrayAllergiesDescription.toString());
        else
            document.getElementById("lblAllergy").setAttribute("data-qtip", "");
    }
}

function loadcommonGrid(type)
{
    if(type == 'Diagnosis')
        for (var i = 0; i < arrayDiagnosisID.length; i++)
            Ext.getCmp("commonGrid").getStore().add({id: arrayDiagnosisID[i], description: arrayDiagnosisDescription[i]}); 
    else
        for (var i = 0; i < arrayAllergiesID.length; i++)
                Ext.getCmp("commonGrid").getStore().add({id: arrayAllergiesID[i], description: arrayAllergiesDescription[i]}); 
}

function commonGrid(type)
{      
    Ext.define('commonModel', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'id', type: 'int'},
            {name: 'description'}
         ]
    });

    var diagnosisStore = new Ext.create('Ext.data.ArrayStore', {
        model: 'commonModel'
    });
    
    if(type == 'Diagnosis')
    {
        diagnosisHidden = false;
        allergyHidden = true;
    }
    else
    {
        diagnosisHidden = true;
        allergyHidden = false;
    }

	var grid = Ext.create('Ext.grid.Panel', {
        id      : 'commonGrid',
        store: diagnosisStore,
        columns: [
            { dataIndex: 'id', hidden: true},
            { text: 'Description', dataIndex: 'description', width: '90%'},
            {
                xtype: 'actioncolumn',
                width: '8%',
                align: 'center',
                items: [{
                    icon   : '../image/delete.gif',
                    tooltip: 'Remove',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = diagnosisStore.getAt(rowIndex);
                        if (!rec) {
                            return false;
                        }
                        grid.store.remove(rec);
                        setLabel(type);
                    }
                }]
            }],
        autoHeight: true,
        border: false,
        columnLines: true,
        width: '100%',
        height: 200
    });


	commonWindow = Ext.create('Ext.window.Window', {
    	title		: type,
    	closable	: true,
    	modal		: true,
    	width		: 400,
    	autoHeight	: true,
    	resizable	: false,
        border      : false,
    	buttonAlign	: 'center',
    	header: {titleAlign: 'center'},
    	items: [grid],
    	tbar: [
        {
            xtype       : 'combo',
            flex: 1,
            id          : 'diagnosis',
            emptyText  : 'Select Diagnosis',   
            valueField  : 'id',
            displayField: 'description',
            triggerAction: 'all',
            matchFieldWidth: false,
            minChars    : 3,
            enableKeyEvents: true,
            readOnly    : false,
            ddReorder: true,
            hidden: diagnosisHidden,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    url: 'commonquery/caseentrylist',
                    extraParams: {query:null, type: 'diagnosis', category:null},
                    reader: {
                        type: 'json',
                        root: 'data',
                        idProperty: 'id'
                    }
                },
                params: {start: 0, limit: 10},
                fields: [{name: 'id', type: 'int'}, 'description']
            }),
            listeners: 
            {
                select: function (combo, record, index)
                {   
                    var validate = true;
                    var storeCommon = Ext.getCmp("commonGrid").getSelectionModel();
                    for (var i = 0; i < storeCommon.store.data.length; i++) {
                        if (storeCommon.store.data.items[i].data.id == record[0].data.id)
                        {
                            validate = false;
                            errorFunction('Error!', 'Diagnosis already exist.');
                        }
                    };

                    if(validate)
                    {
                        Ext.getCmp("commonGrid").getStore().add({id: record[0].data.id, description: record[0].data.description});
                        setLabel(type);
                    }                    
                }
            }
        },
        {
            xtype: 'button',
            hidden: diagnosisHidden,
            text: '...',
            tooltip: 'Add/Edit/Delete Diagnosis Category',
            handler: function (){ viewMaintenance('diagnosis_category', null); }
        },
        {
            xtype: 'button',
            hidden: diagnosisHidden,
            text: '...',
            tooltip: 'Add/Edit/Delete Diagnose',
            handler: function (){ viewMaintenance('diagnosis','diagnosis_category'); }
        },
        {
            xtype       : 'combo',
            flex: 1,
            id          : 'allergies',
            emptyText  : 'Select Allergy',                        
            valueField  : 'id',
            displayField: 'description',
            triggerAction: 'all',
            matchFieldWidth: false,
            minChars    : 3,
            enableKeyEvents: true,
            readOnly    : false,
            ddReorder: true,
            hidden: allergyHidden,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    url: 'commonquery/caseentrylist',
                    extraParams: {query:null, type: 'allergies', category:null},
                    reader: {
                        type: 'json',
                        root: 'data',
                        idProperty: 'id'
                    }
                },
                params: {start: 0, limit: 10},
                fields: [{name: 'id', type: 'int'}, 'description']
            }),
            listeners: 
            {
                select: function (combo, record, index)
                {   
                    var validate = true;
                    var storeCommon = Ext.getCmp("commonGrid").getSelectionModel();
                    for (var i = 0; i < storeCommon.store.data.length; i++) {
                        if (storeCommon.store.data.items[i].data.id == record[0].data.id)
                        {
                            validate = false;
                            errorFunction('Error!', 'Allergy already exist.');
                        }
                    };

                    if(validate)
                    {
                        Ext.getCmp("commonGrid").getStore().add({id: record[0].data.id, description: record[0].data.description});
                        setLabel(type);
                    }                    
                }
            }
        },
        {
            xtype: 'button',
            hidden: allergyHidden,
            text: '...',
            tooltip: 'Add/Edit/Delete Allergies',
            handler: function (){ viewMaintenance('allergies', null); }
        }]
    }).show();
    loadcommonGrid(type);
}