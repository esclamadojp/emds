var staffcrud, staffCRUDID, staffCRUDForm;

function staffMaintenanceCRUD(type, form, grid) 
{
	if (type == "Delete")
	{ 
		Ext.Ajax.request({
		    url: 'commonquery/staffcrud',
		    method	: 'POST',
		    params: {id: staffCRUDID, type: type},
		    success: function(f,a)
		    {
		    	Ext.MessageBox.hide();
		    	if (type == "Close") staffcrud.close();
		    	else
		    	{
					var response = Ext.decode(f.responseText);					
					if (response.success == true)
					{
						Ext.getCmp(form).getStore().reload({params:{reset:1 }, timeout: 300000});      
						infoFunction('Status', response.data);
						if (grid != null)Ext.getCmp(grid).setTitle('Details');
					}
					else
						warningFunction("Error!",response.data);
				}
		    },
			failure: function(f,action) { warningFunction("Error!",'Please contact system administrator.'); }
		});
		processingFunction("Processing data, please wait...");
	}
	else
	{
		params 		= new Object();
		params.id	= staffCRUDID;
		params.positions	= Ext.get('positions').dom.value;
		params.departments	= Ext.get('departments').dom.value;
		params.type	= type;

		addeditFunction('commonquery/staffcrud', params, form, staffCRUDForm, staffcrud);
	}
}

function AddEditDeleteStaff(type, form, grid)
{          
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

	if(type == 'Edit' || type == 'Delete')	
	{
		var sm = Ext.getCmp(form).getSelectionModel();
		if (!sm.hasSelection())
		{
			warningFunction("Warning!","Please select record.");
			return;
		}		
		staffCRUDID = sm.selected.items[0].data.id;
		if (!staffCRUDID)
		{
			warningFunction("Warning!","Please select record.");
			return;
		}		
	}

	if (type == "Delete")
	{
		Ext.Msg.show({
			title	: 'Confirmation',
			msg		: 'Are you sure you want to ' + type + ' record?',
			width	: '100%',
			icon	: Ext.Msg.QUESTION,
			buttons	: Ext.Msg.YESNO,
			fn: function(btn){
				if (btn == 'yes')
					staffMaintenanceCRUD(type, form, grid);
			}
		});
	}
	else
	{
		staffCRUDForm = Ext.create('Ext.form.Panel', {
				border		: false,
				bodyStyle	: 'padding:15px;',		
				fieldDefaults: {
					labelAlign	: 'right',
					labelWidth: 90,
					afterLabelTextTpl: required,
					anchor	: '100%',
					allowBlank: false
		        },
				items: [{
					xtype	: 'textfield',
					id		: 'fname',
					name	: 'fname',
					fieldLabel: 'First Name'
				}, {
					xtype	: 'textfield',
					id		: 'mname',
					name	: 'mname',
					allowBlank: true,
					afterLabelTextTpl: null,
					fieldLabel: 'Middle Name'
				}, {
					xtype	: 'textfield',
					id		: 'lname',
					name	: 'lname',
					fieldLabel: 'Last Name'
				}, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'positions',
			            fieldLabel	: 'Position',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'positions', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('positions').dom.value = record[0].data.id;
			                }
			            }
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Positon',
			        	handler: function (){ viewMaintenance('positions', null); }
			        }]
                }, {
                    xtype: 'fieldcontainer',
                    labelStyle: 'font-weight:bold;padding:0;',
                    layout: 'hbox',
                    items: [
                    {
			            xtype   	: 'combo',
			            flex: 1,
			            id			: 'departments',
			            fieldLabel	: 'Department',
			            valueField	: 'id',
			            displayField: 'description',
			            triggerAction: 'all',
			            minChars    : 3,
			            enableKeyEvents: true,
			            readOnly    : false,
			            forceSelection: true,
			            store: new Ext.data.JsonStore({
					        proxy: {
					            type: 'ajax',
					            url: 'commonquery/caseentrylist',
					            extraParams: {query:null, type: 'departments', category:null},
					            reader: {
					                type: 'json',
					                root: 'data',
					                idProperty: 'id'
					            }
					        },
					        params: {start: 0, limit: 10},
					        fields: [{name: 'id', type: 'int'}, 'description']
			            }),
			            listeners: 
			            {
			                select: function (combo, record, index)
			                {		 
			                	Ext.get('departments').dom.value = record[0].data.id;
			                }
			            }  
			        },
			        {
			        	xtype: 'button',
			        	hidden: crudMaintenance,
			        	margins		: '0 0 0 5',
			        	text: '...',
			        	tooltip: 'Add/Edit/Delete Department',
			        	handler: function (){ viewMaintenance('departments', null); }
			        }]
                }]
			});

			staffcrud = Ext.create('Ext.window.Window', {
			title		: type + ' Staff',
			closable	: true,
			modal		: true,
			width		: 350,
			autoHeight	: true,
			resizable	: false,
			buttonAlign	: 'center',
			header: {titleAlign: 'center'},
			items: [staffCRUDForm],
			buttons: [
			{
			    text	: 'Save',
			    icon	: '../image/save.png',
			    handler: function ()
			    {
					if (!staffCRUDForm.form.isValid()){
						Ext.Msg.show({
							title: 'Error!',
							msg: "Please fill-in the required fields (Marked red).",
							icon: Ext.Msg.ERROR,
							buttons: Ext.Msg.OK
						});
					    return;
			        }
					Ext.Msg.show({
						title	: 'Confirmation',
						msg		: 'Are you sure you want to Save?',
						width	: '100%',
						icon	: Ext.Msg.QUESTION,
						buttons	: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn == 'yes')
								staffMaintenanceCRUD(type, form, grid);
						}
					});
			    }
			},
			{
			    text	: 'Close',
			    icon	: '../image/close.png',
			    handler: function ()
			    {
			    	staffcrud.close();
			    }
			}],
		});

		if(type == 'Edit')
		{
			staffCRUDForm.getForm().load({
				url: 'commonquery/staffview',
				timeout: 30000,
				waitMsg:'Loading data...',
				params: {
					id: this.staffCRUDID, type: type
				},		
				success: function(form, action) {					
					staffcrud.show();
					var data = action.result.data;
					Ext.getCmp("positions").setRawValue(data.position_desc);
					Ext.getCmp("departments").setRawValue(data.department_desc);
					Ext.get('positions').dom.value = data.position_id;
					Ext.get('departments').dom.value = data.department_id;					
				},		
				failure: function(f,action) { warningFunction("Error!",'Please contact system administrator.'); }
			});
		}
		else
			staffcrud.show();

		Ext.getCmp("fname").focus();
	}
}